package com.ckt_works.AX_DSP;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BleScannerService extends Service {
	public enum State {
		UNKNOWN,
		IDLE,
		SCANNING,
		DONE_SCANNING,
		BLUETOOTH_OFF,
		CONNECTED,
		DISCONNECTING
	}
	private static final String TAG = "BleScannerService";

	public static final String AX_DSP_X_DEVICE_NAME = "AX-DSP-X";
	public static final String AX_DSP_L_DEVICE_NAME = "AX-DSP-L";
	public static final String SCAN_DEVICE_NAME = "AX-DSP-";		// This will include AX-DSP-X and AX-DSP-L
    private static String name_filter = SCAN_DEVICE_NAME;

	static final int MSG_REGISTER = 1;
	static final int MSG_START_SCAN = 2;
	static final int MSG_STOP_SCAN = 3;
	static final int MSG_STATE_CHANGED = 4;
	static final int MSG_DEVICE_FOUND = 5;

	private static final long SCAN_PERIOD = 60000;	// 60 seconds

	private static IncomingHandler mHandler = null;
	private static Messenger mMessenger = null;
	private static List<Messenger> mClients = new LinkedList<>();
	private static Map<String, String> mDevices = new HashMap<>();

	private  static long test_count = 0;

	//private BluetoothAdapter mBluetooth = null;
	private static BluetoothLeScanner bluetoothLeScanner;
	private static BtleScanCallback mScanCallback;
	private static BluetoothManager bluetoothMgr;
	private static BluetoothAdapter bluetoothAdapter;
	private  static State mState = State.UNKNOWN;

	public  BleScannerService() {
//		bluetoothMgr = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
//		bluetoothAdapter = bluetoothMgr.getAdapter();
		mHandler = new IncomingHandler(this);
		mMessenger = new Messenger(mHandler);
	}


	@Override
	public IBinder onBind(Intent intent) {
		bluetoothMgr = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
		bluetoothAdapter = bluetoothMgr.getAdapter();
		return mMessenger.getBinder();
	}

	// Handler to process requests from clients
	private static class IncomingHandler extends Handler {
		private WeakReference<BleScannerService> scanner_service;

		public IncomingHandler(BleScannerService service) {
			scanner_service = new WeakReference<>(service);
		}

		@Override
		public void handleMessage(Message msg) {
			BleScannerService service = scanner_service.get();
			if (service != null) {
				switch (msg.what) {
					case MSG_REGISTER:
						service.mClients.add(msg.replyTo);
                      	name_filter = SCAN_DEVICE_NAME;
                        Log.i(TAG, "BLE Registered to scan for " + name_filter);
						break;

					case MSG_START_SCAN:
						service.startScan();
						Log.i(TAG, "BLE Start Scan");
						break;

					case MSG_STOP_SCAN:
						service.stopScan();
						Log.i(TAG, "BLE Stop Scan");
						break;

					default:
						super.handleMessage(msg);
				}
			}
		}
	}



	private static void stopScan()
	{
		Log.i("stopScan", ">>>>>>>>");
		if (bluetoothLeScanner != null) {
			bluetoothLeScanner.stopScan(mScanCallback);
			bluetoothLeScanner.flushPendingScanResults(mScanCallback);
		}
		setState(State.DONE_SCANNING);
	}

	private static void startScan() {
		try {
			mDevices.clear();
			if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
				setState(State.BLUETOOTH_OFF);
			} else {
				bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
				mScanCallback = new BtleScanCallback();

				if (mState == State.SCANNING) {
					bluetoothLeScanner.stopScan(mScanCallback);
					Thread.sleep(2000);
				}

				setState(State.SCANNING);
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						if (mState == State.SCANNING) {
							Log.i(">>>>>>>>", "Done Scanning");
							stopScan();
						}
					}
				}, SCAN_PERIOD);

				test_count = 0;
				bluetoothLeScanner.startScan(mScanCallback);
			}
		}
		catch (Exception ex){
			Log.d("startScan", ex.getMessage());
		}
	}

	private  static class BtleScanCallback extends ScanCallback {

		@Override
		public void onScanResult(int callbackType, ScanResult result) {
			if (callbackType == 1) {
				String info;
				String deviceAddress = result.getDevice().getAddress();
				String deviceName = result.getDevice().getName();
				String RSSI = Integer.toString(result.getRssi());
				if (deviceName != null){
					info = deviceAddress + "; " + "RSSI: " + RSSI;
					info += "; Name: " + deviceName + "; ";
Log.i("Found Device: ", info);
					if (deviceName.contains(name_filter)){                              	// Only add AX-DSP to the list
						if (!mDevices.containsValue(deviceName)){                           // Skip adding if the device is already in our list
							mDevices.put(deviceAddress, deviceName);
							Message msg = Message.obtain(null, MSG_DEVICE_FOUND);
							if (msg != null) {
								Bundle bundle = new Bundle();
								bundle.putString(MainActivity.KEY_DEVICE_ADDRESS, deviceAddress);
								bundle.putString(MainActivity.KEY_DEVICE_NAME, deviceName);
								bundle.putString(MainActivity.KEY_DEVICE_RSSI, RSSI);
								msg.setData(bundle);
								sendMessage(msg);
							}
						}
					}
				}
			}
		}

		@Override
		public void onBatchScanResults(List<ScanResult> results) {
			for (ScanResult sr : results) {
				Log.i("ScanResult - Results", sr.toString());
			}
		}

		@Override
		public void onScanFailed(int errorCode) {
			Log.e("Scan Failed", "Error Code: " + errorCode);
		}
	};


	private static void setState(State newState) {
		if (mState != newState) {
			mState = newState;
			Message msg = getStateMessage();
			if (msg != null) {
				sendMessage(msg);
			}
		}
	}

	private static Message getStateMessage() {
		Message msg = Message.obtain(null, MSG_STATE_CHANGED);
		if (msg != null) {
			msg.arg1 = mState.ordinal();
		}
		return msg;
	}

	private static void sendMessage(Message msg) {
		for (int i = mClients.size() - 1; i >= 0; i--) {
			Messenger messenger = mClients.get(i);
			if (!sendMessage(messenger, msg)) {
				mClients.remove(messenger);
			}
		}
	}


	private static boolean sendMessage(Messenger messenger, Message msg) {
		boolean success = true;
		try {
			messenger.send(msg);
		} catch (RemoteException e) {
			Log.w(TAG, "Lost connection to client", e);
			success = false;
		}
		catch (Exception e) {
			Log.w(TAG, "Exception", e);
			success = false;
		}

		return success;
	}
}

