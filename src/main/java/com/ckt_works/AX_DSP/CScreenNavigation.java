package com.ckt_works.AX_DSP;

import androidx.fragment.app.Fragment;
//import android.app.Fragment;
//import android.support.v7.app.AppCompatDialogFragment;

// Created by Abeck on 12/6/2016. //


// Base class to allow our fragments to manage navigation (menu) buttons
public class CScreenNavigation extends Fragment {

     private int nav_button_id;


    public CScreenNavigation(){}

/*    public void SetNavigation(CScreenNavigation prev, CScreenNavigation next)
    {
        prev_fragment = prev;
        next_fragment = next;
    }
*/
    public void SetNavigationButton(int navigation_button_id)
    {
        nav_button_id = navigation_button_id;
    }


    public int GetNavigationButton()
    {
        return(nav_button_id);
    }

}
