package com.ckt_works.AX_DSP;

// Class that ensures reliable communications with the BLE device
// This class periodically sends a messages and expects a response to that message

public class CommWatchdog {
    long maxReceiveTime = 2000;             // Maximum time to wait for a message to be received
    long receiveTime = 0;                   // Time waiting for message to be received.
    long timeBetweenSends = 5000;           // Amount of time to wait between sending messages
    boolean OkToSendMsg = false;            // True when it's Okay to send a message

    public CommWatchdog(long send_time, long wait_time)
    {
        timeBetweenSends = send_time;
        maxReceiveTime = wait_time;
    }

}
