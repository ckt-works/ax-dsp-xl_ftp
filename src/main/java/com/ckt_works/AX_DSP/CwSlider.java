package com.ckt_works.AX_DSP;
// Created by Abeck on 6/8/2017. //

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

abstract class CwSlider extends FrameLayout implements View.OnTouchListener {
    /// Default constructors. Tell Android that we're doing custom drawing and that we want to listen to touch events.
    public CwSlider(Context context) {
        super(context);
    }

    public CwSlider(Context context, int min_value, int max_value) {
        super(context);
    }

    public CwSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
     }

    public CwSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    abstract public void setDelegateOnTouchListener(OnTouchListener onTouchListener);
    abstract public void setDecimalMultiplier(int multiplier);
    abstract public int GetMinValue();
    abstract public void setRange(int min, int max);
    abstract public int setValue(float value);
    abstract public float getSliderValue();
    abstract public boolean onTouch(View view, MotionEvent event);
    abstract public void setNotUsed(boolean state);

}

