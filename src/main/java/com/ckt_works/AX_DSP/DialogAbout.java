package com.ckt_works.AX_DSP;

//import android.app.DialogFragment;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
        import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

// Created by Abeck on 2/7/2017. //
public class DialogAbout extends androidx.appcompat.app.AppCompatDialogFragment implements Button.OnClickListener{

    private DspService dspService;

    private TextView about_device;
    private TextView about_app;
    private TextView about_386;
    private LinearLayout layoutVehicleInfo;
    private TextView about_vehicle;
    private TextView vehicle_file_version;
    private Button button_ok;

    private int info_text_clicks = 0;           // Tracks number of times  "textVehicleInfo" has been clicked - to display hidden dialog

    private MainActivity mainActivity;

    private final String[] OS_NAMES = {"Lollipop","Marshmallow", "Nougat", "Oreo", "Pie",};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        mainActivity = (MainActivity) getActivity();
        View my_view = inflater.inflate(R.layout.layout_about, container, false);

        about_device = (TextView) my_view.findViewById(R.id.about_device);
        about_app = (TextView) my_view.findViewById(R.id.about_app);
        about_386 = (TextView) my_view.findViewById(R.id.about_386);
        about_vehicle = (TextView) my_view.findViewById(R.id.about_vehicle);
        vehicle_file_version = (TextView) my_view.findViewById(R.id.textVehicleFileVersion);
        //vehicle_file_version.setText("(V" + GetVersionString() + ")");

        button_ok = (Button) my_view.findViewById((R.id.buttonOK));
        button_ok.setOnClickListener(this);

        layoutVehicleInfo = (LinearLayout) my_view.findViewById(R.id.layoutVehicleInfo);

        dspService = ((MainActivity)getActivity()).dspService;
        return(my_view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        Context my_context = getActivity().getApplicationContext();
        String strReleaseMajor = android.os.Build.VERSION.RELEASE;
        int periodIndex = android.os.Build.VERSION.RELEASE.indexOf(".");
        if (periodIndex >= 0)
            strReleaseMajor = android.os.Build.VERSION.RELEASE.substring(0, periodIndex);
        int releaseMajor = Integer.valueOf(strReleaseMajor);
        String osName = "";
        if (releaseMajor > 4) {
            if (releaseMajor <= 9)
                osName = OS_NAMES[releaseMajor - 5];
            else
            {
                char[] osLetter = Character.toChars( 'G' + releaseMajor);
                osName = new String(osLetter);
            }
        }

        // Display the Device Info
        String str_about_device = "Model: " + Build.MANUFACTURER + "  " + android.os.Build.MODEL + "\n"
                + "OS: " + android.os.Build.VERSION.RELEASE + " - " + osName + "\n"
                + "API: " + android.os.Build.VERSION.SDK_INT+ "\n"
                + "Display: " + GetScreenSize() +"\n"
                + "    Dimens: " + Integer.toString(dm.widthPixels) + "W x " +Integer.toString(dm.heightPixels) + "H\n"
                + "    Density: " + getDensityName(my_context) + " - Dpi: " + Integer.toString((int)dm.xdpi) + "W x " + Integer.toString((int)dm.ydpi) + "H";
        about_device.setText(str_about_device);

        // Display the APP info
        String awsInfo = "?";
        VehicleTypeFile vehicleTypeFile = mainActivity.vehicleTypeFile;
        String str_about_app = "Name: " + getString(R.string.app_name) + "\n" + "Version: " + getVersionName();
        str_about_app+= "\nVehicle Data Version: " +  GetVersionString();
        str_about_app+= "\nUpdated: " + vehicleTypeFile.GetVehicleFileDateTime();
        about_app.setText(str_about_app);
        if (((MainActivity) getActivity()).Connected()){
            // Display the Vehicle info
            String vehicle_manuf = ((MainActivity)getActivity()).GetVehicleManufacturerString();
            String vehicle_model = ((MainActivity)getActivity()).GetVehicleModelString();
            int manuf_id = ((MainActivity)getActivity()).GetVehicleID();
            int model_id = ((MainActivity)getActivity()).GetModelID();
            boolean has_oe_amp = ((MainActivity)getActivity()).GetVehicleHasOE_Amp();

            String str_about_vehicle = "Type: " + vehicle_manuf + " " + vehicle_model + " (" + Integer.toString(manuf_id) + "-" + Integer.toString(model_id) + ")\n"
                                     + "Has OE Amp: " + (has_oe_amp ? "Yes" : "No");
            about_vehicle.setText(str_about_vehicle);

            layoutVehicleInfo.setOnClickListener(this);                         // Enable Access to the Diagnostic Dialog
            String str_about_386 =                                              // Display the Board Info
                    "Model - Address: " + mainActivity.GetDeviceName() +"\n"
//                    + "Serial #: " + dspService.GetAxdspSerialNumber() +"\n"
                    + "Version: " + dspService.GetAxdspSoftwareVersion() +"\n"
                    + "BLE\n"
                    + "    HW Version: " + dspService.GetBleHardwareVersion() +"\n"
                    + "    FW Version: " + dspService.GetBleFirmwareVersion();
            about_386.setText(str_about_386);
        }
        else
        {
            about_vehicle.setText("Not Connected to an AX-DSP");
            layoutVehicleInfo.setOnClickListener(null);                         // Disable Access to the Diagnostic Dialog
            about_386.setText("Not Connected to an AX-DSP");
        }
Log.i("CPU Temp", Float.toString(cpuTemperature()));

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onHiddenChanged (boolean hidden) {
        info_text_clicks = 0;                       // Reset number of times "textVehicleInfo" has been clicked
    }


    public static float cpuTemperature()
    {
        Process process;
        try {
            process = Runtime.getRuntime().exec("cat sys/class/thermal/thermal_zone0/temp");
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line = reader.readLine();
            if(line!=null) {
                float temp = Float.parseFloat(line);
                return temp / 1000.0f;
            }else{
                return 51.0f;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0f;
        }
    }

    @Override
    // Handle user clicking the 'OK' button
    public void onClick(View button) {

        if (button.getId() == layoutVehicleInfo.getId()) {
            if (++info_text_clicks >= 5) {
                DialogDiagnostics diagnostics_dialog = new DialogDiagnostics();
                diagnostics_dialog.SetDspService(dspService);
                diagnostics_dialog.setCancelable(false);
                diagnostics_dialog.setTargetFragment(this, 0);
                diagnostics_dialog.show(getFragmentManager(), "Diagnostics");
                info_text_clicks = 0;
            }
        }
        else if (button.getId() == button_ok.getId())
           this.dismiss();
    }


    private String GetScreenSize() {
        String screen_size = "?";

        int screenSize = getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                screen_size = "X-Large";
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                screen_size = "Large";
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                screen_size = "Normal";
                break;
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                screen_size = "Small";
                break;
        }
        return (screen_size);
    }


    private String getVersionName() {
        String name = "";
        String packageName =  getActivity().getPackageName();
        try {
            PackageInfo packageInfo =   getActivity().getPackageManager().getPackageInfo(packageName, 0);
            name =  packageInfo.versionName;
//            name =  ((MainActivity) getActivity()).getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            // Huh? Really?
        }
        return(name);
    }

    private static String getDensityName(Context context) {

        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

    // Get the version of the Vehicle Type file
    private String GetVersionString(){
        String strVersion = "0.0";
        try {
            InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
            if (input_stream != null) {
                XmlPullParser myParser = CreateParser(input_stream);

                if (myParser != null) {
                    int event = myParser.getEventType();
                    process_loop:
                    while (event != XmlPullParser.END_DOCUMENT) {                              // Parse until the end of the document
                        switch (event) {
                            case XmlPullParser.END_TAG:
                                String tag_name = myParser.getName();
                                if (tag_name.equals("version")) {
                                    strVersion = myParser.getAttributeValue(null, "major") + "." + myParser.getAttributeValue(null, "minor");
                                    break process_loop;
                                }
                                break;
                        }
                        event = myParser.next();
                    }
                    input_stream.close();
                }
            }
        }
        catch(IOException | XmlPullParserException ex){ex.printStackTrace();}
        return (strVersion);
    }


    // Create a Parser to use for parsing our data
    private static XmlPullParser CreateParser(InputStream input_stream) throws XmlPullParserException
    {
        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser myParser = xmlFactoryObject.newPullParser();
        if (myParser != null)
            myParser.setInput(input_stream, null);

        return(myParser);
    }
}

