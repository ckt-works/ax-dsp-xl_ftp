package com.ckt_works.AX_DSP;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

// Created by Abeck on 2/7/2017. //

//public class DialogAlert extends android.support.v4.app.DialogFragment implements Button.OnClickListener{
public class DialogAlert extends androidx.appcompat.app.AppCompatDialogFragment implements Button.OnClickListener{

    private TextView textTitle;
    private TextView textMessage;


    static DialogAlert newInstance(String title, String message) {
        DialogAlert frag = new DialogAlert();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        frag.setArguments(args);
        return frag;
    }


/*    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
//        Dialog dialog = getDialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View my_view = inflater.inflate(R.layout.layout_alert, container, false);
        textTitle = (TextView) my_view.findViewById(R.id.textTitle);
        textMessage = (TextView) my_view.findViewById(R.id.textMessage);

        Button buttonOk = (Button) my_view.findViewById(R.id.buttonOk);
        buttonOk.setOnClickListener(this);

        return(my_view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");

        textTitle.setText(title);
        textMessage.setText(message);
        super.onActivityCreated(savedInstanceState);
    }



    @Override
    // Handle user clicking a button
    public void onClick(View button) {

        boolean answer = (button.getId() == R.id.buttonOk);

        try {
            IDialogConfirmListener callback_listener = (IDialogConfirmListener) getTargetFragment();       //    Callback to the fragment that created us with the vehicle info
            if (callback_listener != null)
                callback_listener.onDialogConfirmClick(answer);
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        this.dismiss();
    }

}

