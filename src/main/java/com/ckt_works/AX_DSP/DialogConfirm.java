package com.ckt_works.AX_DSP;

//import android.app.Dialog;
//import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

// Created by Abeck on 2/7/2017. //

public class DialogConfirm extends androidx.appcompat.app.AppCompatDialogFragment implements Button.OnClickListener{

    private TextView textTitle;
    private TextView textMessage;
    private Button buttonOK;
    private Button buttonNo;
    private IDialogConfirmListener callback_listener;


    static DialogConfirm newInstance(String title, String message) {
        DialogConfirm frag = new DialogConfirm();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("deny", "NO");
        args.putString("confirm", "YES");

        frag.setArguments(args);
        return frag;
    }

    static DialogConfirm newInstance(String title, String message, String deny_text, String confirm_text) {
        DialogConfirm frag = new DialogConfirm();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("message", message);
        args.putString("deny", deny_text);
        args.putString("confirm", confirm_text);
        frag.setArguments(args);
        return frag;
    }

/*    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
      //  android.support.v4.app.DialogFragment dialogFragment = new android.support.v4.app.DialogFragment();
     //   dialogFragment.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        View my_view = inflater.inflate(R.layout.layout_confirm, container, false);

        textTitle = (TextView) my_view.findViewById(R.id.textTitle);
        textMessage = (TextView) my_view.findViewById(R.id.textMessage);

        buttonOK = (Button) my_view.findViewById(R.id.buttonYes);
        buttonOK.setOnClickListener(this);

        buttonNo = (Button) my_view.findViewById(R.id.buttonNo);
        buttonNo.setOnClickListener(this);

        return(my_view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String message = getArguments().getString("message");
        String deny_text = getArguments().getString("deny");
        String confirm_text = getArguments().getString("confirm");

        textTitle.setText(title);
        textMessage.setText(message);
        buttonNo.setText(deny_text);
        buttonOK.setText(confirm_text);

        callback_listener = (IDialogConfirmListener) getTargetFragment();       //    Callback to the fragment that created us with the vehicle info
        super.onActivityCreated(savedInstanceState);
    }



    @Override
    // Handle user clicking a button
    public void onClick(View button) {

        boolean answer = (button.getId() == R.id.buttonYes);

        try {
//            IDialogConfirmListener callback_listener = (IDialogConfirmListener) getTargetFragment();       //    Callback to the fragment that created us with the vehicle info
            callback_listener.onDialogConfirmClick(answer);
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

        this.dismiss();
    }

}

