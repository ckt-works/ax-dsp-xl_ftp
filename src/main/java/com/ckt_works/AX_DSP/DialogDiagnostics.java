package com.ckt_works.AX_DSP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

// Created by Abeck on 2/7/2017. //

//public class DialogDiagnostics extends android.support.v4.app.DialogFragment implements Button.OnClickListener, IPostExecuteInterface {
public class DialogDiagnostics extends androidx.appcompat.app.AppCompatDialogFragment implements Button.OnClickListener, IPostExecuteInterface {
    private TaskReadDiagnosticData ReadDiagnosticsData;
    private DspService dspService;
    private Context my_context;

    private TextView textVehicleId;
    private TextView textModelId;
    private TextView textAmpOnIn;
    private TextView textAmpOnOut;
    private TextView textCanActive;
    private TextView textRapActive;
    private TextView textSsMute;
    private TextView textInputSignal;
    private TextView textDspInit;
    private Button button_ok;

    private final int DIAG_READ_INTERVAL = 200;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        dialog = getDialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View my_view = inflater.inflate(R.layout.layout_diagnostics, container, false);
        textVehicleId = my_view.findViewById(R.id.textVehicleId);
        textModelId = my_view.findViewById(R.id.textModelId);
        textAmpOnIn = my_view.findViewById(R.id.textAmpOnIn);
        textAmpOnOut = my_view.findViewById(R.id.textAmpOnOut);
        textCanActive = my_view.findViewById(R.id.textCanActive);
        textRapActive = my_view.findViewById(R.id.textRapActive);
        textSsMute = my_view.findViewById(R.id.textSsMute);
        textInputSignal = my_view.findViewById(R.id.textInputSignal);
        textDspInit = my_view.findViewById(R.id.textDspInit);

        button_ok =  my_view.findViewById((R.id.buttonOK));
        button_ok.setOnClickListener(this);

        my_context = getActivity().getApplicationContext();
        LocalBroadcastManager.getInstance(my_context).registerReceiver(DataReceiver, createGIntentFilter());


        ReadDiagnosticsData = new TaskReadDiagnosticData(this, dspService);
        ReadDiagnosticsData.execute();

        return (my_view);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        LocalBroadcastManager.getInstance(my_context).unregisterReceiver(DataReceiver);
    }

    public void onPostExecute()
    {
        if (dspService.isConnected()) {
            try {
                Thread.currentThread();
                Thread.sleep(DIAG_READ_INTERVAL);
                ReadDiagnosticsData = new TaskReadDiagnosticData(this, dspService);
                ReadDiagnosticsData.execute();
            } catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        else
        {
Log.e(">>>>> DISCONNECT ERROR", "onPostExecute");
            ReadDiagnosticsData.cancel(true);
            this.dismiss();
        }
    }

    @Override
    // Handle user clicking the 'OK' button
    public void onClick(View button) {
        if (button.getId() == button_ok.getId()) {
            ReadDiagnosticsData.cancel(true);
            this.dismiss();
        }
    }

    public void SetDspService(DspService service){
        dspService = service;
    }

    private static IntentFilter createGIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DspService.ACTION_DSP_DATA_AVAILABLE);

        intentFilter.addAction(DspService.ACTION_GATT_DISCONNECTED);        // These are all error messages
        intentFilter.addAction(DspService.ACTION_DATA_TIME_OUT);
        intentFilter.addAction(DspService.ACTION_COMM_ERROR);
        intentFilter.addAction(DspService.ACTION_DEVICE_NOT_AX_DSP);

        return(intentFilter);
     }

    // Receives inter-process messages from other components
    private final BroadcastReceiver DataReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            switch(action)
            {
                case DspService.ACTION_DSP_DATA_AVAILABLE:
                    byte[] data = intent.getExtras().getByteArray(DspService.BLE_BYTES);
                    if (data!= null && data.length >= 20) {
                        int data_type = (int) data[0];                            // First byte is the data type

                        if (data_type == DSP_DATA.DATA_DIAGNOSTICS.ordinal()) {

                            Integer vehicleId = (((int)data[1]) & 0xFF) | ((((int)data[2]) & 0xFF) << 8);
                            String hexVehicleId =  String.format("#%02X", (0xFF & vehicleId));
                            textVehicleId.setText(hexVehicleId + " (" + Integer.toString(vehicleId) +")");

                            Integer modelId = (((int)data[9]) & 0xFF) | ((((int)data[10]) & 0xFF) << 8);
                            String hexModelId =  String.format("#%02X", (0xFF & modelId));
                            textModelId.setText(hexModelId + " (" + Integer.toString(modelId) +")");

                            int canActive = (int)data[3] & 0xFF;
                            canActive |= ((int)data[4] & 0xFF) << 8;
                            String hexCanActive = String.format("#%04X", (0xFFFF & canActive));
                            textCanActive.setText(hexCanActive);

                            String inputSignal = (data[5] == 0) ? "0" : "1";
                            textInputSignal.setText(inputSignal);

                            int amp_on_in = (int)data[6] & 0xFF;
                            amp_on_in |= ((int)data[7] & 0xFF) << 8;
                            String AmpOnIn = String.format("#%04X", (0xFFFF & amp_on_in));
                            textAmpOnIn.setText(AmpOnIn);

                            String DspInitialized = ((data[8] & 0x01) == 0) ? "0" : "1";
                            textDspInit.setText(DspInitialized);

                            String RapActive = ((data[8] & 0x02) == 0) ? "0" : "1";
                            textRapActive.setText(RapActive);

                            String SsMute = ((data[8] & 0x04) == 0) ? "0" : "1";
                            textSsMute.setText(SsMute);

                            String AmpOnOut = ((data[8] & 0x08) == 0) ? "0" : "1";
                            textAmpOnOut.setText(AmpOnOut);
                        }
                    }
                break;

                case DspService.ACTION_GATT_DISCONNECTED:
                case DspService.ACTION_DATA_TIME_OUT:
                case DspService.ACTION_COMM_ERROR:
                case DspService.ACTION_DEVICE_NOT_AX_DSP:
                    Log.e(">>>>> DISCONNECT ERROR", action);
                    ReadDiagnosticsData.cancel(true);
                  //  dialog.dismiss();
                    break;
            }
        }
    };
}