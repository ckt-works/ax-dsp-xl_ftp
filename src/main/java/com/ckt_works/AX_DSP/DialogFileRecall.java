package com.ckt_works.AX_DSP;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

enum DIALOG_ACTION {DIALOG_ACTION_CANCEL, DIALOG_ACTION_SAVE, DIALOG_ACTION_RECALL// Created by Abeck on 1/19/2017. //
}

@SuppressWarnings("unchecked")
public class DialogFileRecall extends androidx.appcompat.app.AppCompatDialogFragment implements  View.OnClickListener, AbsListView.OnItemClickListener  {

    private TextView title;
    private AbsListView recall_file_list;
    private ListAdapter recall_list_adapter;
    private Button recallCancelButton;
    private View my_view;
    private MainActivity  mainActivity;

    private List<Map<String, String>> files_names = new ArrayList<Map<String, String>>();

    static DialogFileRecall newInstance() {
        return(new DialogFileRecall());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
    //    Dialog dialog = getDialog();
    //    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        my_view = inflater.inflate(R.layout.layout_file_recall, container, false);
        title = (TextView) my_view.findViewById(R.id.file_recall_title);

        recall_file_list = (AbsListView) my_view.findViewById(R.id.recall_file_list);
        recallCancelButton = (Button) my_view.findViewById(R.id.buttonRecallCancel);
        recallCancelButton.setOnClickListener(this);
        return(my_view);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ColorDrawable select_color = new ColorDrawable(0xFF56B049);
        recall_list_adapter = new SimpleAdapter(this.getActivity(), files_names, R.layout.layout_list_textview, MainActivity.KEY_FILE_NAMES, MainActivity.IDS);
        recall_file_list.setAdapter(recall_list_adapter);
        recall_file_list.setSelector(select_color);
        recall_file_list.setOnItemClickListener(this);     // Set OnItemClickListener so we can be notified when users clicks on file name

        int number_of_files = PopulateFileList();
        if (number_of_files == 0)
            title.setText("No Config Files Found");
        else
            title.setText("Click on a File to Recall it");

    }

    @Override
    // Handles clicks on the File List
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, String> item = (Map<String, String>) recall_list_adapter.getItem(position);
        String file_name = item.get(MainActivity.KEY_FILE_NAME);
        Intent file_name_intent = new Intent(file_name);
        getTargetFragment().onActivityResult(getTargetRequestCode(), DIALOG_ACTION.DIALOG_ACTION_RECALL.ordinal(), file_name_intent);
        this.dismiss();
    }

    // Handles all Buttons on this screen
    public void onClick(View button) {
        int button_id = button.getId();

        if (button_id == recallCancelButton.getId()) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), DIALOG_ACTION.DIALOG_ACTION_CANCEL.ordinal(), getActivity().getIntent());
            this.dismiss();
        }
    }

    private void AddFileName(String file_name) {
        if (file_name != null) {
            Map<String, String> item = new HashMap<String, String>();
            item.put(MainActivity.KEY_FILE_NAME, file_name);
            files_names.add(item);
        }
        recall_file_list.setAdapter(recall_list_adapter);
    }

    private boolean is_directory;
    boolean made_ax_dsp;
    private int PopulateFileList()
    {
        int viewWidth = my_view.getWidth();
        int number_of_files = 0;
        int index = 0;
        String directory;
        mainActivity = (MainActivity) getActivity();


        if (mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
            directory = "/AX_DSP_LC/";
        else
            directory= "/AX_DSP_X/";
        File ext_fs = Environment.getExternalStorageDirectory();    // Get path to the external storage
        File dir = new File(ext_fs.getAbsolutePath() + directory); // Add our directory path to it

        is_directory = dir.isDirectory();
        if (!is_directory){
            if (!dir.mkdirs()) {
                Log.i("PopulateFileList", "Directory not created");
            }
        }
        if (is_directory)
        {
            for (File filenames : dir.listFiles()) {
                if (filenames.isFile())
                {
                    String file_name = filenames.getName();
                    if (file_name.endsWith(".xml"))
                    {
                        number_of_files++;
                        int dot_index = file_name.lastIndexOf(".");
                        String no_ext_name = file_name.substring(0, dot_index);
                        Long longDate = filenames.lastModified();
                        SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy HH:mm", Locale.US);
                        String date = formatDate.format(longDate);
                        int paddingWidth = 50 - dot_index;
                        String spaces = (paddingWidth==0)?"":String.format("%"+paddingWidth+"s", "");
                        AddFileName(no_ext_name); // + spaces + date);
                    }
                }
            }
            Collections.sort(files_names, fileComparator);      // Sort the files in ascending order
            recall_file_list.setAdapter(recall_list_adapter);   // Update the list view
        }
        return(number_of_files);
    }

    // Compares 2 files by their file name and returns the result of the comparision
    static public Comparator<Map<String, String>> fileComparator = new Comparator<Map<String, String>>() {
        public int compare(Map<String, String> file_1, Map<String, String> file_2) {
            String name_1 = file_1.get("KEY_FILE_NAME");
            String name_2 = file_2.get("KEY_FILE_NAME");
            return(name_1.compareToIgnoreCase(name_2));
        }
    };

}


