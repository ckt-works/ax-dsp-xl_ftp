package com.ckt_works.AX_DSP;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


// Created by Abeck on 1/19/2017. //

@SuppressWarnings("unchecked")

//public class DialogFileSave extends android.support.v4.app.DialogFragment implements  View.OnClickListener, AbsListView.OnItemClickListener,
public class DialogFileSave extends androidx.appcompat.app.AppCompatDialogFragment implements  View.OnClickListener, AbsListView.OnItemClickListener,
    EditText.OnEditorActionListener, TextWatcher, IDialogConfirmListener {

    private TextView title;
    private EditText file_name_input;
    private ListAdapter save_list_adapter;
    private AbsListView save_file_list;
    private Button saveCancelButton;
    private Button saveButton;

    private  String save_file_name = null;
    private List<Map<String, String>> files_names = new ArrayList<Map<String, String>>();

    static DialogFileSave newInstance() {
        return(new DialogFileSave());
    }

/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
//        Dialog dialog = getDialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        View my_view = inflater.inflate(R.layout.layout_file_save, container, false);
        title = (TextView) my_view.findViewById(R.id.file_save_title);
        save_file_list = (AbsListView) my_view.findViewById(R.id.save_file_list);

        file_name_input = (EditText) my_view.findViewById(R.id.editFileName);
        file_name_input.setImeOptions(EditorInfo.IME_ACTION_DONE);
        file_name_input.setSingleLine(true);
        file_name_input.setOnEditorActionListener(this);
        file_name_input.addTextChangedListener(this);

        saveCancelButton= (Button) my_view.findViewById(R.id.buttonSaveCancel);
        saveCancelButton.setOnClickListener(this);
        saveButton= (Button) my_view.findViewById(R.id.buttonSave);
        saveButton.setEnabled(false);
        saveButton.setOnClickListener(this);

        return(my_view);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ColorDrawable select_color = new ColorDrawable(0xFF56B049);

        save_list_adapter = new SimpleAdapter(this.getActivity(), files_names, R.layout.layout_list_textview, MainActivity.KEY_FILE_NAMES, MainActivity.IDS);
        save_file_list.setAdapter(save_list_adapter);
        save_file_list.setSelector(select_color);
        save_file_list.setOnItemClickListener(this);     // Set OnItemClickListener so we can be notified when users clicks on file name
        int number_of_files = PopulateFileList();

        if (number_of_files == 0)
            title.setText("Click on the File Name Box to create a new file");
        else
            title.setText("Click on a File Name to Overwrite it\nor on the File Name Box to create a new file");

    }

    // Called when the user clicks the Yes or No button on the File Overwrite Dialog
    public void onDialogConfirmClick(boolean answer){
        if (answer) {
            Intent file_name_intent = new Intent(save_file_name);
            getTargetFragment().onActivityResult(getTargetRequestCode(),DIALOG_ACTION.DIALOG_ACTION_SAVE.ordinal(), file_name_intent);
            this.dismiss();
        }
    }
    @Override
    // Handles clicks on the File List
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Map<String, String> item = (Map<String, String>) save_list_adapter.getItem(position);
        String file_name = item.get(MainActivity.KEY_FILE_NAME);

        file_name_input.setText(file_name);
           saveButton.setEnabled(true);
    }

    @Override
    public boolean onEditorAction (TextView v, int actionId, KeyEvent event)
    {
        if (actionId == EditorInfo.IME_ACTION_DONE)
            saveButton.setEnabled(true);
        return(false);
    }

    @Override
    public void beforeTextChanged(CharSequence editable, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence editable, int start, int before, int count) {
        saveButton.setEnabled(true);
    }
    @Override
    public void afterTextChanged(Editable editable) {}

        // Handles all Buttons on this screen
    public void onClick(View button) {
        int button_id = button.getId();

        if (button_id == saveCancelButton.getId()) {
            getTargetFragment().onActivityResult(getTargetRequestCode(), DIALOG_ACTION.DIALOG_ACTION_CANCEL.ordinal(), getActivity().getIntent());
           this.dismiss();
        }

        if (button_id == saveButton.getId()) {
            //String duplicate_name = files_names.
            save_file_name = String.valueOf(file_name_input.getText());

            Map<String, String> file_item = new HashMap<String, String>();
            file_item.put(MainActivity.KEY_FILE_NAME, save_file_name);

            if (files_names.contains(file_item)) {
                String title = "Confirm File Overwrite";
                String message = "The file '" + save_file_name + "' already exists. Do you want to Overwrite it?";
                DialogConfirm overwriteDialog = DialogConfirm.newInstance(title , message);
                overwriteDialog.setTargetFragment(this, 0);
                overwriteDialog.show(getFragmentManager(), "confirm");
            }
            else {
                Intent file_name_intent = new Intent(save_file_name);
                getTargetFragment().onActivityResult(getTargetRequestCode(), DIALOG_ACTION.DIALOG_ACTION_SAVE.ordinal(), file_name_intent);
                this.dismiss();
            }

        }
    }


    private void AddFileName(String file_name) {
        if (file_name != null) {
            Map<String, String> item = new HashMap<String, String>();
            item.put(MainActivity.KEY_FILE_NAME, file_name);
            files_names.add(item);
        }
        save_file_list.setAdapter(save_list_adapter);
    }


    private int PopulateFileList()
    {
        int number_of_files = 0;
        String directory;

        MainActivity mainActivity =  ((MainActivity) getActivity());
        if (mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
            directory = "/AX_DSP_LC/";
        else
            directory = "/AX_DSP_X/";

        File ext_fs = Environment.getExternalStorageDirectory();    // Get path to the external storage
        File dir = new File(ext_fs.getAbsolutePath() + directory); // Add our directory path to it

//        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "axdsp");
        boolean is_directory = dir.isDirectory();
        if (!is_directory){
            if (!dir.mkdirs()) {
                Log.i("PopulateFileList", "Directory not created");
            }
        }
        if (is_directory)
        {
            for (File filenames : dir.listFiles()) {
                if (filenames.isFile())
                {
                    String file_name = filenames.getName();
                    if (file_name.endsWith(".xml"))
                    {
                        number_of_files++;
                        int dot_index = file_name.lastIndexOf(".");
                        String no_ext_name = file_name.substring(0, dot_index);
                        AddFileName(no_ext_name);
                    }
                }
            }
            Collections.sort(files_names, DialogFileRecall.fileComparator); // Sort the files in ascending order
            save_file_list.setAdapter(save_list_adapter); // Update the list view
        }
        return(number_of_files);
    }

// --Commented out by Inspection START (5/25/2017 11:29 AM):
//    public void showSoftKeyboard(View view) {
//        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//    }
// --Commented out by Inspection STOP (5/25/2017 11:29 AM)

}