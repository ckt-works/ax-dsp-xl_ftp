package com.ckt_works.AX_DSP;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.ckt_works.AX_DSP.DialogPassword.PASSWORD_DIALOG_TYPE.PASSWORD_DIALOG_ENTER;
import static com.ckt_works.AX_DSP.DialogPassword.PASSWORD_DIALOG_TYPE.PASSWORD_DIALOG_SET;

// Created by Abeck on 2/7/2017. //

public class DialogPassword extends androidx.appcompat.app.AppCompatDialogFragment implements Button.OnClickListener, IDialogConfirmListener{

enum PASSWORD_DIALOG_TYPE{PASSWORD_DIALOG_SET, PASSWORD_DIALOG_ENTER}

    static final private long PASSWORD_MAX_FIRST_ENTRY_TIME =  30;  // Max time to allow user to enter first password character (in seconds)
    private long PASSWORD_MAX_NEXT_ENTRY_TIME =  10;  // Max time to allow user to enter next password character (in seconds)
    private Button buttonSet;
    private Button buttonCancel;
    private IDialogPasswordListener passwordCallbackListener;
    private EditText editPassword;

    private CountDownTimer  maxPasswordEnterTimer;
    private long time;
    private PASSWORD_DIALOG_TYPE dialogType = PASSWORD_DIALOG_ENTER;


    static DialogPassword newInstance() {
        return(new DialogPassword());
    }

    public void SetType(PASSWORD_DIALOG_TYPE type) {
        dialogType = type;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
//        Dialog dialog = getDialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View my_view = inflater.inflate(R.layout.layout_password, container, false);

        if (dialogType == PASSWORD_DIALOG_SET) {
            TextView textTitle = my_view.findViewById(R.id.textTitle);
            textTitle.setText("Enter New Password");
        }

        buttonSet = my_view.findViewById(R.id.buttonSet);
        buttonSet.setOnClickListener(this);
        buttonSet.setEnabled(false);                            // Disable set button until a digit entered

        buttonCancel = my_view.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(this);

        editPassword = my_view.findViewById(R.id.editPassword);
        editPassword.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable password) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                RestartMaxEnterTimer(PASSWORD_MAX_NEXT_ENTRY_TIME);
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                buttonSet.setEnabled(s.length() > 0);

            }
        });

        RestartMaxEnterTimer(PASSWORD_MAX_FIRST_ENTRY_TIME);
        return(my_view);
    }

    private void RestartMaxEnterTimer(long max_time) {
        if (dialogType == PASSWORD_DIALOG_ENTER) {
            time = max_time;
            if (maxPasswordEnterTimer != null) {
                maxPasswordEnterTimer.cancel();
            }
            maxPasswordEnterTimer = new CountDownTimer(max_time * 1000, 1000) {
                public void onTick(long millisUntilFinished) {
                    time--;
                    Log.i("DialogPassword", Long.toString(time));
                }

                public void onFinish() {
                    DisplayTimeoutAlert();
                    // onClick( buttonCancel);
                }
            }.start();
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        passwordCallbackListener  = (IDialogPasswordListener) getTargetFragment();
        super.onActivityCreated(savedInstanceState);
    }



    @Override
    // Handle user clicking a button
    public void onClick(View button) {

        try {
            boolean answer = false;
            if (button.getId() == buttonSet.getId())
                answer = true;
            String password = editPassword.getText().toString();
            passwordCallbackListener.onPasswordDialogClick(answer, password);
        }
        catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
        if (maxPasswordEnterTimer != null)
            maxPasswordEnterTimer.cancel();
        this.dismiss();
    }

    private void DisplayTimeoutAlert() {
        DialogAlert password_timeout_alert = DialogAlert.newInstance("Password Entry Timeout", "You exceeded the time allowed to enter the password");
        password_timeout_alert.setTargetFragment(this, 0);
        password_timeout_alert.show(getFragmentManager(), "alert");
    }



    // Called when the user clicks the button on the Password Entry Timeout dialog
    public void onDialogConfirmClick(boolean answer){
        onClick( buttonCancel);
    }
}

