package com.ckt_works.AX_DSP;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

// Created by Abeck on 2/6/2017. //

public class DialogProgress extends androidx.appcompat.app.AppCompatDialogFragment {
    private View my_view;

    private ProgressBar progress_bar;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
         my_view = inflater.inflate(R.layout.layout_progress_bar, container, false);

        TextView title = (TextView) my_view.findViewById(R.id.progress_title);
        String title_text = getArguments().getString("TITLE");
        title.setText(title_text);
        progress_bar = (ProgressBar) my_view.findViewById(R.id.progress_bar);
        progress_bar.setMax(100);
        return(my_view);
    }

}
