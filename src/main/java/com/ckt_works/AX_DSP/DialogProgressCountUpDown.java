package com.ckt_works.AX_DSP;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

//public class DialogProgressCountdown extends android.support.v4.app.DialogFragment {
public class DialogProgressCountUpDown extends androidx.appcompat.app.AppCompatDialogFragment {
    private View my_view;

    private ProgressBar progress_bar;
    private TextView textCountDown;
    private long time = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
//        Dialog dialog = getDialog();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        my_view = inflater.inflate(R.layout.layout_progress_bar_countdown, container, false);
        TextView title = (TextView) my_view.findViewById(R.id.progress_title);
        String title_text = getArguments().getString("TITLE");
        title.setText(title_text);

        textCountDown = (TextView) my_view.findViewById(R.id.countdown_text);
        String strTime = getArguments().getString("TIME");
        if (strTime != null)
            time = Long.valueOf(strTime);

        //       title.setGravity(Gravity.CENTER_HORIZONTAL);

        progress_bar = (ProgressBar) my_view.findViewById(R.id.progress_bar_countdown);
        progress_bar.setMax(100);

        new CountDownTimer(time * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                textCountDown.setText(String.valueOf(time));
                time--;
            }

            public void onFinish() {
                textCountDown.setText("---");
            }
        }.start();

        return(my_view);
    }

}
