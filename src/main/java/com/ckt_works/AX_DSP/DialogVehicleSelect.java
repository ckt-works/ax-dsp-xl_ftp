package com.ckt_works.AX_DSP;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.ckt_works.AX_DSP.MainActivity.HAS_NO_OE_AMP;

// Created by Abeck on 2/13/2017. //


public class DialogVehicleSelect extends androidx.appcompat.app.AppCompatDialogFragment implements View.OnClickListener, View.OnTouchListener,
                                        Spinner.OnItemSelectedListener, Switch.OnCheckedChangeListener, IPostExecuteInterface{

    //static
    private Context my_context;

//    private enum SELECTED_TYPE {SELECTED_NONE, SELECTED_VEHICLE, SELECTED_OE_AMP, SELECTED_ALL};
    private enum FORCE_OE_AMP_STATE {
        FORCE_OE_AMP_NO(-1), FORCE_OE_AMP_FALSE(0), FORCE_OE_AMP_TRUE(1);
        private int id;

        FORCE_OE_AMP_STATE(int id) {
            this.id = id;
        }

        @SuppressWarnings("unused")
        public int getIdCode() {
            return this.id;
        }
}

    private enum AMP_TYPE {AMP_DIGITAL, AMP_GM_DIGITAL, AMP_GM_MOST}

    static private class AmpData
    {
        AMP_TYPE amp_type;
        FORCE_OE_AMP_STATE force_oe_amp;
    }

    private Dialog dialog;

    private Spinner spinnerManufacturer;
    private Spinner spinnerModel;
    private ArrayAdapter<String> adapterModel = null;

    private RadioGroup radioGroupOeAmp;
    private RadioButton radioWithoutOeAmp;
    private RadioButton radioWithOeAmp;

    private Button buttonApply;
    private Button buttonCancel;

    private TextView textTitle;             // Use this as an Easter Egg to get to hidden vehicles

    private String selected_manuf_name;
    private String selected_model_name;
    private boolean has_oe_amp;
//    private boolean initializing_manuf_list = false;
//    private boolean initializing_model_list = false;

//    private SELECTED_TYPE selected_type = SELECTED_TYPE.SELECTED_NONE;

    private int displayHiddenClicks = 0;                // Track how many times the user clicks the Title box - to display hidden vehicles
    private boolean showHiddenVehicles;
//    private static boolean setting_model = false;
//    static private int init_state = 0;

    private VehicleTypeFile vehicleTypeFile = null;
    private MainActivity  mainActivity;

    private boolean userClickedOnModel = false;

    public DialogVehicleSelect(){

    }

    public void onPostExecute() {
        mainActivity.ProgressBarDismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        dialog = getDialog();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = dialog.getWindow();
        assert window != null;
        window.setGravity(Gravity.TOP);
        WindowManager.LayoutParams layout_parms = window.getAttributes();
        layout_parms.y = 100;   //y position

        View my_view = inflater.inflate(R.layout.layout_vehicle_select, container, false);
        my_context = my_view.getContext();

        radioGroupOeAmp = (RadioGroup) my_view.findViewById(R.id.radioGroupOeAmp);

        radioWithoutOeAmp = (RadioButton)my_view.findViewById(R.id.radioWithoutOeAmp);
        radioWithoutOeAmp.setOnCheckedChangeListener(this);

        radioWithOeAmp = (RadioButton)my_view.findViewById(R.id.radioWithOeAmp);
        radioWithOeAmp.setOnCheckedChangeListener(this);

        buttonApply = (Button)my_view.findViewById(R.id.buttonVehicleApply);
        buttonApply.setEnabled(false);                                          // Disabled until an "OE Amp" options is selected
        buttonApply.setOnClickListener(this);

        buttonCancel = (Button)my_view.findViewById(R.id.buttonVehicleCancel);
        buttonCancel.setOnClickListener(this);

        textTitle =  (TextView) my_view.findViewById(R.id.vehicle_select_title);
        textTitle.setOnClickListener(this);

        spinnerManufacturer = (Spinner)my_view.findViewById(R.id.spinManufacturer);
        spinnerManufacturer.setOnItemSelectedListener(this);

        spinnerModel = (Spinner)my_view.findViewById(R.id.spinModel);
        spinnerModel.setOnItemSelectedListener(this);
        spinnerModel.setOnTouchListener(this);

        mainActivity =  ((MainActivity) getActivity());
        UpdateVehicleFile();
        return(my_view);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        displayHiddenClicks = 0;
        showHiddenVehicles = false;
        UpdateLists();
    }


    private void UpdateVehicleFile() {
        mainActivity.ProgressBarDisplay("Updating Vehicle Info");
        vehicleTypeFile = new VehicleTypeFile(mainActivity, true, this);
    }


    // Rebuilds the manufacturer and model lists
    private void UpdateLists(){

        try {
            selected_manuf_name = getArguments().getString("manufacturer");
            selected_model_name = getArguments().getString("model");
            has_oe_amp = getArguments().getBoolean("has_oe_amp");
            userClickedOnModel = false;
            List<String> manufacturers = GetManufacturerList();
            ArrayAdapter<String> adapterManuf = new ArrayAdapter<>(dialog.getContext(), R.layout.layout_spinner_selected_item, manufacturers);
            adapterManuf.setDropDownViewResource(R.layout.layout_spinner_dropdown);
            spinnerManufacturer.setAdapter(adapterManuf);

            int manuf_position = adapterManuf.getPosition(selected_manuf_name);
            spinnerManufacturer.setSelection(manuf_position, false);

        } catch(XmlPullParserException | IOException ex){
            ex.printStackTrace();
        }

    }

    // Fired when an 'OE Amp option" is selected
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonApply.setEnabled(true);
            /*
            if (selected_type == SELECTED_TYPE.SELECTED_VEHICLE) {
                selected_type = SELECTED_TYPE.SELECTED_ALL;
                buttonApply.setEnabled(true);
            } else if (selected_type != SELECTED_TYPE.SELECTED_ALL)
                selected_type = SELECTED_TYPE.SELECTED_OE_AMP;
*/
        }
    }



    @Override
        // Fired when the user clicks on the Manufacturer or Model dropdn
       public boolean onTouch(View spinner, MotionEvent event) {
    Log.i("<><><><onTouch", "userClickedOnModel=" + Boolean.toString(spinner.getId() == spinnerModel.getId()));

            if (spinner.getId() == spinnerModel.getId())                // If the user clicked on the Model dropdown...
                userClickedOnModel = true;                              // Set the flag that indicates that
            return false;                                               // Let the UI handle handle the onItemSelected event
        }


    @Override
    // Fired when the user clicks on the Manufacturer or Model dropdown
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        displayHiddenClicks = 0;
         try {
            if (parent.equals(spinnerManufacturer)) {
                selected_manuf_name = parent.getItemAtPosition(position).toString();
//                Log.i("onItemSelected", selected_manuf_name);
 //               selected_type = SELECTED_TYPE.SELECTED_VEHICLE;                     // must be before radioWithOeAmp.setChecked & radioWithoutOeAmp.setChecked
                SetOeButtons();

                List<String> models = GetModelList(selected_manuf_name);            // Build Model list for selected manufacturer
                {
                    adapterModel = new ArrayAdapter<>(dialog.getContext(), R.layout.layout_spinner_selected_item, models);
                    adapterModel.setDropDownViewResource(R.layout.layout_spinner_dropdown);

                    spinnerModel.setOnItemSelectedListener(this);
                    spinnerModel.setAdapter(adapterModel);

                    if (!models.isEmpty() && !userClickedOnModel) {
                        int model_position = adapterModel.getPosition(selected_model_name);
                        spinnerModel.setSelection(model_position, false);
                    }
                }
            }


            else if (parent.equals(spinnerModel)) {
Log.i("<><><><onItemSelected", "userClickedOnModel=" + Boolean.toString(userClickedOnModel));
                selected_model_name = parent.getItemAtPosition(position).toString();
                SetOeButtons();
                userClickedOnModel = false;
/*                if (userClickedOnModel) {
                    userClickedOnModel = false;
                    selected_model_name = parent.getItemAtPosition(position).toString();

                    if (selected_model_name == null || selected_model_name.isEmpty())
                        selected_model_name = parent.getItemAtPosition(position).toString();
                    else {
                        int model_position = adapterModel.getPosition(selected_model_name);
                        spinnerModel.setSelection(model_position, false);
                    }
               }
                else
                    selected_model_name = parent.getItemAtPosition(position).toString();
*/
            }
        } catch (XmlPullParserException | IOException ex) {
            ex.printStackTrace();
        }
    }

    private void SetOeButtons()
    {
        AmpData amp_data = GetAmpData(selected_manuf_name, selected_model_name);
        if (amp_data.force_oe_amp != FORCE_OE_AMP_STATE.FORCE_OE_AMP_NO){              // If the selected type has a forced OE AMP state
            radioWithoutOeAmp.setEnabled(false);
            radioWithOeAmp.setEnabled(false);
            if (amp_data.force_oe_amp == FORCE_OE_AMP_STATE.FORCE_OE_AMP_TRUE)
                radioWithOeAmp.setChecked(true);
            else
                radioWithoutOeAmp.setChecked(true);
        }
        else if (!userClickedOnModel) {
            if (has_oe_amp)
                radioWithOeAmp.setChecked(true);
            else
                radioWithoutOeAmp.setChecked(true);

            radioWithOeAmp.setEnabled(true);
            radioWithoutOeAmp.setEnabled(true);
            buttonApply.setEnabled(true);
        }
        else
        {
            radioGroupOeAmp.clearCheck();
            radioWithOeAmp.setEnabled(true);
            radioWithoutOeAmp.setEnabled(true);
            buttonApply.setEnabled(false);
        }

        if (amp_data.amp_type == AMP_TYPE.AMP_GM_DIGITAL)
        {
            radioWithoutOeAmp.setText("Without Amp");
            radioWithOeAmp.setText("STZ, UQS, or Y91");
        }

        else if (amp_data.amp_type == AMP_TYPE.AMP_GM_MOST)
        {
            radioWithoutOeAmp.setText("Without Amp");
            radioWithOeAmp.setText("UQA or UQG");
        }
        else
        {
            radioWithoutOeAmp.setText("Without OE Amp");
            radioWithOeAmp.setText("With OE Amp");
        }


    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Callback when nothing is selected from the list
    }

    @Override
    // Handles all buttons
    public void onClick(View button) {
        int button_id = button.getId();

        switch(button_id) {
            case R.id.buttonVehicleCancel:
                this.dismiss();
                break;

            case R.id.buttonVehicleApply:
                if (spinnerModel.getSelectedItem() == null)
                    selected_model_name = "?";
                else {
                    selected_model_name = spinnerModel.getSelectedItem().toString();
                    if (selected_model_name == null)
                        selected_model_name = "?";
                }
                boolean has_OE_amplifier = radioWithOeAmp.isChecked();

                try {
                    List<Integer> vehicle_ordinals =GetVehicleIds(selected_manuf_name, selected_model_name, has_OE_amplifier);
                    int vehicle_id = vehicle_ordinals.get(0);
                    int model_id = vehicle_ordinals.get(1);

                    IDialogVehicleListener callback_listener = (IDialogVehicleListener) getTargetFragment();       //    Callback to the fragment that created us with the vehicle info
                    callback_listener.onApplyClick(selected_manuf_name, selected_model_name, has_OE_amplifier, vehicle_id, model_id);
                } catch (XmlPullParserException | ClassCastException | IOException e) {
                    throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
                }

                this.dismiss();
                break;

            case R.id.vehicle_select_title:
                if (++displayHiddenClicks > 2) {
                    showHiddenVehicles = true;
                    textTitle.setTextColor(0xFFFF0000);
                    UpdateLists();
                }
                break;
        }
    }


    // Create a Parser to use for parsing our data
    private static XmlPullParser CreateParser(InputStream input_stream) throws XmlPullParserException
    {
        XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser myParser = xmlFactoryObject.newPullParser();
        if (myParser != null)
            myParser.setInput(input_stream, null);

        return(myParser);
    }


    // Get a Unique list of all manufacturers in our XML file - no duplicate names
    private ArrayList <String> GetManufacturerList() throws XmlPullParserException, IOException {
        ArrayList<String> manufacturers = new ArrayList <String>();
        InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
//        InputStream input_stream = GetVehicleTypeStream();

        if (input_stream != null)
        {
            XmlPullParser myParser = CreateParser(input_stream);

            if (myParser != null) {
                int event = myParser.getEventType();
                while (event != XmlPullParser.END_DOCUMENT) {                              // Parse until the end of the document
                    switch (event) {
                        case XmlPullParser.END_TAG:
                            String tag_name = myParser.getName();
                            if (tag_name.equals("vehicle")) {
                              String vehicleIsHidden = myParser.getAttributeValue(null, "Hidden");
                              if (vehicleIsHidden == null || showHiddenVehicles) {
                                  String manuf_name = myParser.getAttributeValue(null, "Manufacturer");
if (manuf_name.equals("VW") || manuf_name.equals("MINI"))
Log.i("Manuf Name:", manuf_name);
                                  Set<String> set = new HashSet<>(manufacturers);       //Load the list into a hashSet
                                  if (!set.contains(manuf_name))
                                      manufacturers.add(manuf_name);
                              }
                            }
                            break;

                    }
                    event = myParser.next();
                }
                input_stream.close();
                Collections.sort(manufacturers);
            }
        }
        return (manufacturers);
    }

    // Get a List of all models for the given manufacturer
    private ArrayList <String> GetModelList(String manufacturer) throws XmlPullParserException, IOException {
        ArrayList <String> models = new  ArrayList <String>();
        InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
//        InputStream input_stream = GetVehicleTypeStream();

        if (input_stream != null) {
            XmlPullParser myParser = CreateParser(input_stream);
            int event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {                   // Parse until the end of the document
                switch (event) {
                    case XmlPullParser.END_TAG:
                        String tag_name = myParser.getName();
                        if (tag_name.equals("vehicle")) {
                            String vehicleIsHidden = myParser.getAttributeValue(null, "Hidden");
                            if (vehicleIsHidden == null || showHiddenVehicles) {
                                String manuf_name = myParser.getAttributeValue(null, "Manufacturer");
                                if (manuf_name.equals(manufacturer))            // If this model is for the passed manufacturer
                                {
                                    String model_name = myParser.getAttributeValue(null, "Model");
                                    if (model_name != null) {
                                        String year = myParser.getAttributeValue(null, "Years");
                                        if (year != null)
                                            model_name += " - " + year;

                                        models.add(model_name);
                                    }
                                }
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            input_stream.close();
            Collections.sort(models);
        }
        return (models);
    }

    List<Integer> GetVehicleIds(String manufacturer, String model_year, boolean has_OE_amplifier) throws XmlPullParserException, IOException{
        InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();

        List<Integer> vehicle_ordinals = DialogVehicleSelect.GetModelOrdinal(input_stream, manufacturer, model_year, has_OE_amplifier);

        return(vehicle_ordinals);
    }


    // Get the ID number for the selected manufacturer & mode year - THIS IS A HACK UNTIL WE GET "GetModelList" to handle an object with both the model year & ID value
    static List<Integer> GetModelOrdinal(InputStream input_stream, String manufacturer, String model_year, boolean has_OE_amplifier) throws XmlPullParserException, IOException {
        List<Integer> vehicle_ordinals = new ArrayList<Integer>();           // Save the vehicle ordinals in an array so we can return them
        int vehicle_id = 0;
        int model_id = 0;
        int amp_offset = 1;

        XmlPullParser myParser = CreateParser(input_stream);

        int event = myParser.getEventType();

        loop: // need a label so we can break out of the while loop when in the switch statement
        while (event != XmlPullParser.END_DOCUMENT) {                   // Parse until the end of the document
//            String name = myParser.getName();
            switch (event) {
                case XmlPullParser.END_TAG:
                    String tag_name = myParser.getName();
                    if (tag_name.equals("vehicle")) {
                        String manuf_name = myParser.getAttributeValue(null, "Manufacturer");
                        if (manuf_name.equals(manufacturer))            // If this model is for the passed manufacturer
                        {
                            String model_name = myParser.getAttributeValue(null, "Model");
                            if (model_name == null){                                                    // This manufacturer has no models
                                String str_id = myParser.getAttributeValue(null, "Id");
                                if (str_id != null)
                                    vehicle_id = Integer.valueOf(str_id);

                                amp_offset =GetAmpOffset(has_OE_amplifier);
                                break loop;
                            }

                            else{
                                String year = myParser.getAttributeValue(null, "Years");
                                if (year != null)
                                    model_name += " - " + year;

                                if (model_name.equals(model_year)) {
                                    String str_vehicle_id = myParser.getAttributeValue(null, "Id");
                                    if (str_vehicle_id != null)
                                        vehicle_id = Integer.valueOf(str_vehicle_id);

                                    String str_model_id = myParser.getAttributeValue(null, "Model_Id");
                                    if (str_model_id != null)
                                        model_id = Integer.valueOf(str_model_id);

                                    amp_offset =GetAmpOffset(has_OE_amplifier);

                                    break loop;
                                }
                            }
                        }
                    }
                    break;
            }
            event = myParser.next();
        }
        input_stream.close();

        vehicle_id |= amp_offset;
        vehicle_ordinals.add(0, vehicle_id);
        vehicle_ordinals.add(1, model_id);

        return (vehicle_ordinals);
    }

    // Return contains data about the amplifier selection
    //  - if the passed model has the "Has OE AMP" set to true or false
    //  - the type of amplifier selection available
    //static
    private AmpData GetAmpData(String manufacturer_name, String model_year)
    {
        AmpData amp_data = new AmpData();
        amp_data.force_oe_amp = FORCE_OE_AMP_STATE.FORCE_OE_AMP_NO;
        amp_data.amp_type = AMP_TYPE.AMP_DIGITAL;

        try {
            InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
//            InputStream input_stream = GetVehicleTypeStream();
            XmlPullParser myParser = CreateParser(input_stream);
            int event = myParser.getEventType();
            loop: // need a label so we can break out of the while loop when in the switch statement
            while (event != XmlPullParser.END_DOCUMENT) {                           // Parse until the end of the document
                switch (event) {
                    case XmlPullParser.END_TAG:
                        String tag_name = myParser.getName();
                        if (tag_name.equals("vehicle")) {
                            String this_manufacturer_name = myParser.getAttributeValue(null, "Manufacturer");
                            if (this_manufacturer_name.equalsIgnoreCase(manufacturer_name)) {
                                String this_model_name = myParser.getAttributeValue(null, "Model");
                                if (this_model_name == null) {
                                    amp_data = SetAmpData(amp_data, myParser);
                                }
                                else{
                                    String years = myParser.getAttributeValue(null, "Years");
                                    if (years != null)
                                        this_model_name += " - " + years;

                                    if (this_model_name.equalsIgnoreCase(model_year)) {
                                        amp_data = SetAmpData(amp_data, myParser);                                            break loop;     // Exit the while loop
                                    }
                                }
                            }
                        }
                        break;
                }
                event = myParser.next();
            }
            input_stream.close();
        } catch (XmlPullParserException | IOException e1) {
            e1.printStackTrace();
        }
        return (amp_data);
    }


    static private AmpData SetAmpData(AmpData amp_data, XmlPullParser parser)
    {
        amp_data.force_oe_amp = FORCE_OE_AMP_STATE.FORCE_OE_AMP_NO;
        amp_data.amp_type = AMP_TYPE.AMP_DIGITAL;

        String str_force_oe_amp_state = parser.getAttributeValue(null, "OE_AMP");        // See if this model has OE_AMP value set
        if (str_force_oe_amp_state != null)
            amp_data.force_oe_amp = (str_force_oe_amp_state.equalsIgnoreCase("true")) ? FORCE_OE_AMP_STATE.FORCE_OE_AMP_TRUE : FORCE_OE_AMP_STATE.FORCE_OE_AMP_FALSE;

        String str_amp_string = parser.getAttributeValue(null, "AMP_TYPE");             // See if this model has an Amp type specified
        amp_data.amp_type = ParseAmpType(str_amp_string);

        return(amp_data);
    }

    static private AMP_TYPE ParseAmpType(String str_amp_string)
    {
        AMP_TYPE amp_type =  AMP_TYPE.AMP_DIGITAL;
        if (str_amp_string != null){
            if (str_amp_string.equalsIgnoreCase("GM_DIGITAL"))
                amp_type = AMP_TYPE.AMP_GM_DIGITAL;
            else if (str_amp_string.equalsIgnoreCase("GM_MOST"))
                amp_type = AMP_TYPE.AMP_GM_MOST;
        }
        return(amp_type);
    }

    static private int GetAmpOffset(boolean has_OE_amplifier)
    {
        int amp_offset = 0;
        if (!has_OE_amplifier)
            amp_offset = 1;
        return(amp_offset);
    }

    // Get the manufacturer, model and has_oe_amp corresponding to "model_id"
    static List<String> GetVehicleTypeInfo(InputStream input_stream, int vehicle_id, int model_id) throws XmlPullParserException, IOException {
        String manufacturer_name = "?";
        String model_name = "?";
        String str_vehicle_id;
        boolean has_OE_amplifier = false;
        String str_force_oe_amp_state;
        FORCE_OE_AMP_STATE force_oe_amp_state = FORCE_OE_AMP_STATE.FORCE_OE_AMP_NO;

        XmlPullParser myParser = CreateParser(input_stream);

        int event = myParser.getEventType();
        loop: // need a label so we can break out of the while loop when in the switch statement
        while (event != XmlPullParser.END_DOCUMENT) {                           // Parse until the end of the document
            switch (event) {
                case XmlPullParser.END_TAG:
                    String tag_name = myParser.getName();
                    int this_model = 0;
                    if (tag_name.equals("vehicle")) {
                        str_force_oe_amp_state = myParser.getAttributeValue(null, "OE_AMP");        // See if this model has OE_AMP value set
                        if (str_force_oe_amp_state != null)                                         // If it does, compare actual ID...
                             str_vehicle_id = Integer.toString(vehicle_id);
                        else
                            str_vehicle_id = Integer.toString(vehicle_id & 0xFFFE);                 // otherwise, ignore "HAS_OE_AMP" bit

                        String Id  = myParser.getAttributeValue(null, "Id");
                        String modelId = myParser.getAttributeValue(null, "Model_Id");
                        manufacturer_name = myParser.getAttributeValue(null, "Manufacturer");   // Get Manufacturer & Model names
                        model_name = myParser.getAttributeValue(null, "Model");
Log.i("GetVehicleTypeInfo", "Id: " + Id + " - modelId: " + modelId +  " - Manuf: " + manufacturer_name + " - Model: " + model_name);
                        if (Id.equals(str_vehicle_id) )                                             // If this entry is for the vehicle_id
                        {
                                    // see if this entry has a Model ID
                            modelId = myParser.getAttributeValue(null, "Model_Id");if (modelId != null)
                            this_model = Integer.parseInt(modelId);

                            if (this_model == model_id) {
                                manufacturer_name = myParser.getAttributeValue(null, "Manufacturer");   // Get Manufacturer & Model names
                                model_name = myParser.getAttributeValue(null, "Model");
                                if (model_name != null) {
                                    String year = myParser.getAttributeValue(null, "Years");
                                    if (year != null)
                                        model_name += " - " + year;
                                }
                                if (str_force_oe_amp_state != null) {
                                    has_OE_amplifier = (Objects.equals(str_force_oe_amp_state, "true"));
                                    force_oe_amp_state = ((Objects.equals(str_force_oe_amp_state, "true")) ? FORCE_OE_AMP_STATE.FORCE_OE_AMP_TRUE : FORCE_OE_AMP_STATE.FORCE_OE_AMP_FALSE);
                                } else
                                    //noinspection RedundantConditionalExpression
                                    has_OE_amplifier = ((vehicle_id & HAS_NO_OE_AMP) > 0) ? false : true;  // Bit 0 of the id indicates if it has an OE amplifier
                                break loop;                                                             // Exit the while loop
                            }
                        }
                    }
                    break;
            }
            event = myParser.next();
        }
        input_stream.close();

        List<String> vehicle_type_info = new ArrayList<String>();           // Save the vehicle info in an array so we can return it
        vehicle_type_info.add(0, manufacturer_name);
        vehicle_type_info.add(1, model_name);
        vehicle_type_info.add(2, Boolean.toString(has_OE_amplifier));
        vehicle_type_info.add(3, Integer.toString(force_oe_amp_state.ordinal()));

        return (vehicle_type_info);
    }

    InputStream GetVehicleTypeStream() {

        InputStream input_stream = null;

        if (vehicleTypeFile != null)
            vehicleTypeFile.GetInputStream();
        return(input_stream);
    }


}
