package com.ckt_works.AX_DSP;

// Created by Abeck on 12/29/2016. //
// These enumeration must be the same as the enumerations in the 2657386 code
@SuppressWarnings("unused")
enum DSP_COMMANDS {DSP_COMMAND_RESET(0), DSP_COMMAND_LOCK(1), DSP_COMMAND_CLIPPING_LEVEL(2),
    DSP_COMMAND_EQ_SCAN_START(3), DSP_COMMAND_EQ_SCAN_STOP(4), DSP_COMMAND_EQ_SCAN_STATUS(5),
    DSP_COMMAND_RTA_START(6), DSP_COMMAND_RTA_STOP(7),
    DSP_COMMAND_CHIME_VOLUME(8), DSP_COMMAND_VEHICLE_TYPE(9), DSP_COMMAND_AMP_TURN_ON(10),
    DSP_COMMAND_IDENTIFY(11), DSP_COMMAND_SET_INPUT_SOURCE(12), DSP_COMMAND_SET_INPUT_LEVEL(13),
    DSP_COMMAND_SET_INVERT_SW(14), DSP_COMMAND_SET_MUTE_SW(15), DSP_COMMAND_SET_SPEAKER_LOCATION(16),
    DSP_COMMAND_SET_GROUP(17), DSP_COMMAND_AMP_TURN_ON_DELAY(18), DSP_COMMAND_FLATTEN_EQ(19),
    DSP_COMMAND_SET_PASSWORD(20), DSP_COMMAND_SET_CAN_PAUSE(21), DSP_COMMAND_CLEAR_PHONE_LIST(22),

    // NOTE - The following commands must be AFTER all of the set commands
    DSP_COMMAND_GET(30), DSP_COMMAND_GET_VEHICLE_TYPE(31),
    DSP_COMMAND_GET_CHIME_VOLUME(32), DSP_COMMAND_GET_CROSSOVER_DATA(33), DSP_COMMAND_GET_EQ_GAIN(34),
    DSP_COMMAND_GET_EQ_BOOST(35), DSP_COMMAND_GET_SPEAKER_DELAY(36), DSP_COMMAND_GET_CLIPPING_LEVEL(37),
    DSP_COMMAND_GET_AMP_TURN_ON(38), DSP_COMMAND_GET_INPUT_CONFIG(39), // DSP_COMMAND_GET_INPUT_LEVEL(40),
    DSP_COMMAND_GET_DIAGNOSTICS(41), DSP_COMMAND_GET_DSP_STATUS(42), DSP_COMMAND_GET_DIRTY_FLAG(43),
    DSP_COMMAND_GET_PASSWORD(44);

    private final int id;
    DSP_COMMANDS(int id) {
        this.id = id;
    }
    public int getIdCode() {
        return this.id;
    }
}

enum DSP_DATA {DSP_DATA_VEHICLE_TYPE(0), DSP_DATA_CHIME_VOLUME(1), DSP_DATA_CROSSOVER_DATA(2), DSP_DATA_EQ_GAIN(3),
     DSP_DATA_EQ_BOOST(4), DSP_DATA_DELAY(5), DSP_DATA_CLIPPING_LEVEL(6), DSP_DATA_AMP_TURN_ON(7),
     DSP_DATA_INPUT_CONFIG(8), DATA_DIAGNOSTICS(9), DATA_DSP_ON_STATUS(10), DATA_DIRTY_FLAG(11), DATA_PASSWORD(12);

    private final int id;
    DSP_DATA(int id) {
        this.id = id;
    }
    public int getIdCode() {
        return this.id;
    }
}


public class DspCommand {

    private DSP_COMMANDS command;
    private final byte[] byte_data;
    @SuppressWarnings("unused")
    private int number_bytes;

    DspCommand(){
        byte_data = new byte[16];
        number_bytes = 16;
    }

    DspCommand(DSP_COMMANDS new_command)
    {
        byte_data = new byte[16];
        number_bytes = 16;
        command = new_command;
    }

    @SuppressWarnings("unused")
    DspCommand(DSP_COMMANDS new_command, int new_data)
    {
        command = new_command;
        byte_data = new byte[2];
        number_bytes = 2;
        byte_data[0] = (byte)(new_data >> 8);
        byte_data[1] = (byte)(new_data & 0xFF);
    }

    public void SetCommand(DSP_COMMANDS command){this.command = command;}
    public DSP_COMMANDS GetCommand(){return(this.command);}

    public void SetData(byte new_data)
    {
        byte_data[0] = new_data;
    }

    public void SetData(byte data_0, byte data_1)
    {
       byte_data[0] = data_0;
       byte_data[1] = data_1;
    }

    public void SetData(byte data_0, byte data_1,  byte data_2)
    {
        byte_data[0] = data_0;
        byte_data[1] = data_1;
        byte_data[2] = data_2;
    }

    public void SetData(byte[] new_data)
    {
        for (int index = 0; index < new_data.length; index++)
            byte_data[index] = new_data[index];
    }

    public void SetData(int new_data)
    {
        byte_data[0] = (byte)(new_data & 0xFF);
        byte_data[1] = (byte)((new_data >> 8) & 0xFF);
    }

    public String GetIdString() {
        return(Integer.toString(command.getIdCode()));
    }
    public int GetDataInt(){
        int data = (((int) byte_data[0]) & 0xff) * 256;      // Funky code to convert signed bytes to unsigned int
        data += ((int) byte_data[1]) & 0xff;
        return(data);}

    public void SetData(byte data_bytes[], int number_bytes)
    {
//        for (int index = 0; index < number_bytes; index++)
//            byte_data[index] = data_bytes[index];
        System.arraycopy(data_bytes, 0, byte_data, 0, number_bytes);
        this.number_bytes = number_bytes;
    }


    public byte[] GetCommandData() {
        byte[] data_bytes = new byte[16]; //[number_bytes + 4];

        int command_value = this.command.getIdCode(); // .ordinal();
        data_bytes[0] = (byte)(command_value);            // Stuff command value into array
        data_bytes[1] = (byte)(command_value >> 8);
//        data_bytes[2] = (byte)(command_value >> 16);
//        data_bytes[3] = (byte)(command_value >> 24);

        int data_index = 2;
        for (int index = 0; index < 8; index++) // number_bytes; index++)
            data_bytes[data_index++] = byte_data[index];                   // Stuff data value into array

        return(data_bytes);
    }
}
