package com.ckt_works.AX_DSP;

// Created by Abeck on 12/16/2016. //

import android.util.Log;

import static com.ckt_works.AX_DSP.CROSSOVER_TYPES.CROSSOVER_BAND_PASS;
import static com.ckt_works.AX_DSP.CROSSOVER_TYPES.CROSSOVER_HIGH_PASS;

@SuppressWarnings("PointlessBitwiseExpression")
public class DspCrossover {

// TODO REMOVE - FOR DEBUGGING ONLY
//public int andyTest;
//void SetTest(int test_value) {andyTest = test_value;}


    // Filter Parameters
    private double Fs = 48000;
    private int id;                      // Channel number
    private CROSSOVER_TYPES type;        // Low Pass, Band Paqss or High Pass
    private int low_pass_frequency;      // Low Pass Frequency
    private int high_pass_frequency;     // High Pass Frequency
    private double gain = 0;
    private int slope = 24;              // Slope (roll off) of the crossover filter (dB/octave)
    private int filter_bits = 24;       // Number of bits used in the filter coefficient calculations - 24 for Analog Devices; 32 for AKM


    // Intermediate Values - computed from above parameters
    private double omega;           //	2 * PI * frequency / Fs
    private double sn;              //	Sin(omega)
    private double cs;              //	Cos(omega)
    private double alpha;           //	sn / (2 * (1 / (2 * Sin(orderangle))))
    private double a0;              //	1 + alpha)

    // LOW PASS FILTER - Computed  coefficients
    double LP_A1;
    double LP_A2;
    double LP_B0;
    double LP_B1;
    double LP_B2;
    // HIGH PASS FILTER - Computed  coefficients
    double HP_A1;
    double HP_A2;
    double HP_B0;
    double HP_B1;
    double HP_B2;

    /*
    private int LP_A1_24Bit;        // 24 bit filter values - Analog Devices DSP
    private int LP_A2_24Bit;
    private int LP_B0_24Bit;
    private int LP_B1_24Bit;
    private int LP_B2_24Bit;

    // HIGH PASS FILTER - Computed  coefficients
    private int HP_A1_24Bit;         // 24 bit filter values - Analog Devices DSP
    private int HP_A2_24Bit;
    private int HP_B0_24Bit;
    private int HP_B1_24Bit;
    private int HP_B2_24Bit;

    // Values used for 32-bit values (257440) Values
    double B0_d;
    double B1_d;
    double B2_d;
    double A1_d;
    double A2_d;
*/


    public void SetFilterDataFormat(int bits) {
        filter_bits = bits;
    }
    
    public DspCrossover(int channel_id, CROSSOVER_TYPES filter_type)
    {
        id = channel_id;
        type = filter_type;
        low_pass_frequency = 250;
        high_pass_frequency = 250;
    }

    public DspCrossover(int channel_id, CROSSOVER_TYPES filter_type, int frequency)
    {
        id = channel_id;
        type = filter_type;
        low_pass_frequency = frequency;
        high_pass_frequency = frequency;
    }

    public DspCrossover(int channel_id, CROSSOVER_TYPES filter_type, int low_frequency, int high_frequency)
    {
        id = channel_id;
        type = filter_type;
        low_pass_frequency = low_frequency;
        high_pass_frequency = high_frequency;
    }

    public void SetData(CROSSOVER_TYPES filter_type, int low_frequency, int high_frequency)
    {
        SetType(filter_type);
        SetLowFrequency(low_frequency);
        SetHighFrequency(high_frequency);
    }

    public void SetType(CROSSOVER_TYPES filter_type)
    {
        type = filter_type;
        if (type == CROSSOVER_BAND_PASS)
            CheckBandPassFrequencies();
    }

    public void SetSlope(int slope)
    {
        this.slope = slope;
    }

    public int GetSlope()
    {
        return(slope);
    }

    public int GetNumberFilters() {
        byte number_filters = 2;                        // Set number of filters used based on filter slope
        if (slope == 12)
            number_filters = 1;
        if (slope == 36)
            number_filters = 3;
        else if (slope == 48)
            number_filters = 4;

        return(number_filters);
    }



    private void CheckBandPassFrequencies() {
        if (low_pass_frequency >= high_pass_frequency-10) {
            if (high_pass_frequency > 10)
                low_pass_frequency = high_pass_frequency-10;
            else
                high_pass_frequency = low_pass_frequency + 10;
        }
    }



    public CROSSOVER_TYPES GetType()
    {
        return(type);
    }

    public void SetFrequency(int frequency)
    {
        if (type == CROSSOVER_TYPES.CROSSOVER_LOW_PASS)
            low_pass_frequency = frequency;
        else
            high_pass_frequency = frequency;
    }
    public void SetLowFrequency(int frequency)
    {
        low_pass_frequency = frequency;
    }

    public void SetHighFrequency(int frequency)
    {
        high_pass_frequency = frequency;
    }

    public int GetFrequency()
    {
        if (type == CROSSOVER_TYPES.CROSSOVER_LOW_PASS)
            return(low_pass_frequency);
        else
            return(high_pass_frequency);
    }

    public int GetLowFrequency()
    {
            return(low_pass_frequency);
    }

    public int GetHighFrequency()
    {
        return(high_pass_frequency);
    }


    public byte[] GetCrossoverData(int block) {
        byte data[];

        if (filter_bits == 32)
            data = Get32BitFilterData(block);                   // This is the data for the 440 (AKM DSP) - it will be rescaled by that firmware
        else
            data = Get24BitFilterData(block);
        return(data);
    }

    private void ComputeCoefficientsLowPass(int frequency)
    {
        DspMath.SetDataFormat(filter_bits);

         //Compute Intermediate values
        omega =	2 * Math.PI * frequency / Fs;
        sn = Math.sin(omega);
        cs = Math.cos (omega);
        alpha = sn / (2 * (1 / Math.pow(2, 0.5)));
        a0 = 1 + alpha ;

        // Compute Coefficients
        LP_A1 = ((2 * cs) / a0);
        LP_A2 = -(1 - alpha) / a0;
        LP_B1 = (1 - cs) / a0 *  Math.pow(10, (gain / 20));
        LP_B0 = LP_B1 / 2;
        LP_B2 = LP_B0;
    }

    private void ComputeCoefficientsHighPass(int frequency)
    {
        DspMath.SetDataFormat(filter_bits);
        //Compute Intermediate values
        omega =	2 * Math.PI * frequency / Fs;
        sn = Math.sin(omega);
        cs = Math.cos (omega);
        alpha = sn / (2 * (1 / Math.pow(2, 0.5)));
        a0 = 1 + alpha ;

        // Compute Coefficients
        HP_A1 = ((2 * cs) / a0);
        HP_A2 = -(1 - alpha) / a0;
        HP_B1 = -( 1 + cs) / a0 * Math.pow(10,(gain/ 20));
        HP_B0 = -HP_B1 / 2;
        HP_B2 = HP_B0;
    }


    byte[] Get32BitFilterData(int block)
    {
        byte[] data = null;
        Log.i("Crossover Block", Integer.toString(block));
        switch (block) {
            case 0:

                if (type == CROSSOVER_BAND_PASS) {
                    CheckBandPassFrequencies();
                    ComputeCoefficientsLowPass(high_pass_frequency);
                    ComputeCoefficientsHighPass(low_pass_frequency);
                } else {
                    ComputeCoefficientsLowPass(low_pass_frequency);
                    ComputeCoefficientsHighPass(high_pass_frequency);
                }

                DspMath.fixed32 LP_B2_32Bit = new DspMath.fixed32(LP_B2);
                DspMath.fixed32 LP_B1_32Bit = new DspMath.fixed32(LP_B1);
                DspMath.fixed32 LP_A2_32Bit = new DspMath.fixed32(LP_A2);
/*                LP_B2_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                LP_B1_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                LP_A2_32Bit.lsw >>= 1;                               // Funky adjust on LSB
*/
                byte number_filters = 1;                        // Set number of filters used based on filter slope
                if (slope == 24)
                    number_filters = 2;
                if (slope == 36)
                    number_filters = 3;
                else if (slope == 48)
                    number_filters = 4;

                data = new byte[20];
                data[0] = (byte) id;                           // Stuff ID value into array
                data[1] = (byte) type.ordinal();                // Stuff Crossover Type into array
                data[2] = (byte) low_pass_frequency;            // Stuff Low frequency into array
                data[3] = (byte) (low_pass_frequency >> 8);
                data[4] = (byte) high_pass_frequency;           // Stuff High frequency into array
                data[5] = (byte) (high_pass_frequency >> 8);

                data[6] = (byte) number_filters;                 // Number of Filters determined by slope (dB/octave)
                data[7] = 0;                                       // Padding to 8-byte boundary for 420 side

                // Put data in the array in the same order it's sent to The DSP
                 data[8] = (byte) (LP_B2_32Bit.lsw >> 0);            // Stuff B2 value into array
                data[9] = (byte) (LP_B2_32Bit.lsw >> 8);
                data[10] = (byte) (LP_B2_32Bit.msw >> 0);
                data[11] = (byte) (LP_B2_32Bit.msw >> 8);

                data[12] = (byte) (LP_B1_32Bit.lsw >> 0);            // Stuff B1 value into array
                data[13] = (byte) (LP_B1_32Bit.lsw >> 8);
                data[14] = (byte) (LP_B1_32Bit.msw >> 0);
                data[15] = (byte) (LP_B1_32Bit.msw >> 8);

                data[16] = (byte) (LP_A2_32Bit.lsw >> 0);            // Stuff A2 value into array
                data[17] = (byte) (LP_A2_32Bit.lsw >> 8);
                data[18] = (byte) (LP_A2_32Bit.msw >> 0);
                data[19] = (byte) (LP_A2_32Bit.msw >> 8);
                break;

            case 1:
                DspMath.fixed32 LP_A1_32Bit = new DspMath.fixed32(LP_A1);
                DspMath.fixed32 LP_B0_32Bit = new DspMath.fixed32(LP_B0);
                DspMath.fixed32 HP_B2_32Bit = new DspMath.fixed32(HP_B2);
                DspMath.fixed32 HP_B1_32Bit = new DspMath.fixed32(HP_B1);
                DspMath.fixed32 HP_A2_32Bit = new DspMath.fixed32(HP_A2);
/*                LP_A1_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                LP_B0_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                HP_B2_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                HP_B1_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                HP_A2_32Bit.lsw >>= 1;                               // Funky adjust on LSB
*/
                data = new byte[20];
                                       // Funky adjust on LSB
                data[0] = (byte) (LP_A1_32Bit.lsw >> 0);            // Stuff A1 value into array
                data[1] = (byte) (LP_A1_32Bit.lsw >> 8);
                data[2] = (byte) (LP_A1_32Bit.msw >> 0);
                data[3] = (byte) (LP_A1_32Bit.msw >> 8);

                                          // Funky adjust on LSB
                data[4] = (byte) (LP_B0_32Bit.lsw >> 0);            // Stuff B1 value into array
                data[5] = (byte) (LP_B0_32Bit.lsw >> 8);
                data[6] = (byte) (LP_B0_32Bit.msw >> 0);
                data[7] = (byte) (LP_B0_32Bit.msw >> 8);

                data[8] = (byte) (HP_B2_32Bit.lsw >> 0);            // Stuff B2 value into array
                data[9] = (byte) (HP_B2_32Bit.lsw >> 8);
                data[10] = (byte) (HP_B2_32Bit.msw >> 0);
                data[11] = (byte) (HP_B2_32Bit.msw >> 8);

                data[12] = (byte) (HP_B1_32Bit.lsw >> 0);            // Stuff B1 value into array
                data[13] = (byte) (HP_B1_32Bit.lsw >> 8);
                data[14] = (byte) (HP_B1_32Bit.msw >> 0);
                data[15] = (byte) (HP_B1_32Bit.msw >> 8);

                data[16] = (byte) (HP_A2_32Bit.lsw >> 0);            // Stuff A2 value into array
                data[17] = (byte) (HP_A2_32Bit.lsw >> 8);
                data[18] = (byte) (HP_A2_32Bit.msw >> 0);
                data[19] = (byte) (HP_A2_32Bit.msw >> 8);
                break;

            case 2:
                DspMath.fixed32 HP_A1_32Bit = new DspMath.fixed32(HP_A1);
                DspMath.fixed32 HP_B0_32Bit = new DspMath.fixed32(HP_B0);
/*                HP_A1_32Bit.lsw >>= 1;                               // Funky adjust on LSB
                HP_B0_32Bit.lsw >>= 1;                               // Funky adjust on LSB
*/
                data = new byte[8];
                 data[0] = (byte) (HP_A1_32Bit.lsw >> 0);            // Stuff A1 value into array
                data[1] = (byte) (HP_A1_32Bit.lsw >> 8);
                data[2] = (byte) (HP_A1_32Bit.msw >> 0);
                data[3] = (byte) (HP_A1_32Bit.msw >> 8);

                data[4] = (byte) (HP_B0_32Bit.lsw >> 0);            // Stuff B0 value into array
                data[5] = (byte) (HP_B0_32Bit.lsw >> 8);
                data[6] = (byte) (HP_B0_32Bit.msw >> 0);
                data[7] = (byte) (HP_B0_32Bit.msw >> 8);
                break;
        }
        return(data);
    }


    byte[] Get24BitFilterData(int block)
    {
        byte[] data = null;
        //Log.i("Crossover Block", Integer.toString(block))   ;
        switch (block) {
            case 0:
                if (type == CROSSOVER_BAND_PASS) {
                    CheckBandPassFrequencies();
                    ComputeCoefficientsLowPass(high_pass_frequency);
                    ComputeCoefficientsHighPass(low_pass_frequency);
                } else {
                    ComputeCoefficientsLowPass(low_pass_frequency);
                    ComputeCoefficientsHighPass(high_pass_frequency);
                }

                byte number_filters = 1;                        // Set number of filters used based on filter slope
                if (slope == 24)
                    number_filters = 2;
                if (slope == 36)
                    number_filters = 3;
                else if (slope == 48)
                    number_filters = 4;

                int LP_A1_24Bit = DspMath.toFix(LP_A1);         // Convert the coefficients we send in this block to 24-bit values
                int LP_A2_24Bit = DspMath.toFix(LP_A2);
                int LP_B0_24Bit = DspMath.toFix(LP_B0);

                data = new byte[20];
                data[0] = (byte) id;                           // Stuff ID value into array
                data[1] = (byte) type.ordinal();               // Stuff Crossover Type into array
                data[2] = (byte) low_pass_frequency;           // Stuff Low frequency into array
                data[3] = (byte) (low_pass_frequency >> 8);
                data[4] = (byte) high_pass_frequency;          // Stuff High frequency into array
                data[5] = (byte) (high_pass_frequency >> 8);

                data[6] = (byte) number_filters;                 // Number of Filters determined by slope (dB/octave)
                data[7] = 0;

                data[8] = (byte) (LP_A1_24Bit >> 0);           // Stuff Low Pass A1 coefficient value into array
                data[9] = (byte) (LP_A1_24Bit >> 8);
                data[10] = (byte) (LP_A1_24Bit >> 16);
                data[11] = (byte) (LP_A1_24Bit >> 24);

                data[12] = (byte) (LP_A2_24Bit >> 0);         // Stuff Low Pass A2 coefficient value into array
                data[13] = (byte) (LP_A2_24Bit >> 8);
                data[14] = (byte) (LP_A2_24Bit >> 16);
                data[15] = (byte) (LP_A2_24Bit >> 24);

                data[16] = (byte) (LP_B0_24Bit >> 0);        // Stuff Low Pass B0 coefficient value into array
                data[17] = (byte) (LP_B0_24Bit >> 8);
                data[18] = (byte) (LP_B0_24Bit >> 16);
                data[19] = (byte) (LP_B0_24Bit >> 24);
                break;
                
            case 1:
                int LP_B1_24Bit = DspMath.toFix(LP_B1);     // Convert the coefficients we send in this block to 24-bit values
                int LP_B2_24Bit = DspMath.toFix(LP_B2);
                int HP_A1_24Bit = DspMath.toFix(HP_A1);
                int HP_A2_24Bit = DspMath.toFix(HP_A2);
                int HP_B0_24Bit = DspMath.toFix(HP_B0);

                data = new byte[20];
                data[0] = (byte) (LP_B1_24Bit >> 0);       // Stuff Low Pass B1 coefficient value into array
                data[1] = (byte) (LP_B1_24Bit >> 8);
                data[2] = (byte) (LP_B1_24Bit >> 16);
                data[3] = (byte) (LP_B1_24Bit >> 24);

                data[4] = (byte) (LP_B2_24Bit >> 0);       // Stuff Low Pass B2 coefficient value into array
                data[5] = (byte) (LP_B2_24Bit >> 8);
                data[6] = (byte) (LP_B2_24Bit >> 16);
                data[7] = (byte) (LP_B2_24Bit >> 24);

                data[8] = (byte) (HP_A1_24Bit >> 0);       // Stuff High Pass A1 coefficient value into array
                data[9] = (byte) (HP_A1_24Bit >> 8);
                data[10] = (byte) (HP_A1_24Bit >> 16);
                data[11] = (byte) (HP_A1_24Bit >> 24);

                data[12] = (byte) (HP_A2_24Bit >> 0);      // Stuff High Pass A2 coefficient value into array
                data[13] = (byte) (HP_A2_24Bit >> 8);
                data[14] = (byte) (HP_A2_24Bit >> 16);
                data[15] = (byte) (HP_A2_24Bit >> 24);

                data[16] = (byte) (HP_B0_24Bit >> 0);     // Stuff High Pass B0 coefficient value into array
                data[17] = (byte) (HP_B0_24Bit >> 8);
                data[18] = (byte) (HP_B0_24Bit >> 16);
                data[19] = (byte) (HP_B0_24Bit >> 24);
                break;

            case 2:
                int HP_B1_24Bit = DspMath.toFix(HP_B1);     // Convert the coefficients we send in this block to 24-bit values
                int HP_B2_24Bit = DspMath.toFix(HP_B2);

                data = new byte[8];
                data[0] = (byte) (HP_B1_24Bit >> 0);       // Stuff High Pass B1 coefficient value into array
                data[1] = (byte) (HP_B1_24Bit >> 8);
                data[2] = (byte) (HP_B1_24Bit >> 16);
                data[3] = (byte) (HP_B1_24Bit >> 24);

                data[4] = (byte) (HP_B2_24Bit >> 0);       // Stuff High Pass B2 coefficient value into array
                data[5] = (byte) (HP_B2_24Bit >> 8);
                data[6] = (byte) (HP_B2_24Bit >> 16);
                data[7] = (byte) (HP_B2_24Bit >> 24);
                break;
        }
        return(data);
    }

}
