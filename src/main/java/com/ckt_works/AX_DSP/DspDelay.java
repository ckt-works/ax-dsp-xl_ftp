package com.ckt_works.AX_DSP;


// Created by Abeck on 12/20/2016. //

public class DspDelay {
    private static final int MAX_DELAY_COUNTS = 384;              // Maximum delay in 'counts'
    private static final int DELAY_COUNTS_PER_FOOT  = 43;
    static final int MAX_DELAY_INCHES =  (MAX_DELAY_COUNTS * 12) / DELAY_COUNTS_PER_FOOT;

    private int left_front_distance;
    private int right_front_distance;
    private int left_rear_distance;
    private int right_rear_distance;
    private int sub_woofer_distance;

    // Constructors
    public DspDelay(){}
    
    public DspDelay(int left_front, int right_front, int left_rear, int right_rear, int sub_woofer)
    {
        left_front_distance = Math.max(0, left_front);              // Don't allow negative values
        right_front_distance = Math.max(0, right_front);
        left_rear_distance = Math.max(0, left_rear);
        right_rear_distance = Math.max(0, right_rear);
        sub_woofer_distance = Math.max(0, sub_woofer);
    }
    
    public void SetLeftFrontDistance(int distance){left_front_distance=Math.max(0, distance);}  // Don't allow negative values
    public void SetRightFrontDistance(int distance){right_front_distance=Math.max(0, distance);}
    public void SetLeftRearDistance(int distance){left_rear_distance=Math.max(0, distance);}
    public void SetRightRearDistance(int distance){right_rear_distance=Math.max(0, distance);}
        
    

    public byte[] GetDelayData()
    {
        byte[] data = new byte[20];

        // Compute number of counts for each speaker delay, and keep in bounds
        int left_front_counts = left_front_distance * DELAY_COUNTS_PER_FOOT / 12;
        int right_front_counts = right_front_distance * DELAY_COUNTS_PER_FOOT / 12;
        int left_rear_counts = left_rear_distance * DELAY_COUNTS_PER_FOOT / 12;
        int right_rear_counts = right_rear_distance * DELAY_COUNTS_PER_FOOT / 12;
        int sub_woofer_counts = sub_woofer_distance * DELAY_COUNTS_PER_FOOT / 12;

        int distance_data[][] = new int[5][2] ;
        distance_data[0][0] = 0;
        distance_data[0][1] = left_front_counts;
        distance_data[1][0] = 1;
        distance_data[1][1] = right_front_counts;
        distance_data[2][0] = 2;
        distance_data[2][1] = left_rear_counts;
        distance_data[3][0] = 3;
        distance_data[3][1] = right_rear_counts;
        distance_data[4][0] = 4;
        distance_data[4][1] = sub_woofer_counts;

        // Sort delays by distance
        SortDelays(distance_data, 1);

        // Make the furthest speaker Zero delay and base the others off of it
        int max_distance = distance_data[4][1];
        distance_data[4][1] = 0;
        distance_data[3][1] = max_distance - distance_data[3][1];
        distance_data[2][1] = max_distance - distance_data[2][1];
        distance_data[1][1] = max_distance - distance_data[1][1];
        distance_data[0][1] = max_distance - distance_data[0][1];

        // Sort the delay by channel
        SortDelays(distance_data, 0);

        // Copy delay counts into array
        distance_data[0][1] = Math.min(distance_data[0][1], MAX_DELAY_COUNTS);
        data[0] = (byte) distance_data[0][1];
        data[1] = (byte) (distance_data[0][1] >> 8);
        distance_data[1][1] = Math.min(distance_data[1][1], MAX_DELAY_COUNTS);
        data[2] = (byte) distance_data[1][1];
        data[3] = (byte) (distance_data[1][1] >> 8);
        distance_data[2][1] = Math.min(distance_data[2][1], MAX_DELAY_COUNTS);
        data[4] = (byte) distance_data[2][1];
        data[5] = (byte) (distance_data[2][1] >> 8);
        distance_data[3][1] = Math.min(distance_data[3][1], MAX_DELAY_COUNTS);
        data[6] = (byte) distance_data[3][1];
        data[7] = (byte) (distance_data[3][1] >> 8);
        distance_data[3][1] = Math.min(distance_data[4][1], MAX_DELAY_COUNTS);
        data[8] = (byte) distance_data[4][1];
        data[9] = (byte) (distance_data[4][1] >> 8);

        // Copy distances into array
        data[10] = (byte) left_front_distance;
        data[11] = (byte) (left_front_distance >> 8);
        data[12] = (byte) right_front_distance;
        data[13] = (byte) (right_front_distance >> 8);
        data[14] = (byte) left_rear_distance;
        data[15] = (byte) (left_rear_distance >> 8);
        data[16] = (byte) right_rear_distance;
        data[17] = (byte) (right_rear_distance >> 8);
        data[18] = (byte) sub_woofer_distance;
        data[19] = (byte) (sub_woofer_distance >> 8);

        return(data);
    }


    private void SortDelays(int[][] data, int sort_index) {
        int n = data.length;
        int[] temp = new int[2];   //holding variable

        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (data[j - 1][sort_index] > data[j][sort_index]) {  //swap elements
                    temp = data[j - 1];
                    data[j - 1] = data[j];
                    data[j] = temp;
                }
            }
        }
    }

}
