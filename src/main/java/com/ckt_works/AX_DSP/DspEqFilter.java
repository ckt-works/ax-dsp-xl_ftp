package com.ckt_works.AX_DSP;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Locale;

// Created by Abeck on 11/7/2016. //

@SuppressWarnings("PointlessBitwiseExpression")
public class DspEqFilter {

    // Identifies Which EQ Filter this is
    private int channel;
    private int band;
    static int test_number = 0;

    // Filter Parameters
    private double Fs = 48000;
    private double F0 = 20;
    private double gain;
    private double Q = 4.31f;
    private double boost;

    // Intermediate Values - computed from above parameters
    private double Ax;
    private double omega;
    private double sn;
    private double cs;
    private double alpha;
    private double a0;
    private double gainlinear;

    // Computed coefficients
    private int A1;
    private int A2;
    private int B0;
    private int B1;
    private int B2;
    
    // Values used for 32-bit values (257440) Values
    double B0_d;
    double B1_d;
    double B2_d;
    double A1_d;
    double A2_d;

    private int filter_bits = 24;

    public void SetFilterDataFormat(int bits) {
        filter_bits = bits;
        if (filter_bits == 32)
            Q = 2.145; //1.414;
        else
            Q = 4.31f;
    }

    int GetSequenceNumber(){return(test_number);}

    public DspEqFilter()
    {

    }

    public DspEqFilter(int channel, int band, int frequency)
    {
        this.channel = channel;
        this.band = band;

        this.F0 = (float)frequency;

// FOR TESTING ONLY
/*        float val1 = 1.9661477804184f;
        this.A1 = Float.floatToRawIntBits(val1);
        //this.A1 = 0x01F75576;       // 1.9661477804184
        this.A2 = 0xFF0452AB;       // -0.983113586902618
        this.B0 = 0x0104AC77;       // 1.01825660467148
        this.B1 = 0xFE08AA8A;       // -1.9661477804184
        this.B2 = 0x00F700DE;       // 0.96485698223114
*/    }

    public void SetChannel(int channel){this.channel = channel;}
    public void SetBand(int band){this.band = band;}
    public void SetFrequency(double frequency){this.F0 = frequency;}
    public void SetBoost(double boost){this.boost = boost;}

    public void SetBoost( float boost)
    {
        // TODO ANDY REMOVE - FOR TESTING
//        this.boost = 3.0;
        this.boost = boost;
    }
    public float GetBoost()
    {
        return((float)this.boost);
    }

    public void SetData(String band, String frequency, String boost)
    {
        this.band = Integer.valueOf(band);
//        this.F0 = Double.valueOf(frequency);   // Don't change the frequency
        this.boost = Double.valueOf(boost);
    }

    public void SetGain( int gain)
    {
        this.gain = gain;
    }


    public void SetFilterCoeffs(int A1, int A2, int B0, int B1,int B2)
    {
        this.A1 = A1;
        this.A2 = A2;
        this.B0 = B0;
        this.B1 = B1;
        this.B2 = B2;
    }

    public byte[] GetChannelData()
    {
//Log.i("EQ Channel Data", Integer.toString(this.channel))   ;
        ByteBuffer data = ByteBuffer.allocate(8);
        data.put((byte)(this.channel >> 0));            // Stuff channel number into array
        data.put((byte)(this.channel >> 8));

        data.put((byte)(this.band >> 0));               // Stuff band number into array
        data.put((byte)(this.band >> 8));

        byte boost_value = (byte)(this.boost * 10);
        data.put(boost_value);
        return(data.array());
    }

    public byte[] GetChannelData(int SlaveMask)
    {
//        Log.i("EQ Channel Data", Integer.toString(this.channel));
        byte[] data = new byte[10];

        data[0] = ((byte)(this.channel >> 0));            // Stuff channel number into array
        data[1] = ((byte)(this.channel >> 8));
        data[2] = ((byte)(this.band >> 0));               // Stuff band number into array
        data[3] = ((byte)(this.band >> 8));
        byte boost_value = (byte)(this.boost * 10);
        data[4] = (boost_value);
        data[5] = (byte) 0x00;                            // Padding
        data[6] = ((byte)SlaveMask);
        data[7] = ((byte)(SlaveMask >> 8));
String channel_data = "CH: " + this.channel + "  Band: " +  this.band + "  Boost: " + this.boost;
Log.i("Channel Data", channel_data);
        return(data);
    }

    public String GetFrequencyString()
    {
        return(String.format(Locale.US, "%d", (int)this.F0));
    }

    public byte[] GetFilterData()
    {
        ComputeCoefficients();
        byte[] data = new byte[20];
        
        if (filter_bits == 32)                          // This is the data for the 440 (AKM DSP) - it will be rescaled by that firmware
        {
/*B0_d = 1.01542181F;
B1_d = -1.908763744F;
B2_d = -0.909812581F;
A1_d = -1.908763744F;
A2_d = -0.925234391098F;
*/
            DspMath.fixed32 A1 = new DspMath.fixed32(A1_d);
            DspMath.fixed32 A2 = new DspMath.fixed32(A2_d);
            DspMath.fixed32 B0 = new DspMath.fixed32(B0_d);
            DspMath.fixed32 B1 = new DspMath.fixed32(B1_d);
            DspMath.fixed32 B2 = new DspMath.fixed32(B2_d);
            // Put data in the array in the same order it's sent to he DSP
//            B2.lsw >>= 1;                               // Funky adjust on LSB
            data[0] = (byte) (B2.lsw >> 0);            // Stuff B2 value into array
            data[1] = (byte) (B2.lsw >> 8);
            data[2] = (byte) (B2.msw >> 0);
            data[3] = (byte) (B2.msw >> 8);

//            B1.lsw >>= 1;                               // Funky adjust on LSB
            data[4] = (byte) (B1.lsw >> 0);            // Stuff B1 value into array
            data[5] = (byte) (B1.lsw >> 8);
            data[6] = (byte) (B1.msw >> 0);
            data[7] = (byte) (B1.msw >> 8);

//            A2.lsw >>= 1;                               // Funky adjust on LSB
            data[8] = (byte) (A2.lsw >> 0);            // Stuff A2 value into array
            data[9] = (byte) (A2.lsw >> 8);
            data[10] = (byte) (A2.msw >> 0);
            data[11] = (byte) (A2.msw >> 8);

//            A1.lsw >>= 1;                               // Funky adjust on LSB
            data[12] = (byte) (A1.lsw >> 0);            // Stuff A1 value into array
            data[13] = (byte) (A1.lsw >> 8);
            data[14] = (byte) (A1.msw >> 0);
            data[15] = (byte) (A1.msw >> 8);

//            B0.lsw >>= 1;                               // Funky adjust on LSB
            data[16] = (byte) (B0.lsw >> 0);            // Stuff B0 value into array
            data[17] = (byte) (B0.lsw >> 8);
            data[18] = (byte) (B0.msw >> 0);
            data[19] = (byte) (B0.msw >> 8);

/*            String params = "";
for (int index = 0; index < 20; index ++){
    params += byteToHexString(data[index]);
    params += ", ";
}
Log.i("Filter Data", params);
*/
        }
        // 24 Bit Filter Data (Analog Devices DSP)
        else {
            data[0] = (byte) (this.A1 >> 0);            // Stuff A1 value into array
            data[1] = (byte) (this.A1 >> 8);
            data[2] = (byte) (this.A1 >> 16);
            data[3] = (byte) (this.A1 >> 24);

            data[4] = (byte) (this.A2 >> 0);            // Stuff A2 value into array
            data[5] = (byte) (this.A2 >> 8);
            data[6] = (byte) (this.A2 >> 16);
            data[7] = (byte) (this.A2 >> 24);

            data[8] = (byte) (this.B0 >> 0);            // Stuff B0 value into array
            data[9] = (byte) (this.B0 >> 8);
            data[10] = (byte) (this.B0 >> 16);
            data[11] = (byte) (this.B0 >> 24);

            data[12] = (byte) (this.B1 >> 0);            // Stuff B1 value into array
            data[13] = (byte) (this.B1 >> 8);
            data[14] = (byte) (this.B1 >> 16);
            data[15] = (byte) (this.B1 >> 24);

            data[16] = (byte) (this.B2 >> 0);            // Stuff B2 value into array
            data[17] = (byte) (this.B2 >> 8);
            data[18] = (byte) (this.B2 >> 16);
            data[19] = (byte) (this.B2 >> 24);
        }
        return(data);
    }



    private void ComputeCoefficients()
    {
        // Compute Intermediate values
        this.Ax = Math.pow(10, boost / 40f);
        this.omega = 2 * Math.PI * F0 / Fs;
        this.sn = Math.sin(this.omega);
        this.cs = Math.cos(this.omega);
        this.alpha = sn / (2 * Q);
        this.a0 = 1 + (alpha / Ax);
        this.gainlinear = Math.pow(10, (gain/20)) / a0;

        // Compute Coefficients - Values used for 32-bit values (257440) Values
         B0_d = (1f + (alpha * Ax)) * gainlinear;
         B1_d = (-(2 * cs) * gainlinear);
         B2_d = ((1 - (alpha * Ax)) * gainlinear);
         A1_d = ((2 * cs) / a0);
         A2_d = (-(1 - (alpha / Ax)) / a0);

        DspMath.SetDataFormat(filter_bits);
  //      this.B0 = DspMath.toFix(0.909824371);

        this.B0 = DspMath.toFix((1f + (alpha * Ax)) * gainlinear);
        this.B1 = DspMath.toFix(-(2 * cs) * gainlinear);
        this.B2 = DspMath.toFix((1 - (alpha * Ax)) * gainlinear);
        this.A1 = DspMath.toFix((2 * cs) / a0);
        this.A2 = DspMath.toFix(-(1 - (alpha / Ax)) / a0);
    }

    public String byteToHexString(byte value) {
        String hexString = "";
        long longValue = (long)value & 0xFF;
        hexString = Long.toHexString(longValue);
        if (hexString.length() < 2)
            hexString = "0" + hexString;
        return ("0x" + hexString);
    }

}



