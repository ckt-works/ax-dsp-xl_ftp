package com.ckt_works.AX_DSP;

// Created by Abeck on 11/9/2016. //

import android.util.Log;

@SuppressWarnings("PointlessBitwiseExpression")
public class DspGain
{
    private boolean channel_based = false;
    private GAIN_CHANNELS id;
    private int channel;
    private float gain;
    private float gain_offset;

    public DspGain(GAIN_CHANNELS id)
    {
        this.id = id;
        this.gain = 0.5f;
        gain_offset = 10;
    }

    public DspGain(int channel)
    {
        this.channel = channel;
        this.channel_based = true;
        this.gain = 0.5f;
        gain_offset = 10;
    }


    public DspGain(GAIN_CHANNELS id, float gain)
    {
        this.id = id;
        this.gain = gain;
        gain_offset = 10;
    }

    public float GetGainOffset(){return(gain_offset);}

    public void SetGain(int gain)
    {
// TODO ANDY CHECK IF THIS AFFECTS 440
//  this.gain = DspMath.floatVal(gain) - gain_offset;
        this.gain = gain - gain_offset;

        Log.i("GAIN (INT)", Float.toString(this.gain));
    }


    public void SetGain(float gain)
    {
        Log.i("GAIN (FLOAT)", Float.toString(gain));
        this.gain = gain;
    }

    public void SetGain(String gain){this.gain = Float.valueOf(gain);}

    public float GetGain()
    {
        return(this.gain);
    }

    public byte[] GetGainData()
    {
        int gain = DspMath.toFix(this.gain + gain_offset, 24);

        byte[] data = new byte[8];

        if (channel_based)
            data[0] = (byte) this.channel;         // Stuff Channel number into array
        else
            data[0] = (byte) this.id.ordinal();         // Stuff ID value into array

        data[1] = (byte)0;
        data[2] = (byte)0;
        data[3] = (byte)0;

        data[4] = (byte)(gain >> 0);            // Stuff Gain value into array
        data[5] = (byte)(gain >> 8);
        data[6] = (byte)(gain >> 16);
        data[7] = (byte)(gain >> 24);

        return(data);
    }
}

