package com.ckt_works.AX_DSP;

import android.util.Log;

public class DspGroup {

    static String strGroups[] = {"None", "Front", "Left Front", "Right Front", "Rear", "Left Rear", "Right Rear", "All"};
    static final int maxNumberGroups = strGroups.length;

    /*static String GetGetName(int id) {
        String name = "?";
        if (id >=0 && id <= maxNumberGroups)
            name =  strGroups[id];
        return(name);
    }
    */


    int groupID = 0;                            // Index of this group; its ID - related to DspInput.strGroups
    private int master_channel = -1;             // Channel the master is assigned to (0 - 9); -1 = not assigned
    private int slaveMask = 0;                  // Bit mask indicating which channels are slaves

    DspGroup(int ID) {
        groupID = ID;
        master_channel = -1;
        slaveMask = 0;
    }

    DspGroup(int ID, int master) {
        groupID = ID;
        master_channel = master;
        slaveMask = 0;
    }

    public boolean AddChannel(int channel) {
        boolean isMaster = false;
        if (groupID > 0) {                                  // Zero group is NONE - don't add slaves to this group
            if (channel >= 0 && channel < 10) {
                if (master_channel == -1)
                    master_channel = channel;
                else if (channel != master_channel)         // If the passed channel is not the master...
                    slaveMask |= (1 << (channel));          // Set the slave mask bit corresponding to the passed channel
        }
        }
        String msg = "Group ID:" + Integer.toString(groupID);
        if (channel == master_channel)
            msg+= " Is Master";
        else
            msg+= " Is Slave";

        Log.i("Channel: " + Integer.toString(channel), " Slave Mask: " + Integer.toString(slaveMask));
        return(channel == master_channel);
    }

    // Sets the channel to be the master if none already set
/*    public boolean SetMaster(int channel) {
        boolean success = false;
        if (master_channel == -1) {
            master_channel = channel;
            success = true;
        }
        return(success);
    }
*/
    // Forces the channel to be the master - leave all slaves intact
    public boolean ForceMaster(int channel) {
        boolean success = true;
        master_channel = channel;
        Log.i(">>>ForceMaster", "Group: " + Integer.toString(this.groupID) + "  Channel: " + Integer.toString(channel));
        return(success);
    }

    // Forces the channel to be a slave whether or not a master is set
    public void ForceSlave(int channel){
        slaveMask |= (1 << (channel));          // Set the slave mask bit corresponding to the passed channel
    }


 /*   // Forces the channel to be the master - removes all slaves
    public boolean ChangeMaster(int channel) {
        boolean success = true;
        master_channel = channel;
        slaveMask = 0;                      // Remove all slave from this group
        return(success);
    }
*/

    // Removes the master and removes all slaves - calls the callback function to do any housekeeping
    public boolean Reset(IGroupInterface callback) {
        boolean success = true;
        master_channel = -1;
        slaveMask &= 0b1111111111;                  // Mask to 10 channel bits
        int slaveChannel = 0;
        while (slaveMask != 0) {
            if ((slaveMask & 0x01) != 0)
                callback.SetGroup(0, slaveChannel);
            slaveMask >>= 1;
            slaveChannel++;
        }
        //slaveMask = 0;                      // Remove all slave from this group
        return(success);
    }

    // Removes the master and removes all slaves
    public boolean Reset() {
        boolean success = true;
        master_channel = -1;
        slaveMask = 0;                      // Remove all slave from this group
        return(success);
    }


    // Returns true if the passed channel is the master
    public boolean ChannelIsMaster(int channel) {
        boolean isMaster  = false;
        if (groupID > 0 && master_channel == channel)       // Group Zero is NONE group - can't have Master or slaves
            isMaster = true;
        return(isMaster);
    }


    public boolean RemoveChannel(int channel, IGroupInterface callback) {
        boolean success = false;
        if (groupID > 0) {                                                  // Group Zero is NONE group - can't have Master or slaves
            if (master_channel >= 0 && channel == master_channel) {
                Reset(callback);                                            // Clears the master and all slaves
            } else {
                int mask = 1 << (channel);
                slaveMask &= (~mask);                                       // Reset bit corresponding to the passed channel
            }
            success = true;
        }
        return(success);
    }
    public boolean RemoveSlave(int channel) {
        boolean success = false;
        if (master_channel >= 0 && channel != master_channel) {
            int mask = 1 << (channel);
            slaveMask &= (~mask);               // Reset bit corresponding to the passed channel
            success = true;
        }
        return(success);
    }

    public boolean ChannelIsSlave(int channel){
        boolean isSlave = false;
        if (groupID > 0) {
            int channelMask = (1 << channel);    // Set bit corresponding to the passed channel
            if ((slaveMask & channelMask) > 0)
                isSlave = true;
        }
        return(isSlave);
    }


    public boolean SlaveIsInGroup(int channel) {
        boolean inGroup = false;
        if (groupID > 0 && master_channel >= 0 && channel != master_channel) { // Group Zero is NONE group - can't have Master or slaves
//            int channelMask = (1 << (channel-1));    // Set bit corresponding to the passed channel
            int channelMask = (1 << channel);    // Set bit corresponding to the passed channel
            int group_mask = slaveMask & channelMask;
            if (group_mask > 0) {
                inGroup = true;
            }
        }
        return(inGroup);
    }

    int GetMasterChannel(){
        return(master_channel);
    }

    public int GetSlaveMask(){
        return(slaveMask);

    }


}

