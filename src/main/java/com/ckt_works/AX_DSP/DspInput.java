package com.ckt_works.AX_DSP;

import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.CENTER;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.LEFT_FRONT;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.LEFT_REAR;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.NOT_USED;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.RIGHT_FRONT;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.RIGHT_REAR;
import static com.ckt_works.AX_DSP.DspInput.SPEAKER_LOCATIONS.SUB_WOOFER;

public class DspInput {


    static String strGroups[] = {"None", "Front", "Left Front", "Right Front", "Rear", "Left Rear", "Right Rear", "All"};

    enum SPEAKER_POSITIONS {
        NOT_USED(0), LEFT_FRONT(1), RIGHT_FRONT(2), LEFT_REAR(3), RIGHT_REAR(4), SUB_WOOFER(5), CENTER(6),  LEFT_FROMT_TWEETER(7),
        LEFT_FRONT_MID(8), LEFT_FRONT_BASS(9), RIGHT_FRONT_TWEETER(10), RIGHT_FRONT_MID(11), RIGHT_FRONT_BASS(12),
        LEFT_REAR_TWEETER(13), LEFT_REAR_MID(14), LEFT_REAR_BASS(15), RIGHT_REAR_TWEETER(16), RIGHT_REAR_MID(17), RIGHT_REAR_BASS(18), MAX_VALUE(19);

        public final int value;
        SPEAKER_POSITIONS(int value) {
            this.value= value;}
            public int GetValue() { return(this.value);}
        }

    enum SPEAKER_LOCATIONS {NOT_USED(0), LEFT_FRONT(1), RIGHT_FRONT(2), LEFT_REAR(3), RIGHT_REAR(4), SUB_WOOFER(5), CENTER(6);
        public final int value;
        SPEAKER_LOCATIONS(int value) {
            this.value= value;
        }
    }

    // NOTE: THE ELEMENTS OF THE FOLLOWING 3 ARRAYS MUST CORRESPOND ONE-TO-ONE TO EACH OTHER
    //private
    private static SPEAKER_LOCATIONS speaker_locationIds[] = {NOT_USED, LEFT_FRONT, RIGHT_FRONT, LEFT_REAR, RIGHT_REAR, SUB_WOOFER, CENTER,
            LEFT_FRONT, LEFT_FRONT, LEFT_FRONT,
            RIGHT_FRONT, RIGHT_FRONT, RIGHT_FRONT,
            LEFT_REAR, LEFT_REAR, LEFT_REAR,
            RIGHT_REAR, RIGHT_REAR, RIGHT_REAR
    };

    static  String strNames[] = {"Not Used", "Left Front", "Right Front", "Left Rear", "Right Rear", "Sub Woofer", "Center",
            "Left Front Tweeter", "Left Front Mid", "Left Front Bass",
            "Right Front Tweeter", "Right Front Mid", "Right Front Bass",
            "Left Rear Tweeter", "Left Rear Mid", "Left Rear Bass",
            "Right Rear Tweeter", "Right Rear Mid", "Right Rear Bass",
    };
    
    //private
    static String positionAbbreviations[] = {"Not Used", "Left Front", "Right Front", "Left Rear", "Right Rear", "Sub Woofer", "Center",
            "Left Frt Tweet","Left Frt Mid", "Left Frt Bass",
            "Right Frt Tweet", "Right Frt Mid", "Right Frt Bass",
            "Left Rear Tweet", "Left Rear Mid", "Left Rear Bass",
            "Right Rear Tweet", "Right Rear Mid", "Right Rear Bass",
    };

    SPEAKER_POSITIONS position;

    DspInput(SPEAKER_POSITIONS newPosition) {
        position = newPosition;
    }

    String GetLocationName() {
        return(strNames[position.value]);
    }

    String GetPositionAbbreviation() {
        return(positionAbbreviations[position.value]);
    }

    void SetSpeakerPosition(int setPosition) {
        position =  SPEAKER_POSITIONS.values()[setPosition];
        }

    int GetPosition() {
        return(position.value);
    }

    int GetSpeakerLocationId() {
        int location_index = position.value;
        return (speaker_locationIds[location_index].value);
    }

    boolean LocationIsUsed() {
        if (position.value == SPEAKER_POSITIONS.NOT_USED.value)
            return(false);
        else
            return(true);
    }



}
