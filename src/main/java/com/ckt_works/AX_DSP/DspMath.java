package com.ckt_works.AX_DSP;
// Created by Abeck on 11/9/2016. //

import android.util.Log;

public class DspMath
{
    static class fixed32 {
        int msw;                // Most Significant Word
        int lsw;                // Least Significant Word

        fixed32(double val) {
            int value = 0;
           // if (val > 0) {
            {
                long lngValue = (long) (1073741824F * val);
                long MSW_1 = lngValue >> 16;
                msw = (int) MSW_1;
                long LSB_1 = lngValue & 0x0000FFFF;
                long LSB_2 = LSB_1 >> 1;
                lsw = (int) LSB_2;
//                Log.i("MSW", Long.toHexString(msw));
//                Log.i("LSW", Long.toHexString(lsw));
            }
        }

        int  GetMsb() {
            return(msw);
        }

        int  GetLsb() {
            return(lsw);
        }
    };


    private static int FIXED_POINT = 24;
    private static long ONE = 1L << FIXED_POINT;

    static public void SetDataFormat(int bits) {
        FIXED_POINT = bits;
        ONE = 1L << bits;
    }

    static public int mul(int a, int b) {
        return (int) ((long) a * (long) b >> FIXED_POINT);
    }

    // Convert a floating point value to a Fixed Point number
    static public int toFix(double val) {
        return (int) (val * ONE);
    }

    static public int toFix(double val, long bits ) {
        return (int) (val * (1L << bits));
    }



    // Convert a byte array to a Fixed Point number
    static public int toFix(byte[] data)
    {
        int intFix = 0;

        intFix = data[0] & 0xFF;
        intFix |= (((int)data[1]) & 0xFF) << 8;
        intFix |= (((int)data[2]) & 0xFF) << 16;
        intFix |= (((int)data[3]) & 0xFF) << 24;

        return(intFix);
    }

    static public int intVal( int fix ) {
        return fix >> FIXED_POINT;
    }

    static public  double doubleVal( int fix ) {
        return ((double) fix) / ONE;
    }
    static public  float floatVal( int fix ) {
        return ((float) fix) / ONE;
    }




}
