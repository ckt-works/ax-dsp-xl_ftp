
package com.ckt_works.AX_DSP;

// Created by Abeck on 11/9/2016. //

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import static android.bluetooth.BluetoothGatt.GATT_SUCCESS;
import static android.bluetooth.BluetoothProfile.GATT;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */

enum DSP_ACK_STATE {DSP_ACK_STATE_WAITING(0), DSP_ACK_STATE_RECEIVED(1), DSP_ACK_STATE_ERROR(-1);

    private final int id;
    DSP_ACK_STATE(int id) {
        this.id = id;
    }
    public int getIdCode() {
        return this.id;
    }
}


public class DspService extends Service {

    // TODO ANDY FIX FOR TESTING ONLY
    private final int MAX_ACK_TIME = 8000;

    private static final String TAG = DspService.class.getSimpleName();

    private int connected_rssi = 0;
    private volatile DSP_ACK_STATE waiting_for_data = DSP_ACK_STATE.DSP_ACK_STATE_RECEIVED;
    private volatile DSP_ACK_STATE waiting_for_ack = DSP_ACK_STATE.DSP_ACK_STATE_RECEIVED;
    private volatile boolean waiting_for_descriptor_write = false;
    private Handler mHandler;


    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private String mBluetoothDeviceAddress;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothDevice connected_device = null;

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;

    public final static String ACTION_GATT_CONNECTED = "ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED = "ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED = "ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DSP_DATA_READ_COMPLETE = "ACTION_DSP_DATA_READ_COMPLETE";
    public final static String ACTION_DATA_AVAILABLE = "ACTION_DATA_AVAILABLE";
    public final static String ACTION_DSP_DATA_AVAILABLE = "ACTION_DSP_DATA_AVAILABLE";
    public final static String ACTION_ACK_RECEIVED = "ACTION_ACK_RECEIVED";
    public final static String ACTION_NAK_RECEIVED = "ACTION_NAK_RECEIVED";
    public final static String ACTION_ACK_TIME_OUT = "ACTION_ACK_TIME_OUT";
    public final static String ACTION_NAK_INVALID_VEHICLE = "ACTION_ACK_INVALID_VEHICLE";
    public final static String ACTION_NAK_DSP_NOT_ON = "ACTION_NAK_DSP_NOT_ON";
    public final static String ACTION_DATA_TIME_OUT = "ACTION_DATA_TIME_OUT";
    public final static String ACTION_COMM_ERROR = "ACTION_COMM_ERROR";
    public final static String ACTION_DEVICE_NOT_AX_DSP = "ACTION_DEVICE_NOT_AX_DSP";
    public final static String ACTION_CHARACTERISTIC_ERROR = "ACTION_CHARACTERISTIC_ERROR";
    public final static String ACTION_CONNECTION_COMPLETE = "SERVICE_DISCOVERY_COMPLETE";
    public final static String ACTION_RSSI_UPDATE = "ACTION_RSSI_UPDATE";

    public final static String EXTRA_DATA = "EXTRA_DATA";
    public final static String BLE_UUID = "BLE_UUID";
    public final static String BLE_VALUE = "BLE_VALUE";
    public final static String BLE_BYTES = "BLE_BYTES";

    private final static UUID GENERIC_ACCESS_UUID = UUID.fromString("1bad1910-978b-11e6-ae22-56b6b6499611");  // Can't use "official' UUID with the BlueNRG
    public final static UUID DSP386_NAME_UUID = UUID.fromString("1bad1911-978b-11e6-ae22-56b6b6499611");
    public final static UUID DSP386_SERIAL_NUMBER_UUID = UUID.fromString("1bad1912-978b-11e6-ae22-56b6b6499611");
    public final static UUID BLUE_NRG_FIRMWARE_VERSION_UUID = UUID.fromString("1bad1913-978b-11e6-ae22-56b6b6499611");
    public final static UUID BLUE_NRG_HARDWARE_VERSION_UUID = UUID.fromString("1bad1914-978b-11e6-ae22-56b6b6499611");
    public final static UUID DSP386_SOFTWARE_VERSION_UUID = UUID.fromString("1bad1915-978b-11e6-ae22-56b6b6499611");
    public final static UUID DSP386_MANUF_NAME_UUID = UUID.fromString("1bad1916-978b-11e6-ae22-56b6b6499611");

    private static final UUID CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
    private static final UUID DSP_SERVICE_UUID = UUID.fromString("1bad1920-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_ACK_UUID = UUID.fromString("1bad1921-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_GAIN_UUID = UUID.fromString("1bad1922-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_EQ_CHANNEL_UUID = UUID.fromString("1bad1925-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_EQ_FILTER_UUID = UUID.fromString("1bad1926-978b-11e6-ae22-56b6b6499611");

    private static final UUID DSP_INPUT_DATA_UUID = UUID.fromString("1bad1927-978b-11e6-ae22-56b6b6499611");

    private static final UUID DSP_DELAY_UUID = UUID.fromString("1bad192C-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_COMMAND_UUID = UUID.fromString("1bad192D-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_SEND_DATA_UUID = UUID.fromString("1bad192E-978b-11e6-ae22-56b6b6499611");

    private static final UUID DSP_XOVER_UUID_1 = UUID.fromString("1bad1930-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_XOVER_UUID_2 = UUID.fromString("1bad1931-978b-11e6-ae22-56b6b6499611");
    private static final UUID DSP_XOVER_UUID_3 = UUID.fromString("1bad1932-978b-11e6-ae22-56b6b6499611");

    //    public DspInterface AppReadyInterface;
    private DspEqFilter sending_filter;
    private DspCrossover sending_crossover;

    private String axdsp_device_name;
    private String axdsp_manuf_name;
    private String axdsp_software_version;
    private String axdsp_serial_number;
    private String ble_hardware_version;
    private String ble_firmware_version;
    private MainActivity m_main_Activity;


    HandlerThread commTimerThread;
    Looper commTimerLooper;
    Handler commTimerHandler;

    /*
    void CommTimerStart() {
        if (commTimerThread.getState() != Thread.State.TERMINATED)
            commTimerThread.quit();
        commTimerThread.start();
    }

    void CommTimerQuit() {
        if (commTimerThread.getState() != Thread.State.TERMINATED)
            commTimerThread.quit();
    }
*/

    enum COMM_TIMER_TYPE {COMM_TIMER_ACK, COMM_TIMER_DATA, COMM_TIMER_DESCRIPT}
    private Timer commTimer;
    COMM_TIMER_TYPE commTimerType;

    void StartCommTimer(COMM_TIMER_TYPE type, int delayTime) {
        commTimer = new Timer();
        int ackTimeout = delayTime;
        commTimerType = type;
        CheckCommTask checkComms = new CheckCommTask();
        commTimer.schedule(checkComms, delayTime);
    }

    class CheckCommTask extends TimerTask {
        public void run() {
            switch(commTimerType) {
                case COMM_TIMER_DATA:
                    //if (waiting_for_data)
                    {
                        waiting_for_data = DSP_ACK_STATE.DSP_ACK_STATE_ERROR;
                        broadcastUpdate(ACTION_ACK_TIME_OUT, "Waiting for DATA");
                        Log.i("WaitingForData", "timed out ");
                        try {
                        Thread.sleep(20);
                            } catch(Exception ex) {
                            ex.getMessage();
                        }
                    }
                    break;

                case COMM_TIMER_ACK:
                    //if (waiting_for_ack)
                    {
                        waiting_for_ack = DSP_ACK_STATE.DSP_ACK_STATE_ERROR;
                        broadcastUpdate(ACTION_ACK_TIME_OUT, "Waiting for ACK");
                        Log.i("WaitingForAck", "timed out ");
                        try {
                            Thread.sleep(20);
                        } catch(Exception ex) {
                            ex.getMessage();
                        }
                    }
                    break;

                case COMM_TIMER_DESCRIPT:
                    if (waiting_for_descriptor_write) {
                        waiting_for_descriptor_write = false;
                        broadcastUpdate(ACTION_ACK_TIME_OUT, "Waiting for Descriptor Write");
                        try {
                            Thread.sleep(2);
                        } catch(Exception ex) {
                            ex.getMessage();
                        }                     }
                    break;


            }
            if (commTimer != null)
                commTimer.cancel();
        }
    }




    @Override
    public IBinder onBind(Intent intent)
    {
      //  commTimerThread = new HandlerThread("MyHandlerThread");
      //  commTimerThread.start();
      //  commTimerLooper = commTimerThread.getLooper();
      //  commTimerHandler = new Handler(commTimerLooper);
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        Log.i(TAG, "onUnbind");

    //    commTimerThread.quit();
        broadcastUpdate(ACTION_GATT_DISCONNECTED);
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    // Initializes a reference to the local Bluetooth adapter.
    // Return true if the initialization is successful.
    public boolean initialize(Context my_context) {
        // For API level 18 and above, get a reference to BluetoothAdapter through BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        mHandler = new Handler();
        LocalBroadcastManager.getInstance(my_context).registerReceiver(ReadGenericAccessReceiver, CreateIntentFilters());
        return true;
    }


    public void SetMainActivity(MainActivity main_activity)
    {
        m_main_Activity = main_activity;
    }
    public String GetAxdspDeviceName()
    {
        return(axdsp_device_name);
    }

    public String GetAxdspManufName()
    {
        return(axdsp_manuf_name);
    }

    public String GetAxdspSoftwareVersion()
    {
        return(axdsp_software_version);
    }

    public String GetAxdspSerialNumber()
    {
        return(axdsp_serial_number);
    }

    public String GetBleHardwareVersion()
    {
        return(ble_hardware_version);
    }

    public String GetBleFirmwareVersion()
    {
        return(ble_firmware_version);
    }

    private final BroadcastReceiver ReadGenericAccessReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (action.equals(DspService.ACTION_DATA_AVAILABLE)) {
                String uuid = intent.getExtras().getString(DspService.BLE_UUID);
                String value = intent.getExtras().getString(DspService.BLE_VALUE);
                if (uuid != null) {
                    if (uuid.equals(String.valueOf(DspService.DSP386_MANUF_NAME_UUID)))                 // Manufacturer name received
                        axdsp_manuf_name = value;

                    else if (uuid.equals(String.valueOf(DspService.DSP386_NAME_UUID)))                  // Device name received
                        axdsp_device_name = value;

                    else if (uuid.equals(String.valueOf(DspService.DSP386_SOFTWARE_VERSION_UUID)))     // 386 Software Version received
                        axdsp_software_version = value;

                    else if (uuid.equals(String.valueOf(DspService.DSP386_SERIAL_NUMBER_UUID)))        // 386 Serial Number received
                        axdsp_serial_number = value;

                    else if (uuid.equals(String.valueOf(DspService.BLUE_NRG_FIRMWARE_VERSION_UUID)))   // BLE Firmware Version received
                        ble_firmware_version = value;

                    else if (uuid.equals(String.valueOf(DspService.BLUE_NRG_HARDWARE_VERSION_UUID)))   // BLE Hardware Version received
                        ble_hardware_version = value;
                }
                SetWaitingForData(false);
            }
        }
    };

    // Create the Intent Filters for our Local LocalBroadcastManager
    private static IntentFilter CreateIntentFilters() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DspService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    public boolean isConnected() {
        boolean connected = false;
        if (connected_device != null && mBluetoothManager != null)
            if (mBluetoothManager.getConnectionState(connected_device, GATT) == STATE_CONNECTED)
                connected = true;
        return (connected);
    }

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;

            if (newState == BluetoothProfile.STATE_CONNECTED) {
                intentAction = ACTION_GATT_CONNECTED;
                broadcastUpdate(intentAction);
                mBluetoothGatt.discoverServices();
                Log.i(TAG, "Connected");
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                intentAction = ACTION_GATT_DISCONNECTED;
                broadcastUpdate(intentAction);
                Log.i(TAG, "Disconnected");
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS)
                connected_rssi = rssi;
            else
                connected_rssi = 0;
            broadcastUpdate(ACTION_RSSI_UPDATE, Integer.toString(rssi));
        }

        List<BluetoothGattService> services ; //= new ArrayList<Map<String, String>>();
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == GATT_SUCCESS) {
                Log.i(TAG, "onServicesDiscovered - mBluetoothGatt = " + mBluetoothGatt);
                services = gatt.getServices();
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.i(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onDescriptorWrite (BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
            SetWaitingForDescriptorWrite(false);
        }

        @Override
        // Called whenever we receive an write complete from the 386
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == GATT_SUCCESS) {
                UUID test_uuid = characteristic.getUuid();
                BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
                if (DspService == null) {
                    broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
                    return;
                }

                // If we received an Write Complete to the EQ Channel Data...
                if (test_uuid.compareTo(DSP_EQ_CHANNEL_UUID) == 0) {
                    // Get DSP Filter Channel Characteristic
                    BluetoothGattCharacteristic DspEq = DspService.getCharacteristic(DSP_EQ_FILTER_UUID);
                    if (DspEq != null) {
                        DspEq.setValue(sending_filter.GetFilterData());     // Send EQ Filter data
                        mBluetoothGatt.writeCharacteristic(DspEq);
                    }
                }

                // If we received Write Complete to the Crossover Filter Block 1
                else if (test_uuid.compareTo(DSP_XOVER_UUID_1) == 0) {

                    // Get next Crossover filter block
                    BluetoothGattCharacteristic DspCrossover = DspService.getCharacteristic(DSP_XOVER_UUID_2);
                    if (DspCrossover != null) {
                        DspCrossover.setValue(sending_crossover.GetCrossoverData(1));
                        mBluetoothGatt.writeCharacteristic(DspCrossover);   // Send Band Pass Crossover Filter data Block 1
                    }
                }

                // If we received Write Complete to the Band Pass Crossover Filter Block 2
                else if (test_uuid.compareTo(DSP_XOVER_UUID_2) == 0) {
                    // Get next Crossover filter block
                BluetoothGattCharacteristic DspCrossover = DspService.getCharacteristic(DSP_XOVER_UUID_3);
                if (DspCrossover != null) {
                    DspCrossover.setValue(sending_crossover.GetCrossoverData(2));
                    mBluetoothGatt.writeCharacteristic(DspCrossover);   // Send Band Pass Crossover Filter data Block 2
                }
            }
            }
        }

        @Override
        // Called whenever we receive data from the 386
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == GATT_SUCCESS) {
                try {
                    Intent intent = new Intent();
                    String strUUID = characteristic.getUuid().toString();
                    intent.putExtra("UUID", strUUID);

                    byte[] data = characteristic.getValue();
                    String data_string = new String(data, StandardCharsets.UTF_8);
                    intent.putExtra("DATA", data_string);
                    broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);

                } catch (Exception e) {
                    Log.i(TAG, "onCharacteristicRead", e);
                }
            }
        }

        @Override
        // Callback triggered as a result of a remote characteristic notification.
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            if (characteristic.getUuid().equals(DSP_ACK_UUID))           // If we received an ACK/NAK packet
            {
Log.i("onCharacteristicChanged", "ACK received");
                SetWaitingForAck(false);
                byte[] data = characteristic.getValue();                     // Get the response and act accordingly
                if (data[0] == 1) {
                    broadcastUpdate(ACTION_ACK_RECEIVED, characteristic);
                }
                else if (data[0] == 0xFF){
                    broadcastUpdate(ACTION_NAK_INVALID_VEHICLE, characteristic);
                }

                else if (data[0] == 0x11) {
                    broadcastUpdate(ACTION_NAK_DSP_NOT_ON, characteristic);
                }

                else
                {
                    Log.i("onCharacteristicChanged", "Comm Error");
                    broadcastUpdate(ACTION_NAK_RECEIVED, characteristic);
                }
            }

            else if (characteristic.getUuid().equals(DSP_SEND_DATA_UUID))     // If we received an DSP Data Packet
            {
                SetWaitingForData(false);
/*                byte data[] = characteristic.getValue();                    // Get the Data and send it o
                Parcel dest = Parcel.obtain();
                dest.writeByteArray(data);

                final Intent intent = new Intent();
                intent.putExtra(EXTRA_DATA, data);

                Bundle b_data = new Bundle();

                String message = "andy";
*/
                broadcastUpdate(ACTION_DSP_DATA_AVAILABLE, characteristic); // characteristic);
            }
        }
    };

    private void broadcastUpdate(final String action) {
        Intent intent = new Intent(action);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final BluetoothGattCharacteristic characteristic) {
        Intent intent = new Intent(action);

        intent.putExtra(BLE_UUID, characteristic.getUuid().toString());
        intent.putExtra(BLE_VALUE, characteristic.getStringValue(0));
        intent.putExtra(BLE_BYTES, characteristic.getValue());


        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final String message) {
        Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA, message);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        DspService getService() {
            return DspService.this;
        }
    }

    // >>>>>>> Functions to Handle ACK/NAK reception
    // Returns the state of the 'waiting_for_ack' variable
    private DSP_ACK_STATE WaitingForAck() {
        return (waiting_for_ack);
    }

    // Waits for the "waiting_for_ack' to not be DSP_ACK_STATE_WAITING
    public int WaitForAck()
    {
        //Log.i("WaitForAck", "Start");
        int loops = 1;
        while (WaitingForAck() == DSP_ACK_STATE.DSP_ACK_STATE_WAITING){
            try
            {
                Thread.sleep(10);
//                Log.i("WaitForAck", Integer.toString((loops++ * 10)) + " ms");
            }
            catch(InterruptedException ex)
            {ex.printStackTrace();}
        }
        if (WaitingForAck() == DSP_ACK_STATE.DSP_ACK_STATE_ERROR)
            return(-1);
        else
            return(loops * 10);
    }

    public void SetWaitingForAck(Boolean state) {SetWaitingForAck( state,  MAX_ACK_TIME);}

    public void SetWaitingForAck(Boolean state, final int max_ack_time) {
        mHandler.removeCallbacksAndMessages(null);
        waiting_for_ack = (state) ?  DSP_ACK_STATE.DSP_ACK_STATE_WAITING : DSP_ACK_STATE.DSP_ACK_STATE_RECEIVED;

        if (commTimer != null)
            commTimer.cancel();
        if (state)
            StartCommTimer(COMM_TIMER_TYPE.COMM_TIMER_ACK, max_ack_time);
    }

    // >>>>>>> Functions to Handle Data Received ACK reception
    // Returns the state of the waiting_for_data semaphore
    private DSP_ACK_STATE WaitingForData() {
        return (waiting_for_data);
    }

    // Waits for "WaitingForData" to be FALSE
    public int WaitForData()
    {
 //       Log.i("WaitForData", "Start");
        int loops = 1;
        while (WaitingForData() == DSP_ACK_STATE.DSP_ACK_STATE_WAITING){
            try
            {
                Thread.sleep(10);
//                Log.i("WaitForData", Integer.toString(loops++ * 10) + "ms");
            }
            catch(InterruptedException ex)
            {ex.printStackTrace();}
        }
        if (WaitingForData() == DSP_ACK_STATE.DSP_ACK_STATE_ERROR)
            return(-1);
        else
            return(loops * 10);
    }

    private void SetWaitingForData(Boolean state) {SetWaitingForData( state,  6000);}
    private void SetWaitingForData(Boolean state, final String caller){SetWaitingForData(state,  6000, caller);}
    private void SetWaitingForData(Boolean state, final int max_wait_time) {SetWaitingForData(state, max_wait_time, " ");}
    private void SetWaitingForData(Boolean state, final int max_wait_time, final String caller)
    {
        mHandler.removeCallbacksAndMessages(null);
        waiting_for_data = (state) ?  DSP_ACK_STATE.DSP_ACK_STATE_WAITING : DSP_ACK_STATE.DSP_ACK_STATE_RECEIVED;
        if (commTimer != null)
            commTimer.cancel();
        if (state)
            StartCommTimer(COMM_TIMER_TYPE.COMM_TIMER_DATA, max_wait_time);
    }


    // >>>>>>> Functions to Descriptor Write ACK reception
    // Returns the state of the "waiting_for_descriptor_write' variable
    private boolean WaitingDescriptorWrite() {
        return (waiting_for_descriptor_write);
    }

    // Waits for the "waiting_for_descriptor_write' to be false
    public void WaitForDescriptorWrite()
    {
        Log.i("WaitForDescriptorWrite", "Start");
        int loops = 1;
        while (WaitingDescriptorWrite()){
            try
            {
                Thread.sleep(10);
                Log.i("WaitForDescriptorWrite", Integer.toString(loops++));
            }
            catch(InterruptedException ex)
            {ex.printStackTrace();}
        }
    }

    private void SetWaitingForDescriptorWrite(Boolean state) {
        int MAX_DESCRIPTOR_WRITE_TIME = 8000;
        SetWaitingForDescriptorWrite( state, MAX_DESCRIPTOR_WRITE_TIME);}

    private void SetWaitingForDescriptorWrite(Boolean state, final int max_ack_time) {
        mHandler.removeCallbacksAndMessages(null);
        waiting_for_descriptor_write = state;
        if (commTimer != null)
            commTimer.cancel();

        if (state)
            StartCommTimer(COMM_TIMER_TYPE.COMM_TIMER_DESCRIPT, max_ack_time);
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final String address) {
        boolean connectionSuccessful = true;
        if (mBluetoothAdapter == null || address == null) {
            Log.i(TAG, "BluetoothAdapter not initialized or unspecified address.");
            connectionSuccessful = false;
        }

        // Previously connected device.  Try to reconnect.
        else if (address.equals(mBluetoothDeviceAddress) && mBluetoothGatt != null) {
            Log.i(TAG, "Trying to use an existing mBluetoothGatt for connection.");
            if (!mBluetoothGatt.connect()) {
                connectionSuccessful = false;
            }
        }

        if (connectionSuccessful) {
            connected_device = mBluetoothAdapter.getRemoteDevice(address);
            if (connected_device == null) {
                Log.i(TAG, "Device not found.  Unable to connect.");
                connectionSuccessful = false;
            }


            // We want to directly connect to the device, so we are setting the autoConnect parameter to false.
            if (connectionSuccessful) {
                mBluetoothGatt = connected_device.connectGatt(this, false, mGattCallback);
                Log.i(TAG, "Trying to create a new connection.");
                mBluetoothDeviceAddress = address;
            }
        }
        return (connectionSuccessful);
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     * */
     public void disconnect() {
         boolean connected = false;
         Log.i(TAG, "disconnect");
         if (mBluetoothAdapter != null && mBluetoothGatt != null) {
                                                                        // Check if we are connected to a device
             List<BluetoothDevice> devices = mBluetoothManager.getConnectedDevices(BluetoothProfile.GATT);
             for(BluetoothDevice device : devices) {
                 if(device.getType() == BluetoothDevice.DEVICE_TYPE_LE) {
                     connected = true;
                     break;
                 }
             }

            if (connected)                                             // Only disconnect if we are connected
                 mBluetoothGatt.disconnect();
// ANDY TODO - DO WE NEEDS THIS BELOW??
            //  mBluetoothGatt.close();
         }

// TODO ANDY REVIEW THIS
//         if (mBluetoothGatt != null)
//            mBluetoothGatt.disconnect();
     }


    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        Log.i(TAG, "mBluetoothGatt closed");
        mBluetoothDeviceAddress = null;
        mBluetoothGatt.close();
        mBluetoothGatt = null;
    }

    public void ReadConnectedRssi()
    {
        if (mBluetoothGatt != null) {
            boolean rssiStatus = mBluetoothGatt.readRemoteRssi();
        //    Log.i("ReadConnectedRssi", (rssiStatus) ? "Passed" : "Failed");
        }
    }



    public int GetConnectedRssi(){return(connected_rssi);}

    String GetConnectedRssiString(){return(Integer.toString(GetConnectedRssi()));}

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.i(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mBluetoothGatt.readCharacteristic(characteristic);
    }


    public void EnableDspServiceNotification()
    {

    	if (mBluetoothGatt == null) {
    		broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
    		return;
    	}

        BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
        if (DspService == null) {
            broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }


        BluetoothGattCharacteristic AckCharacteristic = DspService.getCharacteristic(DSP_ACK_UUID);
        if (AckCharacteristic == null) {
             broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }
        SetWaitingForDescriptorWrite(true);                                 // We have to wait for this write to complete before sending another
        mBluetoothGatt.setCharacteristicNotification(AckCharacteristic,true);
        BluetoothGattDescriptor ack_descriptor = AckCharacteristic.getDescriptor(CCCD);
        ack_descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(ack_descriptor);
        WaitForDescriptorWrite();

        SetWaitingForDescriptorWrite(true);                                 // We have to wait for this write to complete before sending another
        BluetoothGattCharacteristic SendDataCharacteristic = DspService.getCharacteristic(DSP_SEND_DATA_UUID);
        if (SendDataCharacteristic == null) {
            broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }
        mBluetoothGatt.setCharacteristicNotification(SendDataCharacteristic,true);
        BluetoothGattDescriptor send_data_descriptor = SendDataCharacteristic.getDescriptor(CCCD);
        send_data_descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mBluetoothGatt.writeDescriptor(send_data_descriptor);
        WaitForDescriptorWrite();

        broadcastUpdate(ACTION_CONNECTION_COMPLETE);
    }


    public void GetGenericAccessCharacteristic(UUID characteristic)
    {
        if (mBluetoothGatt != null) {
            BluetoothGattService DspService = mBluetoothGatt.getService(GENERIC_ACCESS_UUID);
            if (DspService == null)
                broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);

            else {      // Get Characteristic
                SetWaitingForData(true, characteristic.toString());
                BluetoothGattCharacteristic value = DspService.getCharacteristic(characteristic);
                if (value != null)
                    readCharacteristic(value);
                else {
                    SetWaitingForData(false);
                    String char_id = characteristic.toString();
                    broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, char_id.substring(0, 18));
                }
            }
        }
    }

    public void GetDspData(DspCommand command) {
        if (mBluetoothGatt == null)
            Log.i(TAG, "mBluetoothGatt = null");
        else{
            BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
            if (DspService == null) {
                broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
                return;
            }

            // Get DSP Command Characteristic
            BluetoothGattCharacteristic DspCommand = DspService.getCharacteristic(DSP_COMMAND_UUID);
            if (DspCommand != null) {
                SetWaitingForData(true, 6000, command.GetIdString());
                DspCommand.setValue(command.GetCommandData());
                mBluetoothGatt.writeCharacteristic(DspCommand);
            } else {
                SetWaitingForData(false);
                broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Command");
            }
        }
    }

    public void SendDspGain(DspGain gain)
    {
        BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
        if (DspService == null) {
             broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }

        // Get DSP Gain Characteristic
        BluetoothGattCharacteristic DspGain = DspService.getCharacteristic(DSP_GAIN_UUID);
        if (DspGain != null)
        {
            SetWaitingForAck(true);
            DspGain.setValue(gain.GetGainData());
            mBluetoothGatt.writeCharacteristic(DspGain);
            m_main_Activity.SetDirtyFlag(true);
        }
        else {
            SetWaitingForAck(false);
            broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Gain");
        }
    }


    public void SendDspDelay(DspDelay delay)
    {
        BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
        if (DspService == null) {
            broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }

        // Get DSP Delay Characteristic
        BluetoothGattCharacteristic DspDelay = DspService.getCharacteristic(DSP_DELAY_UUID);
        if (DspDelay != null)
        {
            SetWaitingForAck(true);
            DspDelay.setValue(delay.GetDelayData());
            mBluetoothGatt.writeCharacteristic(DspDelay);
            m_main_Activity.SetDirtyFlag(true);
        }
        else
        {
            SetWaitingForAck(false);
            broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Delay");
        }
    }

    public void SendDspDelay(DspDelay_X delay)
    {
        BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
        if (DspService == null) {
            broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }

        // Get DSP Delay Characteristic
        BluetoothGattCharacteristic DspDelay = DspService.getCharacteristic(DSP_DELAY_UUID);
        if (DspDelay != null)
        {
            SetWaitingForAck(true);
            DspDelay.setValue(delay.GetDelayData());
            mBluetoothGatt.writeCharacteristic(DspDelay);

            m_main_Activity.SetDirtyFlag(true);
        }
        else
        {
            SetWaitingForAck(false);
            broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Delay");
        }
    }


    public void SendDspCommand(DspCommand command){
        SendDspCommand(command, MAX_ACK_TIME);
    }


    public void SendDspCommand(DspCommand command, int max_ack_time)
    {
        if (mBluetoothGatt == null) {
            SetWaitingForAck(false);
            Log.i("SendDspCommand", "mBluetoothGatt == null");

        }
        else {
            BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
            if (DspService == null) {
                broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
                return;
            }

            // Get DSP Command Characteristic
            BluetoothGattCharacteristic DspCommand = DspService.getCharacteristic(DSP_COMMAND_UUID);
            if (DspCommand != null) {
                SetWaitingForAck(true, max_ack_time);
                DspCommand.setValue(command.GetCommandData());
                mBluetoothGatt.writeCharacteristic(DspCommand);
                WaitForAck();
                // Identify & lock commands don't change 386 data
                if (command.GetCommand() != DSP_COMMANDS.DSP_COMMAND_IDENTIFY && command.GetCommand() != DSP_COMMANDS.DSP_COMMAND_LOCK)
                    m_main_Activity.SetDirtyFlag(true);
            } else {
                SetWaitingForAck(false);
                broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Command");
            }
        }
    }

    public void SendDspEqFilter(DspEqFilter filter) {
        SendDspEqFilter(filter, 0);
    }

    public void SendDspEqFilter(DspEqFilter filter, int SlaveMask)
    {
        sending_filter = filter;

        if (m_main_Activity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
            filter.SetFilterDataFormat(32);
        else
            filter.SetFilterDataFormat(24);

        // Get DSP Service
        BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
        if (DspService == null) {
            broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
            return;
        }

        // Get DSP Filter Channel Characteristic
        BluetoothGattCharacteristic DspEq = DspService.getCharacteristic(DSP_EQ_CHANNEL_UUID);
        if (DspEq != null)
        {
            DspEq.setValue(filter.GetChannelData(SlaveMask));
            SetWaitingForAck(true);
            boolean status = mBluetoothGatt.writeCharacteristic(DspEq);
            if (!status) {
                Log.e("SendDspEqFilter", "Write Error");
                SetWaitingForAck(false);
            }
            m_main_Activity.SetDirtyFlag(true);
        }
        else
        {
            SetWaitingForAck(false);
            broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "EQ");
        }
    }

    public void SendDspCrossover(DspCrossover crossover) {
        if (mBluetoothGatt != null) {
            SetWaitingForAck(true);
            sending_crossover = crossover;
            // Get DSP Service
            BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
            if (DspService == null) {
                broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
                return;
            }
            if (m_main_Activity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
                crossover.SetFilterDataFormat(32);
            else
                crossover.SetFilterDataFormat(24);

            // Get first block of DSP Crossover Characteristic
            BluetoothGattCharacteristic DspCrossoverChar;
/*        if (crossover.GetType() == CROSSOVER_TYPES.CROSSOVER_LOW_PASS)
            DspCrossover = DspService.getCharacteristic(DSP_LOW_XOVER_UUID_1);
        else if (crossover.GetType() == CROSSOVER_TYPES.CROSSOVER_HIGH_PASS)
            DspCrossover = DspService.getCharacteristic(DSP_HIGH_XOVER_UUID_1);
        else
            DspCrossover = DspService.getCharacteristic(DSP_BAND_XOVER_UUID_1);
*/
            DspCrossoverChar = DspService.getCharacteristic(DSP_XOVER_UUID_1);


            if (DspCrossoverChar != null) {
                DspCrossoverChar.setValue(crossover.GetCrossoverData(0));
                mBluetoothGatt.writeCharacteristic(DspCrossoverChar);

                m_main_Activity.SetDirtyFlag(true);
            } else {
                SetWaitingForAck(false);
                broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Xover");
            }
        }
    }

    public void SendDspInput(byte[] input_data) {
        if (mBluetoothGatt != null) {
             // Get DSP Service
            BluetoothGattService DspService = mBluetoothGatt.getService(DSP_SERVICE_UUID);
            if (DspService == null) {
                broadcastUpdate(ACTION_DEVICE_NOT_AX_DSP);
                return;
            }

            BluetoothGattCharacteristic DspCharacteristic;
            DspCharacteristic = DspService.getCharacteristic(DSP_INPUT_DATA_UUID);

            if (DspCharacteristic != null) {
                DspCharacteristic.setValue(input_data);
                mBluetoothGatt.writeCharacteristic(DspCharacteristic);

                m_main_Activity.SetDirtyFlag(true);
            } else {
                SetWaitingForAck(false);
                broadcastUpdate(ACTION_CHARACTERISTIC_ERROR, "Input");
            }
        }
    }

}

