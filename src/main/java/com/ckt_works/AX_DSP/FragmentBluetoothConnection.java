package com.ckt_works.AX_DSP;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unchecked")
public class FragmentBluetoothConnection extends CScreenNavigation implements AbsListView.OnItemClickListener,
        IDialogPasswordListener, View.OnClickListener, IDialogConfirmListener {

    public interface IBluetoothActionListener {
        void onConnect(String macAddress);
        void onPasswordGet(String macAddress);
    }
    private IBluetoothActionListener bluetoothActionListener;

    // Declare our GUI components
    private AbsListView device_list;
    private ListAdapter device_list_adapter;
    private boolean scanning;
    private Button scanButton;
    private Button disconnectButton;
    private List<Map<String, String>> m_devices = new ArrayList<>();
    private MainActivity mainActivity;

    private int clicked_button_id;          // Tracks last button that was clicked

    private final String DEFAULT_DEVICE_PASSWORD = "0000";
    private String devicePassword = "1234"; //DEFAULT_DEVICE_PASSWORD;
    private boolean waitingForPasswordFromDevice = false;
    public String deviceMacAddress = "";
    public String deviceName = "";


    //Obligatory Constructor & newInstance function
    public FragmentBluetoothConnection() {	}
    public static FragmentBluetoothConnection newInstance() {
        return new FragmentBluetoothConnection();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainActivity =  ((MainActivity) getActivity());
        View view = inflater.inflate(R.layout.layout_bluetooth_connection, container, false);

        if (view != null) {
            // Set the adapter
            device_list_adapter = new SimpleAdapter(this.getActivity(), m_devices, R.layout.layout_list_textview, MainActivity.KEYS, MainActivity.IDS);
            device_list = (AbsListView) view.findViewById(R.id.listDevices);
            device_list.setAdapter(device_list_adapter);
            device_list.setOnItemClickListener(this); // Set OnItemClickListener so we can be notified when users clicks on device name

            scanButton= (Button) view.findViewById(R.id.buttonScan);
            scanButton.setOnClickListener(this);
            scanning = false;

            disconnectButton = (Button) view.findViewById(R.id.buttonDisconnect);
            disconnectButton.setOnClickListener(this);
        }
        return view;
    }

    @Override
    public void onHiddenChanged (boolean hidden)
    {
        if (scanButton != null)
            scanButton.setEnabled(mainActivity.BluetoothIsEnabled());

        if (disconnectButton != null)
            disconnectButton.setEnabled(mainActivity.BluetoothIsEnabled());

        if (!hidden)
            ClearDeviceList();
    }

    // Handles Scan & Disconnect buttons
    public  void onClick(View button) {
        clicked_button_id = button.getId();
        String message  = "?";
        String title = "?";
        String deny_text = "Cancel";
        String confirm_text = "";
        boolean display_confirmation = mainActivity.GetDirtyFlag() || mainActivity.GetNotAppliedFlag();

        if (display_confirmation)                                               // We have to ask the user to confirm Scan or disconnect
        {
            if (clicked_button_id == R.id.buttonScan) {
                if (mainActivity.GetNotAppliedFlag())                       // Data has been recalled but not Applied - always do this check first
                    message = getString(R.string.message_not_applied_scan);

                else if (mainActivity.GetDirtyFlag())                        // Data has changed, but not been Locked Down...
                    message =  getString(R.string.message_dirty_scan);
                title = getString(R.string.title_confirm_scan);
                confirm_text = getString(R.string.label_scan);
            }

            else if (clicked_button_id == R.id.buttonDisconnect) {
                if (mainActivity.GetNotAppliedFlag())                        // Data has been recalled but not Applied  - always do this check first
                    message = getString(R.string.message_not_applied_disconnect);
                else if (mainActivity.GetDirtyFlag())                        // Data has changed, but not been Locked Down...
                    message =  getString(R.string.message_dirty_disconnect);
                title = getString(R.string.title_confirm_disconnect);
                confirm_text = getString(R.string.label_disconnect);
            }

            DialogConfirm dirty_alert_dialog = DialogConfirm.newInstance(title, message, deny_text, confirm_text);
            dirty_alert_dialog.setTargetFragment(this, 0);
            dirty_alert_dialog.show(getFragmentManager(), "confirm");
        }

        else                        // No Data to save to 386 - Not Dirty or Not Applied
            onDialogConfirmClick(true);
    }

    // Called when the user clicks the Yes or No button on Alert or Confirm dialog disconnect confirmation dialog - also called from 'onClick'
    public void onDialogConfirmClick(boolean answer){
        if (answer) {
            if (clicked_button_id == R.id.buttonScan) {
                if (scanning == false) {
                    scanning = true;
                    EnableScanButton(false);
                    mainActivity.Disconnect();
                    mainActivity.startScan();
                }
                else {
                    mainActivity.stopScan();
                    scanning = false;
                    EnableScanButton(true);
                    mainActivity.Disconnect();
                }
            }

            else if (clicked_button_id == R.id.buttonDisconnect) {
                mainActivity.Disconnect();
                EnableScanButton(true);
            }

            else if (clicked_button_id == -1) {
                mainActivity.UpdateConnectionStatus(" ", false);
                mainActivity.ProgressBarDismiss();
                mainActivity.Disconnect();
            }
            mainActivity.SetDirtyFlag(false);
            mainActivity.SetNotAppliedFlag(false);
        }
    }

    public void UpdateScanState(boolean enabled)
    {
        scanning = !enabled;
        EnableScanButton(enabled);
    }

    private void EnableScanButton(boolean enable){
        if (enable == false)
            scanButton.setText("STOP");
        else
            scanButton.setText("SCAN");
    }


    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            bluetoothActionListener = (IBluetoothActionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        bluetoothActionListener = null;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (bluetoothActionListener != null) {
            mainActivity.SetConnectionType(CONNECTION_TYPE.CONNECT_PASSWORD_VALIDATE);
             mainActivity.stopScan();
            mainActivity.CancelScanCallback();
            UpdateScanState(true);

            Map<String, String> item = (Map<String, String>)(device_list_adapter.getItem(position));
            item = (Map<String, String>)device_list.getItemAtPosition(position);
            deviceMacAddress = item.get(MainActivity.KEY_DEVICE_ADDRESS);
            //String rssi = item.get(MainActivity.KEY_DEVICE_RSSI);
            deviceName = item.get(MainActivity.KEY_DEVICE_NAME);
            mainActivity.SetDeviceName(deviceName);

            if (deviceName.contains(BleScannerService.AX_DSP_L_DEVICE_NAME))
                mainActivity.SetAxDspType(AX_DSP_TYPE.AX_DSP_440);
            else
                mainActivity.SetAxDspType(AX_DSP_TYPE.AX_DSP_420);

// TODO WOULD BE NICE TO ADD RSSI TO DEVICE LIST"
            if(mainActivity.GetConnectionType() == CONNECTION_TYPE.CONNECT_PASSWORD_VALIDATE) {
Log.i("PASSWORD_VALIDATE", "Starting Authorization");
                mainActivity.UpdateConnectionStatus("Authorizing Connection to " + item.get(MainActivity.KEY_DEVICE_NAME), false);
                mainActivity.ProgressBarDisplay("Connecting to AX-DSP");
//                mainActivity.ProgressBarDisplay("Authorizing Connection");
                Thread thread = new Thread(new Runnable() {                                 // Run a thread that updates the Available Vehicle Files
                    @Override
                    public void run() {
                        clicked_button_id = -1;
                        waitingForPasswordFromDevice = true;                            // Indicate we are waiting for the password from the device
                        bluetoothActionListener.onPasswordGet(deviceMacAddress);        // Start connection process and retrieve password
                    }
                });
                thread.start();
            }
            else
                ConnectToAXDSP();
        }
    }


    public String GetPassword(){
        return(devicePassword);
    }

    public void SetPassword(String password){
        devicePassword = password;
        if (devicePassword.equals(DEFAULT_DEVICE_PASSWORD))
            onPasswordDialogClick(true, devicePassword);
        else
            ValidatePassword();
    }

    public void ValidatePassword() {
        DialogPassword password_dialog = new DialogPassword();
        password_dialog.setCancelable(false);
        password_dialog.setTargetFragment(this, 0);
//        Bundle args = new Bundle();
        password_dialog.SetType(DialogPassword.PASSWORD_DIALOG_TYPE.PASSWORD_DIALOG_ENTER);
        password_dialog.show(getFragmentManager(), "Password");
    }

    public void onPasswordDialogClick(boolean answer, String password){

        if (answer == false) {                                                // User clicked "Cancel" Button
            mainActivity.UpdateConnectionStatus(" ", false);
            mainActivity.ProgressBarDismiss();
            mainActivity.Disconnect();
        }
        else {
            if (bluetoothActionListener != null) {
                if (password.equals(devicePassword)) {
                    mainActivity.DisplayConfigurationScreen();
                    mainActivity.GetDspStatus();
                    mainActivity.UpdateConnectionStatus("Connected to " + deviceName, true);
                    mainActivity.SetConnected(true);
                }
                else {
                    DialogAlert password_error_alert = DialogAlert.newInstance("Invalid Password", "Password is not valid for this device");
                    password_error_alert.show(getFragmentManager(), "alert");
                    password_error_alert.setTargetFragment(this, 0);
                    mainActivity.UpdateConnectionStatus(" ", false);
                }
            }
        }
    }

    private void ConnectToAXDSP(){
        bluetoothActionListener.onConnect(deviceMacAddress);
        mainActivity.UpdateConnectionStatus("Connecting to " + deviceName, false);
    }


    public void ClearDeviceList()
    {
        m_devices.clear();
        if (device_list != null && device_list_adapter != null)
            device_list.setAdapter(device_list_adapter);
    }


    public void AddDevice(String device_name, String device_address, String rssi) {
        if (device_name != null) {
            Map<String, String> item = new HashMap<>();
            item.put(MainActivity.KEY_DEVICE_NAME, device_name);
            item.put(MainActivity.KEY_DEVICE_ADDRESS, device_address);
            item.put(MainActivity.KEY_DEVICE_RSSI, rssi);
            m_devices.add(item);

        }
        device_list.setAdapter(device_list_adapter);
    }


}
