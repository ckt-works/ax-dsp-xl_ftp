package com.ckt_works.AX_DSP;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import static com.ckt_works.AX_DSP.MainActivity.HAS_NO_OE_AMP;


// Created by Abeck on 12/2/2016. //

public class FragmentConfig extends CScreenNavigation implements View.OnClickListener, IDialogVehicleListener,
                            IPostExecuteInterface, IPostExecuteInterfaceArg, IDialogConfirmListener, IDialogPasswordListener {

    private final int MAX_LOCK_TIME = 40000;        // Max time for lock down to finish

    private final DspCommand vehicle_type_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_VEHICLE_TYPE);
    private final DspCommand pause_can_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_CAN_PAUSE);

    private MainActivity mainActivity = null;   // So we can callback to main to do some global stuff for us

    private Context my_context;

    // Declare our Command objects
    private final DspCommand reset_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_RESET);
    private final DspCommand lock_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_LOCK);
    private final DspCommand identify_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_IDENTIFY);
    private final DspCommand set_password_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_PASSWORD);
    private final DspCommand clear_phone_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_CLEAR_PHONE_LIST);

    private int vehicle_id;                         // Number that corresponds to the vehicle type
    private int model_id;                           // Number that corresponds to the vehicle model

    // Declare our controls
    private LinearLayout clearPhonesLayout;
    private Button clearPhoneButton;
    private Button defaultButton;
    private Button lockButton;
    private Button vehicleTypeButton;
    private Button identifyButton;
    private Button saveButton;
    private Button recallButton;
    private Button applyButton;

    private Button aboutButton;

    private Button passwordButton;

    private String vehicle_manufacturer;
    private String vehicle_model;
    private String old_vehicle_manufacturer;
    private String old_vehicle_model;
    private boolean vehicle_has_oe_amp;

    private boolean mExternalStorageAvailable = false;
    private boolean mExternalStorageWritable = false;


    View configView;

    // Obligatory Constructor, newInstance & onCreate functions
    public FragmentConfig() {
    }

    public static FragmentConfig newInstance() {
        return new FragmentConfig();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity = ((MainActivity) getActivity());

        configView = inflater.inflate(R.layout.layout_config, container, false);
        if (configView != null) {
            my_context = container.getContext();

            clearPhonesLayout = (LinearLayout) configView.findViewById(R.id.layoutClearPhones);
            clearPhonesLayout.setVisibility(View.GONE);

            clearPhoneButton = (Button)configView.findViewById(R.id.buttonClearPhones);
            clearPhoneButton.setOnClickListener(this);

            defaultButton = (Button) configView.findViewById(R.id.buttonSetDefaults);
            defaultButton.setOnClickListener(this);

            lockButton = (Button) configView.findViewById(R.id.buttonLockDown);
            lockButton.setOnClickListener(this);

            saveButton = (Button) configView.findViewById(R.id.buttonFileSave);
            saveButton.setOnClickListener(this);

            recallButton = (Button) configView.findViewById(R.id.buttonFileRecall);
            recallButton.setOnClickListener(this);

            // Removed and integrated into  the "Recall" button function because somebody might be confused?
            //applyButton = (Button) configView.findViewById(R.id.buttonFileApply);
            //applyButton.setOnClickListener(this);
            //applyButton.setEnabled(false);                  // Disabled until we recall a file

            vehicleTypeButton = (Button) configView.findViewById(R.id.buttonVehicle);
            vehicleTypeButton.setOnClickListener(this);

            identifyButton = (Button) configView.findViewById(R.id.buttonIdentify);
            identifyButton.setOnClickListener(this);

            aboutButton = (Button) configView.findViewById(R.id.buttonAbout);
            aboutButton.setOnClickListener(this);

            passwordButton = (Button) configView.findViewById(R.id.buttonSetPassword);
            passwordButton.setOnClickListener(this);

            CheckExternalStorage();
            EnableControls(false);
        }
        return (configView);
    }

    public void SetVehicleID(int vehicle, int model) {
        vehicle_id = vehicle;
        model_id = model;

        try {
            InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
            List<String> vehicle_type_info = DialogVehicleSelect.GetVehicleTypeInfo(input_stream, vehicle, model);
            vehicle_manufacturer = vehicle_type_info.get(0);
            vehicle_model = vehicle_type_info.get(1);
            vehicle_has_oe_amp = ((vehicle_id & HAS_NO_OE_AMP) > 0) ? false : true;  // Bit 0 of the id indicates if it has an OE amplifier
        } catch (XmlPullParserException | IOException e1) {
            e1.printStackTrace();
        }
    }


    public int GetVehicleID() {
        return (vehicle_id);
    }
    public int GetModelID() {
        return (model_id);
    }


    public String GetVehicleManufacturerString() {
        if (vehicle_manufacturer == null)
            return ("?");
        else if (vehicle_manufacturer.length() == 0)
            return ("?");
        else
            return (vehicle_manufacturer);
    }

    public String GetVehicleModelString() {
        if (vehicle_model == null || vehicle_manufacturer == null)
            return (" ");
        else if (vehicle_model.length() == 0 || vehicle_manufacturer.length() == 0)
            return (" ");
        else
            return (vehicle_model);
    }

    public boolean GetVehicleHasOE_Amp() {
        return (vehicle_has_oe_amp);
    }


    // Interface to allow the "vehicle_select_dialog" to callback to us with the vehicle info
    public void onApplyClick(String manufacturer, String model, boolean has_oe_amp, int vehicle_ordinal, int model_ordinal) {
        vehicle_manufacturer = manufacturer;
        vehicle_model = model;
        vehicle_has_oe_amp = has_oe_amp;
        vehicle_id = vehicle_ordinal;
        model_id = model_ordinal;
        SendVehicleType();
    }


    // Interface to allow the progress dialog to be dismissed after task is complete
    public void onPostExecute() {
        mainActivity.ProgressBarDismiss();
   //     if (Read386Datax != null)
   //         Read386Datax = null;
    }


    // Interface to allow the "TaskSendData" to callback when done sending data
    public void onPostExecuteArg(String arg) {
        final DspService dspService = mainActivity.GetDspService();
        try {
            if (dspService.isConnected())
                mainActivity.ProgressBarDisplay("Reading from AX-DSP");
                Thread.sleep(3000);
                Read386Data(this);
        } catch (Exception Ex) {

        }
    }

    public void onPasswordDialogClick(boolean answer, String password){
Log.i("Password", password);
        DspService dspService = mainActivity.GetDspService();
        if (dspService != null) {
            byte passwordBytes[] = password.getBytes();;
            set_password_command.SetData(passwordBytes, passwordBytes.length);
            dspService.SendDspCommand(set_password_command);
            //dspService.WaitForAck();
       //     mainActivity.ProgressBarDisplay("Reading from AX-DSP");
        }
    }

    // Called when the user clicks the Yes or No button on the confirm Defaults dialog
    public void onDialogConfirmClick(boolean answer) {
        final DspService dspService = mainActivity.GetDspService();

        if (answer && dspService != null) {
            mainActivity.ProgressBarDisplay("Resetting AX-DSP");

           // Send Default command via a task - it takes a long time and if done on
            // the GUI thread the progress bar will not be displayed until it is done
            TaskSendCommand taskCommand = new TaskSendCommand(dspService, this, reset_command, "Reset", 15000);
            taskCommand.execute();
        }
    }


    // Handles all controls on this screen
    public void onClick(View button) {
        DspService dspService = mainActivity.GetDspService();
        switch (button.getId()) {
            case R.id.buttonSetDefaults: {
//                DisplayProgressCountdownDialog("Locking Down Data", 5);
                DialogConfirm confirm_default_dialog = DialogConfirm.newInstance("Confirm Reset", "Reset to Factory Default Settings?", "Cancel", "Reset");
                confirm_default_dialog.setTargetFragment(this, 0);
                confirm_default_dialog.show(getFragmentManager(), "confirm");
            }
            break;

            case R.id.buttonLockDown: {
                if (dspService != null) {

                mainActivity.DisplayProgressCountdownDialog("Locking Down Data", MAX_LOCK_TIME/1000);


                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mainActivity.ProgressBarDismiss();
//                            progress_dialog_countdown.dismiss();                      // This is called after "MAX_LOCK_TIME" seconds
                            Log.i("Lock Down Button", "time out");
                        }
                    }, MAX_LOCK_TIME);

                    mainActivity.SetDirtyFlag(false);
                    // Send Lock Down command via a task - it takes a long time and if done on
                    // the GUI thread the progress bar will not be displayed until it is done
                    TaskSendCommand taskCommand = new TaskSendCommand(dspService, this, lock_command, MAX_LOCK_TIME);
                    taskCommand.execute();

//                    dspService.SendDspCommand(lock_command, MAX_LOCK_TIME);                // Send command to 386
                }
            }
            break;

            case R.id.buttonFileRecall: {
                old_vehicle_manufacturer = vehicle_manufacturer;
                old_vehicle_model = vehicle_model;
                androidx.appcompat.app.AppCompatDialogFragment file_selector = DialogFileRecall.newInstance();
                file_selector.setCancelable(false);
                file_selector.setTargetFragment(this, 0);
                file_selector.show(getFragmentManager(), "RecallFile");
            }
            break;

            case R.id.buttonFileSave: {
                androidx.appcompat.app.AppCompatDialogFragment file_selector = DialogFileSave.newInstance();
                file_selector.setCancelable(false);
                file_selector.setTargetFragment(this, 0);
                file_selector.show(getFragmentManager(), "SaveFile");
            }
            break;

// Removed and integrated into  the "Recall" button function because some dumb-ass might be confused
/*             case R.id.buttonFileApply:
                applyButton.setEnabled(false);                          // Disable button until another file is recalled
                mainActivity.SetNotAppliedFlag(false);
                mainActivity.DisplayProgressCountupDialog("Sending Data to AX-DSP", 80);
//                progress_dialog = new DialogProgress();                 // Display a Progress Bar while we send data
                TaskSendData send_data_task = new TaskSendData(mainActivity);
                send_data_task.SetPostCallBeck(this);
                send_data_task.execute();                         // Send all data to 386 in the background
                break;
*/
            case R.id.buttonIdentify:
                if (dspService != null) {
                    dspService.SendDspCommand(identify_command);
                    mainActivity.ShowSnackbar(configView, "Chime is Playing", Snackbar.LENGTH_LONG);
                }
                break;

            case R.id.buttonAbout:
                DialogAbout about_dialog = new DialogAbout();
                about_dialog.setCancelable(false);
                about_dialog.setTargetFragment(this, 0);
                about_dialog.show(getFragmentManager(), "About");
                break;


            case R.id.buttonVehicle:
                if (dspService != null) {
                    DialogVehicleSelect vehicle_select_dialog = new DialogVehicleSelect();
                    vehicle_select_dialog.setCancelable(false);
                    vehicle_select_dialog.setTargetFragment(this, 0);
                    Bundle args = new Bundle();
                    args.putString("manufacturer", vehicle_manufacturer);
                    args.putString("model", vehicle_model);
                    args.putBoolean("has_oe_amp", vehicle_has_oe_amp);
                    vehicle_select_dialog.setArguments(args);
                    vehicle_select_dialog.show(getFragmentManager(), "Vehicle Select");
                }
                break;

            case R.id.buttonSetPassword:
                if (dspService != null) {
                    DialogPassword password_dialog = new DialogPassword();
                    password_dialog.setCancelable(false);
                    password_dialog.setTargetFragment(this, 0);
//                    Bundle args = new Bundle();
                    password_dialog.SetType(DialogPassword.PASSWORD_DIALOG_TYPE.PASSWORD_DIALOG_SET);
                    password_dialog.show(getFragmentManager(), "Password");
                }
                break;

            case R.id.buttonClearPhones:
                TaskSendCommand taskCommand = new TaskSendCommand(dspService, this, clear_phone_command);
                EnableControls(false);
                taskCommand.execute();
                break;

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:

                if (resultCode == DIALOG_ACTION.DIALOG_ACTION_RECALL.ordinal()) {
                    String file_name = data.getAction();
                    try {
                        mainActivity.SetEqChannelsBands();
                        ReadConfigFile(file_name += ".xml", true);
//                        if (mainActivity.Connected())
//                            applyButton.setEnabled(true);               // Enable, since we have a file to apply
                        mainActivity.SetNotAppliedFlag(true);

                        mainActivity.ShowSnackbar(configView,"File '" + file_name + "' has been Recalled", Snackbar.LENGTH_SHORT);
/*                        if ((!GetVehicleManufacturerString().equalsIgnoreCase(old_vehicle_manufacturer)) || (!GetVehicleModelString().equalsIgnoreCase(old_vehicle_model))) {
                            String message = "Recalled File changed vehicle type from:\n" + old_vehicle_manufacturer + " " + old_vehicle_model
                                    + "\nto\n" + GetVehicleManufacturerString() + " " + GetVehicleModelString() ;
                            DialogAlert comm_error_alert = DialogAlert.newInstance("Vehicle Type Changed", message);
                            comm_error_alert.show(getFragmentManager(), "alert");
                        }
*/
                        // Apply new data from recalled file - was previously handled by the "Apply" button
                        mainActivity.SetNotAppliedFlag(false);
                        mainActivity.DisplayProgressCountupDialog("Sending Data to AX-DSP", 80);
//                progress_dialog = new DialogProgress();                 // Display a Progress Bar while we send data
                        TaskSendData send_data_task = new TaskSendData(mainActivity);
                        send_data_task.SetPostCallBeck(this);
                        send_data_task.execute();                         // Send all data to 3
                    } catch (XmlPullParserException | IOException e) {
                        e.printStackTrace();
                    }
                } else if (resultCode == DIALOG_ACTION.DIALOG_ACTION_SAVE.ordinal()) {
                    String file_name = data.getAction();
                    try {
                        WriteConfigFile(file_name += ".xml");
                        mainActivity.ShowSnackbar(configView,"File '" + file_name + "' has been Saved", Snackbar.LENGTH_LONG);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

//                else if (resultCode == DialogFileSave.DIALOG_ACTION_CANCEL){// we don't have anything to do in this case}

                break;
        }
    }

    public void IPostExecuteInterface(){
        Log.i("I'm Here", "okay");

    }


/*    public void SetMainActivity(MainActivity main_activity) {
        mainActivity = main_activity;
    }

    public void SetDspService(DspService service) {
        dspService = service;
    }
*/

    public void EnableControls(Boolean state) {

        if (defaultButton != null) {
            defaultButton.setEnabled(state);
            lockButton.setEnabled(state);
            vehicleTypeButton.setEnabled(state);
            identifyButton.setEnabled(state);
            passwordButton.setEnabled(state);
            aboutButton.setEnabled(true);
            clearPhonesLayout.setVisibility(View.GONE);
            if (mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440) {
                DspService dspService = mainActivity.dspService;
                if (dspService != null) {
                    if (dspService.isConnected()) {
                        clearPhonesLayout.setVisibility(View.VISIBLE);
                        clearPhoneButton.setEnabled(state);
                    }
                }
            }

            MainActivity main_activity = (MainActivity) getActivity();
            if (main_activity != null) {
                boolean file_save_enabled = main_activity.FileSaveIsEnabled();
                saveButton.setEnabled(file_save_enabled);
                recallButton.setEnabled(file_save_enabled);

            }
        }
    }


    /* Checks if external storage is available for read and write */
    private void CheckExternalStorage() {
        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWritable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWritable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWritable = false;
        }
    }

    // Send the vehicle type to the 386
    public int SendVehicleType() {
        int send_time = 0;
        try {
            DspService dspService = ((MainActivity) getActivity()).dspService;
            if (dspService != null) {

                InputStream input_stream = ((MainActivity) getActivity()).GetVehicleTypeStream();
                List<Integer> vehicle_ordinals = DialogVehicleSelect.GetModelOrdinal(input_stream, vehicle_manufacturer, vehicle_model, vehicle_has_oe_amp);
                int vehicle_id = vehicle_ordinals.get(0);
                int model_id = vehicle_ordinals.get(1);
                byte data[] = new byte[4];
                data[0] = (byte) (vehicle_id & 0xFF);
                data[1] = (byte) ((vehicle_id >> 8) & 0xFF);
                data[2] = (byte) (model_id & 0xFF);
                data[3] = (byte) ((model_id >> 8) & 0xFF);
                vehicle_type_command.SetData(data, 4);
                dspService.SendDspCommand(vehicle_type_command);
                send_time = dspService.WaitForAck();
            }
        } catch (XmlPullParserException | IOException e1) {
            e1.printStackTrace();
        }
        return(send_time);
    }

    public int SetCANPause(byte state) {
        int send_time = 0;
        try {
            DspService dspService = ((MainActivity) getActivity()).dspService;
            if (dspService != null) {
                pause_can_command.SetData(state);
                dspService.SendDspCommand(pause_can_command);
                send_time = dspService.WaitForAck();
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return(send_time);
    }


    // Read the XML configuration file and call each data-type's parser to extract its data
    private void ReadConfigFile(String file_name, Boolean external_file) throws XmlPullParserException, IOException, NullPointerException {
        XmlPullParser myParser = null;
        InputStream xml_stream = null;
        FileInputStream in_file = null;

        if (external_file) {                                            // If reading from an external file...
            String directory;
            if (mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
                directory = "/AX_DSP_LC/";
            else
                directory = "/AX_DSP_X/";
            File ext_fs = Environment.getExternalStorageDirectory();    // Get path to the external storage
            File dir = new File(ext_fs.getAbsolutePath() + directory); // Add our directory path to it
            dir.mkdir();                                                // Create this directory if not already created
            File file = new File(dir, file_name);                       // Create the file to which we will write the contents

/*            File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "axdsp");
            boolean is_directory = dir.isDirectory();
            if (!is_directory){
                if (!dir.mkdirs()) {
                    Log.i("WriteConfigFile", "Directory not created");
                }
            }
            File file = new File(dir, file_name);                       // Create the file to which we will write the contents
*/
            try {
                in_file = new FileInputStream(file);                        // Create an input stream to read from
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else
            xml_stream = getResources().getAssets().open("sample1.xml");   // Reading from an Asset file

        try {
            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            myParser = xmlFactoryObject.newPullParser();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        if (myParser != null) {
            if (external_file)
                myParser.setInput(in_file, null);
            else
                myParser.setInput(xml_stream, null);

            int event = myParser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {                   // Parse until the end of the document
                String name = myParser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:                           // We pass parsing to the fragments when we encounter their START tag
                        switch (name) {
                            case "configuration":
                                ParseConfigurationXmlData(myParser);
                                break;

                            case "equalizer":
                                ((MainActivity) getActivity()).EqualizerFragmentParseXML(myParser);
                                break;

                            case "crossover":
                                ((MainActivity) getActivity()).CrossoverFragmentParseXML(myParser);
                                break;

                            case "input_sources":
                            case "input_levels":
                            case "amp_turn_on":
                            case "input_channel":
                                ((MainActivity) getActivity()).InputFragmentParseXML(myParser);
                                break;

                            case "clipping_sensitivity":
                            case "chime_volume":
                                ((MainActivity) getActivity()).mLevelsFragment.ParseXmlData(myParser);
                                break;

                            case "speaker_delay":
                                ((MainActivity) getActivity()).DelayFragmentParseXML(myParser);
                                break;
                        }
                        break;

                    case XmlPullParser.END_TAG:
Log.i("ReadConfigFile", "END_TAG");
                        break;
                }
                event = myParser.next();                                    // Get next field
            }
            if (external_file) {
                if (in_file != null)
                    in_file.close();                                             // Done parsing the file... close it
            } else
                xml_stream.close();
        }
    }


    // Write the XML configuration file and call each data-type's Serializer to generate its XML data
    private void WriteConfigFile(String file_name) throws IOException {
        String directory;
        if (mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
            directory = "/AX_DSP_LC/";
        else
            directory= "/AX_DSP_X/";
        File ext_fs = Environment.getExternalStorageDirectory();                        // Get path to the external storage
        File dir = new File(ext_fs.getAbsolutePath() + directory);            // Add our directory path to it

//        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS), "axdsp");
        boolean is_directory = dir.isDirectory();
        if (!is_directory) {
            if (!dir.mkdirs()) {
                Log.i("WriteConfigFile", "Directory not created");
            }
        }

        File file = new File(dir, file_name);                                           // Create the file to which we will write the contents
        FileOutputStream out_file = new FileOutputStream(file);                         // Create an output stream to write to

        //noinspection ConstantConditions
        if (out_file != null) {
            XmlSerializer serializer = Xml.newSerializer();                             // Create an XML Serializer...
            serializer.setOutput(out_file, "UTF-8");                            // and bind our output file to it
            serializer.startDocument(null, true);
            serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

            serializer.startTag(null, "ax_dsp");                        // Write the root tag
            WriteConfigurationXml(serializer);                                          // Write Config data not associated with a fragment
            ((MainActivity) getActivity()).mLevelsFragment.WriteXml(serializer);        // Write the data for each of our fragments
            ((MainActivity) getActivity()).DelayFragmentWriteXML(serializer);
            ((MainActivity) getActivity()).InputFragmentWriteXML(serializer);
            ((MainActivity) getActivity()).CrossoverFragmentWriteXML(serializer);
            ((MainActivity) getActivity()).EqualizerFragmentWriteXML(serializer);

            serializer.endTag(null, "ax_dsp");                          // Write the end-root tag
            serializer.endDocument();                                                   // Close the document
            serializer.flush();                                                         // Write it to the file
            out_file.close();
            Log.i("File Length", Long.toString(file.length()));

        }
    }


    // Parses an XML file and updates the configuration data
    private void ParseConfigurationXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();
            if (event == XmlPullParser.END_TAG) {
                if (name.equals("vehicle_type")) {
                    vehicle_manufacturer = myParser.getAttributeValue(null, "manufacturer");
                    vehicle_model = myParser.getAttributeValue(null, "model");
                    vehicle_has_oe_amp = Boolean.valueOf(myParser.getAttributeValue(null, "has_oe_amp"));
                    done = true;
                } else if (name.equals("configuration"))
                    done = true;
            }
            event = myParser.next();
        }
    }


    // Write our configuration data to the XML file using the Serializer passed to us
    private void WriteConfigurationXml(XmlSerializer serializer) throws IOException {
        if (vehicle_manufacturer == null)
            vehicle_manufacturer = "?";

        if (vehicle_model == null)
            vehicle_model = "?";

        serializer.startTag(null, "configuration");         // Block Start Tag

        serializer.startTag(null, "vehicle_type");
        serializer.attribute(null, "manufacturer", vehicle_manufacturer);
        serializer.attribute(null, "model", vehicle_model);
        serializer.attribute(null, "has_oe_amp", Boolean.toString(vehicle_has_oe_amp));
        serializer.endTag(null, "vehicle_type");

        serializer.endTag(null, "configuration");         // Block End Tag
    }

    // Called just after we connect to a 386
    public void ConnectionComplete() {
        Log.i("ConnectionComplete", " ");
        mainActivity.ProgressBarDismiss();
        //dspService.ReadConnectedRssi();
        GetDspStatus();                                 // Fire off the command that read's the DSP On State
    }

    // Read the 386's DSP status - On or Off
    void GetDspStatus() {
        DspService dspService = mainActivity.GetDspService();

        DspCommand get_command = new DspCommand();
        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_DSP_STATUS);  // Before doing anything, check id the DSP is on
        dspService.GetDspData(get_command);
        dspService.WaitForData();
        Log.i("GetDspStatus", "-");
    }

    public void UpdateDspIsOnState(int state) {
        if (state == 1)
            Read386Data( this);                          // Read the Data from the DSP
        else {
            Read386Data(this);
        }
    }


    // Read all of the data from the 386
    private void Read386Data(IPostExecuteInterface post_activity) {
        DspService dspService = mainActivity.GetDspService();
        if (dspService != null) {
            TaskRead386DataX Read386Datax = new TaskRead386DataX(mainActivity, this);
            Read386Datax.dsp_service = dspService;
            Read386Datax.my_context = my_context;
            Read386Datax.execute();//execute the async task
        }
    }

 /*   private void DisplayProgressDialog(String title) {
        progress_dialog = new DialogProgress();                 // Display a Progress Bar while we wait for 386 to lock down data

        Bundle progress_title = new Bundle();
        progress_title.putString("TITLE", title);
        progress_dialog.setArguments(progress_title);
        progress_dialog.setCancelable(false);
        progress_dialog.show(getFragmentManager(), "DisplayProgress");
    }
*/


}
