package com.ckt_works.AX_DSP;

// Created by Abeck on 12/16/2016.

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class FragmentCrossover extends CScreenNavigation implements TextWatcher, EditText.OnEditorActionListener,
                                        View.OnFocusChangeListener,  View.OnTouchListener{ // RadioGroup.OnCheckedChangeListener,

    private final int MIN_FREQUENCY = 10;
    private final int MAX_FREQUENCY = 20000;

    // Declare our GUI members
    private EditText TextFocusHolder;           // Used to remove focus from TextEdit boxes
    private HorizontalSlider SliderCrossoverFront;
    private EditText TextFrequencyFront;
    private RadioGroup GroupSelectFrontPassType;
    private RadioButton RadioFrontLowPass;
    private RadioButton RadioFrontHighPass;

    private HorizontalSlider SliderCrossoverRear;
    private EditText TextFrequencyRear;
    private RadioGroup GroupSelectRearPassType;
    private RadioButton RadioRearLowPass;
    private RadioButton RadioRearHighPass;

    private HorizontalSlider SliderCrossoverWoofer;
    private EditText TextFrequencyWoofer;
    private RadioGroup GroupSelectWooferPassType;
    private RadioButton RadioWooferLowPass;
    private RadioButton RadioWooferHighPass;

    private View my_view;
    private InputMethodManager input_manager;

    // Declare our Data members
    private DspCrossover crossover_front;
    private DspCrossover crossover_rear;
    private DspCrossover crossover_woofer;
//    private DspService dspService;
    MainActivity mainActivity;

    private boolean inputting_data = false;


    // Declare the constants used to logarithmically scale the sliders
    private final double slider_min = 0;
    private final double slider_max = 1000;
    private final double freq_min = Math.log(10.1);
    private final double freq_max = Math.log(20000.1);
    private final double scale = (freq_max - freq_min) / (slider_max - slider_min);   // calculate adjustment factor


    public static FragmentCrossover newInstance() {
        return new FragmentCrossover();
    }

    public FragmentCrossover(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  (MainActivity) getActivity();

        my_view = inflater.inflate(R.layout.layout_crossover, container, false);
        if (my_view != null)
        {
            TextFocusHolder = (EditText) my_view.findViewById(R.id.textFocusholder);
            TextFocusHolder.setOnFocusChangeListener(this);

            // Instantiate our data
            crossover_front = new DspCrossover(0, CROSSOVER_TYPES.CROSSOVER_HIGH_PASS, 100, 100);
            crossover_rear = new DspCrossover(1, CROSSOVER_TYPES.CROSSOVER_HIGH_PASS, 100, 100);
            crossover_woofer = new DspCrossover(2, CROSSOVER_TYPES.CROSSOVER_LOW_PASS, 100, 100);

            // Instantiate our Controls
            SliderCrossoverFront = (HorizontalSlider) my_view.findViewById(R.id.sliderCrossoverFront);
            SliderCrossoverFront.setValue(0);       // This will force update of the frequency display
            SliderCrossoverFront.setImages(R.drawable.slider_bar_horz, R.drawable.slider_bar_horz_disabled);
            SliderCrossoverFront.setDelegateOnTouchListener(this);
            GroupSelectFrontPassType = (RadioGroup)my_view.findViewById(R.id.groupSelectFrontPass);
            RadioFrontLowPass = (RadioButton)my_view.findViewById(R.id.radioFrontLowPass);
            RadioFrontLowPass.setOnTouchListener(this);
            RadioFrontHighPass = (RadioButton)my_view.findViewById(R.id.radioFrontHighPass);
            RadioFrontHighPass.setOnTouchListener(this);
            TextFrequencyFront = (EditText) my_view.findViewById(R.id.textCrossoverFreqFront);
            TextFrequencyFront.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            TextFrequencyFront.setOnEditorActionListener(this);
            TextFrequencyFront.addTextChangedListener(this);
            TextFrequencyFront.setOnFocusChangeListener(this);


            SliderCrossoverRear = (HorizontalSlider) my_view.findViewById(R.id.sliderCrossoverRear);
            SliderCrossoverRear.setValue(0);       // This will force update of the frequency display
            SliderCrossoverRear.setImages(R.drawable.slider_bar_horz, R.drawable.slider_bar_horz_disabled);
            SliderCrossoverRear.setDelegateOnTouchListener(this);
            GroupSelectRearPassType = (RadioGroup)my_view.findViewById(R.id.groupSelectRearPass);
            RadioRearLowPass = (RadioButton)my_view.findViewById(R.id.radioRearLowPass);
            RadioRearLowPass.setOnTouchListener(this);
            RadioRearHighPass = (RadioButton)my_view.findViewById(R.id.radioRearHighPass);
            RadioRearHighPass.setOnTouchListener(this);
            TextFrequencyRear = (EditText) my_view.findViewById(R.id.textCrossoverFreqRear);
            TextFrequencyRear.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            TextFrequencyRear.setOnEditorActionListener(this);
            TextFrequencyRear.addTextChangedListener(this);
            TextFrequencyRear.setOnFocusChangeListener(this);

            SliderCrossoverWoofer = (HorizontalSlider) my_view.findViewById(R.id.sliderCrossoverWoofer);
            SliderCrossoverWoofer.setValue(0);       // This will force update of the frequency display
            SliderCrossoverWoofer.setImages(R.drawable.slider_bar_horz, R.drawable.slider_bar_horz_disabled);
            SliderCrossoverWoofer.setDelegateOnTouchListener(this);
            GroupSelectWooferPassType = (RadioGroup)my_view.findViewById(R.id.groupSelectWooferPass);
            RadioWooferLowPass = (RadioButton)my_view.findViewById(R.id.radioWooferLowPass);
            RadioWooferLowPass.setOnTouchListener(this);
            RadioWooferHighPass = (RadioButton)my_view.findViewById(R.id.radioWooferHighPass);
            RadioWooferHighPass.setOnTouchListener(this);
            TextFrequencyWoofer = (EditText) my_view.findViewById(R.id.textCrossoverFreqWoofer);
            TextFrequencyWoofer.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            TextFrequencyWoofer.setOnEditorActionListener(this);
            TextFrequencyWoofer.addTextChangedListener(this);
            TextFrequencyWoofer.setOnFocusChangeListener(this);

            input_manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            
            EnableControls(false);
            UpdateSliderPosition(SliderCrossoverFront);
            UpdateFreqTextBox(crossover_front);
            UpdateSliderPosition(SliderCrossoverRear);
            UpdateFreqTextBox(crossover_rear);
            UpdateSliderPosition(SliderCrossoverWoofer);
            UpdateFreqTextBox(crossover_woofer);

            TextFocusHolder.requestFocus();
        }
        return(my_view);
    }


    @Override
    // This function handles the Touch Event for all controls on this screen
    public boolean onTouch (View control, MotionEvent event) {
        EnableControls(false);
        DspService dspService = mainActivity.GetDspService();

        if (control.equals(SliderCrossoverFront)) {
            float progressValue = SliderCrossoverFront.getSliderValue();
            double frequency = Math.exp(freq_min + scale * (progressValue - slider_min));
            int display_freq = (int)frequency;
            TextFrequencyFront.setText(Integer.toString(display_freq));        // Update Frequency Display Text window
            crossover_front.SetFrequency(display_freq);
            dspService.SendDspCrossover(crossover_front);
        }

        else if (control.equals(SliderCrossoverRear)) {
            float progressValue = SliderCrossoverRear.getSliderValue();
            double frequency = Math.exp(freq_min + scale * (progressValue - slider_min));
            int display_freq = (int)frequency;            TextFrequencyRear.setText(Integer.toString(display_freq));
            crossover_rear.SetFrequency(display_freq);
            dspService.SendDspCrossover(crossover_rear);
        }

        else if (control.equals(SliderCrossoverWoofer)){
            float progressValue = SliderCrossoverWoofer.getSliderValue();
            double frequency = Math.exp(freq_min + scale * (progressValue - slider_min));
            int display_freq = (int)frequency;            TextFrequencyWoofer.setText(Integer.toString(display_freq));
            crossover_woofer.SetFrequency(display_freq);
            dspService.SendDspCrossover(crossover_woofer);
        }

        else if (control.equals(RadioFrontLowPass)) {
            RadioFrontLowPass.setChecked(true);
            SliderCrossoverFront.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            crossover_front.SetType(CROSSOVER_TYPES.CROSSOVER_LOW_PASS);
            UpdateSliderPosition(SliderCrossoverFront);
            UpdateFreqTextBox(crossover_front);
            dspService.SendDspCrossover(crossover_front);
        }

        else if (control.equals(RadioFrontHighPass)) {
            RadioFrontHighPass.setChecked(true);
            SliderCrossoverFront.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            crossover_front.SetType(CROSSOVER_TYPES.CROSSOVER_HIGH_PASS);
            UpdateSliderPosition(SliderCrossoverFront);
            UpdateFreqTextBox(crossover_front);
            dspService.SendDspCrossover(crossover_front);
        }
        else if (control.equals(RadioRearLowPass)) {
            RadioRearLowPass.setChecked(true);
            SliderCrossoverRear.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            crossover_rear.SetType(CROSSOVER_TYPES.CROSSOVER_LOW_PASS);
            UpdateSliderPosition(SliderCrossoverRear);
            UpdateFreqTextBox(crossover_rear);
            dspService.SendDspCrossover(crossover_rear);
        }

        else if (control.equals(RadioRearHighPass)) {
            RadioRearHighPass.setChecked(true);
            SliderCrossoverRear.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            crossover_rear.SetType(CROSSOVER_TYPES.CROSSOVER_HIGH_PASS);
            UpdateSliderPosition(SliderCrossoverRear);
            UpdateFreqTextBox(crossover_rear);
            dspService.SendDspCrossover(crossover_rear);
        }

        else if (control.equals(RadioWooferLowPass)) {
            RadioWooferLowPass.setChecked(true);
            SliderCrossoverWoofer.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            crossover_woofer.SetType(CROSSOVER_TYPES.CROSSOVER_LOW_PASS);
            UpdateSliderPosition(SliderCrossoverWoofer);
            UpdateFreqTextBox(crossover_woofer);
            dspService.SendDspCrossover(crossover_woofer);
        }
        else if (control.equals(RadioWooferHighPass)) {
            RadioWooferHighPass.setChecked(true);
            SliderCrossoverWoofer.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            crossover_woofer.SetType(CROSSOVER_TYPES.CROSSOVER_HIGH_PASS);
            UpdateSliderPosition(SliderCrossoverWoofer);
            UpdateFreqTextBox(crossover_woofer);
            dspService.SendDspCrossover(crossover_woofer);
        }



            return(true);
    }

    // Update Frequency Display Text window
    private void UpdateFreqTextBox(DspCrossover filter)
    {
         if (filter.equals(crossover_front))
            TextFrequencyFront.setText(Integer.toString(crossover_front.GetFrequency()));

        else if (filter.equals(crossover_rear))
            TextFrequencyRear.setText(Integer.toString(crossover_rear.GetFrequency()));

        if (filter.equals(crossover_woofer))
            TextFrequencyWoofer.setText(Integer.toString(crossover_woofer.GetFrequency()));
    }

/*    public void SetDspService(DspService service) {
         dspService = service;
    }
*/

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden)
            HideKeypad();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        int frequency = 0;
        switch (view.getId()) {
            case R.id.textCrossoverFreqFront:
                if (hasFocus)
                    LockControls(TextFrequencyFront, true);
                TextFrequencyFront.setCursorVisible(hasFocus);
                if (hasFocus)
                    inputting_data = true;
                else
                {
                    frequency = Integer.parseInt(TextFrequencyFront.getText().toString());
                    if (frequency > MAX_FREQUENCY) {
                        frequency = MAX_FREQUENCY;
                        TextFrequencyFront.setText(Integer.toString(frequency));
                    }
                    else if (frequency < MIN_FREQUENCY) {
                        frequency = MIN_FREQUENCY;
                        TextFrequencyFront.setText(Integer.toString(frequency));
                    }
                    crossover_front.SetFrequency(frequency);
                    UpdateSliderPosition(SliderCrossoverFront);
                    LockControls(TextFrequencyFront, false);
                    SendData(AUDIO_CHANNEL.FRONT);
                }
                break;

            case R.id.textCrossoverFreqRear:
                if (hasFocus)
                    LockControls(TextFrequencyRear, true);
                TextFrequencyRear.setCursorVisible(hasFocus);
                if (hasFocus)
                    inputting_data = true;
                else {
                    frequency = Integer.parseInt(TextFrequencyRear.getText().toString());
                    if (frequency > MAX_FREQUENCY) {
                        frequency = MAX_FREQUENCY;
                        TextFrequencyRear.setText(Integer.toString(frequency));
                    } else if (frequency < MIN_FREQUENCY) {
                        frequency = MIN_FREQUENCY;
                        TextFrequencyRear.setText(Integer.toString(frequency));
                    }
                    crossover_rear.SetFrequency(frequency);
                    UpdateSliderPosition(SliderCrossoverRear);
                    LockControls(TextFrequencyRear, false);
                    SendData(AUDIO_CHANNEL.REAR);
                }
                break;

            case R.id.textCrossoverFreqWoofer:
                if (hasFocus)
                    LockControls(TextFrequencyWoofer, true);
                TextFrequencyWoofer.setCursorVisible(hasFocus);
                if (hasFocus)
                    inputting_data = true;
                else
                    {
                    frequency = Integer.parseInt(TextFrequencyWoofer.getText().toString());
                    if (frequency > MAX_FREQUENCY) {
                        frequency = MAX_FREQUENCY;
                        TextFrequencyWoofer.setText(Integer.toString(frequency));
                    }
                    else if (frequency < MIN_FREQUENCY) {
                        frequency = MIN_FREQUENCY;
                        TextFrequencyWoofer.setText(Integer.toString(frequency));
                    }
                    crossover_woofer.SetFrequency(frequency);
                    UpdateSliderPosition(SliderCrossoverWoofer);
                    LockControls(TextFrequencyWoofer, false);
                    SendData(AUDIO_CHANNEL.SUBWOOFER);
                }
                break;
        }
    }


    @Override
    public boolean onEditorAction (TextView view, int actionId, KeyEvent event)
    {
        Log.i("----> onKey", Integer.toString(actionId) + " - "); // + Integer.toString(keyCode));
        HideKeypad();
        return(true);
    }


    @Override
    public void beforeTextChanged(CharSequence editable, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence editable, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (inputting_data) {
            int frequency = 0;
            boolean update_data = false;

            String data = editable.toString();
            int data_length = data.length();

            if (data.isEmpty())                             // Empty Text
                update_data = true;

            else {
                if (data.length() > 5) {                    // Limit length of text
                    data = data.substring(1);
                    update_data = true;

                }

                while (data.startsWith("0") && data.length() > 1)                 // Remove any leading zeros
                {
                    data = data.substring(1);
                    update_data = true;
                }

                if (data.isEmpty())                         // Data can't be empty
                    data = "0";
//                    data = Integer.toString(MIN_FREQUENCY);

                frequency = Integer.valueOf(data);          // Keep frequency in bounds
                if (frequency > MAX_FREQUENCY) {
                    data = data.substring(1);
                    frequency = Integer.valueOf(data);
                    update_data = true;
                }
/*
                if (frequency < MIN_FREQUENCY) {
                    frequency = MIN_FREQUENCY;
                    update_data = true;
                }
*/
            }

            if (update_data) {
                String str_frequency = Integer.toString(frequency);
                editable.replace(0, data_length, str_frequency);
//                TextFrequencyFront.setSelection(str_frequency.length());
            }
        }
    }

    public void UpdateSliderPosition(HorizontalSlider slider)
    {
        double frequency = 0;
        if (slider.equals(SliderCrossoverFront))
            frequency = crossover_front.GetFrequency();

        else if (slider.equals(SliderCrossoverRear))
            frequency = crossover_rear.GetFrequency();

        else if (slider.equals(SliderCrossoverWoofer))
            frequency = crossover_woofer.GetFrequency();

        double progress = (Math.log(frequency)-freq_min) / scale + slider_min;
        slider.setValue((int)progress);
    }

    private void LockControls(EditText active_control, boolean lock)
    {
//Log.i("LockControls", active_control.toString());
        SliderCrossoverFront.setEnabled(!lock);
        TextFrequencyFront.setEnabled((active_control.equals(TextFrequencyFront) ? true: !lock));
        for (int i = 0; i < GroupSelectFrontPassType.getChildCount(); i++) {
            GroupSelectFrontPassType.getChildAt(i).setEnabled(!lock);
        }

        SliderCrossoverRear.setEnabled(!lock);
        TextFrequencyRear.setEnabled((active_control.equals(TextFrequencyRear) ? true: !lock));
        for (int i = 0; i < GroupSelectRearPassType.getChildCount(); i++) {
            GroupSelectRearPassType.getChildAt(i).setEnabled(!lock);
        }

        SliderCrossoverWoofer.setEnabled(!lock);
        TextFrequencyWoofer.setEnabled((active_control.equals(TextFrequencyWoofer) ? true: !lock));
        for (int i = 0; i < GroupSelectWooferPassType.getChildCount(); i++) {
            GroupSelectWooferPassType.getChildAt(i).setEnabled(!lock);
        }
    }

    public void EnableControls(Boolean state) {
        if (SliderCrossoverFront != null) {
            SliderCrossoverFront.setEnabled(state);
            for (int i = 0; i < GroupSelectFrontPassType.getChildCount(); i++) {
                GroupSelectFrontPassType.getChildAt(i).setEnabled(state);
            }

            SliderCrossoverRear.setEnabled(state);
            for (int i = 0; i < GroupSelectRearPassType.getChildCount(); i++) {
                GroupSelectRearPassType.getChildAt(i).setEnabled(state);
            }

            SliderCrossoverWoofer.setEnabled(state);
            for (int i = 0; i < GroupSelectWooferPassType.getChildCount(); i++) {
                GroupSelectWooferPassType.getChildAt(i).setEnabled(state);
            }
        }
    }

    private void HideKeypad() {
        inputting_data = false;

        if (input_manager != null)
            input_manager.hideSoftInputFromWindow(my_view.getWindowToken(), 0);
        if (TextFocusHolder != null)
            TextFocusHolder.requestFocus();

    }

    private String GetCrossoverType(DspCrossover crossover_filter)
    {
        String str_type;
        if (crossover_filter.GetType() ==CROSSOVER_TYPES.CROSSOVER_HIGH_PASS)
            str_type = "highPass";
        else
            str_type = "lowPass";
        return(str_type);
    }



    // Sets the filter type and frequency for the indicated channel
    public void SetCrossoverFilterData(AUDIO_CHANNEL channel, CROSSOVER_TYPES filter_type, int low_frequency, int high_frequency) {
        if (channel == AUDIO_CHANNEL.FRONT) {
            crossover_front.SetData(filter_type, low_frequency, high_frequency);
            UpdateFreqTextBox(crossover_front);
            UpdateSliderPosition(SliderCrossoverFront);
            if (filter_type == CROSSOVER_TYPES.CROSSOVER_LOW_PASS) {
                RadioFrontLowPass.setChecked(true);
                SliderCrossoverFront.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            }
            else
            {
                RadioFrontHighPass.setChecked(true);
                SliderCrossoverFront.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            }
        }

        if (channel == AUDIO_CHANNEL.REAR) {
            crossover_rear.SetData(filter_type, low_frequency, high_frequency);
            UpdateFreqTextBox(crossover_rear);
            UpdateSliderPosition(SliderCrossoverRear);
            if (filter_type == CROSSOVER_TYPES.CROSSOVER_LOW_PASS) {
                RadioRearLowPass.setChecked(true);
                SliderCrossoverRear.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            }
            else
            {
                RadioRearHighPass.setChecked(true);
                SliderCrossoverRear.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            }
        }

        if (channel == AUDIO_CHANNEL.SUBWOOFER) {
            crossover_woofer.SetData(filter_type, low_frequency, high_frequency);
            UpdateFreqTextBox(crossover_woofer);
            UpdateSliderPosition(SliderCrossoverWoofer);
            if (filter_type == CROSSOVER_TYPES.CROSSOVER_LOW_PASS) {
                RadioWooferLowPass.setChecked(true);
                SliderCrossoverWoofer.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
            }
            else {
                RadioWooferHighPass.setChecked(true);
                SliderCrossoverWoofer.SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
            }
        }
    }


    // Send data for a single channel to the 386
    public boolean SendData(AUDIO_CHANNEL channel) {
        boolean sent = false;

        if (mainActivity.Connected()) {
            DspService dspService = mainActivity.GetDspService();
            if (channel == AUDIO_CHANNEL.FRONT) {
                dspService.SendDspCrossover(crossover_front);
                dspService.WaitForAck();
                sent = true;
            }

            else if (channel == AUDIO_CHANNEL.REAR) {
                dspService.SendDspCrossover(crossover_rear);
                dspService.WaitForAck();
                sent = true;
            }

            else if (channel == AUDIO_CHANNEL.SUBWOOFER) {
                dspService.SendDspCrossover(crossover_woofer);
                dspService.WaitForAck();
                sent = true;
            }
        }
        return(sent);
    }


    // Parses and XML file and updates the Crossover Filter data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("crossover"))
                {
                    String channel_name = myParser.getAttributeValue(null, "channel");
                    String type_string = myParser.getAttributeValue(null, "type");
                    CROSSOVER_TYPES type = CROSSOVER_TYPES.CROSSOVER_LOW_PASS;
                    if (type_string.contains("high"))
                        type = CROSSOVER_TYPES.CROSSOVER_HIGH_PASS;

                    int low_frequency = Integer.valueOf(myParser.getAttributeValue(null, "low_frequency"));
                    int high_frequency = Integer.valueOf(myParser.getAttributeValue(null, "high_frequency"));

                    if (channel_name.contains("front")) {
                        crossover_front.SetType(type);
                        crossover_front.SetLowFrequency(low_frequency);
                        crossover_front.SetHighFrequency(high_frequency);
                        UpdateFreqTextBox(crossover_front);
                        UpdateSliderPosition(SliderCrossoverFront);
                    }
                    else if (channel_name.contains("rear")) {
                        crossover_rear.SetType(type);
                        crossover_rear.SetLowFrequency(low_frequency);
                        crossover_rear.SetHighFrequency(high_frequency);
                        UpdateFreqTextBox(crossover_rear);
                        UpdateSliderPosition(SliderCrossoverRear);
                    }

                    else if (channel_name.contains("woofer") || channel_name.contains("sub")) {
                        crossover_woofer.SetType(type);
                        crossover_woofer.SetLowFrequency(low_frequency);
                        crossover_woofer.SetHighFrequency(high_frequency);
                        UpdateFreqTextBox(crossover_woofer);
                        UpdateSliderPosition(SliderCrossoverWoofer);
                    }

                    done = true;
                }
            }
            event = myParser.next();
        }
    }



    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        serializer.startTag(null, "crossover");                     // Write the front crossover data
        serializer.attribute(null, "channel", "front");
        serializer.attribute(null, "type", GetCrossoverType(crossover_front));
        serializer.attribute(null, "low_frequency", Integer.toString(crossover_front.GetLowFrequency()));
        serializer.attribute(null, "high_frequency", Integer.toString(crossover_front.GetHighFrequency()));
        serializer.endTag(null, "crossover");

        serializer.startTag(null, "crossover");                     // Write the rear crossover data
        serializer.attribute(null, "channel", "rear");
        serializer.attribute(null, "type", GetCrossoverType(crossover_rear));
        serializer.attribute(null, "low_frequency", Integer.toString(crossover_rear.GetLowFrequency()));
        serializer.attribute(null, "high_frequency", Integer.toString(crossover_rear.GetHighFrequency()));
        serializer.endTag(null, "crossover");

        serializer.startTag(null, "crossover");                     // Write the sub woofer crossover data
        serializer.attribute(null, "channel", "woofer");
        serializer.attribute(null, "type", GetCrossoverType(crossover_woofer));
        serializer.attribute(null, "low_frequency", Integer.toString(crossover_woofer.GetLowFrequency()));
        serializer.attribute(null, "high_frequency", Integer.toString(crossover_woofer.GetHighFrequency()));
        serializer.endTag(null, "crossover");
    }

}


