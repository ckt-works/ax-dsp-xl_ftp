package com.ckt_works.AX_DSP;

// Created by Abeck on 12/16/2016.

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class FragmentCrossover_X extends CScreenNavigation implements TextWatcher, EditText.OnEditorActionListener,
                                        View.OnFocusChangeListener,  View.OnTouchListener, RadioButton.OnCheckedChangeListener {

    private final int MIN_FREQUENCY = 10;
    private final int MAX_FREQUENCY = 20000;

    // Declare our GUI members
    private EditText TextFocusHolder;           // Used to remove focus from TextEdit boxes

    private TextView TextNoOutptsAssigned;


    // These arrays hold the id's of all the crossover controls
    private int layout_crossover_ids[] = {R.id.LayoutCrossover_1, R.id.LayoutCrossover_2, R.id.LayoutCrossover_3, R.id.LayoutCrossover_4, R.id.LayoutCrossover_5,
                                          R.id.LayoutCrossover_6, R.id.LayoutCrossover_7, R.id.LayoutCrossover_8, R.id.LayoutCrossover_9, R.id.LayoutCrossover_10};

    private int text_channel_name_ids[] = {R.id.textChannelName_1, R.id.textChannelName_2, R.id.textChannelName_3, R.id.textChannelName_4, R.id.textChannelName_5,
                                           R.id.textChannelName_6, R.id.textChannelName_7, R.id.textChannelName_8, R.id.textChannelName_9, R.id.textChannelName_10};

    private int radio_group_ids[]  = {R.id.radioGroup_1, R.id.radioGroup_2, R.id.radioGroup_3, R.id.radioGroup_4, R.id.radioGroup_5,
                                      R.id.radioGroup_6, R.id.radioGroup_7, R.id.radioGroup_8, R.id.radioGroup_9, R.id.radioGroup_10};
    private int radio_lowPass_ids[] = {R.id.radioLowPass_1, R.id.radioLowPass_2, R.id.radioLowPass_3, R.id.radioLowPass_4, R.id.radioLowPass_5,
                                       R.id.radioLowPass_6, R.id.radioLowPass_7, R.id.radioLowPass_8, R.id.radioLowPass_9, R.id.radioLowPass_10};
    private int radio_bandPass_ids[] = {R.id.radioBandpassPass_1, R.id.radioBandpassPass_2, R.id.radioBandpassPass_3, R.id.radioBandpassPass_4, R.id.radioBandpassPass_5,
                                        R.id.radioBandpassPass_6, R.id.radioBandpassPass_7, R.id.radioBandpassPass_8, R.id.radioBandpassPass_9, R.id.radioBandpassPass_10};
    private int radio_highPass_ids[] = {R.id.radioHighpassPass_1, R.id.radioHighpassPass_2, R.id.radioHighpassPass_3, R.id.radioHighpassPass_4, R.id.radioHighpassPass_5,
                                        R.id.radioHighpassPass_6, R.id.radioHighpassPass_7, R.id.radioHighpassPass_8, R.id.radioHighpassPass_9, R.id.radioHighpassPass_10};

    private int radio_12Db_ids[] = {R.id.radio12Db_1, R.id.radio12Db_2, R.id.radio12Db_3, R.id.radio12Db_4, R.id.radio12Db_5,
                                    R.id.radio12Db_6, R.id.radio12Db_7, R.id.radio12Db_8, R.id.radio12Db_9, R.id.radio12Db_10};

    private int radio_24Db_ids[] = {R.id.radio24Db_1, R.id.radio24Db_2, R.id.radio24Db_3, R.id.radio24Db_4, R.id.radio24Db_5,
                                    R.id.radio24Db_6, R.id.radio24Db_7, R.id.radio24Db_8, R.id.radio24Db_9, R.id.radio24Db_10};
    
    private int radio_36Db_ids[] = {R.id.radio36Db_1, R.id.radio36Db_2, R.id.radio36Db_3, R.id.radio36Db_4, R.id.radio36Db_5,
                                    R.id.radio36Db_6, R.id.radio36Db_7, R.id.radio36Db_8, R.id.radio36Db_9, R.id.radio36Db_10};
    
    private int radio_48Db_ids[] = {R.id.radio48Db_1, R.id.radio48Db_2, R.id.radio48Db_3, R.id.radio48Db_4, R.id.radio48Db_5,
                                    R.id.radio48Db_6, R.id.radio48Db_7, R.id.radio48Db_8, R.id.radio48Db_9, R.id.radio48Db_10};

    private int layout_low_freq_ids[] = {R.id.LayoutLowerFreq_1, R.id.LayoutLowerFreq_2, R.id.LayoutLowerFreq_3, R.id.LayoutLowerFreq_4, R.id.LayoutLowerFreq_5,
                                         R.id.LayoutLowerFreq_6, R.id.LayoutLowerFreq_7, R.id.LayoutLowerFreq_8, R.id.LayoutLowerFreq_9, R.id.LayoutLowerFreq_10};
    private int label_low_freq_ids[] = {R.id.LabelFrequencyLowPass_1, R.id.LabelFrequencyLowPass_2, R.id.LabelFrequencyLowPass_3, R.id.LabelFrequencyLowPass_4, R.id.LabelFrequencyLowPass_5,
                                        R.id.LabelFrequencyLowPass_6, R.id.LabelFrequencyLowPass_7, R.id.LabelFrequencyLowPass_8, R.id.LabelFrequencyLowPass_9, R.id.LabelFrequencyLowPass_10};
    private int edit_low_freq_ids[] = {R.id.TextFrequencyLowPass_1, R.id.TextFrequencyLowPass_2, R.id.TextFrequencyLowPass_3, R.id.TextFrequencyLowPass_4, R.id.TextFrequencyLowPass_5,
                                       R.id.TextFrequencyLowPass_6, R.id.TextFrequencyLowPass_7, R.id.TextFrequencyLowPass_8, R.id.TextFrequencyLowPass_9, R.id.TextFrequencyLowPass_10};
    
    private int layout_high_freq_ids[] = {R.id.LayoutUpperFreq_1, R.id.LayoutUpperFreq_2, R.id.LayoutUpperFreq_3, R.id.LayoutUpperFreq_4, R.id.LayoutUpperFreq_5,
                                          R.id.LayoutUpperFreq_6, R.id.LayoutUpperFreq_7, R.id.LayoutUpperFreq_8, R.id.LayoutUpperFreq_9, R.id.LayoutUpperFreq_10};
    private int label_high_freq_ids[] = {R.id.LabelFrequencyHighPass_1, R.id.LabelFrequencyHighPass_2, R.id.LabelFrequencyHighPass_3, R.id.LabelFrequencyHighPass_4, R.id.LabelFrequencyHighPass_5,
                                         R.id.LabelFrequencyHighPass_6, R.id.LabelFrequencyHighPass_7, R.id.LabelFrequencyHighPass_8, R.id.LabelFrequencyHighPass_9, R.id.LabelFrequencyHighPass_10};
    private int edit_high_freq_ids[] = {R.id.TextFrequencyHighPass_1, R.id.TextFrequencyHighPass_2, R.id.TextFrequencyHighPass_3, R.id.TextFrequencyHighPass_4, R.id.TextFrequencyHighPass_5,
                                        R.id.TextFrequencyHighPass_6, R.id.TextFrequencyHighPass_7, R.id.TextFrequencyHighPass_8, R.id.TextFrequencyHighPass_9, R.id.TextFrequencyHighPass_10};
    
    private int slider_ids[] = {R.id.sliderCrossover_1, R.id.sliderCrossover_2, R.id.sliderCrossover_3, R.id.sliderCrossover_4, R.id.sliderCrossover_5,
                                R.id.sliderCrossover_6, R.id.sliderCrossover_7, R.id.sliderCrossover_8, R.id.sliderCrossover_9, R.id.sliderCrossover_10};
    
    // These arrays contain references to all of the crossover controls
    private LinearLayout[] LayoutCrossovers;

    private TextView[] TextChannelNames;

    private RadioGroup[] RadioGroups;
    private RadioButton[] RadioLowPass;
    private RadioButton[] RadioBandPass;
    private RadioButton[] RadioHighPass;

    private RadioButton[] Radio12Db;
    private RadioButton[] Radio24Db;
    private RadioButton[] Radio36Db;
    private RadioButton[] Radio48Db;

    private LinearLayout[] LayoutFrequency;
    private LinearLayout[] LayoutFrequencyLowPass;
    private TextView[] LabelFrequencyLowPass;
    private EditText[] EditFrequencyLowPass;  
    
    private LinearLayout[] LayoutFrequencyHighPass;
    private TextView[] LabelFrequencyHighPass;
    private EditText[] EditFrequencyHighPass;

    private HorizontalSlider_2_thumb[] SliderCrossovers;
    private View my_view;
    private InputMethodManager input_manager;


    // Declare our Data members
    private DspCrossover[] crossoverFilters;
    MainActivity mainActivity;

    private boolean inputting_data = false;


    // Declare the constants used to logarithmically scale the sliders
    private final double slider_min = 0;
    private final double slider_max = 1000;
    private final double freq_min = Math.log(10.1);
    private final double freq_max = Math.log(20000.1);
    private final double scale = (freq_max - freq_min) / (slider_max - slider_min);   // calculate adjustment factor

    int last_display_frequency_lower[] = new int[10];
    int last_display_frequency_upper[] = new int[10];

    private int numberSupportedChannels = layout_crossover_ids.length;

    public static FragmentCrossover_X newInstance() {
        return new FragmentCrossover_X();
    }

    //public FragmentCrossover_X(int channels){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        crossoverFilters = new DspCrossover[10];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  (MainActivity) getActivity();

        my_view = inflater.inflate(R.layout.layout_crossover_x, container, false);
        if (my_view != null)
        {
            TextFocusHolder = (EditText) my_view.findViewById(R.id.textFocusholder);
            TextFocusHolder.setOnFocusChangeListener(this);

            TextNoOutptsAssigned = (TextView) my_view.findViewById(R.id.textNoOutputAssigned);

            // Instantiate our Controls & Data Objects
            LayoutCrossovers = new LinearLayout[layout_crossover_ids.length];

//            crossoverFilters = new DspCrossover[layout_crossover_ids.length];

            TextChannelNames = new TextView[layout_crossover_ids.length];

            RadioGroups = new RadioGroup[layout_crossover_ids.length];
            RadioLowPass = new RadioButton[layout_crossover_ids.length];
            RadioBandPass = new RadioButton[layout_crossover_ids.length];
            RadioHighPass = new RadioButton[layout_crossover_ids.length];

            Radio12Db = new RadioButton[layout_crossover_ids.length];
            Radio24Db = new RadioButton[layout_crossover_ids.length];
            Radio36Db = new RadioButton[layout_crossover_ids.length];
            Radio48Db = new RadioButton[layout_crossover_ids.length];

            LayoutFrequency = new LinearLayout[layout_crossover_ids.length];
            LayoutFrequencyLowPass = new LinearLayout[layout_crossover_ids.length];
            LabelFrequencyLowPass = new TextView[layout_crossover_ids.length];
            EditFrequencyLowPass = new EditText[layout_crossover_ids.length];
            
            LayoutFrequencyHighPass = new LinearLayout[layout_crossover_ids.length];
            LabelFrequencyHighPass = new TextView[layout_crossover_ids.length];
            EditFrequencyHighPass = new EditText[layout_crossover_ids.length];

            SliderCrossovers = new HorizontalSlider_2_thumb[layout_crossover_ids.length];

            int low_frequency = 100;
            int high_frequency = 2000;
            for (int channel = 0; channel < layout_crossover_ids.length; channel++) {
                crossoverFilters[channel] =  new DspCrossover(channel, CROSSOVER_TYPES.CROSSOVER_HIGH_PASS, low_frequency, high_frequency);
                LayoutCrossovers[channel] = (LinearLayout) my_view.findViewById(layout_crossover_ids[channel]);

                TextChannelNames[channel] = (TextView) my_view.findViewById(text_channel_name_ids[channel]);
                TextChannelNames[channel].setText(mainActivity.mOutputs_X_Fragment.GetLocationName(channel));

                // TODO DO WE NEED THE RADIO  GROUP??
                RadioGroups[channel] = (RadioGroup) my_view.findViewById(radio_group_ids[channel]);
                RadioGroups[channel].setEnabled(true);

                RadioLowPass[channel] = (RadioButton) my_view.findViewById(radio_lowPass_ids[channel]);
                RadioLowPass[channel].setOnCheckedChangeListener(this);
                RadioLowPass[channel].setEnabled(true);
                RadioBandPass[channel] = (RadioButton) my_view.findViewById(radio_bandPass_ids[channel]);
                RadioBandPass[channel].setOnCheckedChangeListener(this);
                RadioBandPass[channel].setEnabled(true);
                RadioHighPass[channel] = (RadioButton) my_view.findViewById(radio_highPass_ids[channel]);
                RadioHighPass[channel].setOnCheckedChangeListener(this);
                RadioHighPass[channel].setEnabled(true);

                Radio12Db[channel] = (RadioButton) my_view.findViewById(radio_12Db_ids[channel]);
                Radio12Db[channel].setOnCheckedChangeListener(this);
                Radio12Db[channel].setEnabled(true);

                Radio24Db[channel] = (RadioButton) my_view.findViewById(radio_24Db_ids[channel]);
                Radio24Db[channel].setOnCheckedChangeListener(this);
                Radio24Db[channel].setEnabled(true);

                Radio36Db[channel] = (RadioButton) my_view.findViewById(radio_36Db_ids[channel]);
                Radio36Db[channel].setOnCheckedChangeListener(this);
                Radio36Db[channel].setEnabled(true);

                Radio48Db[channel] = (RadioButton) my_view.findViewById(radio_48Db_ids[channel]);
                Radio48Db[channel].setOnCheckedChangeListener(this);
                Radio48Db[channel].setEnabled(true);

                LayoutFrequencyLowPass[channel] = (LinearLayout) my_view.findViewById(layout_low_freq_ids[channel]);
                LabelFrequencyLowPass[channel] = (TextView) my_view.findViewById(label_low_freq_ids[channel]);
                EditFrequencyLowPass[channel] = (EditText) my_view.findViewById(edit_low_freq_ids[channel]);
                EditFrequencyLowPass[channel].setRawInputType(InputType.TYPE_CLASS_NUMBER);
                EditFrequencyLowPass[channel].setOnEditorActionListener(this);
                EditFrequencyLowPass[channel].addTextChangedListener(this);
                EditFrequencyLowPass[channel].setOnFocusChangeListener(this);

                LayoutFrequencyHighPass [channel] = (LinearLayout) my_view.findViewById(layout_high_freq_ids[channel]);
                LabelFrequencyHighPass[channel] = (TextView) my_view.findViewById(label_high_freq_ids[channel]);
                EditFrequencyHighPass[channel] = (EditText) my_view.findViewById(edit_high_freq_ids[channel]);
                EditFrequencyHighPass[channel].setRawInputType(InputType.TYPE_CLASS_NUMBER);
                EditFrequencyHighPass[channel].setOnEditorActionListener(this);
                EditFrequencyHighPass[channel].addTextChangedListener(this);
                EditFrequencyHighPass[channel].setOnFocusChangeListener(this);

                SliderCrossovers[channel] = (HorizontalSlider_2_thumb) my_view.findViewById(slider_ids[channel]);
                SliderCrossovers[channel].setValue(SLIDER_VALUE.SLIDER_LOWER, low_frequency);       // This will force update of the frequency display
                SliderCrossovers[channel].setDelegateOnTouchListener(this);
                SliderCrossovers[channel].setEnabled(true);

                EditFrequencyLowPass[channel].setText(Integer.toString(low_frequency));
                EditFrequencyHighPass[channel].setText(Integer.toString(high_frequency));
//                UpdateSliderPosition(SliderCrossovers[channel], crossoverFilters[channel].GetLowFrequency());
                SliderCrossovers[channel].setValue(SLIDER_VALUE.SLIDER_UPPER, crossoverFilters[channel].GetHighFrequency());       // This will force update of the frequency display
            }

            input_manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            EnableControls(false);
            TextFocusHolder.requestFocus();
        }
        return(my_view);
    }


    @Override
    // Invoked when the user clicks on any of the radio buttons
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
        if (mainActivity.GetIsReading() == false) {
            if (isChecked) {
                DspService dspService = mainActivity.GetDspService();
                for (int index = 0; index < numberSupportedChannels; index++) {

                    int buttonId = buttonView.getId();
                    if (buttonId == radio_lowPass_ids[index]) {
                        SliderCrossovers[index].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
                        crossoverFilters[index].SetType(CROSSOVER_TYPES.CROSSOVER_LOW_PASS);
                        crossoverFilters[index].SetLowFrequency(100);
                        LayoutFrequencyLowPass[index].setVisibility(View.VISIBLE);
                        LayoutFrequencyHighPass[index].setVisibility(View.INVISIBLE);
                        LabelFrequencyLowPass[index].setText("Upper Freq:");
                        UpdateSliderPosition(SliderCrossovers[index], SLIDER_VALUE.SLIDER_LOWER, crossoverFilters[index].GetLowFrequency());
                        EditText low_freq_text = EditFrequencyLowPass[index];
                        low_freq_text.setText(Integer.toString(crossoverFilters[index].GetLowFrequency()));
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }
                    else if (buttonId == radio_bandPass_ids[index]) {
                        SliderCrossovers[index].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_BETWEEN);
                        crossoverFilters[index].SetType(CROSSOVER_TYPES.CROSSOVER_BAND_PASS);
                        crossoverFilters[index].SetLowFrequency(100);
                        crossoverFilters[index].SetHighFrequency(2000);
                        LayoutFrequencyLowPass[index].setVisibility(View.VISIBLE);
                        LayoutFrequencyHighPass[index].setVisibility(View.VISIBLE);
                        LabelFrequencyLowPass[index].setText("Lower Freq:");
                        LabelFrequencyHighPass[index].setText("Upper Freq:");
                        UpdateSliderPosition(SliderCrossovers[index], SLIDER_VALUE.SLIDER_LOWER, crossoverFilters[index].GetLowFrequency());
                        EditText low_freq_text = EditFrequencyLowPass[index];
                        low_freq_text.setText(Integer.toString(crossoverFilters[index].GetLowFrequency()));

                        UpdateSliderPosition(SliderCrossovers[index], SLIDER_VALUE.SLIDER_UPPER, crossoverFilters[index].GetHighFrequency());
                        EditText high_freq_text = EditFrequencyHighPass[index];
                        high_freq_text.setText(Integer.toString(crossoverFilters[index].GetHighFrequency()));
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);

                    }
                    else if (buttonId == radio_highPass_ids[index]) {
                        SliderCrossovers[index].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
                        crossoverFilters[index].SetType(CROSSOVER_TYPES.CROSSOVER_HIGH_PASS);
                        crossoverFilters[index].SetHighFrequency(100);
                        LayoutFrequencyLowPass[index].setVisibility(View.INVISIBLE);
                        LayoutFrequencyHighPass[index].setVisibility(View.VISIBLE);
                        LabelFrequencyHighPass[index].setText("Lower Freq:");
                        UpdateSliderPosition(SliderCrossovers[index], SLIDER_VALUE.SLIDER_UPPER, crossoverFilters[index].GetHighFrequency());
                        EditText high_freq_text = EditFrequencyHighPass[index];
                        high_freq_text.setText(Integer.toString(crossoverFilters[index].GetHighFrequency()));
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }
                    else if (buttonId == radio_12Db_ids[index]) {
                        crossoverFilters[index].SetSlope(12);
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }
                    else if (buttonId == radio_24Db_ids[index]) {
                        crossoverFilters[index].SetSlope(24);
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }
                    else if (buttonId == radio_36Db_ids[index]) {
                        crossoverFilters[index].SetSlope(36);
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }
                    else if (buttonId == radio_48Db_ids[index]) {
                        crossoverFilters[index].SetSlope(48);
                        if (dspService != null)
                            dspService.SendDspCrossover(crossoverFilters[index]);
                    }

                }
            }
        }
    }


    @Override
    // This function handles the Touch Event for all controls on this screen
    public boolean onTouch (View control, MotionEvent event) {

//        EnableControls(false);
        DspService dspService = mainActivity.GetDspService();

        int container_id = R.id.ScrollViewCrossovers;
        int control_id = control.getId();
        int action = event.getAction();
        ViewParent parent = control.getParent();

        if  (action == MotionEvent.ACTION_DOWN) {                    // This prevents the Scrollview from capturing events while the slider is being moved
            parent.requestDisallowInterceptTouchEvent(true);
        }
        else if(action == MotionEvent.ACTION_UP)
            parent.requestDisallowInterceptTouchEvent(false);

        if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE || action == MotionEvent.ACTION_UP) {
            for (int channel = 0; channel < numberSupportedChannels; channel++) {

                if (control_id == slider_ids[channel]) {
                    float progressValue = 0;
                    HorizontalSlider_2_thumb this_slider = SliderCrossovers[channel];
                    progressValue = this_slider.getSliderValue(SLIDER_VALUE.SLIDER_LOWER);
                    double frequency_lower = Math.exp(freq_min + scale * (progressValue - slider_min));
                    int display_freq_lower = (int) frequency_lower;
                    progressValue = this_slider.getSliderValue(SLIDER_VALUE.SLIDER_UPPER);
                    double frequency_upper = Math.exp(freq_min + scale * (progressValue - slider_min));
                    int display_freq_upper = (int) frequency_upper;

                    if ((last_display_frequency_lower[channel] != display_freq_lower) || (last_display_frequency_upper[channel] != display_freq_upper)) {
                        //EnableControls(false);
                        last_display_frequency_lower[channel] = display_freq_lower;
                        last_display_frequency_upper[channel] = display_freq_upper;
                        EditText low_freq_text = EditFrequencyLowPass[channel];
                        EditText high_freq_text = EditFrequencyHighPass[channel];
                        switch (this_slider.GetSliderType()) {
                            case HORZ_SLIDER_LEFT: {

                                low_freq_text.setText(Integer.toString(display_freq_lower));        // Update Frequency Display Text window
                                crossoverFilters[channel].SetLowFrequency(display_freq_lower);
                            }
                            break;

                            case HORZ_SLIDER_FULL: {
                                low_freq_text.setText(Integer.toString(display_freq_lower));        // Update Frequency Display Text window
                                crossoverFilters[channel].SetLowFrequency(display_freq_lower);
                            }
                            break;

                            case HORZ_SLIDER_BETWEEN: {
                                low_freq_text.setText(Integer.toString(display_freq_lower));        // Update Frequency Display Text window
                                crossoverFilters[channel].SetLowFrequency(display_freq_lower);

                                high_freq_text.setText(Integer.toString(display_freq_upper));        // Update Frequency Display Text window
                                crossoverFilters[channel].SetHighFrequency(display_freq_upper);
                            }
                            break;

                            case HORZ_SLIDER_RIGHT: {

                                high_freq_text.setText(Integer.toString(display_freq_upper));        // Update Frequency Display Text window
                                crossoverFilters[channel].SetHighFrequency(display_freq_upper);
                            }
                            break;
                        }
                    }
                    if (action == MotionEvent.ACTION_UP) {                                            // Send Data on Up Evant
//                        Log.i("onTouch", "ACTION UP");
                        EnableControls(false);
                        dspService.SendDspCrossover(crossoverFilters[channel]);
                        dspService.WaitForAck();
                    }
                    return(true);
                }
            }
         }
        return(true);
    }


    @Override
    public void onViewCreated (View view,Bundle savedInstanceState) {
        Log.i("onViewCreated", "X");
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden)
            HideKeypad();
        else {
            numberSupportedChannels = mainActivity.GetNumberSupportedChannels();

            if (mainActivity.AnyChannelAssigned() == false) {
                for (int channel = 0; channel < layout_crossover_ids.length; channel++)
                    LayoutCrossovers[channel].setVisibility(View.GONE);
                TextNoOutptsAssigned.setVisibility(View.VISIBLE);
            }
            else {
                TextNoOutptsAssigned.setVisibility(View.GONE);
                for (int index = 0; index < layout_crossover_ids.length; index++) {
                    if (index >= numberSupportedChannels)
                        LayoutCrossovers[index].setVisibility(View.GONE);

                    else {
                        TextNoOutptsAssigned.setVisibility(View.GONE);
                        TextChannelNames[index].setText(mainActivity.mOutputs_X_Fragment.GetLocationName(index));
                        LayoutCrossovers[index].setVisibility((mainActivity.mOutputs_X_Fragment.IsUsed(index) ? View.VISIBLE : View.GONE));

                        int frequency = crossoverFilters[index].GetLowFrequency();
                        double progress = (Math.log(frequency) - freq_min) / scale + slider_min;
                        SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_LOWER, (int) progress);
                        EditFrequencyLowPass[index].setText(Integer.toString(frequency));

                        frequency = crossoverFilters[index].GetHighFrequency();
                        progress = (Math.log(frequency) - freq_min) / scale + slider_min;
                        SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_UPPER, (int) progress);
                        EditFrequencyHighPass[index].setText(Integer.toString(frequency));
                    }
                }
            }
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        int frequency = 0;
        DspService dspService = mainActivity.GetDspService();
        int id = view.getId();
        for (int index = 0; index < numberSupportedChannels; index++) {
            if (id == edit_low_freq_ids[index]) {
                if (hasFocus)
                    LockControls(EditFrequencyLowPass[index], true);
                EditFrequencyLowPass[index].setCursorVisible(hasFocus);

                if (hasFocus)
                    inputting_data = true;
                else {
                    int low_frequency = Integer.parseInt(EditFrequencyLowPass[index].getText().toString());
                    if (low_frequency > MAX_FREQUENCY) {
                        low_frequency = MAX_FREQUENCY;
                        EditFrequencyLowPass[index].setText(Integer.toString(low_frequency));
                    } else if (low_frequency < MIN_FREQUENCY) {
                        low_frequency = MIN_FREQUENCY;
                        EditFrequencyLowPass[index].setText(Integer.toString(low_frequency));
                    }
                    crossoverFilters[index].SetLowFrequency(low_frequency);
                    // TODO FIX THIS
                    //SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_LOWER, (int)low_frequency);
                    //UpdateSliderPosition(SliderCrossovers[index], low_frequency);
                    double progress = (Math.log(low_frequency)-freq_min) / scale + slider_min;
                    SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_LOWER, (int)progress);
                    LockControls(EditFrequencyLowPass[index], false);
                    dspService.SendDspCrossover(crossoverFilters[index]);
                    dspService.WaitForAck();
                }
                break;
            }

            else if (id == edit_high_freq_ids[index]) {
                if (hasFocus)
                    LockControls(EditFrequencyHighPass[index], true);
                EditFrequencyHighPass[index].setCursorVisible(hasFocus);

                if (hasFocus)
                    inputting_data = true;
                else {
                    int high_frequency = Integer.parseInt(EditFrequencyHighPass[index].getText().toString());
                    if (high_frequency > MAX_FREQUENCY) {
                        high_frequency = MAX_FREQUENCY;
                        EditFrequencyHighPass[index].setText(Integer.toString(high_frequency));
                    } else if (high_frequency < MIN_FREQUENCY) {
                        high_frequency = MIN_FREQUENCY;
                        EditFrequencyHighPass[index].setText(Integer.toString(high_frequency));
                    }
                    crossoverFilters[index].SetHighFrequency(high_frequency);
                    // TODO FIX THIS
                    //SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_LOWER, (int)high_frequency);
                    //UpdateSliderPosition(SliderCrossovers[index], high_frequency);
                    double progress = (Math.log(high_frequency)-freq_min) / scale + slider_min;
                    SliderCrossovers[index].setValue(SLIDER_VALUE.SLIDER_UPPER, (int)progress);
                    LockControls(EditFrequencyHighPass[index], false);
                    dspService.SendDspCrossover(crossoverFilters[index]);
                    dspService.WaitForAck();
                }
                break;
            }
        }
    }


    @Override
    public boolean onEditorAction (TextView view, int actionId, KeyEvent event)
    {
        Log.i("----> onKey", Integer.toString(actionId) + " - "); // + Integer.toString(keyCode));
        HideKeypad();
        return(true);
    }


    @Override
    public void beforeTextChanged(CharSequence editable, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence editable, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (inputting_data) {
            int frequency = 0;
            boolean update_data = false;

            String data = editable.toString();
            int data_length = data.length();

            if (data.isEmpty())                             // Empty Text
                update_data = true;

            else {
                if (data.length() > 5) {                    // Limit length of text
                    data = data.substring(1);
                    update_data = true;

                }

                while (data.startsWith("0") && data.length() > 1)                 // Remove any leading zeros
                {
                    data = data.substring(1);
                    update_data = true;
                }

                if (data.isEmpty())                         // Data can't be empty
                    data = "0";
//                    data = Integer.toString(MIN_FREQUENCY);

                frequency = Integer.valueOf(data);          // Keep frequency in bounds
                if (frequency > MAX_FREQUENCY) {
                    data = data.substring(1);
                    frequency = Integer.valueOf(data);
                    update_data = true;
                }
/*
                if (frequency < MIN_FREQUENCY) {
                    frequency = MIN_FREQUENCY;
                    update_data = true;
                }
*/
            }

            if (update_data) {
                String str_frequency = Integer.toString(frequency);
                editable.replace(0, data_length, str_frequency);
//                TextFrequencyLowPass_1.setSelection(str_frequency.length());
            }
        }
    }

    public void UpdateSliderPosition(HorizontalSlider_2_thumb slider_group, SLIDER_VALUE slider, double frequency)
    {
        double progress = (Math.log(frequency)-freq_min) / scale + slider_min;
        slider_group.setValue(slider, (int)progress);
    }

    private void LockControls(EditText active_control, boolean lock)
    {
        for (int channel = 0; channel < SliderCrossovers.length; channel++) {
//Log.i("LockControls", active_control.toString());
            SliderCrossovers[channel].setEnabled(!lock);
            EditFrequencyLowPass[channel].setEnabled((active_control.equals(EditFrequencyLowPass[channel]) ? true: !lock));
            RadioGroup thisGroup = RadioGroups[channel];
            for (int type = 0; type < thisGroup.getChildCount(); type++) {
                thisGroup.getChildAt(type).setEnabled(!lock);
            }
        }
    }

    public void EnableControls(Boolean state) {
//if (state == false)

//if (mainActivity.dspService != null) {
//    if (mainActivity.dspService.AndyTest == 1)
//        Log.i("EnableControls", Boolean.toString(state));
//}

        if (SliderCrossovers != null) {
           for (int channel = 0; channel < SliderCrossovers.length; channel++) {
               SliderCrossovers[channel].setEnabled(state);
               RadioGroup thisGroup = RadioGroups[channel];
               for (int type = 0; type < thisGroup.getChildCount(); type++) {
                   thisGroup.getChildAt(type).setEnabled(state);
               }
           }
        }
    }

    private void HideKeypad() {
        inputting_data = false;

        if (input_manager != null)
            input_manager.hideSoftInputFromWindow(my_view.getWindowToken(), 0);
        if (TextFocusHolder != null)
            TextFocusHolder.requestFocus();

    }

    private String GetCrossoverType(int channel)
    {
        DspCrossover crossover_filter = crossoverFilters[channel];
        String str_type = "";
        switch (crossover_filter.GetType()) {
            case CROSSOVER_HIGH_PASS:
                str_type = "highPass";
                break;

            case CROSSOVER_LOW_PASS:
                str_type = "lowPass";
                break;

            case CROSSOVER_BAND_PASS:
                str_type = "bandPass";
        }
        return(str_type);
    }


    public void SetCrossoverFilterData(int channel, CROSSOVER_TYPES filter_type, int low_frequency, int high_frequency, int number_filters) {
        if (channel >= 0 && channel < crossoverFilters.length) {
            crossoverFilters[channel].SetData(filter_type, low_frequency, high_frequency);

            int slope = 24;                                         // Set slope based on number of cascaded filters
            switch (number_filters) {
                case 1:                                             // slope = 12dB
                    Radio12Db[channel].setChecked(true);
                    slope = 12;
                    break;
                case 3:                                             // slope = 36dB
                    Radio36Db[channel].setChecked(true);
                    slope = 36;
                    break;
                case 4:                                             // slope = 48dB
                    Radio48Db[channel].setChecked(true);
                    slope = 48;
                    break;
                default:                                            // slope = 24dB
                    Radio24Db[channel].setChecked(true);
                    slope = 24;
                    break;
            }
            crossoverFilters[channel].SetSlope(slope);
 /*           switch (slope) {
                case 12:
                    Radio12Db[channel].setChecked(true);
                    break;
                case 36:
                    Radio36Db[channel].setChecked(true);
                    break;
                case 48:
                    Radio48Db[channel].setChecked(true);
                    break;
                default:
                    Radio24Db[channel].setChecked(true);
                    break;
            }
*/
            switch (filter_type) {
                case CROSSOVER_LOW_PASS:
                    RadioLowPass[channel].setChecked(true);
                    SliderCrossovers[channel].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT);
                    UpdateSliderPosition(SliderCrossovers[channel], SLIDER_VALUE.SLIDER_UPPER, low_frequency);
                    LabelFrequencyLowPass[channel].setText("Upper Freq:");
                    EditFrequencyLowPass[channel].setText(Integer.toString(low_frequency));
                    LayoutFrequencyLowPass[channel].setVisibility(View.VISIBLE);
                    LayoutFrequencyHighPass[channel].setVisibility(View.INVISIBLE);
                    break;

                case CROSSOVER_BAND_PASS:
                    RadioBandPass[channel].setChecked(true);
                    SliderCrossovers[channel].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_BETWEEN);
                    UpdateSliderPosition(SliderCrossovers[channel], SLIDER_VALUE.SLIDER_LOWER, low_frequency);
                    LabelFrequencyLowPass[channel].setText("Lower Freq:");
                    EditFrequencyLowPass[channel].setText(Integer.toString(low_frequency));
                    LayoutFrequencyLowPass[channel].setVisibility(View.VISIBLE);
                    UpdateSliderPosition(SliderCrossovers[channel], SLIDER_VALUE.SLIDER_UPPER, high_frequency);
                    LabelFrequencyHighPass[channel].setText("Upper Freq:");
                    EditFrequencyHighPass[channel].setText(Integer.toString(high_frequency));
                    LayoutFrequencyHighPass[channel].setVisibility(View.VISIBLE);
                    break;

                case CROSSOVER_HIGH_PASS:
                    RadioHighPass[channel].setChecked(true);
                    SliderCrossovers[channel].SetType(HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT);
                    UpdateSliderPosition(SliderCrossovers[channel], SLIDER_VALUE.SLIDER_UPPER, high_frequency);
                    LabelFrequencyHighPass[channel].setText("Lower Freq:");
                    EditFrequencyHighPass[channel].setText(Integer.toString(high_frequency));
                    LayoutFrequencyHighPass[channel].setVisibility(View.VISIBLE);
                    LayoutFrequencyLowPass[channel].setVisibility(View.INVISIBLE);
                    break;
            }
        }
    }

    // Send data for a single channel to the 386
    public int SendData(int channel) {
        int send_time = 0;

        if (mainActivity.Connected()) {
            DspService dspService = mainActivity.GetDspService();
            if (channel < crossoverFilters.length) {
                dspService.SetWaitingForAck(true);
                dspService.SendDspCrossover(crossoverFilters[channel]);
                send_time = dspService.WaitForAck();
Log.i("Crossover", Integer.toString(channel));
            }
        }
        return(send_time);
    }



    // Parses and XML file and updates the Crossover Filter data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("crossover"))
                {
                    String type_string = myParser.getAttributeValue(null, "type");
                    CROSSOVER_TYPES filter_type = CROSSOVER_TYPES.CROSSOVER_LOW_PASS;
                    if (type_string.contains("high"))
                        filter_type = CROSSOVER_TYPES.CROSSOVER_HIGH_PASS;
                    else if (type_string.contains("band"))
                        filter_type = CROSSOVER_TYPES.CROSSOVER_BAND_PASS;

                    int low_frequency = Integer.valueOf(myParser.getAttributeValue(null, "low_frequency"));
                    int high_frequency = Integer.valueOf(myParser.getAttributeValue(null, "high_frequency"));

                    String channel_name = myParser.getAttributeValue(null, "channel");
                    int channel = Integer.valueOf(channel_name);

                    int number_filters = 2;
                    String str_number_filters = myParser.getAttributeValue(null, "number_filters");
                    if (str_number_filters != null)
                        number_filters = Integer.valueOf(str_number_filters);

                    SetCrossoverFilterData(channel, filter_type, low_frequency, high_frequency, number_filters);
                    done = true;
                }
            }
            event = myParser.next();
        }
    }



    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        for (int channel = 0; channel < crossoverFilters.length; channel ++) {
            serializer.startTag(null, "crossover");                     // Write each channel's crossover data
            serializer.attribute(null, "channel", Integer.toString(channel));
            serializer.attribute(null, "type", GetCrossoverType(channel));
            serializer.attribute(null, "low_frequency", Integer.toString(crossoverFilters[channel].GetLowFrequency()));
            serializer.attribute(null, "high_frequency", Integer.toString(crossoverFilters[channel].GetHighFrequency()));
            serializer.attribute(null, "number_filters",  Integer.toString(crossoverFilters[channel].GetNumberFilters()));
            serializer.endTag(null, "crossover");
        }
    }

}


