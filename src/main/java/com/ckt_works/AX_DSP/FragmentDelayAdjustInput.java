package com.ckt_works.AX_DSP;
// Created by Abeck on 3/6/2017. //

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class FragmentDelayAdjustInput extends CScreenNavigation implements Button.OnClickListener, EditText.OnEditorActionListener, TextWatcher,
        View.OnFocusChangeListener{

    final int MAX_DELAY_INCHES = 99; //DspDelay.MAX_DELAY_INCHES;

//    private DspService dspService;

    // Declare our GUI members
    private View my_view;

    private TextView textFocusHolder;
    private EditText editLeftFront;
    private EditText editRightFront;
    private EditText editLeftRear;
    private EditText editRightRear;
    private EditText editSubWoofer;

    private boolean keypad_visible = false;

    private InputMethodManager input_manager;

    MainActivity mainActivity;


        //Obligatory Constructor & newInstance function
        public FragmentDelayAdjustInput() {
   }

    public static FragmentDelayAdjustInput newInstance() {
        return new FragmentDelayAdjustInput();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  (MainActivity) getActivity();

        my_view = inflater.inflate(R.layout.layout_delay_adjust_input, container, false);
        if (my_view != null) {

            editLeftFront = (EditText) my_view.findViewById(R.id.editLeftFront);
            editLeftFront.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            editLeftFront.setFocusableInTouchMode(true);
            editLeftFront.setOnEditorActionListener(this);
            editLeftFront.addTextChangedListener(this);
            editLeftFront.setOnFocusChangeListener(this);

            editRightFront = (EditText) my_view.findViewById(R.id.editRightFront);
            editRightFront.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            editRightFront.setFocusableInTouchMode(true);
            editRightFront.setOnEditorActionListener(this);
            editRightFront.addTextChangedListener(this);
            editRightFront.setOnFocusChangeListener(this);

            editLeftRear = (EditText) my_view.findViewById(R.id.editLeftRear);
            editLeftRear.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            editLeftRear.setFocusableInTouchMode(true);
            editLeftRear.setOnEditorActionListener(this);
            editLeftRear.addTextChangedListener(this);
            editLeftRear.setOnFocusChangeListener(this);

            editRightRear = (EditText) my_view.findViewById(R.id.editRightRear);
            editRightRear.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            editRightRear.setFocusableInTouchMode(true);
            editRightRear.setOnEditorActionListener(this);
            editRightRear.addTextChangedListener(this);
            editRightRear.setOnFocusChangeListener(this);

            editSubWoofer = (EditText) my_view.findViewById(R.id.editSubWoofer);
            editSubWoofer.setRawInputType(InputType.TYPE_CLASS_NUMBER);
            editSubWoofer.setFocusableInTouchMode(true);
            editSubWoofer.setOnEditorActionListener(this);
            editSubWoofer.addTextChangedListener(this);
            editSubWoofer.setOnFocusChangeListener(this);

            textFocusHolder = (TextView) my_view.findViewById(R.id.textFocusHolder);
            textFocusHolder.setFocusable(true);
            textFocusHolder.setFocusableInTouchMode(true);
            textFocusHolder.setOnFocusChangeListener(this);
            textFocusHolder.requestFocus();

            input_manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            EnableControls(false);      // Disable
         }
        return (my_view);
    }

/*    public void SetDspService(DspService service){
        dspService = service;
    }
*/

    @Override
    public void onResume() {
        super.onResume();
        textFocusHolder.requestFocus();
        HideKeypad();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus)
    {
        if (hasFocus == true) {
            switch (view.getId()) {
                case R.id.editLeftFront:
                case R.id.editRightFront:
                case R.id.editLeftRear:
                case R.id.editRightRear:
                case R.id.editSubWoofer:
                    keypad_visible = true;
                    break;

                default:
                    keypad_visible = false;
                    break;
            }
        }
    }



    int GetEditTextValue(EditText control)
    {
        String str_value = control.getText().toString();
        if (str_value.isEmpty())
            str_value = "0";
        int inches = Integer.parseInt(str_value);

        return(inches);
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden)
            HideKeypad();
    }


    @Override
    public void beforeTextChanged(CharSequence editable, int start, int count, int after) {
//        String data = editable.toString();
//        Log.i("beforeTextChanged", data);
    }

    @Override
    public void onTextChanged(CharSequence editable, int start, int before, int count) {

//        String data = editable.toString();
//        Log.i("onTextChanged", data);
    }

    private int last_inches_sent = 0;
    @Override
    public void afterTextChanged(Editable editable) {
        if (keypad_visible) {                           // Ignore changes if the keypad is not visible
            int inches = 0;
//            boolean update_data = false;

            String data = editable.toString();
            Log.i("afterTextChanged", data);
            if (data.isEmpty()) {
                data = "0";
                last_inches_sent = 9999;        // Forces update of text box
            }

//            else
                {
                if (data.length() > 2) {
                    data = data.substring(1);
                    //                update_data = true;
                }
                inches = Integer.valueOf(data);
                if (inches > MAX_DELAY_INCHES) {
                    inches = MAX_DELAY_INCHES;
                    //                update_data = true;
                }
            }

            //        if (update_data )
            if (inches != last_inches_sent) {
                last_inches_sent = inches;
                String str_inches = Integer.toString(inches);

                if (editable == editLeftFront.getEditableText()) {
                    editLeftFront.setText(str_inches);
                    editLeftFront.setSelection(str_inches.length());                           // Move cursor to end of text
                } else if (editable == editRightFront.getEditableText()) {
                    editRightFront.setText(str_inches);
                    editRightFront.setSelection(str_inches.length());                           // Move cursor to end of text
                } else if (editable == editLeftRear.getEditableText()) {
                    editLeftRear.setText(str_inches);
                    editLeftRear.setSelection(str_inches.length());                           // Move cursor to end of text
                } else if (editable == editRightRear.getEditableText()) {
                    editRightRear.setText(str_inches);
                    editRightRear.setSelection(str_inches.length());                           // Move cursor to end of text
                } else if (editable == editSubWoofer.getEditableText()) {
                    editSubWoofer.setText(str_inches);
                    editSubWoofer.setSelection(str_inches.length());                           // Move cursor to end of text
                }
                SendData();
            }
        }
    }


    @Override
    // Handle user clicking the 'OK' button
    public void onClick(View button) {

        Integer distance;
        HideKeypad();
    }


    @Override
    public boolean onEditorAction (TextView view, int actionId, KeyEvent event)
    {
        Log.i("----> onKey", Integer.toString(actionId) + " - "); // + Integer.toString(keyCode));
        HideKeypad();

        return(true);
    }

    private void HideKeypad(){
        if (textFocusHolder != null)
            textFocusHolder.requestFocus();

        if (input_manager != null)
            input_manager.hideSoftInputFromWindow(my_view.getWindowToken(), 0);
    }


    public void SetDelayData(int left_front_delay, int right_front_delay, int left_rear_delay, int right_rear_delay, int sub_woofer_delay)
    {
        String text_value = String.valueOf(left_front_delay);
        editLeftFront.setText(text_value);

        text_value = String.valueOf(right_front_delay);
        editRightFront.setText(text_value);

        text_value = String.valueOf(left_rear_delay);
        editLeftRear.setText(text_value);

        text_value = String.valueOf(right_rear_delay);
        editRightRear.setText(text_value);

        text_value = String.valueOf(sub_woofer_delay);
        editSubWoofer.setText(text_value);
    }


    public void EnableControls(Boolean state)
    {
        if(editLeftFront != null) {
            editLeftFront.setEnabled(state);
            editRightFront.setEnabled(state);
            editLeftRear.setEnabled(state);
            editRightRear.setEnabled(state);
            editSubWoofer.setEnabled(state);
        }
    }


    // Send all of our data to the 386
    public boolean SendData() {
        boolean sent = false;
        MainActivity main_Activity = (MainActivity) getActivity();
        if (main_Activity.Connected()) {
            DspService dspService = mainActivity.GetDspService();

            int left_front_inches = GetEditTextValue(editLeftFront);
            int right_front_inches =  GetEditTextValue(editRightFront);
            int left_rear_inches = GetEditTextValue(editLeftRear);
            int right_rear_inches = GetEditTextValue(editRightRear);
            int sub_woofer_inches = GetEditTextValue(editSubWoofer);

            DspDelay delay_data = new DspDelay(left_front_inches, right_front_inches,
                                               left_rear_inches, right_rear_inches, sub_woofer_inches);
            dspService.SendDspDelay(delay_data);
            main_Activity.dspService.WaitForAck();
            sent = true;
        }
        return(sent);
    }


    // Parses and XML file and updates the Delay Position data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;
        String text_value = null;

        int event = myParser.getEventType();
        while (!done) {


            String name = myParser.getName();
            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("speaker_delay"))
                {
                    text_value = myParser.getAttributeValue(null, "left_front_distance");
                    editLeftFront.setText(text_value);

                    text_value = myParser.getAttributeValue(null, "right_front_distance");
                    editRightFront.setText(text_value);

                    text_value = myParser.getAttributeValue(null, "left_rear_distance");
                    editLeftRear.setText(text_value);

                    text_value = myParser.getAttributeValue(null, "right_rear_distance");
                    editRightRear.setText(text_value);

                    text_value = myParser.getAttributeValue(null, "sub_woofer_distance");
                    editSubWoofer.setText(text_value);
                    done = true;
                }
            }
            event = myParser.next();
        }
    }


    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        serializer.startTag(null, "speaker_delay");
        serializer.attribute(null, "left_front_distance", editLeftFront.getText().toString());
        serializer.attribute(null, "right_front_distance", editRightFront.getText().toString());
        serializer.attribute(null, "left_rear_distance", editLeftRear.getText().toString());
        serializer.attribute(null, "right_rear_distance", editRightRear.getText().toString());
        serializer.attribute(null, "sub_woofer_distance", editSubWoofer.getText().toString());

        serializer.endTag(null, "speaker_delay");
    }
}

