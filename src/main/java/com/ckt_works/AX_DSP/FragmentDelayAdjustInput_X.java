package com.ckt_works.AX_DSP;
// Created by Abeck on 3/6/2017. //

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

public class FragmentDelayAdjustInput_X extends CScreenNavigation implements Button.OnClickListener, EditText.OnEditorActionListener, TextWatcher,
        View.OnFocusChangeListener{

    final int MAX_DELAY_INCHES = 99; //DspDelay.MAX_DELAY_INCHES;

//    private DspService dspService;

    // Declare our GUI members
    private View my_view;

    private TextView textFocusHolder;
    private int layoutIds[] = {R.id.layoutDelay_1, R.id.layoutDelay_2, R.id.layoutDelay_3, R.id.layoutDelay_4, R.id.layoutDelay_5,
                               R.id.layoutDelay_6, R.id.layoutDelay_7, R.id.layoutDelay_8, R.id.layoutDelay_9, R.id.layoutDelay_10};
    private int delayNameIds[] = {R.id.textDelayName_1, R.id.textDelayName_2, R.id.textDelayName_3, R.id.textDelayName_4, R.id.textDelayName_5,
                                  R.id.textDelayName_6, R.id.textDelayName_7, R.id.textDelayName_8, R.id.textDelayName_9, R.id.textDelayName_10};
    private int delayValueIds[] = {R.id.editDelayValue_1, R.id.editDelayValue_2, R.id.editDelayValue_3, R.id.editDelayValue_4, R.id.editDelayValue_5,
                                   R.id.editDelayValue_6, R.id.editDelayValue_7, R.id.editDelayValue_8, R.id.editDelayValue_9, R.id.editDelayValue_10};

    private TextView TextNoOutputsAssigned;
    private TextView TextTitle;
    private TextView TextInstructions;
    private LinearLayout[] LayoutDelays;
    private TextView[] TextDelayNames;
    private EditText[] EditDelayValues;

    private boolean keypad_visible = false;
    private InputMethodManager input_manager;
    private int numberSupportedChannels = 10;
    private MainActivity mainActivity;

        //Obligatory Constructor & newInstance function
    public FragmentDelayAdjustInput_X() {}

    public static FragmentDelayAdjustInput_X newInstance() {
        return new FragmentDelayAdjustInput_X();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  (MainActivity) getActivity();

        my_view = inflater.inflate(R.layout.layout_delay_adjust_input_x, container, false);
        if (my_view != null) {
            TextNoOutputsAssigned = (TextView) my_view.findViewById(R.id.textNoOutputAssigned);
            TextTitle = (TextView) my_view.findViewById(R.id.textTitle);
            TextInstructions =(TextView) my_view.findViewById(R.id.txtDelayInstructions);

            LayoutDelays = new LinearLayout[10];
            TextDelayNames = new TextView[10];
            EditDelayValues =  new EditText[10];
            for (int channel = 0; channel < layoutIds.length; channel++) {
                LayoutDelays[channel] = (LinearLayout) my_view.findViewById(layoutIds[channel]);
                TextDelayNames[channel] = (TextView) my_view.findViewById(delayNameIds[channel]);
                EditDelayValues[channel] = (EditText) my_view.findViewById(delayValueIds[channel]);
                EditDelayValues[channel].setRawInputType(InputType.TYPE_CLASS_NUMBER);
                EditDelayValues[channel].setFocusableInTouchMode(true);
                EditDelayValues[channel].setOnEditorActionListener(this);
                EditDelayValues[channel].addTextChangedListener(this);
                EditDelayValues[channel].setOnFocusChangeListener(this);
//                EditDelayValues[channel].setText("0");
            }

            textFocusHolder = (TextView) my_view.findViewById(R.id.textFocusHolder);
            textFocusHolder.setFocusable(true);
            textFocusHolder.setFocusableInTouchMode(true);
            textFocusHolder.setOnFocusChangeListener(this);
            textFocusHolder.requestFocus();

            input_manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            EnableControls(false);      // Disable
         }
        return (my_view);
    }

/*    public void SetDspService(DspService service){
        dspService = service;
    }
*/

    @Override
    public void onResume() {
        super.onResume();
        textFocusHolder.requestFocus();
        HideKeypad();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus)
    {
        int view_id = view.getId();
        keypad_visible = false;
        if (hasFocus) {
            for (int channel = 0; channel < numberSupportedChannels; channel++) {
//                EditDelayValues[channel].setText(Integer.toString(channel));
                if (view_id == delayValueIds[channel]) {
                    keypad_visible = true;
                    break;
                }
            }
        }
    }



    int GetEditTextValue(EditText control)
    {
        String str_value = control.getText().toString();
        if (str_value.isEmpty())
            str_value = "0";
        int inches = Integer.parseInt(str_value);

        return(inches);
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        numberSupportedChannels = mainActivity.GetNumberSupportedChannels();
        if (hidden)
            HideKeypad();
        else {
            if (mainActivity.AnyChannelAssigned() == false) {
                for (int channel = 0; channel < LayoutDelays.length; channel++)
                    LayoutDelays[channel].setVisibility(View.GONE);
                TextNoOutputsAssigned.setVisibility(View.VISIBLE);
                TextTitle.setVisibility((View.GONE));
                TextInstructions.setVisibility(View.GONE);
            }
            else {
                TextNoOutputsAssigned.setVisibility(View.GONE);
                TextTitle.setVisibility((View.VISIBLE));
                TextInstructions.setVisibility(View.VISIBLE);

                for (int channel = 0; channel < EditDelayValues.length; channel++) {
                    if (channel >= numberSupportedChannels)
                        LayoutDelays[channel].setVisibility(View.GONE);
                    else {
                        TextDelayNames[channel].setText(mainActivity.mOutputs_X_Fragment.GetLocationName(channel));
                        LayoutDelays[channel].setVisibility((mainActivity.mOutputs_X_Fragment.IsUsed(channel) ? View.VISIBLE : View.GONE));
                    }
                }
            }
        }
    }


    @Override
    public void beforeTextChanged(CharSequence editable, int start, int count, int after) {
//        String data = editable.toString();
//        Log.i("beforeTextChanged", data);
    }

    @Override
    public void onTextChanged(CharSequence editable, int start, int before, int count) {

//        String data = editable.toString();
//        Log.i("onTextChanged", data);
    }

    private int last_inches_sent = 0;
    @Override
    public void afterTextChanged(Editable editable) {
        if (keypad_visible) {                           // Ignore changes if the keypad is not visible
            int inches = 0;
//            boolean update_data = false;

            String data = editable.toString();
            if (data.isEmpty()) {
                data = "0";
                last_inches_sent = 9999;        // Forces update of text box
            }

//            else
            {
                if (data.length() > 2) {
                    data = data.substring(1);
                    //                update_data = true;
                }
                inches = Integer.valueOf(data);
                if (inches > MAX_DELAY_INCHES) {
                    inches = MAX_DELAY_INCHES;
                    //                update_data = true;
                }
            }

            //        if (update_data )
            if (inches != last_inches_sent) {
                last_inches_sent = inches;
                String str_inches = Integer.toString(inches);
                for (int channel = 0; channel < EditDelayValues.length; channel ++) {
                    if (editable == EditDelayValues[channel].getEditableText()) {
                        EditDelayValues[channel].setText(str_inches);
                        EditDelayValues[channel].setSelection(str_inches.length());             // Move cursor to end of text
                        break;
                    }
                }
                SendData();
            }
        }
    }


    @Override
    // Handle user clicking the 'OK' button
    public void onClick(View button) {

        Integer distance;
        HideKeypad();
    }


    @Override
    public boolean onEditorAction (TextView view, int actionId, KeyEvent event)
    {
        HideKeypad();
        return(true);
    }

    private void HideKeypad(){
        if (textFocusHolder != null)
            textFocusHolder.requestFocus();

        if (input_manager != null)
            input_manager.hideSoftInputFromWindow(my_view.getWindowToken(), 0);
    }


    public void SetDelayData(int delayValues[])
    {
        String text_value = "";
        for (int channel = 0; channel < delayValues.length; channel++) {
            text_value = String.valueOf(delayValues[channel]);
            EditDelayValues[channel].setText(text_value);
        }
    }


    public void EnableControls(Boolean state)
    {
        for (int channel = 0; channel < EditDelayValues.length; channel ++)
            EditDelayValues[channel].setEnabled(state);
    }


    // Send all of our data to the 386
    public int SendData() {
        int send_time = 0;
        MainActivity main_Activity = (MainActivity) getActivity();
        if (main_Activity.Connected()) {
            DspService dspService = mainActivity.GetDspService();
            int[] delay_data = new int[EditDelayValues.length];
            for (int channel = 0; channel < EditDelayValues.length; channel++)
                delay_data[channel] = GetEditTextValue(EditDelayValues[channel]);
            DspDelay_X dspDelay_x = new DspDelay_X(delay_data);

            main_Activity.dspService.SetWaitingForAck(true);
            dspService.SendDspDelay(dspDelay_x);
            send_time = main_Activity.dspService.WaitForAck();
        }
        return(send_time);
    }


    // Parses the XML file and updates the Delay Position data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;
        String text_value = null;
        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("speaker_delay"))
                {
                    String strDistance = myParser.getAttributeValue(null, "distance");
                    int distance = (strDistance == null) ? 0 : Integer.valueOf(strDistance);

                    String strChannel = myParser.getAttributeValue(null, "channel");
                    int channel = (strChannel == null) ? 0 : Integer.valueOf(strChannel);
                    if (channel >=0 && channel < numberSupportedChannels) {
                        EditDelayValues[channel].setText(Integer.toString(distance));
                    }
                    done = true;
                }
            }
            event = myParser.next();
        }
    }


    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        for (int channel = 0; channel < numberSupportedChannels; channel ++) {
            serializer.startTag(null, "speaker_delay");                     // Write each channel's delay data
            serializer.attribute(null, "channel", Integer.toString(channel));
            serializer.attribute(null, "distance",EditDelayValues[channel].getText().toString());
            serializer.endTag(null, "speaker_delay");
        }
    }
}

