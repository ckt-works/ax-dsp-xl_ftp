package com.ckt_works.AX_DSP;

import android.content.Context;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

// Created by Abeck on 12/2/2016. //

public class FragmentEqSettings extends CScreenNavigation implements View.OnClickListener, View.OnTouchListener, IPostExecuteInterface
{

    private class SliderFilter
    {
        protected Integer slider_id;            // ID of the slider associated with this Filter
        protected DspEqFilter dsp_filter;       // DSP Filter object associated with this Filter
        protected Integer text_id;              // ID of the text box associated with this Filter

        SliderFilter(Integer slider, DspEqFilter filter, Integer text_box)
        {
            slider_id = slider;
            dsp_filter = filter;
            text_id = text_box;
        }
    }

/*    private ListPopupWindow valuePopUpWindow;
    private String[] gainValues;
    private String[] eqBoosts;
    private ArrayAdapter gainValueArray;
    private ArrayAdapter eqBoostArray;
*/

    private DialogProgress progress_dialog;

    private boolean updating_sliders = false;

    private MainActivity m_main_Activity;

    private View myView;

    // These arrays maintain the data for the EQ Filters
    private SliderFilter[] slider_filters_front = new SliderFilter[31];
    private SliderFilter[] slider_filters_rear = new SliderFilter[31];
    private SliderFilter[] slider_filters_woofer = new SliderFilter[31];

    // Declare all of our Sliders and value text boxes
    private ConstraintLayout ViewGain;
    private CwSlider SliderEqGain;
    private TextView TextGain;
    private Button ButtonFlat;

    private CwSlider SliderEq20;
    private CwSlider SliderEq25;
    private CwSlider SliderEq31;
    private CwSlider SliderEq40;
    private CwSlider SliderEq50;
    private CwSlider SliderEq63;
    private CwSlider SliderEq80;
    private CwSlider SliderEq100;
    private CwSlider SliderEq125;
    private CwSlider SliderEq160;
    private CwSlider SliderEq200;
    private CwSlider SliderEq250;
    private CwSlider SliderEq315;
    private CwSlider SliderEq400;
    private CwSlider SliderEq500;
    private CwSlider SliderEq630;
    private CwSlider SliderEq800;
    private CwSlider SliderEq1000;
    private CwSlider SliderEq1250;
    private CwSlider SliderEq1600;
    private CwSlider SliderEq2000;
    private CwSlider SliderEq2500;
    private CwSlider SliderEq3150;
    private CwSlider SliderEq4000;
    private CwSlider SliderEq5000;
    private CwSlider SliderEq6300;
    private CwSlider SliderEq8000;
    private CwSlider SliderEq10000;
    private CwSlider SliderEq12500;
    private CwSlider SliderEq16000;
    private CwSlider SliderEq20000;


    private TextView textEq20;
    private TextView textEq25;
    private TextView textEq31;
    private TextView textEq40;
    private TextView textEq50;
    private TextView textEq63;
    private TextView textEq80;
    private TextView textEq100;
    private TextView textEq125;
    private TextView textEq160;
    private TextView textEq200;
    private TextView textEq250;
    private TextView textEq315;
    private TextView textEq400;
    private TextView textEq500;
    private TextView textEq630;
    private TextView textEq800;
    private TextView textEq1000;
    private TextView textEq1250;
    private TextView textEq1600;
    private TextView textEq2000;
    private TextView textEq2500;
    private TextView textEq3150;
    private TextView textEq4000;
    private TextView textEq5000;
    private TextView textEq6300;
    private TextView textEq8000;
    private TextView textEq10000;
    private TextView textEq12500;
    private TextView textEq16000;
    private TextView textEq20000;


    private DspGain eq_gain_front;
    private DspGain eq_gain_rear;
    private DspGain eq_gain_woofer;

    // Declare our Speaker Select Buttons
    private Button ButtonSelectFront;
    private Button ButtonSelectRear;
    private Button ButtonSelectSubwoofer;
    private AUDIO_CHANNEL selected_speakers = AUDIO_CHANNEL.FRONT;


    /*public void SetMainActivity(MainActivity main_activity) {
        m_main_Activity = main_activity;
    }
*/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    public static FragmentEqSettings newInstance() {
        return new FragmentEqSettings();
    }
    public FragmentEqSettings() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        m_main_Activity =  ((MainActivity) getActivity());
        myView = inflater.inflate(R.layout.layout_eq_settings, container, false);
         if (myView != null) {

            Context my_context = myView.getContext();
            ButtonFlat = (Button) myView.findViewById(R.id.butEqFlat);
            ButtonFlat.setOnClickListener(this);

            // Create the Speaker Select Buttons
            ButtonSelectFront = (Button) myView.findViewById(R.id.butEqFront);
            ButtonSelectRear = (Button) myView.findViewById(R.id.butEqRear);
            ButtonSelectSubwoofer = (Button) myView.findViewById(R.id.butEqSubwoofer);

            // Create the Gain Slider and set it's Range
            ViewGain = (ConstraintLayout) myView.findViewById(R.id.layoutGain);
            ViewGain.setVisibility(View.GONE);

            TextGain = (TextView) myView.findViewById(R.id.text_gain_value);
            TextGain.setOnClickListener(this);

/*            // Build list of valid Gain values (+10 to -10)
            DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
            gainValues = new String[201];
            float gain_value = 100;
            for (int i = 0; i < gainValues.length; i++)
            {
                if (gain_value == 0)
                    gainValues[i] = " 0";
                else
                    gainValues[i] = decimalFormat.format(gain_value/10);
                gain_value--;
            }
*/
/*            // Build list of valid EQ Boost values (+10 to -10)
            eqBoosts = new String[201];
            float eg_level = 100;
            for (int i = 0; i < eqBoosts.length; i++)
            {
                if (eg_level == 0)
                    eqBoosts[i] = " 0";
                else
                    eqBoosts[i] = decimalFormat.format(eg_level/10);
                eg_level--;
            }

            // Create the adapters to populate the valuePopUpWindow list
            gainValueArray = new ArrayAdapter(my_context, R.layout.list_item, gainValues);
            eqBoostArray = new ArrayAdapter(my_context, R.layout.list_item, eqBoosts);

            // Initialize the Slider Popup Window
            valuePopUpWindow = new ListPopupWindow(my_context);
            valuePopUpWindow.setAdapter(gainValueArray);
            valuePopUpWindow.setWidth(65);
            valuePopUpWindow.setModal(true);
            valuePopUpWindow.setOnItemClickListener(this);
*/
            SliderEqGain = (CwSlider) myView.findViewById(R.id.sliderGain);
            SliderEqGain.setRange(-10, +10);
            SliderEqGain.setValue(0);
            SliderEqGain.setDecimalMultiplier(1);

            SliderEq20 = (CwSlider) myView.findViewById(R.id.sliderEq1);
            SliderEq20.setRange(-10, 10);
            SliderEq25 = (CwSlider) myView.findViewById(R.id.sliderEq2);
            SliderEq25.setRange(-10, 10);
            SliderEq31 = (CwSlider) myView.findViewById(R.id.sliderEq3);
            SliderEq31.setRange(-10, 10);
            SliderEq40 = (CwSlider) myView.findViewById(R.id.sliderEq4);
            SliderEq40.setRange(-10, 10);
            SliderEq50 = (CwSlider) myView.findViewById(R.id.sliderEq5);
            SliderEq50.setRange(-10, 10);
            SliderEq63 = (CwSlider) myView.findViewById(R.id.sliderEq6);
            SliderEq63.setRange(-10, 10);
            SliderEq80 = (CwSlider) myView.findViewById(R.id.sliderEq7);
            SliderEq80.setRange(-10, 10);
            SliderEq100 = (CwSlider) myView.findViewById(R.id.sliderEq8);
            SliderEq100.setRange(-10, 10);
            SliderEq125 = (CwSlider) myView.findViewById(R.id.sliderEq9);
            SliderEq125.setRange(-10, 10);
            SliderEq160 = (CwSlider) myView.findViewById(R.id.sliderEq10);
            SliderEq160.setRange(-10, 10);
            SliderEq200 = (CwSlider) myView.findViewById(R.id.sliderEq11);
            SliderEq200.setRange(-10, 10);
            SliderEq250 = (CwSlider) myView.findViewById(R.id.sliderEq12);
            SliderEq250.setRange(-10, 10);
            SliderEq315 = (CwSlider) myView.findViewById(R.id.sliderEq13);
            SliderEq315.setRange(-10, 10);
            SliderEq400 = (CwSlider) myView.findViewById(R.id.sliderEq14);
            SliderEq400.setRange(-10, 10);
            SliderEq500 = (CwSlider) myView.findViewById(R.id.sliderEq15);
            SliderEq500.setRange(-10, 10);
            SliderEq630 = (CwSlider) myView.findViewById(R.id.sliderEq16);
            SliderEq630.setRange(-10, 10);
            SliderEq800 = (CwSlider) myView.findViewById(R.id.sliderEq17);
            SliderEq800.setRange(-10, 10);
            SliderEq1000 = (CwSlider) myView.findViewById(R.id.sliderEq18);
            SliderEq1000.setRange(-10, 10);
            SliderEq1250 = (CwSlider) myView.findViewById(R.id.sliderEq19);
            SliderEq1250.setRange(-10, 10);
            SliderEq1600 = (CwSlider) myView.findViewById(R.id.sliderEq20);
            SliderEq1600.setRange(-10, 10);
            SliderEq2000 = (CwSlider) myView.findViewById(R.id.sliderEq21);
            SliderEq2000.setRange(-10, 10);
            SliderEq2500 = (CwSlider) myView.findViewById(R.id.sliderEq22);
            SliderEq2500.setRange(-10, 10);
            SliderEq3150 = (CwSlider) myView.findViewById(R.id.sliderEq23);
            SliderEq3150.setRange(-10, 10);
            SliderEq4000 = (CwSlider) myView.findViewById(R.id.sliderEq24);
            SliderEq4000.setRange(-10, 10);
            SliderEq5000 = (CwSlider) myView.findViewById(R.id.sliderEq25);
            SliderEq5000.setRange(-10, 10);
            SliderEq6300 = (CwSlider) myView.findViewById(R.id.sliderEq26);
            SliderEq6300.setRange(-10, 10);
            SliderEq8000 = (CwSlider) myView.findViewById(R.id.sliderEq27);
            SliderEq8000.setRange(-10, 10);
            SliderEq10000 = (CwSlider) myView.findViewById(R.id.sliderEq28);
            SliderEq10000.setRange(-10, 10);
            SliderEq12500 = (CwSlider) myView.findViewById(R.id.sliderEq29);
            SliderEq12500.setRange(-10, 10);
            SliderEq16000 = (CwSlider) myView.findViewById(R.id.sliderEq30);
            SliderEq16000.setRange(-10, 10);
            SliderEq20000 = (CwSlider) myView.findViewById(R.id.sliderEq31);
            SliderEq20000.setRange(-10, 10);

            textEq20 = (TextView) myView.findViewById(R.id.text_eq1_value);
            textEq20.setOnClickListener(this);
            textEq25 = (TextView) myView.findViewById(R.id.text_eq2_value);
            textEq25.setOnClickListener(this);
            textEq31 = (TextView) myView.findViewById(R.id.text_eq3_value);
            textEq31.setOnClickListener(this);
            textEq40 = (TextView) myView.findViewById(R.id.text_eq4_value);
            textEq40.setOnClickListener(this);
            textEq50 = (TextView) myView.findViewById(R.id.text_eq5_value);
            textEq50.setOnClickListener(this);
            textEq63 = (TextView) myView.findViewById(R.id.text_eq6_value);
            textEq63.setOnClickListener(this);
            textEq80 = (TextView) myView.findViewById(R.id.text_eq7_value);
            textEq80.setOnClickListener(this);
            textEq100 = (TextView) myView.findViewById(R.id.text_eq8_value);
            textEq100.setOnClickListener(this);
            textEq125 = (TextView) myView.findViewById(R.id.text_eq9_value);
            textEq125.setOnClickListener(this);
            textEq160 = (TextView) myView.findViewById(R.id.text_eq10_value);
            textEq160.setOnClickListener(this);
            textEq200 = (TextView) myView.findViewById(R.id.text_eq11_value);
            textEq200.setOnClickListener(this);
            textEq250 = (TextView) myView.findViewById(R.id.text_eq12_value);
            textEq250.setOnClickListener(this);
            textEq315 = (TextView) myView.findViewById(R.id.text_eq13_value);
            textEq315.setOnClickListener(this);
            textEq400 = (TextView) myView.findViewById(R.id.text_eq14_value);
            textEq400.setOnClickListener(this);
            textEq500 = (TextView) myView.findViewById(R.id.text_eq15_value);
            textEq500.setOnClickListener(this);
            textEq630 = (TextView) myView.findViewById(R.id.text_eq16_value);
            textEq630.setOnClickListener(this);
            textEq800 = (TextView) myView.findViewById(R.id.text_eq17_value);
            textEq800.setOnClickListener(this);
            textEq1000 = (TextView) myView.findViewById(R.id.text_eq18_value);
            textEq1000.setOnClickListener(this);
            textEq1250 = (TextView) myView.findViewById(R.id.text_eq19_value);
            textEq1250.setOnClickListener(this);
            textEq1600 = (TextView) myView.findViewById(R.id.text_eq20_value);
            textEq1600.setOnClickListener(this);
            textEq2000 = (TextView) myView.findViewById(R.id.text_eq21_value);
            textEq2000.setOnClickListener(this);
            textEq2500 = (TextView) myView.findViewById(R.id.text_eq22_value);
            textEq2500.setOnClickListener(this);
            textEq3150 = (TextView) myView.findViewById(R.id.text_eq23_value);
            textEq3150.setOnClickListener(this);
            textEq4000 = (TextView) myView.findViewById(R.id.text_eq24_value);
            textEq4000.setOnClickListener(this);
            textEq5000 = (TextView) myView.findViewById(R.id.text_eq25_value);
            textEq5000.setOnClickListener(this);
            textEq6300 = (TextView) myView.findViewById(R.id.text_eq26_value);
            textEq6300.setOnClickListener(this);
            textEq8000 = (TextView) myView.findViewById(R.id.text_eq27_value);
            textEq8000.setOnClickListener(this);
            textEq10000 = (TextView) myView.findViewById(R.id.text_eq28_value);
            textEq10000.setOnClickListener(this);
            textEq12500 = (TextView) myView.findViewById(R.id.text_eq29_value);
            textEq12500.setOnClickListener(this);
            textEq16000 = (TextView) myView.findViewById(R.id.text_eq30_value);
            textEq16000.setOnClickListener(this);
            textEq20000 = (TextView) myView.findViewById(R.id.text_eq31_value);
            textEq20000.setOnClickListener(this);

            // Instantiate Data Classes
            eq_gain_front = new DspGain(GAIN_CHANNELS.GAIN_FRONT, 0);
            DspEqFilter eq_filter_front_20 = new DspEqFilter(0, 0, 20);
            slider_filters_front[0] = new SliderFilter(SliderEq20.getId(), eq_filter_front_20, textEq20.getId());
            DspEqFilter eq_filter_front_25 = new DspEqFilter(0, 1, 25);
            slider_filters_front[1] = new SliderFilter(SliderEq25.getId(), eq_filter_front_25, textEq25.getId());
            DspEqFilter eq_filter_front_31 = new DspEqFilter(0, 2, 31);
            slider_filters_front[2] = new SliderFilter(SliderEq31.getId(), eq_filter_front_31, textEq31.getId());
            DspEqFilter eq_filter_front_40 = new DspEqFilter(0, 3, 40);
            slider_filters_front[3] = new SliderFilter(SliderEq40.getId(), eq_filter_front_40, textEq40.getId());
            DspEqFilter eq_filter_front_50 = new DspEqFilter(0, 4, 50);
            slider_filters_front[4] = new SliderFilter(SliderEq50.getId(), eq_filter_front_50, textEq50.getId());
            DspEqFilter eq_filter_front_63 = new DspEqFilter(0, 5, 63);
            slider_filters_front[5] = new SliderFilter(SliderEq63.getId(), eq_filter_front_63, textEq63.getId());
            DspEqFilter eq_filter_front_80 = new DspEqFilter(0, 6, 80);
            slider_filters_front[6] = new SliderFilter(SliderEq80.getId(), eq_filter_front_80, textEq80.getId());
            DspEqFilter eq_filter_front_100 = new DspEqFilter(0, 7, 100);
            slider_filters_front[7] = new SliderFilter(SliderEq100.getId(), eq_filter_front_100, textEq100.getId());
            DspEqFilter eq_filter_front_125 = new DspEqFilter(0, 8, 125);
            slider_filters_front[8] = new SliderFilter(SliderEq125.getId(), eq_filter_front_125, textEq125.getId());
            DspEqFilter eq_filter_front_160 = new DspEqFilter(0, 9, 160);
            slider_filters_front[9] = new SliderFilter(SliderEq160.getId(), eq_filter_front_160, textEq160.getId());
            DspEqFilter eq_filter_front_200 = new DspEqFilter(0, 10, 200);
            slider_filters_front[10] = new SliderFilter(SliderEq200.getId(), eq_filter_front_200, textEq200.getId());
            DspEqFilter eq_filter_front_250 = new DspEqFilter(0, 11, 250);
            slider_filters_front[11] = new SliderFilter(SliderEq250.getId(), eq_filter_front_250, textEq250.getId());
            DspEqFilter eq_filter_front_315 = new DspEqFilter(0, 12, 315);
            slider_filters_front[12] = new SliderFilter(SliderEq315.getId(), eq_filter_front_315, textEq315.getId());
            DspEqFilter eq_filter_front_400 = new DspEqFilter(0, 13, 400);
            slider_filters_front[13] = new SliderFilter(SliderEq400.getId(), eq_filter_front_400, textEq400.getId());
            DspEqFilter eq_filter_front_500 = new DspEqFilter(0, 14, 500);
            slider_filters_front[14] = new SliderFilter(SliderEq500.getId(), eq_filter_front_500, textEq500.getId());
            DspEqFilter eq_filter_front_630 = new DspEqFilter(0, 15, 630);
            slider_filters_front[15] = new SliderFilter(SliderEq630.getId(), eq_filter_front_630, textEq630.getId());
            DspEqFilter eq_filter_front_800 = new DspEqFilter(0, 16, 800);
            slider_filters_front[16] = new SliderFilter(SliderEq800.getId(), eq_filter_front_800, textEq800.getId());
            DspEqFilter eq_filter_front_1000 = new DspEqFilter(0, 17, 1000);
            slider_filters_front[17] = new SliderFilter(SliderEq1000.getId(), eq_filter_front_1000, textEq1000.getId());
            DspEqFilter eq_filter_front_1250 = new DspEqFilter(0, 18, 1250);
            slider_filters_front[18] = new SliderFilter(SliderEq1250.getId(), eq_filter_front_1250, textEq1250.getId());
            DspEqFilter eq_filter_front_1600 = new DspEqFilter(0, 19, 1600);
            slider_filters_front[19] = new SliderFilter(SliderEq1600.getId(), eq_filter_front_1600, textEq1600.getId());
            DspEqFilter eq_filter_front_2000 = new DspEqFilter(0, 20, 2000);
            slider_filters_front[20] = new SliderFilter(SliderEq2000.getId(), eq_filter_front_2000, textEq2000.getId());
            DspEqFilter eq_filter_front_2500 = new DspEqFilter(0, 21, 2500);
            slider_filters_front[21] = new SliderFilter(SliderEq2500.getId(), eq_filter_front_2500, textEq2500.getId());
            DspEqFilter eq_filter_front_3150 = new DspEqFilter(0, 22, 3150);
            slider_filters_front[22] = new SliderFilter(SliderEq3150.getId(), eq_filter_front_3150, textEq3150.getId());
            DspEqFilter eq_filter_front_4000 = new DspEqFilter(0, 23, 4000);
            slider_filters_front[23] = new SliderFilter(SliderEq4000.getId(), eq_filter_front_4000, textEq4000.getId());
            DspEqFilter eq_filter_front_5000 = new DspEqFilter(0, 24, 5000);
            slider_filters_front[24] = new SliderFilter(SliderEq5000.getId(), eq_filter_front_5000, textEq5000.getId());
            DspEqFilter eq_filter_front_6300 = new DspEqFilter(0, 25, 6300);
            slider_filters_front[25] = new SliderFilter(SliderEq6300.getId(), eq_filter_front_6300, textEq6300.getId());
            DspEqFilter eq_filter_front_8000 = new DspEqFilter(0, 26, 8000);
            slider_filters_front[26] = new SliderFilter(SliderEq8000.getId(), eq_filter_front_8000, textEq8000.getId());
            DspEqFilter eq_filter_front_10000 = new DspEqFilter(0, 27, 10000);
            slider_filters_front[27] = new SliderFilter(SliderEq10000.getId(), eq_filter_front_10000, textEq10000.getId());
            DspEqFilter eq_filter_front_12500 = new DspEqFilter(0, 28, 12500);
            slider_filters_front[28] = new SliderFilter(SliderEq12500.getId(), eq_filter_front_12500, textEq12500.getId());
            DspEqFilter eq_filter_front_16000 = new DspEqFilter(0, 29, 16000);
            slider_filters_front[29] = new SliderFilter(SliderEq16000.getId(), eq_filter_front_16000, textEq16000.getId());
            DspEqFilter eq_filter_front_20000 = new DspEqFilter(0, 30, 20000);
            slider_filters_front[30] = new SliderFilter(SliderEq20000.getId(), eq_filter_front_20000, textEq20000.getId());

             eq_gain_rear = new DspGain(GAIN_CHANNELS.GAIN_REAR, 0);
            DspEqFilter eq_filter_rear_20 = new DspEqFilter(1, 0, 20);
            slider_filters_rear[0] = new SliderFilter(SliderEq20.getId(), eq_filter_rear_20, textEq20.getId());
            DspEqFilter eq_filter_rear_25 = new DspEqFilter(1, 1, 25);
            slider_filters_rear[1] = new SliderFilter(SliderEq25.getId(), eq_filter_rear_25, textEq25.getId());
            DspEqFilter eq_filter_rear_31 = new DspEqFilter(1, 2, 31);
            slider_filters_rear[2] = new SliderFilter(SliderEq31.getId(), eq_filter_rear_31, textEq31.getId());
            DspEqFilter eq_filter_rear_40 = new DspEqFilter(1, 3, 40);
            slider_filters_rear[3] = new SliderFilter(SliderEq40.getId(), eq_filter_rear_40, textEq40.getId());
            DspEqFilter eq_filter_rear_50 = new DspEqFilter(1, 4, 50);
            slider_filters_rear[4] = new SliderFilter(SliderEq50.getId(), eq_filter_rear_50, textEq50.getId());
            DspEqFilter eq_filter_rear_63 = new DspEqFilter(1, 5, 63);
            slider_filters_rear[5] = new SliderFilter(SliderEq63.getId(), eq_filter_rear_63, textEq63.getId());
            DspEqFilter eq_filter_rear_80 = new DspEqFilter(1, 6, 80);
            slider_filters_rear[6] = new SliderFilter(SliderEq80.getId(), eq_filter_rear_80, textEq80.getId());
            DspEqFilter eq_filter_rear_100 = new DspEqFilter(1, 7, 100);
            slider_filters_rear[7] = new SliderFilter(SliderEq100.getId(), eq_filter_rear_100, textEq100.getId());
            DspEqFilter eq_filter_rear_125 = new DspEqFilter(1, 8, 125);
            slider_filters_rear[8] = new SliderFilter(SliderEq125.getId(), eq_filter_rear_125, textEq125.getId());
            DspEqFilter eq_filter_rear_160 = new DspEqFilter(1, 9, 160);
            slider_filters_rear[9] = new SliderFilter(SliderEq160.getId(), eq_filter_rear_160, textEq160.getId());
            DspEqFilter eq_filter_rear_200 = new DspEqFilter(1, 10, 200);
            slider_filters_rear[10] = new SliderFilter(SliderEq200.getId(), eq_filter_rear_200, textEq200.getId());
            DspEqFilter eq_filter_rear_250 = new DspEqFilter(1, 11, 250);
            slider_filters_rear[11] = new SliderFilter(SliderEq250.getId(), eq_filter_rear_250, textEq250.getId());
            DspEqFilter eq_filter_rear_315 = new DspEqFilter(1, 12, 315);
            slider_filters_rear[12] = new SliderFilter(SliderEq315.getId(), eq_filter_rear_315, textEq315.getId());
            DspEqFilter eq_filter_rear_400 = new DspEqFilter(1, 13, 400);
            slider_filters_rear[13] = new SliderFilter(SliderEq400.getId(), eq_filter_rear_400, textEq400.getId());
            DspEqFilter eq_filter_rear_500 = new DspEqFilter(1, 14, 500);
            slider_filters_rear[14] = new SliderFilter(SliderEq500.getId(), eq_filter_rear_500, textEq500.getId());
            DspEqFilter eq_filter_rear_630 = new DspEqFilter(1, 15, 630);
            slider_filters_rear[15] = new SliderFilter(SliderEq630.getId(), eq_filter_rear_630, textEq630.getId());
            DspEqFilter eq_filter_rear_800 = new DspEqFilter(1, 16, 800);
            slider_filters_rear[16] = new SliderFilter(SliderEq800.getId(), eq_filter_rear_800, textEq800.getId());
            DspEqFilter eq_filter_rear_1000 = new DspEqFilter(1, 17, 1000);
            slider_filters_rear[17] = new SliderFilter(SliderEq1000.getId(), eq_filter_rear_1000, textEq1000.getId());
            DspEqFilter eq_filter_rear_1250 = new DspEqFilter(1, 18, 1250);
            slider_filters_rear[18] = new SliderFilter(SliderEq1250.getId(), eq_filter_rear_1250, textEq1250.getId());
            DspEqFilter eq_filter_rear_1600 = new DspEqFilter(1, 19, 1600);
            slider_filters_rear[19] = new SliderFilter(SliderEq1600.getId(), eq_filter_rear_1600, textEq1600.getId());
            DspEqFilter eq_filter_rear_2000 = new DspEqFilter(1, 20, 2000);
            slider_filters_rear[20] = new SliderFilter(SliderEq2000.getId(), eq_filter_rear_2000, textEq2000.getId());
            DspEqFilter eq_filter_rear_2500 = new DspEqFilter(1, 21, 2500);
            slider_filters_rear[21] = new SliderFilter(SliderEq2500.getId(), eq_filter_rear_2500, textEq2500.getId());
            DspEqFilter eq_filter_rear_3150 = new DspEqFilter(1, 22, 3150);
            slider_filters_rear[22] = new SliderFilter(SliderEq3150.getId(), eq_filter_rear_3150, textEq3150.getId());
            DspEqFilter eq_filter_rear_4000 = new DspEqFilter(1, 23, 4000);
            slider_filters_rear[23] = new SliderFilter(SliderEq4000.getId(), eq_filter_rear_4000, textEq4000.getId());
            DspEqFilter eq_filter_rear_5000 = new DspEqFilter(1, 24, 5000);
            slider_filters_rear[24] = new SliderFilter(SliderEq5000.getId(), eq_filter_rear_5000, textEq5000.getId());
            DspEqFilter eq_filter_rear_6300 = new DspEqFilter(1, 25, 6300);
            slider_filters_rear[25] = new SliderFilter(SliderEq6300.getId(), eq_filter_rear_6300, textEq6300.getId());
            DspEqFilter eq_filter_rear_8000 = new DspEqFilter(1, 26, 8000);
            slider_filters_rear[26] = new SliderFilter(SliderEq8000.getId(), eq_filter_rear_8000, textEq8000.getId());
            DspEqFilter eq_filter_rear_10000 = new DspEqFilter(1, 27, 10000);
            slider_filters_rear[27] = new SliderFilter(SliderEq10000.getId(), eq_filter_rear_10000, textEq10000.getId());
            DspEqFilter eq_filter_rear_12500 = new DspEqFilter(1, 28, 12500);
            slider_filters_rear[28] = new SliderFilter(SliderEq12500.getId(), eq_filter_rear_12500, textEq12500.getId());
            DspEqFilter eq_filter_rear_16000 = new DspEqFilter(1, 29, 16000);
            slider_filters_rear[29] = new SliderFilter(SliderEq16000.getId(), eq_filter_rear_16000, textEq1600.getId());
            DspEqFilter eq_filter_rear_20000 = new DspEqFilter(1, 30, 20000);
            slider_filters_rear[30] = new SliderFilter(SliderEq20000.getId(), eq_filter_rear_20000, textEq20000.getId());

            eq_gain_woofer = new DspGain(GAIN_CHANNELS.GAIN_SUBWOOFER, 0);
            DspEqFilter eq_filter_woofer_20 = new DspEqFilter(2, 0, 20);
            slider_filters_woofer[0] = new SliderFilter(SliderEq20.getId(), eq_filter_woofer_20, textEq20.getId());
            DspEqFilter eq_filter_woofer_25 = new DspEqFilter(2, 1, 25);
            slider_filters_woofer[1] = new SliderFilter(SliderEq25.getId(), eq_filter_woofer_25, textEq25.getId());
            DspEqFilter eq_filter_woofer_31 = new DspEqFilter(2, 2, 31);
            slider_filters_woofer[2] = new SliderFilter(SliderEq31.getId(), eq_filter_woofer_31, textEq31.getId());
            DspEqFilter eq_filter_woofer_40 = new DspEqFilter(2, 3, 40);
            slider_filters_woofer[3] = new SliderFilter(SliderEq40.getId(), eq_filter_woofer_40, textEq40.getId());
            DspEqFilter eq_filter_woofer_50 = new DspEqFilter(2, 4, 50);
            slider_filters_woofer[4] = new SliderFilter(SliderEq50.getId(), eq_filter_woofer_50, textEq50.getId());
            DspEqFilter eq_filter_woofer_63 = new DspEqFilter(2, 5, 63);
            slider_filters_woofer[5] = new SliderFilter(SliderEq63.getId(), eq_filter_woofer_63, textEq63.getId());
            DspEqFilter eq_filter_woofer_80 = new DspEqFilter(2, 6, 80);
            slider_filters_woofer[6] = new SliderFilter(SliderEq80.getId(), eq_filter_woofer_80, textEq80.getId());
            DspEqFilter eq_filter_woofer_100 = new DspEqFilter(2, 7, 100);
            slider_filters_woofer[7] = new SliderFilter(SliderEq100.getId(), eq_filter_woofer_100, textEq100.getId());
            DspEqFilter eq_filter_woofer_125 = new DspEqFilter(2, 8, 125);
            slider_filters_woofer[8] = new SliderFilter(SliderEq125.getId(), eq_filter_woofer_125, textEq125.getId());
            DspEqFilter eq_filter_woofer_160 = new DspEqFilter(2, 9, 160);
            slider_filters_woofer[9] = new SliderFilter(SliderEq160.getId(), eq_filter_woofer_160, textEq160.getId());
            DspEqFilter eq_filter_woofer_200 = new DspEqFilter(2, 10, 200);
            slider_filters_woofer[10] = new SliderFilter(SliderEq200.getId(), eq_filter_woofer_200, textEq200.getId());
            DspEqFilter eq_filter_woofer_250 = new DspEqFilter(2, 11, 250);
            slider_filters_woofer[11] = new SliderFilter(SliderEq250.getId(), eq_filter_woofer_250, textEq250.getId());
            DspEqFilter eq_filter_woofer_315 = new DspEqFilter(2, 12, 315);
            slider_filters_woofer[12] = new SliderFilter(SliderEq315.getId(), eq_filter_woofer_315, textEq315.getId());
            DspEqFilter eq_filter_woofer_400 = new DspEqFilter(2, 13, 400);
            slider_filters_woofer[13] = new SliderFilter(SliderEq400.getId(), eq_filter_woofer_400, textEq400.getId());
            DspEqFilter eq_filter_woofer_500 = new DspEqFilter(2, 14, 500);
            slider_filters_woofer[14] = new SliderFilter(SliderEq500.getId(), eq_filter_woofer_500, textEq500.getId());
            DspEqFilter eq_filter_woofer_630 = new DspEqFilter(2, 15, 630);
            slider_filters_woofer[15] = new SliderFilter(SliderEq630.getId(), eq_filter_woofer_630, textEq630.getId());
            DspEqFilter eq_filter_woofer_800 = new DspEqFilter(2, 16, 800);
            slider_filters_woofer[16] = new SliderFilter(SliderEq800.getId(), eq_filter_woofer_800, textEq800.getId());
            DspEqFilter eq_filter_woofer_1000 = new DspEqFilter(2, 17, 1000);
            slider_filters_woofer[17] = new SliderFilter(SliderEq1000.getId(), eq_filter_woofer_1000, textEq1000.getId());
            DspEqFilter eq_filter_woofer_1250 = new DspEqFilter(2, 18, 1250);
            slider_filters_woofer[18] = new SliderFilter(SliderEq1250.getId(), eq_filter_woofer_1250, textEq1250.getId());
            DspEqFilter eq_filter_woofer_1600 = new DspEqFilter(2, 19, 1600);
            slider_filters_woofer[19] = new SliderFilter(SliderEq1600.getId(), eq_filter_woofer_1600, textEq1600.getId());
            DspEqFilter eq_filter_woofer_2000 = new DspEqFilter(2, 20, 2000);
            slider_filters_woofer[20] = new SliderFilter(SliderEq2000.getId(), eq_filter_woofer_2000, textEq2000.getId());
            DspEqFilter eq_filter_woofer_2500 = new DspEqFilter(2, 21, 2500);
            slider_filters_woofer[21] = new SliderFilter(SliderEq2500.getId(), eq_filter_woofer_2500, textEq2500.getId());
            DspEqFilter eq_filter_woofer_3150 = new DspEqFilter(2, 22, 3150);
            slider_filters_woofer[22] = new SliderFilter(SliderEq3150.getId(), eq_filter_woofer_3150, textEq3150.getId());
            DspEqFilter eq_filter_woofer_4000 = new DspEqFilter(2, 23, 4000);
            slider_filters_woofer[23] = new SliderFilter(SliderEq4000.getId(), eq_filter_woofer_4000, textEq4000.getId());
            DspEqFilter eq_filter_woofer_5000 = new DspEqFilter(2, 24, 5000);
            slider_filters_woofer[24] = new SliderFilter(SliderEq5000.getId(), eq_filter_woofer_5000, textEq5000.getId());
            DspEqFilter eq_filter_woofer_6300 = new DspEqFilter(2, 25, 6300);
            slider_filters_woofer[25] = new SliderFilter(SliderEq6300.getId(), eq_filter_woofer_6300, textEq6300.getId());
            DspEqFilter eq_filter_woofer_8000 = new DspEqFilter(2, 26, 8000);
            slider_filters_woofer[26] = new SliderFilter(SliderEq8000.getId(), eq_filter_woofer_8000, textEq8000.getId());
            DspEqFilter eq_filter_woofer_10000 = new DspEqFilter(2, 27, 10000);
            slider_filters_woofer[27] = new SliderFilter(SliderEq10000.getId(), eq_filter_woofer_10000, textEq10000.getId());
            DspEqFilter eq_filter_woofer_12500 = new DspEqFilter(2, 28, 12500);
            slider_filters_woofer[28] = new SliderFilter(SliderEq12500.getId(), eq_filter_woofer_12500, textEq12500.getId());
            DspEqFilter eq_filter_woofer_16000 = new DspEqFilter(2, 29, 16000);
            slider_filters_woofer[29] = new SliderFilter(SliderEq16000.getId(), eq_filter_woofer_16000, textEq16000.getId());
            DspEqFilter eq_filter_woofer_20000 = new DspEqFilter(2, 30, 20000);
            slider_filters_woofer[30] = new SliderFilter(SliderEq20000.getId(), eq_filter_woofer_20000, textEq20000.getId());


            // Set the Control listeners to this object
            ButtonFlat.setOnClickListener(this);
            ButtonSelectFront.setOnTouchListener(this);
            ButtonSelectRear.setOnTouchListener(this);
            ButtonSelectSubwoofer.setOnTouchListener(this);

            SliderEqGain.setDelegateOnTouchListener(this);
            SliderEq20.setDelegateOnTouchListener(this);
            SliderEq25.setDelegateOnTouchListener(this);
            SliderEq31.setDelegateOnTouchListener(this);
            SliderEq40.setDelegateOnTouchListener(this);
            SliderEq50.setDelegateOnTouchListener(this);
            SliderEq63.setDelegateOnTouchListener(this);
            SliderEq80.setDelegateOnTouchListener(this);
            SliderEq100.setDelegateOnTouchListener(this);
            SliderEq125.setDelegateOnTouchListener(this);
            SliderEq160.setDelegateOnTouchListener(this);
            SliderEq200.setDelegateOnTouchListener(this);
            SliderEq250.setDelegateOnTouchListener(this);
            SliderEq315.setDelegateOnTouchListener(this);
            SliderEq400.setDelegateOnTouchListener(this);
            SliderEq500.setDelegateOnTouchListener(this);
            SliderEq630.setDelegateOnTouchListener(this);
            SliderEq800.setDelegateOnTouchListener(this);
            SliderEq1000.setDelegateOnTouchListener(this);
            SliderEq1250.setDelegateOnTouchListener(this);
            SliderEq1600.setDelegateOnTouchListener(this);
            SliderEq2000.setDelegateOnTouchListener(this);
            SliderEq2500.setDelegateOnTouchListener(this);
            SliderEq3150.setDelegateOnTouchListener(this);
            SliderEq4000.setDelegateOnTouchListener(this);
            SliderEq5000.setDelegateOnTouchListener(this);
            SliderEq6300.setDelegateOnTouchListener(this);
            SliderEq8000.setDelegateOnTouchListener(this);
            SliderEq10000.setDelegateOnTouchListener(this);
            SliderEq12500.setDelegateOnTouchListener(this);
            SliderEq16000.setDelegateOnTouchListener(this);
            SliderEq20000.setDelegateOnTouchListener(this);

            EnableControls(false);
        }
        return (myView);
    }// End 'onCreateView'


    @Override
    public void onHiddenChanged (boolean hidden)
    {
        if (!hidden)
            UpdateControls();

    }

    // Interface to allow the "TaskSendData" to callback when done sending data
    public void onPostExecute()
    {
        progress_dialog.dismiss();
    }

    // Set the EQ Filter Boost value - update the slider position & the textbox
    public void SetEqBoostLevel(int channel, int band, int boost)
    {
        SliderFilter this_filter =  slider_filters_front[band];             // Get correct Filter
        if (channel == 1)
            this_filter = slider_filters_rear[band];
        else if (channel == 2)
            this_filter = slider_filters_woofer[band];

        float boost_value = ((float) boost) / 10;                           // Set Boost value

        int textId = this_filter.text_id;                                   // Update Value Text Box
        TextView textBoost = (TextView) myView.findViewById(textId);
        SetValueTextBox(textBoost, boost_value);

        int slider_id = this_filter.slider_id;                              // Update Slider
        CwSlider sliderBoost = (CwSlider) myView.findViewById(slider_id);
        sliderBoost.setValue(boost_value);

        DspEqFilter eqFilter = this_filter.dsp_filter;                      // Update Filter
        eqFilter.SetBoost(boost_value);

/*
        if (channel == 0) {
            slider_filters_front[band].dsp_filter.SetBoost(boost_value);

            int textId = slider_filters_front[band].text_id;
            TextView textBoost = (TextView)myView.findViewById(textId);
            textBoost.setText(eqBoosts[100 - boost]);

            CwSlider this_slider = (CwSlider) myView.findViewById(slider_filters_front[band].slider_id);
            this_slider.setValue(boost_value);
        }

        else if (channel == 1) {
            slider_filters_rear[band].dsp_filter.SetBoost(boost_value);
            CwSlider this_slider = (CwSlider) myView.findViewById(slider_filters_rear[band].slider_id);
            this_slider.setValue(boost_value);
        }

        else if (channel == 2) {
            slider_filters_woofer[band].dsp_filter.SetBoost(boost_value);
            CwSlider this_slider = (CwSlider) myView.findViewById(slider_filters_woofer[band].slider_id);
            this_slider.setValue(boost_value);
        }
*/
    }


    // Handles all clickable controls on this screen
    public void onClick(View control) {
        int id  = control.getId();
        switch (id) {
            case R.id.butEqFlat:
                progress_dialog = new DialogProgress();                 // Display a Progress Bar while we wait for 386 to lock down data
                Bundle progress_title = new Bundle();
                progress_title.putString("TITLE", "Updating EQ Data");
                progress_dialog.setArguments(progress_title);
                progress_dialog.setCancelable(false);
                progress_dialog.show(getFragmentManager(), "ResetEqData");

                updating_sliders = true;
                ResetEqFilters(selected_speakers, false);                // Reset the filter for the displayed channels

                TaskSendData send_data_task = new TaskSendData(m_main_Activity);
                send_data_task.SetPostCallBeck(this);
                send_data_task.SendEqOnly(true);                        // Send only Eq Data...
                send_data_task.SendEqChannel(selected_speakers);        // and only the changed channel (to speed things up)
                send_data_task.execute();
                updating_sliders = false;
                break;

            case R.id.text_gain_value:
/*                int popupHeight = SliderEqGain.getHeight();
                valuePopUpWindow.setHeight(popupHeight);
                valuePopUpWindow.setAnchorView(TextGain);
                valuePopUpWindow.setAdapter(gainValueArray);
                valuePopUpWindow.show();

                String strGainValue = (String)TextGain.getText();
                float floatGainValue = Float.valueOf(strGainValue);
                int intGainValue = (int)(floatGainValue * 10);
                int gainIndex = 60 - intGainValue;
                if (gainIndex > 6)
                    gainIndex -= 6;
                else
                    gainIndex = 0;
                valuePopUpWindow.setSelection(gainIndex);
*/
                break;

            default:                                                                    // Must be a Eq Value textbox
/*                SliderFilter sliderFilter = FindSliderFilter((TextView)control);
                if (sliderFilter != null)
                {
                    String strBoostValue = (String)((TextView) myView.findViewById(sliderFilter.text_id)).getText();
                    float floatBoostValue = Float.valueOf(strBoostValue);
                    int intBoostValue = (int)(floatBoostValue * 10);
                    int boostIndex = 100 - intBoostValue;
                    if (boostIndex > 6)
                        boostIndex -= 6;
                    else
                        boostIndex = 0;
                    popupHeight = SliderEq20.getHeight();
                    valuePopUpWindow.setHeight(popupHeight);
                    valuePopUpWindow.setAnchorView(control);
                    valuePopUpWindow.setAdapter(eqBoostArray);
                    valuePopUpWindow.show();
                    valuePopUpWindow.setSelection(boostIndex);
                }
*/
                break;
        }
    }
    
/*    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int anchor_id = valuePopUpWindow.getAnchorView().getId();

        if (anchor_id == TextGain.getId()) {                            // If it was the Gain valule
            TextGain.setText(gainValues[position]);
            float gain = Float.valueOf(gainValues[position]);
            SliderEqGain.setValue(gain);
        }
        else                                                            // Assume it's a EQ Value
        {
            SliderFilter filter = FindSliderFilter((TextView) valuePopUpWindow.getAnchorView());
            if (filter != null)
            {
                TextView textLevel = (TextView) myView.findViewById(filter.text_id);
                if (textLevel != null)
                    textLevel.setText(eqBoosts[position]);

                float boost = Float.valueOf(eqBoosts[position]);
                CwSlider this_slider = (CwSlider) myView.findViewById(filter.slider_id);
                this_slider.setValue((boost));                          // Set the Boost value
                filter.dsp_filter.SetBoost(boost);
                m_main_Activity.SendDspEQ(filter.dsp_filter);           // Send the data
            }
        }

        valuePopUpWindow.dismiss();
    }
*/
    @Override
    // This function handles the Touch Event for all sliders on this screen
    public boolean onTouch (View slider, MotionEvent event){
        int control_id = slider.getId();

        switch (control_id) {
            case R.id.butEqFront:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    selected_speakers = AUDIO_CHANNEL.FRONT;
                    ButtonSelectFront.setBackgroundResource(R.drawable.but_eq_selected);
                    ButtonSelectRear.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ButtonSelectSubwoofer.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ViewGain.setVisibility(View.GONE);
                    UpdateControls();
                }
                break;


            case R.id.butEqRear:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    selected_speakers = AUDIO_CHANNEL.REAR;
                    ButtonSelectFront.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ButtonSelectRear.setBackgroundResource(R.drawable.but_eq_selected);
                    ButtonSelectSubwoofer.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ViewGain.setVisibility(View.GONE);
                    UpdateControls();
                }
                break;

            case R.id.butEqSubwoofer:
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    selected_speakers = AUDIO_CHANNEL.SUBWOOFER;
                    ButtonSelectFront.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ButtonSelectRear.setBackgroundResource(R.drawable.but_eq_not_selected);
                    ButtonSelectSubwoofer.setBackgroundResource(R.drawable.but_eq_selected);
                    ViewGain.setVisibility(View.VISIBLE);
                    UpdateControls();
                }
                break;

            case R.id.sliderGain:
                CwSlider this_slider = (CwSlider) slider;
                GainChanged(this_slider.getSliderValue());
                break;

            default:                     // Must be an EQ slider
                {
                    this_slider = (CwSlider) slider;
                    float boost_value = this_slider.getSliderValue();
Log.i("Boost Value:", Float.toString(boost_value));
                    int slider_id = slider.getId();

                    if (!updating_sliders) {
                        EnableControls(false);

                        SliderFilter slider_filters[] = new SliderFilter[31];
                        if (selected_speakers == AUDIO_CHANNEL.FRONT)
                            slider_filters = slider_filters_front;
                        else if (selected_speakers == AUDIO_CHANNEL.REAR)
                            slider_filters = slider_filters_rear;
                        else
                            slider_filters = slider_filters_woofer;

                        for (int index = 0; index < 31; index++) {                              // Find the filter for the slider we touched
                            int this_slider_id = slider_filters[index].slider_id;
                            if (this_slider_id == slider_id) {
                                SetValueTextBox(slider_filters[index], boost_value);
                                slider_filters[index].dsp_filter.SetBoost(boost_value);         // Update the value
                                m_main_Activity.SendDspEQ(slider_filters[index].dsp_filter);    // Send the data
                                break;
                            }
                        }
                    }
                }
                break;
        }
        return(true);
    }


    private SliderFilter FindSliderFilter(CwSlider slider) {
        SliderFilter this_filter = null;

        SliderFilter slider_filters[] = new SliderFilter[31];
        if (selected_speakers == AUDIO_CHANNEL.FRONT)
            slider_filters = slider_filters_front;
        else if (selected_speakers == AUDIO_CHANNEL.REAR)
            slider_filters = slider_filters_rear;
        else
            slider_filters = slider_filters_woofer;

        int slider_id = slider.getId();
        for (int index = 0; index < 31; index++) {                              // Find the filter for the slider whose ID we passed
            int this_slider_id = slider_filters[index].slider_id;
            if (this_slider_id == slider_id) {
                this_filter = slider_filters[index];
            }
        }
        return(this_filter);
    }

    private SliderFilter FindSliderFilter(TextView textView) {
        SliderFilter this_filter = null;

        SliderFilter slider_filters[] = new SliderFilter[31];
        if (selected_speakers == AUDIO_CHANNEL.FRONT)
            slider_filters = slider_filters_front;
        else if (selected_speakers == AUDIO_CHANNEL.REAR)
            slider_filters = slider_filters_rear;
        else
            slider_filters = slider_filters_woofer;

        int view_id = textView.getId();
        for (int index = 0; index < 31; index++) {                              // Find the filter for the slider whose ID we passed
            int this_view_id = slider_filters[index].text_id;
            if (this_view_id == view_id) {
                this_filter = slider_filters[index];
            }
        }
        return(this_filter);
    }


    void SetValueTextBox(SliderFilter slider_filter, float boost_value) {
        TextView TextBox = (TextView) myView.findViewById(slider_filter.text_id);
        SetValueTextBox(TextBox, boost_value);
    }

    void SetValueTextBox( TextView TextBox, float value){
        String displayString;
        if (value > 9.95)
            displayString = "+10";
        else if (value < -9.95)
            displayString = "-10";
        else if (value == 0)
            displayString = "0";
        else
            displayString =  String.format(Locale.US, "%+2.1f", value);

        TextBox.setText(displayString);
    }


    public void GainChanged(float gain_value)
    {
        if (!updating_sliders)
        {
            EnableControls(false);
            switch(selected_speakers)
            {
                case FRONT:
                    eq_gain_front.SetGain(gain_value);
                   m_main_Activity.SendDspGain(eq_gain_front);
                    break;

                case REAR:
                    eq_gain_rear.SetGain(gain_value);
                    m_main_Activity.SendDspGain(eq_gain_rear);
                    break;

                case SUBWOOFER:
                   eq_gain_woofer.SetGain(gain_value);
                   m_main_Activity.SendDspGain(eq_gain_woofer);
                    break;
            }
            SetValueTextBox(TextGain, gain_value);
        }
    }


    // Set the position of all sliders to the value in their associated data object
    // as well as the value textbox
    public void UpdateControls()
    {
        updating_sliders = true;                        // Prevent the slider changes from sending data to the 386
        SliderFilter slider_filters[] = new SliderFilter[31];

        float gain_value = 0;

        if (selected_speakers == AUDIO_CHANNEL.FRONT) {
            gain_value = eq_gain_front.GetGain();
            slider_filters = slider_filters_front;
        }

        else if (selected_speakers == AUDIO_CHANNEL.REAR) {
            gain_value = eq_gain_rear.GetGain();
            slider_filters = slider_filters_rear;
        }

        else
        {
           gain_value =  eq_gain_woofer.GetGain();
            slider_filters = slider_filters_woofer;
        }

        SliderEqGain.setValue(gain_value);
        DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
        TextGain.setText(decimalFormat.format(gain_value));

        // Update all of the DSP Filter sliders
        for (int index = 0; index < 31; index++)
        {
            SliderFilter this_filter = slider_filters[index];
            float this_boost = this_filter.dsp_filter.GetBoost();

            CwSlider this_slider = (CwSlider) myView.findViewById(slider_filters[index].slider_id);
            this_slider.setValue((this_boost));
            SetValueTextBox(this_filter, this_boost);
        }
        updating_sliders = false;
    }

    // Reset all filters & gains to their Factory Default position
    public void ResetEqFilters(Boolean include_gains){
        ResetEqFilters(AUDIO_CHANNEL.ALL, include_gains);
    }


    // Reset select filters & gains to their Factory Default position
    public void ResetEqFilters(AUDIO_CHANNEL channels, Boolean include_gains)
    {
        CwSlider this_slider;
        TextView this_value_text;
        updating_sliders = true;

        if (include_gains) {
            eq_gain_front.SetGain(0);
            eq_gain_rear.SetGain(0);
            eq_gain_woofer.SetGain(0);
            SliderEqGain.setValue(0);
        }

        for (int index = 0; index < 31; index++)
        {
            // Reset physical slider
            this_slider = (CwSlider) myView.findViewById(slider_filters_front[index].slider_id);
            this_slider.setValue(0);

            this_value_text = (TextView) myView.findViewById(slider_filters_front[index].text_id);
            SetValueTextBox(this_value_text, 0);

            // Reset Front EQ's
            if (channels == AUDIO_CHANNEL.FRONT || channels == AUDIO_CHANNEL.ALL)
                slider_filters_front[index].dsp_filter.SetBoost(0);

            // Reset Rear EQ's
            if (channels == AUDIO_CHANNEL.REAR || channels == AUDIO_CHANNEL.ALL)
                slider_filters_rear[index].dsp_filter.SetBoost(0);

            // Reset Subwoofer EQ's
            if (channels == AUDIO_CHANNEL.SUBWOOFER|| channels == AUDIO_CHANNEL.ALL)
                slider_filters_woofer[index].dsp_filter.SetBoost(0);
        }
        updating_sliders = false;

    }

    public void EnableControls(Boolean state)
    {
        if ( SliderEqGain != null) {
            ButtonFlat.setEnabled(state);
            SliderEqGain.setEnabled(state);

            CwSlider eqSlider;
            TextView eqTextbox;
            for (int index=0; index < 31; index++)
            {
                eqSlider = (CwSlider) myView.findViewById(slider_filters_front[index].slider_id);
                eqSlider.setEnabled(state);

                eqTextbox = (TextView)myView.findViewById(slider_filters_front[index].text_id);
                eqTextbox.setEnabled(state);
            }
        }
    }

    // Send all of our data to the 386
    public boolean SendData(AUDIO_CHANNEL channel) {
        boolean sent = false;
        if (m_main_Activity.Connected())
        {
            if (channel == AUDIO_CHANNEL.FRONT)
            {
                 for (int index = 0; index < 31; index++) {
                    float boost_value = slider_filters_front[index].dsp_filter.GetBoost();
                    slider_filters_front[index].dsp_filter.SetBoost(boost_value);
                    m_main_Activity.SendDspEQ(slider_filters_front[index].dsp_filter);
                    m_main_Activity.dspService.WaitForAck();
                }
                sent = true;
            }

            else if (channel == AUDIO_CHANNEL.REAR)
            {
                for (int index = 0; index < 31; index++) {
                    float boost_value = slider_filters_rear[index].dsp_filter.GetBoost();
                    slider_filters_rear[index].dsp_filter.SetBoost(boost_value);
                    m_main_Activity.SendDspEQ(slider_filters_rear[index].dsp_filter);
                    m_main_Activity.dspService.WaitForAck();
                }
                sent = true;
            }

            else if (channel == AUDIO_CHANNEL.SUBWOOFER) {
//                m_main_Activity.SendDspGain(eq_gain_woofer);
//                m_main_Activity.dspService.WaitForAck();

                for (int index = 0; index < 31; index++) {
                    float boost_value = slider_filters_woofer[index].dsp_filter.GetBoost();
                    slider_filters_woofer[index].dsp_filter.SetBoost(boost_value);
                    m_main_Activity.SendDspEQ(slider_filters_woofer[index].dsp_filter);
                    m_main_Activity.dspService.WaitForAck();
                }
                sent = true;
            }
        }
        return(sent);
    }


    public void SetGains(int front_gain, int rear_gain, int woofer_gain)
    {
        eq_gain_front.SetGain(front_gain);
        eq_gain_rear.SetGain(rear_gain);
        eq_gain_woofer.SetGain(woofer_gain);
    }

    // Parses an XML file and updates the EQ Filter data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;
        AUDIO_CHANNEL channel_id = AUDIO_CHANNEL.FRONT;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            switch (event) {
                case XmlPullParser.START_TAG:
                    break;

                case XmlPullParser.END_TAG:
                    if (name.equals("channel"))
                    {
                        String channel_name = myParser.getAttributeValue(null, "id");
                        String gain = myParser.getAttributeValue(null, "gain");
                        if (channel_name.contains("front")) {
                            channel_id = AUDIO_CHANNEL.FRONT;
                            eq_gain_front.SetGain(gain);
                        }

                        else if (channel_name.contains("rear")) {
                            channel_id = AUDIO_CHANNEL.REAR;
                            eq_gain_rear.SetGain(gain);
                        }

                        else if (channel_name.contains("woofer") || channel_name.contains("sub")) {
                            channel_id = AUDIO_CHANNEL.SUBWOOFER;
                            eq_gain_woofer.SetGain(gain);
                        }
                    }

                    else if (name.equals("band"))
                    {
                        String band = myParser.getAttributeValue(null, "id");
                        String frequency = myParser.getAttributeValue(null, "frequency");
                        String boost = myParser.getAttributeValue(null, "boost");
                        if (band != null)
                        {
                            switch (channel_id)
                            {
                                case FRONT:
                                    slider_filters_front[Integer.valueOf(band)].dsp_filter.SetData(band, frequency, boost);
                                    break;

                                case REAR:
                                    slider_filters_rear[Integer.valueOf(band)].dsp_filter.SetData(band, frequency, boost);
                                    break;

                                case SUBWOOFER:
                                    slider_filters_woofer[Integer.valueOf(band)].dsp_filter.SetData(band, frequency, boost);
                                    break;
                            }
                        }
                    }

                    if (name.equals("equalizer")) {
                        UpdateControls();
                        done = true;
                    }
                    break;
            }

            try {
                if (!done)
                    event = myParser.next();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException {
/*
        serializer.startTag(null, "equalizer");                     // Write the front Equalizer data
        serializer.startTag(null, "channel");                       // First we write Channel ID & gain
        serializer.attribute(null, "id", "front");
        serializer.attribute(null, "gain", Float.toString(eq_gain_front.GetGain()));
        serializer.endTag(null, "channel");

        for (int index = 0; index < 31; index++) {              // Now write the EQ data
            serializer.startTag(null, "band");
            serializer.attribute(null, "id", Integer.toString(index));
            serializer.attribute(null, "frequency", slider_filters_front[index].dsp_filter.GetFrequencyString());
            serializer.attribute(null, "boost", Float.toString(slider_filters_front[index].dsp_filter.GetBoost()));
            serializer.endTag(null, "band");
        }
        serializer.endTag(null, "equalizer");                       // End tag for Front Equalizer

        serializer.startTag(null, "equalizer");                     // Write the Rear Equalizer data
        serializer.startTag(null, "channel");                       // First we write Channel ID & gain
        serializer.attribute(null, "id", "rear");
        serializer.attribute(null, "gain", Float.toString(eq_gain_rear.GetGain()));
        serializer.endTag(null, "channel");

        for (int index = 0; index < 31; index++) {              // Now write the EQ data
            serializer.startTag(null, "band");
            serializer.attribute(null, "id", Integer.toString(index));
            serializer.attribute(null, "frequency", slider_filters_rear[index].dsp_filter.GetFrequencyString());
            serializer.attribute(null, "boost", Float.toString(slider_filters_rear[index].dsp_filter.GetBoost()));
            serializer.endTag(null, "band");
        }
        serializer.endTag(null, "equalizer");                       // End tag for Rear Equalizer

        serializer.startTag(null, "equalizer");                     // Write the Sub Woofer Equalizer data
        serializer.startTag(null, "channel");                       // First we write Channel ID & gain
        serializer.attribute(null, "id", "woofer");
        serializer.attribute(null, "gain", Float.toString(eq_gain_woofer.GetGain()));
        serializer.endTag(null, "channel");

        for (int index = 0; index < 31; index++) {              // Now write the EQ data
            serializer.startTag(null, "band");
            serializer.attribute(null, "id", Integer.toString(index));
            serializer.attribute(null, "frequency", slider_filters_woofer[index].dsp_filter.GetFrequencyString());
            serializer.attribute(null, "boost", Float.toString(slider_filters_woofer[index].dsp_filter.GetBoost()));
            serializer.endTag(null, "band");
        }
        serializer.endTag(null, "equalizer");                       // End tag for Rear Equalizer}
        */
    }
}