package com.ckt_works.AX_DSP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.widget.ListPopupWindow;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

// Created by Abeck on 12/2/2016. //

public class FragmentEqSettings_LC extends CScreenNavigation implements View.OnClickListener, View.OnTouchListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener
,IPostExecuteInterface{
// TODO ANDY REMOVE ALL onPOst Execute Stuff
    // Interface to allow the "TaskSendData" to callback when done sending data
    public void onPostExecute()
    {
        Log.i("FragmentEqSettings", "Tone Controls TEST Response");
    }

    private class SliderFilter
    {
        protected Integer slider_id;            // ID of the slider associated with this Filter
        protected DspEqFilter dsp_filter;       // DSP Filter object associated with this Filter
        protected Integer text_id;              // ID of the text box associated with this Filter

        SliderFilter(Integer slider, DspEqFilter filter, Integer text_box)
        {
            slider_id = slider;
            dsp_filter = filter;
            text_id = text_box;
        }
    }

    // ID's of all of the EQ controls
    private int channelContainerIds[] = {R.id.containerEqLocation_1, R.id.containerEqLocation_2, R.id.containerEqLocation_3, R.id.containerEqLocation_4, R.id.containerEqLocation_5};
    private int eqFrequencies[] = {25, 40, 63, 100, 160, 250, 400, 630, 1000, 1600, 2500, 4000, 6300, 10000, 16000};

    private int channelButtonIds[] = {R.id.butEqLocation_1, R.id.butEqLocation_2, R.id.butEqLocation_3, R.id.butEqLocation_4, R.id.butEqLocation_5};
    private int GroupImageIds[] = {R.id.imageEqGroup_1, R.id.imageEqGroup_2, R.id.imageEqGroup_3, R.id.imageEqGroup_4, R.id.imageEqGroup_5};

    private int sliderIds[] = {R.id.sliderEq1, R.id.sliderEq2, R.id.sliderEq3, R.id.sliderEq4, R.id.sliderEq5, R.id.sliderEq6, R.id.sliderEq7,
                               R.id.sliderEq8, R.id.sliderEq9, R.id.sliderEq10, R.id.sliderEq11, R.id.sliderEq12, R.id.sliderEq13, R.id.sliderEq14,
                               R.id.sliderEq15};

    private int textIds[] = {R.id.text_eq1_value, R.id.text_eq2_value, R.id.text_eq3_value, R.id.text_eq4_value, R.id.text_eq5_value, R.id.text_eq6_value,
                             R.id.text_eq7_value, R.id.text_eq8_value, R.id.text_eq9_value, R.id.text_eq10_value, R.id.text_eq11_value, R.id.text_eq12_value,
                             R.id.text_eq13_value, R.id.text_eq14_value, R.id.text_eq15_value};


    private boolean updating_sliders = false;
    private MainActivity m_main_Activity;
    private View myView;
    int selectedChannel;     // EQ Channel currently selected;

    private DspGain eqGains[];
    private SliderFilter[][] sliderFilters;
    private DspEqFilter[][] dspFilters;
    private CwSlider[] SlidersEq;
    private TextView[] EqTexts;
    private TextView TextNoOutputsAssigned;
    private LinearLayout layoutEqControls;
    private ListPopupWindow valuePopUpWindow;
    private ArrayAdapter<String> gainValueArray;
    private String[] gainValues;

    private String[] eqBoosts;
    private ArrayAdapter<String> eqBoostArray;
    private int last_boost_value[] = new int[15];

    private int numberSupportedChannels = 5;                   // Number of channels for the connected device
    private int numberSupportedEqBands = 15;


    private View containerChannels[];
    private Button buttonsChannel[];
    private Button imageGroup[];
    private ConstraintLayout ViewGain;
    private CwSlider SliderEqGain;
    private TextView TextGain;
    private Button ButtonFlat;
    private DspCommand dsp_flatten_eq_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_FLATTEN_EQ);

    private boolean displayingProgressBar = false;
    public static FragmentEqSettings_LC newInstance() {
        return new FragmentEqSettings_LC();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        m_main_Activity =  ((MainActivity) getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DspService.ACTION_ACK_RECEIVED);
        intentFilter.addAction(DspService.ACTION_NAK_RECEIVED);
        LocalBroadcastManager.getInstance(m_main_Activity).registerReceiver(BleStatusChangeReceiver, intentFilter);
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        numberSupportedChannels = m_main_Activity.GetNumberSupportedChannels();
        numberSupportedEqBands = m_main_Activity.GetNumberEqBands();
        myView = inflater.inflate(R.layout.layout_eq_settings_lc, container, false);
        if (myView != null) {
            Context my_context = myView.getContext();
            TextNoOutputsAssigned = (TextView) myView.findViewById(R.id.textNoOutputAssigned);
            layoutEqControls = (LinearLayout) myView.findViewById(R.id.layoutEqControls);

            ButtonFlat = (Button) myView.findViewById(R.id.butEqFlat);
            ButtonFlat.setOnClickListener(this);

            containerChannels = new View[5];
            for (int index = 0; index < containerChannels.length; index++)
                containerChannels[index] = (View) myView.findViewById(channelContainerIds[index]);

            buttonsChannel = new Button[5];
            for (int index = 0; index < buttonsChannel.length; index++) {
                buttonsChannel[index] = (Button) myView.findViewById(channelButtonIds[index]);
                buttonsChannel[index].setText(m_main_Activity.mOutputs_X_Fragment.GetLocationAbbreviation(index));
                buttonsChannel[index].setOnClickListener(this);
            }

            imageGroup = new Button[5];
            for (int index = 0; index < imageGroup.length; index++) {
                imageGroup[index] = (Button) myView.findViewById(GroupImageIds[index]);
            }

            // Create the EQ Slider objects
            SlidersEq =  new CwSlider[15];
            for (int band = 0; band < 15; band++)
            {
                SlidersEq[band] = (CwSlider) myView.findViewById(sliderIds[band]);
                SlidersEq[band].setRange(-10, 10);
                SlidersEq[band].setDelegateOnTouchListener(this);
            }

            EqTexts = new TextView[15];
            for (int band = 0; band < 15; band++) {
                EqTexts[band] = (TextView) myView.findViewById(textIds[band]);
                EqTexts[band].setOnClickListener(this);
            }
            // Create the adapters to populate the valuePopUpWindow list
            // Build list of valid Gain values (+10 to -10)
            DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
            gainValues = new String[21];
            float gain_value = 10;
            for (int i = 0; i < gainValues.length; i++)
            {
                if (gain_value == 0)
                    gainValues[i] = " 0";
                else
                    gainValues[i] = decimalFormat.format(gain_value);
                gain_value--;
            }
            gainValueArray = new ArrayAdapter<String>(my_context, R.layout.list_item, gainValues);

            // Build list of valid EQ Boost values (+10 to -10)
            DecimalFormat boostFormat = new DecimalFormat("+0.0;-0.0");
            eqBoosts = new String[201];
            float eg_level = 100;
            for (int i = 0; i < eqBoosts.length; i++)
            {
                if (eg_level == 0)
                    eqBoosts[i] = " 0";
                else
                    eqBoosts[i] = boostFormat.format(eg_level/10);
                eg_level--;
            }
            eqBoostArray = new ArrayAdapter<String>(my_context, R.layout.list_item, eqBoosts);


            // Create the Gain and EQ Filter objects
            eqGains = new DspGain [5];
            sliderFilters = new SliderFilter[5][15];
            dspFilters = new DspEqFilter[5][15];
            for (int channel = 0; channel < 5; channel++) {
                eqGains[channel] = new DspGain(channel);
                sliderFilters[channel] = new SliderFilter[15];
                for (int band=0; band < 15; band++) {
                    dspFilters[channel][band] = new DspEqFilter(channel, band, eqFrequencies[band]);
                    sliderFilters[channel][band] = new SliderFilter(sliderIds[band], dspFilters[channel][band], textIds[band]);
//                    DisplayFilterAddress(channel, band);
                }
            }

            // Create the Gain View, Slider and Text box
            ViewGain = (ConstraintLayout) myView.findViewById(R.id.layoutGain);
            //ViewGain.setVisibility(View.GONE);
            SliderEqGain = (CwSlider) myView.findViewById(R.id.sliderGain);
            SliderEqGain.setRange(-10, +10);
            SliderEqGain.setValue(0);
            SliderEqGain.setDecimalMultiplier(1);
            SliderEqGain.setDelegateOnTouchListener(this);

            TextGain = (TextView) myView.findViewById(R.id.text_gain_value);
            TextGain.setOnClickListener(this);

            // Initialize the Slider Popup Window
            valuePopUpWindow = new ListPopupWindow(my_context);
            valuePopUpWindow.setAdapter(gainValueArray);
            valuePopUpWindow.setWidth(250);
            valuePopUpWindow.setModal(true);
            valuePopUpWindow.setOnItemClickListener(this);


            EnableControls(false);
        }
//        Log.i("FragmentEqSettings_X","onCreateView - END");
        return (myView);
    }// End 'onCreateView'

    public void SetChannelsBands(int number_channels, int number_bands) {
        this.numberSupportedChannels = number_channels;
        this.numberSupportedEqBands = number_bands;
    }


    // Receives inter-process messages from other components
    private final BroadcastReceiver BleStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {

                    case DspService.ACTION_ACK_RECEIVED:
                        Log.i("FragmentEqSettings", "ACK Received");
                        if (displayingProgressBar) {
                            m_main_Activity.ProgressBarDismiss();
                            displayingProgressBar = false;
                        }
                        break;

                    case DspService.ACTION_NAK_RECEIVED: {
                        Log.i("FragmentEqSettings", "NAK Received");
                        if (displayingProgressBar) {
                            m_main_Activity.ProgressBarDismiss();
                            displayingProgressBar = false;
                        }
                    }
                    break;
                }
            }
        }
    };



    void DisplayFilterAddress(int channel, int band) {
        String filterInfo = dspFilters[channel][band].toString();
        String sliderInfo = sliderFilters[channel][band].toString();
        Log.i("SP CH:     " + Integer.toString(channel) + "  BD: " + Integer.toString(band), filterInfo);
        Log.i("SLIDER CH: " + Integer.toString(channel) + "  BD: " + Integer.toString(band), sliderInfo);
    }



    @Override
    public void onHiddenChanged (boolean hidden)
    {
        numberSupportedChannels = m_main_Activity.GetNumberSupportedChannels();
        numberSupportedEqBands = m_main_Activity.GetNumberEqBands();
//        Log.i("FragmentEqSettings_X","onHiddenChanged - START");
        if (!hidden) {
            if (m_main_Activity.AnyChannelAssigned() == false) {
                layoutEqControls.setVisibility(View.GONE);
                TextNoOutputsAssigned.setVisibility(View.VISIBLE);
            } else {
                layoutEqControls.setVisibility(View.VISIBLE);
                TextNoOutputsAssigned.setVisibility(View.GONE);

                for (int index = 0; index < buttonsChannel.length; index++) {
                    boolean visible = m_main_Activity.mOutputs_X_Fragment.IsUsed(index);
                    containerChannels[index].setVisibility((visible ? View.VISIBLE : View.GONE));
                    if (visible) {
                        buttonsChannel[index].setText(m_main_Activity.mOutputs_X_Fragment.GetLocationAbbreviation(index));
                        int pngId = m_main_Activity.mOutputs_X_Fragment.GetPngId(index);
                        if (pngId >= 0) {
                            imageGroup[index].setBackgroundResource(pngId);
                            imageGroup[index].setVisibility(View.VISIBLE);
                        } else
                            imageGroup[index].setVisibility(View.GONE);
                    }
                }
                UpdateControls();
            }
//            Log.i("FragmentEqSettings_X","onHiddenChanged - END");
        }
//        Log.i("FragmentEqSettings_X","onHiddenChanged - END");
    }

    // Interface to allow the "TaskSendData" to callback when done sending data
 /*   public void onPostExecute()
    {
        progress_dialog.dismiss();
    }*/

    // Set the EQ Filter Boost value - update the slider position & the textbox
    public void SetEqBoostLevel(int channel, int band, int boost)
    {
        if ((channel < 4 && band < 15) || band < 7) {
            SliderFilter this_filter = sliderFilters[channel][band];
            float boost_value = ((float) boost) / 10;                           // Set Boost value

                int textId = this_filter.text_id;                                   // Update Value Text Box
                TextView textBoost = (TextView) myView.findViewById(textId);
                SetValueTextBox(textBoost, boost_value);

                int slider_id = this_filter.slider_id;                              // Update Slider
                CwSlider sliderBoost = (CwSlider) myView.findViewById(slider_id);
                sliderBoost.setValue(boost_value);

            DspEqFilter eqFilter = this_filter.dsp_filter;                      // Update Filter
            eqFilter.SetBoost(boost_value);
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonView.setBackgroundResource(R.drawable.speaker_icon_ax_dsp_green_25);
        }
        else {
            buttonView.setBackgroundResource(R.drawable.speaker_icon_ax_dsp_muted_w_gray2_35);
        }
    }

    // Handles all clickable controls on this screen
    public void onClick(View control) {
        int id  = control.getId();

        if (id == R.id.butEqFlat) {                                          // "Set To Flat" button pressed
            m_main_Activity.ProgressBarDisplay("Flattening EQ Data");   // Display a Progress Bar while we wait for 386 to lock down data
            displayingProgressBar = true;
            updating_sliders = true;
Log.i("%%% Flatten Master", Integer.toString(selectedChannel));
            ResetEqFilters(selectedChannel, false);            // Reset the filter for the displayed channels
                                                                           // Reset all slave channels in the group
            DspGroup group = m_main_Activity.mOutputs_X_Fragment.GetChannelGroup(selectedChannel);
            for (int channel = 0; channel < 5; channel++) {
                if (group.SlaveIsInGroup(channel)) {
Log.i("%%%-- Flatten Slave", Integer.toString(channel));
                    for (int band = 0; band < 15; band++)
                        sliderFilters[channel][band].dsp_filter.SetBoost(0);
                }
            }

            int slaveMask = group.GetSlaveMask();
Log.i("%%%-- Slave Mask", Integer.toString(group.GetSlaveMask()));
            dsp_flatten_eq_command.SetData((byte)selectedChannel, (byte)slaveMask, (byte)(slaveMask >> 8));
            DspService dspService = ((MainActivity) getActivity()).dspService;
            dspService.SendDspCommand(dsp_flatten_eq_command);
            updating_sliders = false;
        }

        else if (id == R.id.text_gain_value) {
            int popupHeight = SliderEqGain.getHeight();
            valuePopUpWindow.setHeight(popupHeight);
            valuePopUpWindow.setWidth(250);
//            valuePopUpWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            valuePopUpWindow.setAnchorView(TextGain);
            valuePopUpWindow.setAdapter(gainValueArray);
            valuePopUpWindow.show();

            String strGainValue = (String)TextGain.getText();
            float floatGainValue = Float.valueOf(strGainValue);
            int intGainValue = (int)(floatGainValue * 10);
            int gainIndex = 60 - intGainValue;
            if (gainIndex > 6)
                gainIndex -= 6;
            else
                gainIndex = 0;
            valuePopUpWindow.setSelection(gainIndex);
        }

        else {
            SliderFilter sliderFilter = FindSliderFilter((TextView)control);
            if (sliderFilter != null)
            {
                String strBoostValue = (String)((TextView) myView.findViewById(sliderFilter.text_id)).getText();
                float floatBoostValue = Float.valueOf(strBoostValue);
                int intBoostValue = (int)(floatBoostValue * 10);
                int boostIndex = 100 - intBoostValue;
                if (boostIndex > 6)
                    boostIndex -= 6;
                else
                    boostIndex = 0;
                int popupHeight = SlidersEq[0].getHeight();
                valuePopUpWindow.setHeight(popupHeight);
                valuePopUpWindow.setWidth(225);
                valuePopUpWindow.setAnchorView(control);
                valuePopUpWindow.setAdapter(eqBoostArray);
                valuePopUpWindow.show();
                valuePopUpWindow.setSelection(boostIndex);
            }

            else {
                // Channel button clicked
                for (int channel = 0; channel < channelButtonIds.length; channel++) {
                    if (id == channelButtonIds[channel]) {
                        buttonsChannel[channel].setBackgroundResource(R.drawable.but_eq_selected_inverted_510);
                        selectedChannel = channel;
                    } else
                        buttonsChannel[channel].setBackgroundResource(R.drawable.but_eq_not_selected_inverted_510);
                }
                UpdateControls();
                EnableControls(true);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int anchor_id = valuePopUpWindow.getAnchorView().getId();

        if (anchor_id == TextGain.getId()) {                            // If it was the Gain valule
            TextGain.setText(gainValues[position]);
            float gain = Float.valueOf(gainValues[position]);
            SliderEqGain.setValue(gain);
        }
        else                                                            // Assume it's a EQ Value
        {
            SliderFilter filter = FindSliderFilter((TextView) valuePopUpWindow.getAnchorView());
            if (filter != null)
            {
                TextView textLevel = (TextView) myView.findViewById(filter.text_id);
                if (textLevel != null)
                    textLevel.setText(eqBoosts[position]);

                float boost = Float.valueOf(eqBoosts[position]);
                CwSlider this_slider = (CwSlider) myView.findViewById(filter.slider_id);
                this_slider.setValue((boost));                          // Set the Boost value
                filter.dsp_filter.SetBoost(boost);
                m_main_Activity.SendDspEQ(filter.dsp_filter);           // Send the data
            }
        }
        valuePopUpWindow.dismiss();
    }

    @Override
    // This function handles the Touch Event for all sliders on this screen
    public boolean onTouch (View slider, MotionEvent event){
        int control_id = slider.getId();
        boolean processed = false;
//Log.i("onTouch", "action = " + Integer.toString(event.getAction()));
        if (control_id == R.id.sliderGain) {
            CwSlider this_slider = (CwSlider) slider;
            GainChanged(this_slider.getSliderValue());
            processed = true;
        }

        if (processed == false) { // Must be an EQ slider
            if (!updating_sliders) {
                CwSlider this_slider = (CwSlider) slider;
                int slider_id = slider.getId();

                for (int band = 0; band < 15; band++) {                              // Find the filter for the slider we touched
                    int this_slider_id = sliderFilters[selectedChannel][band].slider_id;
                    if (this_slider_id == slider_id) {
                        float boost_value = this_slider.getSliderValue();
                        int intBoostValue = Math.round(boost_value * 10);
                        if (last_boost_value[band] != intBoostValue) {
                            last_boost_value[band] = intBoostValue;
                            EnableControls(false);
                            SetValueTextBox(sliderFilters[selectedChannel][band], boost_value);
                            sliderFilters[selectedChannel][band].dsp_filter.SetBoost(boost_value);         // Update the value
                            UpdateSlaveEqs(selectedChannel, band, boost_value);
                            int group = m_main_Activity.mOutputs_X_Fragment.GetGroupIndex(selectedChannel);
                            m_main_Activity.SendDspEQ(sliderFilters[selectedChannel][band].dsp_filter, group);  // Send the data and indicate group
                            break;
                        }
                    }
                }
            }
        }
        return(true);
    }

    void SetValueTextBox(SliderFilter slider_filter, float boost_value) {
        TextView TextBox = (TextView) myView.findViewById(slider_filter.text_id);
        SetValueTextBox(TextBox, boost_value);
    }

    void SetValueTextBox( TextView TextBox, float value){
        this.SetValueTextBox(TextBox, value, 1);
    }

    void SetValueTextBox( TextView TextBox, float value, int decimal_places){
        String displayString;
        if (value > 9.95)
            displayString = "+10";
        else if (value < -9.95)
            displayString = "-10";
        else if (value == 0)
            displayString = "0";
        else {
            String format_string = "%+1.0f";
            if (decimal_places > 0)
                format_string = "%+2.1f";
            displayString = String.format(Locale.US, format_string, value);
        }
        TextBox.setText(displayString);
    }


    public void GainChanged(float gain_value)
    {
        if (!updating_sliders)
        {
            EnableControls(false);
            eqGains[selectedChannel].SetGain(gain_value);
            m_main_Activity.SendDspGain(eqGains[selectedChannel]);
            SetValueTextBox(TextGain, gain_value, 0);
        }
    }

    boolean UpdateSlaveEqs(int selectedChannel, int band, float boost_value) {
        boolean hasSlaves = false;
        if (m_main_Activity.mOutputs_X_Fragment.isMaster(selectedChannel)) {
            int masterGroup = m_main_Activity.mOutputs_X_Fragment.GetGroupIndex(selectedChannel);
            for (int channel = 0; channel < 5; channel++) {
                if (m_main_Activity.mOutputs_X_Fragment.isSlaveOfGroup(masterGroup, channel)) {
                    sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);
//Log.i("UpdateSlaveEqs", "Master: " + Integer.toString(selectedChannel) + " Slave: " + Integer.toString(channel));
                }
            }
        }
        return(hasSlaves);
    }


    // Set the position of all sliders to the value in their associated data object
    // as well as the value textbox
    public void UpdateControls()
    {
//        Log.i("UpdateControls","START");
        updating_sliders = true;                        // Prevent the slider changes from sending data to the 386
        float gain_value = eqGains[selectedChannel].GetGain();
        SliderEqGain.setValue(gain_value);
        DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
        TextGain.setText(decimalFormat.format(gain_value));


        // Update all of the DSP Filter sliders
        for (int band = 0; band < 15; band++) {
            if (selectedChannel < 4 || band < 7) {
                SliderFilter this_filter = sliderFilters[selectedChannel][band];
                float this_boost = this_filter.dsp_filter.GetBoost();

                CwSlider this_slider = (CwSlider) myView.findViewById(sliderIds[band]);
                this_slider.setValue((this_boost));
                SetValueTextBox(this_filter, this_boost);
                //DisplayFilterAddress(selectedChannel, band);
            }
            else {                                                                  // Bands above 400 Hz on Sub-woofer channel not used
                CwSlider this_slider = (CwSlider) myView.findViewById(sliderIds[band]);
                this_slider.setValue((0));
            }
        }
// TODO Add code to handle this
        if (m_main_Activity.Connected())
            EnableControls(true);
//        SetSlaveEnableState(selectedChannel);
        updating_sliders = false;
//        Log.i("UpdateControls","END");
    }

    void CopyEqFilter(int source_channel, int dest_channel) {
        for (int band = 0; band < 15; band++) {
            sliderFilters[dest_channel][band].dsp_filter.SetBoost(sliderFilters[source_channel][band].dsp_filter.GetBoost());
        }
    }

    // Reset select filters & gains to their Factory Default position
    public void ResetEqFilters(int channel, Boolean include_gains)
    {
        CwSlider this_slider;
        TextView this_value_text;
    //    updating_sliders = true;

        if (channel < eqGains.length) {
            if (include_gains) {
                eqGains[channel].SetGain(0);
                SliderEqGain.setValue(0);
            }

            for (int band = 0; band < 15; band++) {
                // Reset physical slider
                this_slider = (CwSlider) myView.findViewById(sliderIds[band]);
                this_slider.setValue(0);
                sliderFilters[channel][band].dsp_filter.SetBoost(0);
                this_value_text = (TextView)  myView.findViewById(sliderFilters[channel][band].text_id);
                SetValueTextBox(this_value_text, 0);
            }
        }
  //      updating_sliders = false;
    }


    public void EnableControls(Boolean state)
    {
        if (SliderEqGain != null) {
            boolean eqState = state;
            if (state == true) {
                boolean isMaster = m_main_Activity.mOutputs_X_Fragment.isMaster(selectedChannel);
                boolean isNotSlave = !m_main_Activity.mOutputs_X_Fragment.isSlave(selectedChannel);
                state = isMaster | isNotSlave;
            }

            SliderEqGain.setEnabled(eqState);
            ButtonFlat.setEnabled(state);
            for (int band = 0; band < 15; band++) {
                if (selectedChannel < 4 || band < 7) {      // Don't enable bands above 400Hz on channel 4 of 440
                    SlidersEq[band].setNotUsed(false);
                    SlidersEq[band].setEnabled(state);
                    EqTexts[band].setVisibility(View.VISIBLE);
                    EqTexts[band].setEnabled(state);
                }
                else
                {
                    SlidersEq[band].setEnabled(false);
                    SlidersEq[band].setNotUsed(true);
                    EqTexts[band].setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    // Send all of our data to the 440
    public int SendData(int channel) {
        int send_time = 0;
        if (m_main_Activity.Connected() && channel < sliderFilters.length)
        {
            for (int band = 0; band < numberSupportedEqBands; band++) {
                if (channel < 4 || band < 7) {                                  // Don't send bands above 400Hz on Sub-woofer channel
                    float boost_value = sliderFilters[channel][band].dsp_filter.GetBoost();
                    sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);                  // TODO DO WE NEED THIS LINE
                    m_main_Activity.dspService.SetWaitingForAck(true);
                    m_main_Activity.SendDspEQ(sliderFilters[channel][band].dsp_filter);
                    int this_send_time = m_main_Activity.dspService.WaitForAck();
                    if (this_send_time >= 0)
                        send_time += m_main_Activity.dspService.WaitForAck();
                    else {
                        send_time = this_send_time;
                        break;
                    }
                }
            }
        }
        return(send_time);
    }


    public void SetGains(int gains[])
    {
        for (int band=0; band < gains.length; band++)
            eqGains[band].SetGain((float)gains[band] -  eqGains[band].GetGainOffset());
    }


    private SliderFilter FindSliderFilter(TextView textView) {
        SliderFilter this_filter = null;
        int text_id = textView.getId();

        for (int band = 0; band < numberSupportedEqBands; band++) {
            if (sliderFilters[selectedChannel][band].text_id == text_id) {
                this_filter = sliderFilters[selectedChannel][band];
                break;
            }
        }
        return(this_filter);
    }

        // Parses an XML file and updates the EQ Filter data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;
        int channel = 0;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            switch (event) {
                case XmlPullParser.START_TAG:
                    break;

                case XmlPullParser.END_TAG:
                    if (name.equals("channel"))
                    {
                        String channel_name = myParser.getAttributeValue(null, "id");
                        channel = Integer.valueOf(channel_name);
                        String gain = myParser.getAttributeValue(null, "gain");
                        eqGains[channel].SetGain(gain);
                    }

                    else if (name.equals("band"))
                    {
                        String band_name = myParser.getAttributeValue(null, "id");
                        int band = Integer.valueOf(band_name);
                        if (channel < 4 || band < 7) {
                            String frequency = myParser.getAttributeValue(null, "frequency");
                            String boost = myParser.getAttributeValue(null, "boost");
                            float boost_value = Float.valueOf(boost);
                            sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);
                        }
                    }

                    if (name.equals("equalizer")) {
                        UpdateControls();
                        done = true;
                    }
                    break;
            }

            try {
                if (!done)
                    event = myParser.next();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException {
        for (int channel = 0; channel < 5; channel++) {
            serializer.startTag(null, "equalizer");                             // Write the Channel's Equalizer data
            serializer.startTag(null, "channel");                               // First we write Channel ID & gain
            serializer.attribute(null, "id", Integer.toString(channel));
            serializer.attribute(null, "gain", Float.toString(eqGains[channel].GetGain()));
            serializer.endTag(null, "channel");

            // Now write the EQ data
            for (int band = 0; band < 15; band++) {
                if (channel < 4 || band < 7) {
                    serializer.startTag(null, "band");
                    serializer.attribute(null, "id", Integer.toString(band));
                    serializer.attribute(null, "frequency", sliderFilters[channel][band].dsp_filter.GetFrequencyString());
                    serializer.attribute(null, "boost", Float.toString(sliderFilters[channel][band].dsp_filter.GetBoost()));
                    serializer.endTag(null, "band");
                }
            }
            serializer.endTag(null, "equalizer");
        }
    }
}