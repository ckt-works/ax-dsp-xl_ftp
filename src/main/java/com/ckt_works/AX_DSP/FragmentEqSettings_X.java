package com.ckt_works.AX_DSP;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.appcompat.widget.ListPopupWindow;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;

// Created by Abeck on 12/2/2016. //

public class FragmentEqSettings_X extends CScreenNavigation implements View.OnClickListener, View.OnTouchListener, CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener {

    private class SliderFilter
    {
        protected Integer slider_id;            // ID of the slider associated with this Filter
        protected DspEqFilter dsp_filter;       // DSP Filter object associated with this Filter
        protected Integer text_id;              // ID of the text box associated with this Filter

        SliderFilter(Integer slider, DspEqFilter filter, Integer text_box)
        {
            slider_id = slider;
            dsp_filter = filter;
            text_id = text_box;
        }
    }

    // ID's of all of the EQ controls
    private int eqFrequencies_X[] = {20, 25, 31, 40, 50, 63, 80, 100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000,
            1250, 1600, 2000, 2500, 3150, 4000, 5000, 6300, 8000, 10000, 12500, 16000, 20000,};
    private int channelContainerIds[] = {R.id.containerEqLocation_1, R.id.containerEqLocation_2, R.id.containerEqLocation_3, R.id.containerEqLocation_4, R.id.containerEqLocation_5,
                                        R.id.containerEqLocation_6, R.id.containerEqLocation_7, R.id.containerEqLocation_8, R.id.containerEqLocation_9, R.id.containerEqLocation_10};

    private int channelButtonIds[] = {R.id.butEqLocation_1, R.id.butEqLocation_2, R.id.butEqLocation_3, R.id.butEqLocation_4, R.id.butEqLocation_5,
                                       R.id.butEqLocation_6, R.id.butEqLocation_7, R.id.butEqLocation_8, R.id.butEqLocation_9, R.id.butEqLocation_10};
    private int GroupImageIds[] = {R.id.imageEqGroup_1, R.id.imageEqGroup_2, R.id.imageEqGroup_3, R.id.imageEqGroup_4, R.id.imageEqGroup_5,
                                   R.id.imageEqGroup_6, R.id.imageEqGroup_7, R.id.imageEqGroup_8, R.id.imageEqGroup_9, R.id.imageEqGroup_10};

    private int sliderIds[] = {R.id.sliderEq1, R.id.sliderEq2, R.id.sliderEq3, R.id.sliderEq4, R.id.sliderEq5, R.id.sliderEq6, R.id.sliderEq7,
                               R.id.sliderEq8, R.id.sliderEq9, R.id.sliderEq10, R.id.sliderEq11, R.id.sliderEq12, R.id.sliderEq13, R.id.sliderEq14,
                               R.id.sliderEq15, R.id.sliderEq16, R.id.sliderEq17, R.id.sliderEq18, R.id.sliderEq19, R.id.sliderEq20, R.id.sliderEq21,
                               R.id.sliderEq22, R.id.sliderEq23, R.id.sliderEq24, R.id.sliderEq25, R.id.sliderEq26, R.id.sliderEq27, R.id.sliderEq28,
                               R.id.sliderEq29, R.id.sliderEq30, R.id.sliderEq31};

    private int textIds[] = {R.id.text_eq1_value, R.id.text_eq2_value, R.id.text_eq3_value, R.id.text_eq4_value, R.id.text_eq5_value, R.id.text_eq6_value,
                             R.id.text_eq7_value, R.id.text_eq8_value, R.id.text_eq9_value, R.id.text_eq10_value, R.id.text_eq11_value, R.id.text_eq12_value,
                             R.id.text_eq13_value, R.id.text_eq14_value, R.id.text_eq15_value, R.id.text_eq16_value, R.id.text_eq17_value, R.id.text_eq18_value,
                             R.id.text_eq19_value, R.id.text_eq20_value, R.id.text_eq21_value, R.id.text_eq22_value, R.id.text_eq23_value, R.id.text_eq24_value,
                             R.id.text_eq25_value, R.id.text_eq26_value, R.id.text_eq27_value, R.id.text_eq28_value, R.id.text_eq29_value,
                             R.id.text_eq30_value, R.id.text_eq31_value};

    private boolean updating_sliders = false;
    private MainActivity m_main_Activity;
    private View myView;
    private TextView TextNoOutputsAssigned;
    private LinearLayout layoutEqControls;

    int selectedChannel;     // EQ Channel currently selected;

    private DspGain eqGains[];
    private SliderFilter[][] sliderFilters;
    private DspEqFilter[][] dspFilters;
    private CwSlider[] SlidersEq;
    private TextView[] EqTexts;

    private ListPopupWindow valuePopUpWindow;
    private ArrayAdapter<String> gainValueArray;
    private String[] gainValues;


    private int last_boost_value[] = new int[31];

    private int numberSupportedChannels = 10;                   // Number of channels for the connected device
    private int numberSupportedEqBands = 31;


    private View containerChannels[];
    private Button buttonsChannel[];
    private Button imageGroup[];
    private ConstraintLayout ViewGain;
    private CwSlider SliderEqGain;
    private TextView TextGain;
    private Button ButtonFlat;
    private DspCommand dsp_flatten_eq_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_FLATTEN_EQ);

    private boolean displayingProgressBar = false;

    public static FragmentEqSettings_X newInstance() {
        return new FragmentEqSettings_X();
    }
    public FragmentEqSettings_X() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        m_main_Activity =  ((MainActivity) getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DspService.ACTION_ACK_RECEIVED);
        intentFilter.addAction(DspService.ACTION_NAK_RECEIVED);
        LocalBroadcastManager.getInstance(m_main_Activity).registerReceiver(BleStatusChangeReceiver, intentFilter);
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        Log.i("FragmentEqSettings_X","onCreateView - START");

        numberSupportedChannels = m_main_Activity.GetNumberSupportedChannels();
        numberSupportedEqBands = m_main_Activity.GetNumberEqBands();

        myView = inflater.inflate(R.layout.layout_eq_settings_x, container, false);
        if (myView != null) {

            Context my_context = myView.getContext();

            TextNoOutputsAssigned = (TextView) myView.findViewById(R.id.textNoOutputAssigned);
            layoutEqControls = (LinearLayout) myView.findViewById(R.id.layoutEqControls);

            ButtonFlat = (Button) myView.findViewById(R.id.butEqFlat);
            ButtonFlat.setOnClickListener(this);

            containerChannels = new View[10];
            for (int index = 0; index < containerChannels.length; index++)
                containerChannels[index] = (View) myView.findViewById(channelContainerIds[index]);

            buttonsChannel = new Button[10];
            for (int index = 0; index < buttonsChannel.length; index++) {
                buttonsChannel[index] = (Button) myView.findViewById(channelButtonIds[index]);
                buttonsChannel[index].setText(m_main_Activity.mOutputs_X_Fragment.GetLocationAbbreviation(index));
                buttonsChannel[index].setOnClickListener(this);
            }

            imageGroup = new Button[10];
            for (int index = 0; index < imageGroup.length; index++) {
                imageGroup[index] = (Button) myView.findViewById(GroupImageIds[index]);
            }

            // Create the EQ Slider objects
            SlidersEq =  new CwSlider[31];
            for (int band = 0; band < 31; band++)
            {
                SlidersEq[band] = (CwSlider) myView.findViewById(sliderIds[band]);
                SlidersEq[band].setRange(-10, 10);
                SlidersEq[band].setDelegateOnTouchListener(this);
            }

            EqTexts = new TextView[31];
            for (int band = 0; band < 31; band++) {
                EqTexts[band] = (TextView) myView.findViewById(textIds[band]);
// TODO ANDY - REENABLE TETX VIEW EDITING????                EqTexts[band].setOnClickListener(this);
            }
            // Create the adapters to populate the valuePopUpWindow list
            // Build list of valid Gain values (+10 to -10)
            DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
            gainValues = new String[201];
            float gain_value = 100;
            for (int i = 0; i < gainValues.length; i++)
            {
                if (gain_value == 0)
                    gainValues[i] = " 0";
                else
                    gainValues[i] = decimalFormat.format(gain_value/10);
                gain_value--;
            }
            gainValueArray = new ArrayAdapter<String>(my_context, R.layout.list_item, gainValues);

            // TODO ANDY MOVE eqBoostArray = new ArrayAdapter(my_context, R.layout.list_item, eqBoosts);


            // Create the Gain and EQ Filter objects
            eqGains = new DspGain [10];
            sliderFilters = new SliderFilter[10][31];
            dspFilters = new DspEqFilter[10][31];
            for (int channel = 0; channel < 10; channel++) {
                eqGains[channel] = new DspGain(channel);
                sliderFilters[channel] = new SliderFilter[31];
                for (int band=0; band < 31; band++) {
                    dspFilters[channel][band] = new DspEqFilter(channel, band, eqFrequencies_X[band]);
                    sliderFilters[channel][band] = new SliderFilter(sliderIds[band], dspFilters[channel][band], textIds[band]);
//                    DisplayFilterAddress(channel, band);
                }
            }

            // Create the Gain View, Slider and Text box
            ViewGain = (ConstraintLayout) myView.findViewById(R.id.layoutGain);
            //ViewGain.setVisibility(View.GONE);
            SliderEqGain = (CwSlider) myView.findViewById(R.id.sliderGain);
            SliderEqGain.setRange(-10, +10);
            SliderEqGain.setValue(0);
            SliderEqGain.setDecimalMultiplier(1);
            SliderEqGain.setDelegateOnTouchListener(this);

            TextGain = (TextView) myView.findViewById(R.id.text_gain_value);
            TextGain.setOnClickListener(this);

            // Initialize the Slider Popup Window
            valuePopUpWindow = new ListPopupWindow(my_context);
            valuePopUpWindow.setAdapter(gainValueArray);
            valuePopUpWindow.setWidth(250);
            valuePopUpWindow.setModal(true);
            valuePopUpWindow.setOnItemClickListener(this);


            EnableControls(false);
        }
//        Log.i("FragmentEqSettings_X","onCreateView - END");
        return (myView);
    }// End 'onCreateView'

    public void SetChannelsBands(int number_channels, int number_bands) {
        this.numberSupportedChannels = number_channels;
        this.numberSupportedEqBands = number_bands;
    }

    // Receives inter-process messages from other components
    private final BroadcastReceiver BleStatusChangeReceiver = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action != null) {
                switch (action) {

                    case DspService.ACTION_ACK_RECEIVED:
//                        Log.i("FragmentEqSettings", "ACK Received");
                        if (displayingProgressBar) {
                            m_main_Activity.ProgressBarDismiss();
                            displayingProgressBar = false;
                        }
                        break;

                    case DspService.ACTION_NAK_RECEIVED: {
//                        Log.i("FragmentEqSettings", "NAK Received");
                        if (displayingProgressBar) {
                            m_main_Activity.ProgressBarDismiss();
                            displayingProgressBar = false;
                        }
                    }
                    break;
                }
            }
        }
    };



    void DisplayFilterAddress(int channel, int band) {
        String filterInfo = dspFilters[channel][band].toString();
        String sliderInfo = sliderFilters[channel][band].toString();
//        Log.i("SP CH:     " + Integer.toString(channel) + "  BD: " + Integer.toString(band), filterInfo);
//        Log.i("SLIDER CH: " + Integer.toString(channel) + "  BD: " + Integer.toString(band), sliderInfo);
    }



    @Override
    public void onHiddenChanged (boolean hidden)
    {
//        Log.i("FragmentEqSettings_X","onHiddenChanged - START");
        if (!hidden) {

            if (m_main_Activity.AnyChannelAssigned() == false)  {
                layoutEqControls.setVisibility(View.GONE);
                TextNoOutputsAssigned.setVisibility(View.VISIBLE);
            }
            else {
                layoutEqControls.setVisibility(View.VISIBLE);
                TextNoOutputsAssigned.setVisibility(View.GONE);

                for (int index = 0; index < buttonsChannel.length; index++) {
                    boolean visible = m_main_Activity.mOutputs_X_Fragment.IsUsed(index);
                    containerChannels[index].setVisibility((visible ? View.VISIBLE : View.GONE));
                    if (visible) {
                        buttonsChannel[index].setText(m_main_Activity.mOutputs_X_Fragment.GetLocationAbbreviation(index));
                        int pngId = m_main_Activity.mOutputs_X_Fragment.GetPngId(index);
                        if (pngId >= 0) {
                            imageGroup[index].setBackgroundResource(pngId);
                            imageGroup[index].setVisibility(View.VISIBLE);
                        } else
                            imageGroup[index].setVisibility(View.INVISIBLE);
                    }
                }
                UpdateControls();
            }

        }
    }

    // Interface to allow the "TaskSendData" to callback when done sending data
 /*   public void onPostExecute()
    {
        progress_dialog.dismiss();
    }
*/
    // Set the EQ Filter Boost value - update the slider position & the textbox
    public void SetEqBoostLevel(int channel, int band, int boost)
    {

        SliderFilter this_filter = sliderFilters[channel][band];
        float boost_value = ((float) boost) / 10;                           // Set Boost value

        int textId = this_filter.text_id;                                   // Update Value Text Box
        TextView textBoost = (TextView) myView.findViewById(textId);
        SetValueTextBox(textBoost, boost_value);

        int slider_id = this_filter.slider_id;                              // Update Slider
        CwSlider sliderBoost = (CwSlider) myView.findViewById(slider_id);
        sliderBoost.setValue(boost_value);

        DspEqFilter eqFilter = this_filter.dsp_filter;                      // Update Filter
        eqFilter.SetBoost(boost_value);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            buttonView.setBackgroundResource(R.drawable.speaker_icon_ax_dsp_green_25);
        }
        else {
            buttonView.setBackgroundResource(R.drawable.speaker_icon_ax_dsp_muted_w_gray2_35);
        }
    }

    // Handles all clickable controls on this screen
    public void onClick(View control) {
        int id  = control.getId();

        if (id == R.id.butEqFlat) {                                          // "Set To Flat" button pressed
            m_main_Activity.ProgressBarDisplay("Flattening EQ Data");   // Display a Progress Bar while we wait for 386 to lock down data
            displayingProgressBar = true;
            updating_sliders = true;
//Log.i("%%% Flatten Master", Integer.toString(selectedChannel));
            ResetEqFilters(selectedChannel, false);            // Reset the filter for the displayed channels
                                                                           // Reset all slave channels in the group
            DspGroup group = m_main_Activity.mOutputs_X_Fragment.GetChannelGroup(selectedChannel);
            for (int channel = 0; channel < 10; channel++) {
                if (group.SlaveIsInGroup(channel)) {
//Log.i("%%%-- Flatten Slave", Integer.toString(channel));
                    for (int band = 0; band < numberSupportedEqBands; band++)
                        sliderFilters[channel][band].dsp_filter.SetBoost(0);
                }
            }

            int slaveMask = group.GetSlaveMask();
//Log.i("%%%-- Slave Mask", Integer.toString(group.GetSlaveMask()));
            dsp_flatten_eq_command.SetData((byte)selectedChannel, (byte)slaveMask, (byte)(slaveMask >> 8));
            DspService dspService = ((MainActivity) getActivity()).dspService;
            dspService.SendDspCommand(dsp_flatten_eq_command);
            updating_sliders = false;
        }

        else if (id == R.id.text_gain_value) {
            int popupHeight = SliderEqGain.getHeight();
            valuePopUpWindow.setHeight(popupHeight);
            valuePopUpWindow.setWidth(250);
//            valuePopUpWindow.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
            valuePopUpWindow.setAnchorView(TextGain);
            valuePopUpWindow.setAdapter(gainValueArray);
            valuePopUpWindow.show();

            String strGainValue = (String)TextGain.getText();
            float floatGainValue = Float.valueOf(strGainValue);
            int intGainValue = (int)(floatGainValue * 10);
            int gainIndex = 60 - intGainValue;
            if (gainIndex > 6)
                gainIndex -= 6;
            else
                gainIndex = 0;
            valuePopUpWindow.setSelection(gainIndex);


        }

        else {                                                      // Channel button clicked
            for (int channel = 0; channel < channelButtonIds.length; channel++) {
                if (id == channelButtonIds[channel]) {
                    buttonsChannel[channel].setBackgroundResource(R.drawable.but_eq_selected_inverted_510);
                    selectedChannel = channel;
                }
                else
                    buttonsChannel[channel].setBackgroundResource(R.drawable.but_eq_not_selected_inverted_510);
            }
            UpdateControls();
            EnableControls(true);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int anchor_id = valuePopUpWindow.getAnchorView().getId();

        if (anchor_id == TextGain.getId()) {                            // If it was the Gain valule
            TextGain.setText(gainValues[position]);
            float gain = Float.valueOf(gainValues[position]);
            SliderEqGain.setValue(gain);
        }
/*        else                                                            // Assume it's a EQ Value
        {
            SliderFilter filter = FindSliderFilter((TextView) valuePopUpWindow.getAnchorView());
            if (filter != null)
            {
                TextView textLevel = (TextView) myView.findViewById(filter.text_id);
                if (textLevel != null)
                    textLevel.setText(eqBoosts[position]);

                float boost = Float.valueOf(eqBoosts[position]);
                CwSlider this_slider = (CwSlider) myView.findViewById(filter.slider_id);
                this_slider.setValue((boost));                          // Set the Boost value
                filter.dsp_filter.SetBoost(boost);
                m_main_Activity.SendDspEQ(filter.dsp_filter);           // Send the data
            }
        }
*/
        valuePopUpWindow.dismiss();
    }



    @Override
    // This function handles the Touch Event for all sliders on this screen
    public boolean onTouch (View slider, MotionEvent event){
        int control_id = slider.getId();
        boolean processed = false;
//Log.i("onTouch", "action = " + Integer.toString(event.getAction()));
        if (control_id == R.id.sliderGain) {
            CwSlider this_slider = (CwSlider) slider;
            GainChanged(this_slider.getSliderValue());
            processed = true;
        }

        if (processed == false) { // Must be an EQ slider
            if (!updating_sliders) {
                CwSlider this_slider = (CwSlider) slider;
                int slider_id = slider.getId();

                for (int band = 0; band < 31; band++) {                              // Find the filter for the slider we touched
                    int this_slider_id = sliderFilters[selectedChannel][band].slider_id;
                    if (this_slider_id == slider_id) {
                        float boost_value = this_slider.getSliderValue();
                        int intBoostValue = Math.round(boost_value * 10);
                        if (last_boost_value[band] != intBoostValue) {
                            last_boost_value[band] = intBoostValue;
                            EnableControls(false);
                            SetValueTextBox(sliderFilters[selectedChannel][band], boost_value);
                            sliderFilters[selectedChannel][band].dsp_filter.SetBoost(boost_value);         // Update the value
                            UpdateSlaveEqs(selectedChannel, band, boost_value);
                            int group = m_main_Activity.mOutputs_X_Fragment.GetGroupIndex(selectedChannel);
                            m_main_Activity.SendDspEQ(sliderFilters[selectedChannel][band].dsp_filter, group);  // Send the data and indicate group
//                            m_main_Activity.SendDspEQ(sliderFilters[selectedChannel][band].dsp_filter);    // Send the data
//Log.i("Boost Value:", Float.toString(boost_value) + "  " + Integer.toString(intBoostValue));
                            break;
                        }
                    }
                }
            }
        }
        return(true);
    }

    void SetValueTextBox(SliderFilter slider_filter, float boost_value) {
        TextView TextBox = (TextView) myView.findViewById(slider_filter.text_id);
        SetValueTextBox(TextBox, boost_value);
    }

    void SetValueTextBox( TextView TextBox, float value){
        String displayString;
        if (value > 9.95)
            displayString = "+10";
        else if (value < -9.95)
            displayString = "-10";
        else if (value == 0)
            displayString = "0";
        else
            displayString =  String.format(Locale.US, "%+2.1f", value);

        TextBox.setText(displayString);
    }


    public void GainChanged(float gain_value)
    {
        if (!updating_sliders)
        {
            EnableControls(false);
            eqGains[selectedChannel].SetGain(gain_value);
            m_main_Activity.SendDspGain(eqGains[selectedChannel]);
            SetValueTextBox(TextGain, gain_value);
        }
    }

    boolean UpdateSlaveEqs(int selectedChannel, int band, float boost_value) {
        boolean hasSlaves = false;
        if (m_main_Activity.mOutputs_X_Fragment.isMaster(selectedChannel)) {
            int masterGroup = m_main_Activity.mOutputs_X_Fragment.GetGroupIndex(selectedChannel);
            for (int channel = 0; channel < 10; channel++) {
                if (m_main_Activity.mOutputs_X_Fragment.isSlaveOfGroup(masterGroup, channel)) {
                    sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);
//Log.i("UpdateSlaveEqs", "Master: " + Integer.toString(selectedChannel) + " Slave: " + Integer.toString(channel));
                }
            }
        }
        return(hasSlaves);
    }


    // Set the position of all sliders to the value in their associated data object
    // as well as the value textbox
    public void UpdateControls()
    {
//        Log.i("UpdateControls","START");
        updating_sliders = true;                        // Prevent the slider changes from sending data to the 386
        float gain_value = eqGains[selectedChannel].GetGain();
        SliderEqGain.setValue(gain_value);
        DecimalFormat decimalFormat = new DecimalFormat("+0.0;-0.0");
        TextGain.setText(decimalFormat.format(gain_value));


        // Update all of the DSP Filter sliders
        for (int band = 0; band < numberSupportedEqBands; band++)
        {
            SliderFilter this_filter = sliderFilters[selectedChannel][band];
            float this_boost = this_filter.dsp_filter.GetBoost();

            CwSlider this_slider = (CwSlider) myView.findViewById(sliderIds[band]);
            this_slider.setValue((this_boost));
            SetValueTextBox(this_filter, this_boost);
//DisplayFilterAddress(selectedChannel, band);
        }
// TODO Add code to handle this
        if (m_main_Activity.Connected())
            EnableControls(true);
//        SetSlaveEnableState(selectedChannel);
        updating_sliders = false;
//        Log.i("UpdateControls","END");
    }

    void CopyEqFilter(int source_channel, int dest_channel) {
        for (int band = 0; band < 31; band++) {
            sliderFilters[dest_channel][band].dsp_filter.SetBoost(sliderFilters[source_channel][band].dsp_filter.GetBoost());
        }
    }

    // Reset select filters & gains to their Factory Default position
    public void ResetEqFilters(int channel, Boolean include_gains)
    {
        CwSlider this_slider;
        TextView this_value_text;
    //    updating_sliders = true;

        if (channel < eqGains.length) {
            if (include_gains) {
                eqGains[channel].SetGain(0);
                SliderEqGain.setValue(0);
            }

            for (int band = 0; band < 31; band++) {
                // Reset physical slider
                this_slider = (CwSlider) myView.findViewById(sliderIds[band]);
                this_slider.setValue(0);
                sliderFilters[channel][band].dsp_filter.SetBoost(0);
                this_value_text = (TextView)  myView.findViewById(sliderFilters[channel][band].text_id);
                SetValueTextBox(this_value_text, 0);
            }
        }
  //      updating_sliders = false;
    }


    public void EnableControls(Boolean state)
    {
        if (SliderEqGain != null) {
            boolean eqState = state;
            if (state == true) {
                boolean isMaster = m_main_Activity.mOutputs_X_Fragment.isMaster(selectedChannel);
                boolean isNotSlave = !m_main_Activity.mOutputs_X_Fragment.isSlave(selectedChannel);
                state = isMaster | isNotSlave;
            }

            SliderEqGain.setEnabled(eqState);
            ButtonFlat.setEnabled(state);
            for (int band = 0; band < 31; band++) {
                SlidersEq[band].setEnabled(state);
                EqTexts[band].setEnabled(state);
            }
        }
    }

    // Send all of our data to the 386
    public int SendData(int channel) {
        int send_time = 0;
        if (m_main_Activity.Connected() && channel < numberSupportedChannels)
        {
            for (int band = 0; band < numberSupportedEqBands; band++) {
                float boost_value = sliderFilters[channel][band].dsp_filter.GetBoost();
                sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);                  // TODO DO WE NEED THIS LINE
                m_main_Activity.dspService.SetWaitingForAck(true);
                m_main_Activity.SendDspEQ(sliderFilters[channel][band].dsp_filter);
                int this_send_time = m_main_Activity.dspService.WaitForAck();
                if (this_send_time >= 0)
                    send_time += m_main_Activity.dspService.WaitForAck();
                else{
                    send_time = this_send_time;
                    break;
                }
            }
        }
        return(send_time);
    }


    public void SetGains(int gains[])
    {
        for (int band=0; band < gains.length; band++)
            eqGains[band].SetGain(gains[band]);
    }

    // Parses an XML file and updates the EQ Filter data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;
        int channel = 0;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_DOCUMENT)
                break;              // Exit if we hit end of file

            switch (event) {
                case XmlPullParser.START_TAG:
                    break;

                case XmlPullParser.END_TAG:
                    if (name.equals("channel"))
                    {
                        String channel_name = myParser.getAttributeValue(null, "id");
                        channel = Integer.valueOf(channel_name);
                        String gain = myParser.getAttributeValue(null, "gain");
                        eqGains[channel].SetGain(gain);
                    }

                    else if (name.equals("band"))
                    {
                        String band_name = myParser.getAttributeValue(null, "id");
                        int band = Integer.valueOf(band_name);
                        String frequency = myParser.getAttributeValue(null, "frequency");
                        String boost = myParser.getAttributeValue(null, "boost");
                        float boost_value = Float.valueOf(boost);
                        sliderFilters[channel][band].dsp_filter.SetBoost(boost_value);
                    }

                    if (name.equals("equalizer")) {
                        UpdateControls();
// TODO andy CHECK - Handled in UpdateControls                        EnableControls(true);
                        done = true;
                    }
                    break;
            }

            try {
                if (!done)
                    event = myParser.next();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException {
        for (int channel = 0; channel < dspFilters.length; channel++) {
            serializer.startTag(null, "equalizer");                             // Write the Channel's Equalizer data
            serializer.startTag(null, "channel");                               // First we write Channel ID & gain
            serializer.attribute(null, "id", Integer.toString(channel));
            serializer.attribute(null, "gain", Float.toString(eqGains[channel].GetGain()));
            serializer.endTag(null, "channel");

            for (int index = 0; index < numberSupportedEqBands; index++) {                                          // Now write the EQ data
                serializer.startTag(null, "band");
                serializer.attribute(null, "id", Integer.toString(index));
                serializer.attribute(null, "frequency", sliderFilters[channel][index].dsp_filter.GetFrequencyString());
                serializer.attribute(null, "boost", Float.toString(sliderFilters[channel][index].dsp_filter.GetBoost()));
                serializer.endTag(null, "band");
            }
            serializer.endTag(null, "equalizer");
        }
    }
}