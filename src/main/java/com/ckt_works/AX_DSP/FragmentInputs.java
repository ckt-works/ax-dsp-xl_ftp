package com.ckt_works.AX_DSP;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.Locale;

import static com.ckt_works.AX_DSP.MainActivity.HAS_NO_OE_AMP;

// Created by Abeck on 12/2/2016. //

public class FragmentInputs extends CScreenNavigation implements View.OnTouchListener, Button.OnClickListener
{
     // Declare our Objects
    private DspService dspService;  // Interface to the Applications DspService
    private final DspCommand input_source_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INPUT_SOURCE);
    private final DspCommand input_level_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INPUT_LEVEL);
    private final DspCommand amp_turn_on_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_AMP_TURN_ON);

    // Declare our variable
    private boolean controlsAreVisible = false;
    private boolean inputControlsAreEnabled = true;

    // Declare our controls
    private RadioButton radioSubWooferInputFrontRear;
    private RadioButton radioSubWooferInputSubWoofer;
    private RadioButton radioFrontLevelLow;
    private RadioButton radioFrontLevelHigh;
    private RadioButton radioRearLevelLow;
    private RadioButton radioRearLevelHigh;
    private RadioButton radioSubWooferLevelLow;
    private RadioButton radioSubWooferLevelHigh;
    private RadioButton radioAmpOnAlways;
    private RadioButton radioAmpOnSense;
    private TextView textInputDisabled;

    private MainActivity mainActivity;

    // Obligatory Constructor, newInstance & onCreate functions
    public static FragmentInputs newInstance() {
        return new FragmentInputs();
    }

    public FragmentInputs(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  ((MainActivity) getActivity());

        View view = inflater.inflate(R.layout.layout_inputs, container, false);
        if (view != null)
        {
            radioSubWooferInputFrontRear = (RadioButton) view.findViewById(R.id.radioSubWooferFrontRear);
            radioSubWooferInputFrontRear.setOnClickListener(this);

            radioSubWooferInputSubWoofer = (RadioButton) view.findViewById(R.id.radioSubWooferSubWoofer);
            radioSubWooferInputSubWoofer.setOnClickListener(this);

            radioFrontLevelLow = (RadioButton) view.findViewById(R.id.radioFrontLevelLow);
            radioFrontLevelLow.setOnClickListener(this);

            radioFrontLevelHigh = (RadioButton) view.findViewById(R.id.radioFrontLevelHigh);
            radioFrontLevelHigh.setOnClickListener(this);

            radioRearLevelLow = (RadioButton) view.findViewById(R.id.radioRearLevelLow);
            radioRearLevelLow.setOnClickListener(this);

            radioRearLevelHigh = (RadioButton) view.findViewById(R.id.radioRearLevelHigh);
            radioRearLevelHigh.setOnClickListener(this);

            radioSubWooferLevelLow = (RadioButton) view.findViewById(R.id.radioSubWooferLevelLow);
            radioSubWooferLevelLow.setOnClickListener(this);

            radioSubWooferLevelHigh = (RadioButton) view.findViewById(R.id.radioSubWooferLevelHigh);
            radioSubWooferLevelHigh.setOnClickListener(this);
            
            radioAmpOnAlways = (RadioButton) view.findViewById(R.id.radioAmpOnAlways);
            radioAmpOnAlways.setOnClickListener(this);

            radioAmpOnSense = (RadioButton) view.findViewById(R.id.radioAmpOnSense);
            radioAmpOnSense.setOnClickListener(this);

            textInputDisabled = (TextView) view.findViewById(R.id.textInputSelectDisabled);
            textInputDisabled.setVisibility(View.GONE);
            EnableControls(false);
        }
        return(view);
    }

    @Override
    public void onHiddenChanged (boolean hidden){
        if (!hidden){
            MainActivity main_Activity = (MainActivity) getActivity();
            int vehicle_id = main_Activity.GetVehicleID();
            if ((vehicle_id & HAS_NO_OE_AMP) != HAS_NO_OE_AMP && controlsAreVisible) {
                textInputDisabled.setVisibility(View.VISIBLE);
                inputControlsAreEnabled = false;
                EnableInputControls(false);
            }
            else {
                textInputDisabled.setVisibility(View.GONE);
                inputControlsAreEnabled = true;
                EnableInputControls(controlsAreVisible);
            }
        }

    }

    @Override
    // This function handles the Touch Event for all sliders on this screen
    public boolean onTouch (View control, MotionEvent event){
        {
            int button_id = control.getId();
            int action = event.getAction();

        }
        return(false);
    }

    @Override
    // Handle user clicking radio buttons
    public void onClick(View button) {
        int button_id = button.getId();
        DspService dspService = mainActivity.GetDspService();

        switch (button_id) {

            case R.id.radioSubWooferFrontRear:
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.FRONT.ordinal());
                dspService.SendDspCommand(input_source_command);
                break;

            case R.id.radioSubWooferSubWoofer:
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.SUBWOOFER.ordinal());
                dspService.SendDspCommand(input_source_command);
                break;


            case R.id.radioFrontLevelLow:
                input_level_command.SetData((byte)AUDIO_CHANNEL.FRONT.ordinal(), (byte)0);
                dspService.SendDspCommand(input_level_command);
                break;

            case R.id.radioFrontLevelHigh:
                input_level_command.SetData((byte)AUDIO_CHANNEL.FRONT.ordinal(), (byte)1);
                dspService.SendDspCommand(input_level_command);
                break;

            case R.id.radioRearLevelLow:
                input_level_command.SetData((byte)AUDIO_CHANNEL.REAR.ordinal(), (byte)0);
                dspService.SendDspCommand(input_level_command);
                break;

            case R.id.radioRearLevelHigh:
                input_level_command.SetData((byte)AUDIO_CHANNEL.REAR.ordinal(), (byte)1);
                dspService.SendDspCommand(input_level_command);
                break;

            case R.id.radioSubWooferLevelLow:
                input_level_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)0);
                dspService.SendDspCommand(input_level_command);
                break;

            case R.id.radioSubWooferLevelHigh:
                input_level_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)1);
                dspService.SendDspCommand(input_level_command);
                break;


            case R.id.radioAmpOnAlways:
                amp_turn_on_command.SetData((byte)0);
                dspService.SendDspCommand(amp_turn_on_command);
                break;

            case R.id.radioAmpOnSense:
                amp_turn_on_command.SetData((byte)1);
                dspService.SendDspCommand(amp_turn_on_command);
                break;
        }
        EnableControls(false);
    }


    public void EnableControls(Boolean state)
    {
        if (radioAmpOnAlways != null) {
            controlsAreVisible = state;
            radioAmpOnAlways.setEnabled(state);
            radioAmpOnSense.setEnabled(state);
            if ( inputControlsAreEnabled)
                EnableInputControls(state);
        }
    }

    public void EnableInputControls(Boolean state)
    {
        if (radioSubWooferInputFrontRear != null) {
            radioSubWooferInputFrontRear.setEnabled(state);
            radioSubWooferInputSubWoofer.setEnabled(state);
            radioRearLevelLow.setEnabled(state);
            radioFrontLevelLow.setEnabled(state);
            radioFrontLevelHigh.setEnabled(state);
            radioRearLevelLow.setEnabled(state);
            radioRearLevelHigh.setEnabled(state);
            radioSubWooferLevelLow.setEnabled(state);
            radioSubWooferLevelHigh.setEnabled(state);
        }
    }

    // Send all of our data to the 386
    public boolean SendData() {

        boolean sent = false;
        DspService dspService = mainActivity.GetDspService();

        if (mainActivity.Connected()) {


/*            if (radioSubWooferInputFrontRear.isChecked())
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.FRONT.ordinal());
            else
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.SUBWOOFER.ordinal());
            dspService.SendDspCommand(input_source_command);
            dspService.WaitForAck();

            if (radioFrontLevelLow.isChecked())
                input_level_command.SetData((byte)AUDIO_CHANNEL.FRONT.ordinal(), (byte)0);
            else
                input_level_command.SetData((byte)AUDIO_CHANNEL.FRONT.ordinal(), (byte)1);
            dspService.SendDspCommand(input_level_command);
            dspService.WaitForAck();

            if (radioRearLevelLow.isChecked())
                input_level_command.SetData((byte)AUDIO_CHANNEL.REAR.ordinal(), (byte)0);
            else
                input_level_command.SetData((byte)AUDIO_CHANNEL.REAR.ordinal(), (byte)1);
            dspService.SendDspCommand(input_level_command);
            dspService.WaitForAck();

            if (radioSubWooferLevelLow.isChecked())
                input_level_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)0);
            else
                input_level_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)1);
            dspService.SendDspCommand(input_level_command);
            dspService.WaitForAck();
*/
            if (radioAmpOnAlways.isChecked())
                amp_turn_on_command.SetData((byte)0);
            else
                amp_turn_on_command.SetData((byte)1);
            dspService.SendDspCommand(amp_turn_on_command);
            dspService.WaitForAck();

            sent = true;
        }
        return(sent);
    }

    // Sets the Source input for the selected channel - Handles only Sub Woofer input
    void SetInputSource(AUDIO_CHANNEL channel, AUDIO_CHANNEL source) {
        if (channel == AUDIO_CHANNEL.SUBWOOFER)
        {
            if (source == AUDIO_CHANNEL.FRONT)
                radioSubWooferInputFrontRear.setChecked(true);
            else
                radioSubWooferInputSubWoofer.setChecked(true);
        }
    }


    // Sets the Input Level for the selected channel
    void SetInputLevel(AUDIO_CHANNEL channel, byte level) {
        switch(channel) {
            case FRONT:
                if (level == 0)
                    radioFrontLevelLow.setChecked(true);
                else
                    radioFrontLevelHigh.setChecked(true);
                break;

            case REAR:
                if (level == 0)
                    radioRearLevelLow.setChecked(true);
                else
                    radioRearLevelHigh.setChecked(true);
                break;

            case SUBWOOFER:
                if (level == 0)
                    radioSubWooferLevelLow.setChecked(true);
                else
                    radioSubWooferLevelHigh.setChecked(true);
                break;
        }
    }


// TODO ANDY FIX THIS UP
    // Sets the Amp Turn On command data and the radio buttons to the indicated state
    void SetAmpTurnOnState(int amp_turn_on_state, int delay) {
        this.SetAmpTurnOnState(amp_turn_on_state);
    }

        void SetAmpTurnOnState(int amp_turn_on_state) {
        boolean state = (amp_turn_on_state != 0);
//        radioAmpOnAlways.setChecked(!state);
//        radioAmpOnSense.setChecked(state);
    }

    // Parses and XML file and updates the Clipping Sensitivity data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("amp_turn_on"))
                {
                    String str_amp_turn_on_state = myParser.getAttributeValue(null, "state");
                    byte amp_turn_on_state = 0;
                    if (str_amp_turn_on_state != null)
                        if (str_amp_turn_on_state.contains("sense"))
                            amp_turn_on_state = 1;
                    SetAmpTurnOnState(amp_turn_on_state);
                   done = true;
                }

                else if (name.equals("input_sources"))
                {
                    String str_sub_woofer_source = myParser.getAttributeValue(null, "sub_woofer");
                    int sub_woofer_source = 0;
                    if (str_sub_woofer_source != null)
                        sub_woofer_source = Integer.valueOf(str_sub_woofer_source);
                    SetInputSource(AUDIO_CHANNEL.SUBWOOFER, AUDIO_CHANNEL.values()[sub_woofer_source]);
                    done = true;
                }

                else if (name.equals("input_levels"))
                {
                    String str_level = myParser.getAttributeValue(null, "front");
                    if (str_level != null) {
                        str_level.toLowerCase(Locale.US);
                        if (str_level.contains("high"))
                            SetInputLevel(AUDIO_CHANNEL.FRONT, (byte) 1);
                        else
                            SetInputLevel(AUDIO_CHANNEL.FRONT, (byte) 0);
                    }

                    str_level = myParser.getAttributeValue(null, "rear");
                    if (str_level != null) {
                        str_level.toLowerCase(Locale.US);
                        if (str_level.contains("high"))
                            SetInputLevel(AUDIO_CHANNEL.REAR, (byte) 1);
                        else
                            SetInputLevel(AUDIO_CHANNEL.REAR, (byte) 0);
                    }

                    str_level = myParser.getAttributeValue(null, "sub_woofer");
                    if (str_level != null) {
                        str_level.toLowerCase(Locale.US);
                        if (str_level.contains("high"))
                            SetInputLevel(AUDIO_CHANNEL.SUBWOOFER, (byte) 1);
                        else
                            SetInputLevel(AUDIO_CHANNEL.SUBWOOFER, (byte) 0);
                    }
                    done = true;
                }
            }
            event = myParser.next();
        }
    }

    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        serializer.startTag(null, "input_sources");
        if (radioSubWooferInputSubWoofer.isChecked())
            serializer.attribute(null, "sub_woofer", Integer.toString(AUDIO_CHANNEL.SUBWOOFER.ordinal()));
        else
            serializer.attribute(null, "sub_woofer", Integer.toString(AUDIO_CHANNEL.FRONT.ordinal()));
        serializer.endTag(null, "input_sources");

        serializer.startTag(null, "input_levels");
        if (radioFrontLevelHigh.isChecked())
            serializer.attribute(null, "front", "high");
        else
            serializer.attribute(null, "front", "low");

        if (radioRearLevelHigh.isChecked())
            serializer.attribute(null, "rear", "high");
        else
            serializer.attribute(null, "rear", "low");

        if (radioSubWooferLevelHigh.isChecked())
            serializer.attribute(null, "sub_woofer", "high");
        else
            serializer.attribute(null, "sub_woofer", "low");
        serializer.endTag(null, "input_levels");

        serializer.startTag(null, "amp_turn_on");
        if (radioAmpOnSense.isChecked())
            serializer.attribute(null, "state", "on_sense");
        else
            serializer.attribute(null, "state", "always_on");
        serializer.endTag(null, "amp_turn_on");
    }

}


