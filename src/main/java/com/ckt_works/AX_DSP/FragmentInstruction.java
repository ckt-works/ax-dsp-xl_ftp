package com.ckt_works.AX_DSP;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

// Created by Abeck on 12/2/2016. //

public class FragmentInstruction extends CScreenNavigation implements Button.OnClickListener {

    public static FragmentInstruction newInstance() {
        return new FragmentInstruction();
    }

    public FragmentInstruction(){}

    private Button buttonVisitXWebSite;
    private Button buttonVisitLWebSite;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.layout_instruction, container, false);

        buttonVisitXWebSite = (Button) myView.findViewById(R.id.buttonVisit_X_WebSite);
        buttonVisitXWebSite.setOnClickListener(this);

        buttonVisitLWebSite = (Button) myView.findViewById(R.id.buttonVisit_L_WebSite);
        buttonVisitLWebSite.setOnClickListener(this);
        return(myView);
    }


    public void onClick(View button){
        if (button == buttonVisitXWebSite)
            openWebPage("https://metra-static.s3.amazonaws.com/documents/INSTAX-DSP-X_web.pdf");
        else
            openWebPage("https://metra-static.s3.amazonaws.com/documents/INSTAXDSP-L_web.pdf");
    }

    // Open a web page of the specified URL
    public void openWebPage(String url) {
        Uri webpage = Uri.parse(url);
        final Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(url));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startActivity(intent);

    }
}
