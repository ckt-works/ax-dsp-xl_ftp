
package com.ckt_works.AX_DSP;

import android.os.Bundle;
import androidx.appcompat.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

// Created by Abeck on 12/2/2016. //

public class FragmentLevels extends CScreenNavigation implements View.OnTouchListener, Button.OnClickListener, IPostExecuteInterface
{
    private int trigger_level = 10;
    private  byte volume_level = 10;
    private int ampTurnOnDelay = 0;
    private int lastAmpTurnOnDelay = 0;

    // Declare our Objects
    private final DspCommand level_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_CLIPPING_LEVEL);
    private final DspCommand chime_volume = new DspCommand(DSP_COMMANDS.DSP_COMMAND_CHIME_VOLUME);
    private final DspCommand amp_turn_on_delay_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_AMP_TURN_ON_DELAY);
    private final DspCommand input_source_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INPUT_SOURCE);
    private final DspCommand amp_turn_on_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_AMP_TURN_ON);

    // Declare our controls
    private LinearLayout layoutClippingLevel;
    private CwSlider SliderClippingTriggerLevel;
    private TextView TextClippingTriggerLevel;
    private SwitchCompat SwitchClippingEnable;
    private TextView TextClippingOff;
    private TextView TextClippingOn;

    private CwSlider SliderVolume;
    private TextView TextVolume;

    private LinearLayout layoutAmpTurnOn;
    private CwSlider sliderAmpTurnOnDelay;
    private RadioButton radioAmpOnAlways;
    private RadioButton radioAmpOnSense;
    private TextView textAmpTurnOnDelay;

    private LinearLayout layoutSubWooferInputFrontRear;
    private RadioButton radioSubWooferInputFrontRear;
    private RadioButton radioSubWooferInputSubWoofer;

    boolean clipping_is_enabled = false;

    // Obligatory Constructor, newInstance & onCreate functions
    public static FragmentLevels newInstance() {
        return new FragmentLevels();
    }

    public FragmentLevels(){}
    MainActivity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  ((MainActivity) getActivity());

        View view = inflater.inflate(R.layout.layout_levels, container, false);
        if (view != null)
        {
            layoutClippingLevel = (LinearLayout) view.findViewById(R.id.layoutClippingLevel);

            SliderClippingTriggerLevel = (CwSlider) view.findViewById(R.id.sliderClippingLevel);
            SliderClippingTriggerLevel.setRange(1, 10);
            SliderClippingTriggerLevel.setValue(1);
            SliderClippingTriggerLevel.setDelegateOnTouchListener(this);

            TextClippingTriggerLevel = (TextView) view.findViewById(R.id.TextClippingTriggerLevel);

            SwitchClippingEnable = (SwitchCompat) view.findViewById(R.id.switchClippingEnable);
            SwitchClippingEnable.setOnClickListener(this);

            TextClippingOff = (TextView) view.findViewById(R.id.textClippingOff);
            TextClippingOff.setOnClickListener(this);

            TextClippingOn = (TextView) view.findViewById(R.id.textClippingOn);
            TextClippingOn.setOnClickListener(this);

            SliderVolume = (CwSlider) view.findViewById(R.id.sliderChimeVolume);
            SliderVolume.setRange(1, 10);
            SliderVolume.setValue(1);
            SliderVolume.setDelegateOnTouchListener(this);
            TextVolume = (TextView) view.findViewById(R.id.TextChimeLevel);

            layoutSubWooferInputFrontRear = (LinearLayout) view.findViewById(R.id.layoutSubWooferFrontRear);
            radioSubWooferInputFrontRear = (RadioButton) view.findViewById(R.id.radioSubWooferFrontRear);
            radioSubWooferInputFrontRear.setOnClickListener(this);
            radioSubWooferInputSubWoofer = (RadioButton) view.findViewById(R.id.radioSubWooferSubWoofer);
            radioSubWooferInputSubWoofer.setOnClickListener(this);

            radioAmpOnAlways = (RadioButton) view.findViewById(R.id.radioAmpOnAlways);
            radioAmpOnAlways.setOnClickListener(this);

            radioAmpOnSense = (RadioButton) view.findViewById(R.id.radioAmpOnSense);
            radioAmpOnSense.setOnClickListener(this);

            layoutAmpTurnOn = (LinearLayout) view.findViewById(R.id.layoutAmpTurnOn);
            textAmpTurnOnDelay = (TextView) view.findViewById(R.id.textAmpTurnOnDelay);
            textAmpTurnOnDelay.setText("0");
            sliderAmpTurnOnDelay = (CwSlider) view.findViewById(R.id.sliderAmpTurnOnDelay);
            sliderAmpTurnOnDelay.setRange(0, 10);
            sliderAmpTurnOnDelay.setValue(0);       // This will force update of the display value
            sliderAmpTurnOnDelay.setDelegateOnTouchListener(this);

            EnableControls(false);
        }
        return(view);
    }

   // Interface to allow the "TaskSendData" to callback when done sending data
    public void onPostExecute()
    {
        Log.i("FragmentLevels", "onPostExecute");
    }



    @Override
    // This function handles the Touch Event for all sliders on this screen
    public boolean onTouch (View control, MotionEvent event){
        {
            DspService dspService = mainActivity.GetDspService();
            int button_id = control.getId();
            int action = event.getAction();

            if (button_id == R.id.sliderClippingLevel) {
                if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
                    EnableControls(false);
                    trigger_level = (int)SliderClippingTriggerLevel.getSliderValue();
                    TextClippingTriggerLevel.setText(Integer.toString(trigger_level));
                    byte[] byte_data = new byte[2];
                    byte_data[0] = (byte)trigger_level;             // Set the clipping level
                    byte_data[1] = (byte)trigger_level;             // Set the slider level
                    level_command.SetData(byte_data, 2);
                    TaskSendCommand taskCommand = new TaskSendCommand(dspService, this, level_command);
                    taskCommand.execute();
                    return (true);
                }
            }

            else if (button_id == SliderVolume.getId()) {
                if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
                    EnableControls(false);
                    volume_level = (byte)SliderVolume.getSliderValue();
                    TextVolume.setText(Byte.toString(volume_level));
                    chime_volume.SetData(volume_level);
                    dspService.SendDspCommand(chime_volume);
                    return (true);
                }
            }

            else  if (control == sliderAmpTurnOnDelay) {
                int progressValue = (int)sliderAmpTurnOnDelay.getSliderValue();
                textAmpTurnOnDelay.setText(Integer.toString(progressValue));
                if (dspService != null) {
                    ampTurnOnDelay = progressValue * 10;
                    if (lastAmpTurnOnDelay != ampTurnOnDelay) {
                        EnableControls(false);
                        lastAmpTurnOnDelay = ampTurnOnDelay;
                        amp_turn_on_delay_command.SetData(ampTurnOnDelay);
                        dspService.SendDspCommand(amp_turn_on_delay_command);
                        Log.i("Amp Turn On", Integer.toString(progressValue));
                    }
                }
            }
        }
        return(false);
    }


    @Override
    // Handle user clicking radio buttons
    public void onClick(View button) {
        int button_id = button.getId();
        DspService dspService = mainActivity.GetDspService();
        switch (button_id) {
            case R.id.radioSubWooferFrontRear:
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.FRONT.ordinal());
                dspService.SendDspCommand(input_source_command);
                break;

            case R.id.radioSubWooferSubWoofer:
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.SUBWOOFER.ordinal());
                dspService.SendDspCommand(input_source_command);
                break;

            case R.id.radioAmpOnAlways:
                amp_turn_on_command.SetData((byte)0);
                dspService.SendDspCommand(amp_turn_on_command);
                break;

            case R.id.radioAmpOnSense:
                amp_turn_on_command.SetData((byte) 1);
                dspService.SendDspCommand(amp_turn_on_command);
                break;

            case R.id.switchClippingEnable:
            case R.id.textClippingOff:
            case R.id.textClippingOn:
                clipping_is_enabled = SwitchClippingEnable.isChecked();      // Determine checked state by the ID of the button/switch clicked
                if (button_id == R.id.textClippingOn)
                    clipping_is_enabled = true;
                else  if (button_id == R.id.textClippingOff)
                    clipping_is_enabled = false;

                SwitchClippingEnable.setChecked(clipping_is_enabled);

                if (clipping_is_enabled) {
                    byte[] byte_data = new byte[2];
                    byte_data[0] = (byte)trigger_level;             // Set the clipping level
                    byte_data[1] = (byte)trigger_level;             // Set the slider level
                    level_command.SetData(byte_data, 2);
                    SliderClippingTriggerLevel.setEnabled(true);
                }
                else {
                    byte[] byte_data = new byte[2];
                    byte_data[0] = 0;                               // Indicate clipping is off
                    byte_data[1] = (byte)trigger_level;             // Set the slider level
                    level_command.SetData(byte_data, 2);
                    SliderClippingTriggerLevel.setEnabled(false);
                }

                if (mainActivity.Connected())
                    dspService.SendDspCommand(level_command);
                break;

        }
        EnableControls(false);
    }

    // Send Amplifier data
    public int SendAmpData() {
        int send_time = 0;
        DspService dspService = mainActivity.GetDspService();

        if (mainActivity.Connected()) {
            if (radioSubWooferInputFrontRear.isChecked())
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.FRONT.ordinal());
            else
                input_source_command.SetData((byte)AUDIO_CHANNEL.SUBWOOFER.ordinal(), (byte)AUDIO_CHANNEL.SUBWOOFER.ordinal());
            dspService.SendDspCommand(input_source_command);
            dspService.WaitForAck();

            if (radioAmpOnAlways.isChecked())
                amp_turn_on_command.SetData((byte)0);
            else
                amp_turn_on_command.SetData((byte)1);
            dspService.SendDspCommand(amp_turn_on_command);
            dspService.WaitForAck();

            amp_turn_on_delay_command.SetData(ampTurnOnDelay);
            dspService.SendDspCommand(amp_turn_on_delay_command);
            send_time = dspService.WaitForAck();
        }
        return(send_time);
    }

    void SetAmpTurnOnState(int amp_turn_on_state, int amp_turn_on_delay) {
        this.SetAmpTurnOnState(amp_turn_on_state);
        sliderAmpTurnOnDelay.setValue(amp_turn_on_delay);
        textAmpTurnOnDelay.setText(Integer.toString(amp_turn_on_delay));
    }

    // Sets the Amp Turn On command data and the radio buttons to the indicated state
    void SetAmpTurnOnState(int amp_turn_on_state) {
        boolean state = (amp_turn_on_state != 0);
        radioAmpOnAlways.setChecked(!state);
        radioAmpOnSense.setChecked(state);
    }

    public void EnableControls(Boolean state)
    {
        Log.d("FragmentLevels", "EnableControl");
        if (SwitchClippingEnable != null) {
            layoutClippingLevel.setVisibility((mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)? View.GONE : View.VISIBLE);
            SwitchClippingEnable.setEnabled(state);
            TextClippingOff.setEnabled(state);
            TextClippingOn.setEnabled(state);
            SliderClippingTriggerLevel.setEnabled((clipping_is_enabled) ? state : false);

            layoutAmpTurnOn.setVisibility((mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)? View.GONE : View.VISIBLE);

            SliderVolume.setEnabled(state);
            sliderAmpTurnOnDelay.setEnabled(state);
            radioAmpOnAlways.setEnabled(state);
            radioAmpOnSense.setEnabled(state);
            sliderAmpTurnOnDelay.setEnabled(state);

            layoutSubWooferInputFrontRear.setVisibility((mainActivity.GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)? View.GONE : View.VISIBLE);
            radioSubWooferInputFrontRear.setEnabled(state);
            radioSubWooferInputSubWoofer.setEnabled(state);
        }
    }

    // Send all of our data to the 386
    public int SendData() {
        int send_time = 0;
        MainActivity main_Activity = (MainActivity) getActivity();
        if (main_Activity.Connected()) {
            DspService dspService = mainActivity.GetDspService();
            byte[] byte_data = new byte[2];
            byte_data[0] = (byte)trigger_level;             // Set the clipping level
            byte_data[1] = (byte)trigger_level;             // Set the slider level
            level_command.SetData(byte_data, 2);
            dspService.SendDspCommand(level_command);
            dspService.WaitForAck();

            chime_volume.SetData(volume_level);
            dspService.SendDspCommand(chime_volume);
            send_time = dspService.WaitForAck();
        }
        return(send_time);
    }


    // Sets the Clipping Level command data and the slider & switch positions
    void SetClippingLevel(int clipping_level, int slider_level) {
        trigger_level = slider_level;
        if (clipping_level > 0){
            SwitchClippingEnable.setChecked(true);
            clipping_is_enabled = true;
        }
        else
        {
            SwitchClippingEnable.setChecked(false);
            clipping_is_enabled = false;
        }

        SliderClippingTriggerLevel.setValue((float)slider_level);
        TextClippingTriggerLevel.setText(Integer.toString(slider_level));
    }

    // Update the Chime Level Command & the slider position
    public void SetChimeVolume(int level)
    {
        volume_level = (byte)level;
        SliderVolume.setValue((float)volume_level);
        TextVolume.setText(Integer.toString(volume_level));
    }

    // Sets the Source input for Sub Woofer input
    void SetSubWooferSource(int source) {
        if (source == AUDIO_CHANNEL.FRONT.value)
            radioSubWooferInputFrontRear.setChecked(true);
        else
            radioSubWooferInputSubWoofer.setChecked(true);
    }

    // Parses an XML file and updates the Clipping Sensitivity data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {
        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_TAG)
            {
                if (name.equals("clipping_sensitivity"))
                {
                    clipping_is_enabled = Boolean.valueOf(myParser.getAttributeValue(null, "enabled"));
                    if (clipping_is_enabled)
                        SwitchClippingEnable.setChecked(true);
                    else
                        SwitchClippingEnable.setChecked(false);

                    trigger_level = Integer.valueOf(myParser.getAttributeValue(null, "trigger_level"));
                    TextClippingTriggerLevel.setText(Integer.toString(trigger_level));
                    SliderClippingTriggerLevel.setValue((float)trigger_level);
                    done = true;
                }

                else if (name.equals("chime_volume")) {
                    SetChimeVolume(Integer.valueOf(myParser.getAttributeValue(null, "level")));
                    done = true;
                }

                else if (name.equals("amp_turn_on"))
                {
                    String str_amp_turn_on_state = myParser.getAttributeValue(null, "state");
                    byte amp_turn_on_state = 0;
                    if (str_amp_turn_on_state != null)
                        if (str_amp_turn_on_state.contains("sense"))
                            amp_turn_on_state = 1;

                    int level = 0;
                    String str_amp_turn_on_level = myParser.getAttributeValue(null, "level");
                    if (str_amp_turn_on_level != null)
                        level = Integer.valueOf(str_amp_turn_on_level) / 10;
                    SetAmpTurnOnState(amp_turn_on_state, level);
                    done = true;
                }

                else if (name.equals("SubWooferInput"))
                {
                    String str_sub_woofer_source = myParser.getAttributeValue(null, "sub_woofer");
                    int sub_woofer_source = 0;
                    if (str_sub_woofer_source != null)
                        sub_woofer_source = Integer.valueOf(str_sub_woofer_source);
                    SetSubWooferSource(sub_woofer_source);
                    done = true;
                }

            }
            event = myParser.next();
        }
    }

    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        serializer.startTag(null, "clipping_sensitivity");
        serializer.attribute(null, "enabled", Boolean.toString(clipping_is_enabled));
        serializer.attribute(null, "trigger_level", Integer.toString(trigger_level));
        serializer.endTag(null, "clipping_sensitivity");

        serializer.startTag(null, "chime_volume");
        serializer.attribute(null, "level", Byte.toString(volume_level));
        serializer.endTag(null, "chime_volume");

        serializer.startTag(null, "input_sources");
        serializer.startTag(null, "SubWooferInput");
        if (radioSubWooferInputSubWoofer.isChecked())
            serializer.attribute(null, "sub_woofer", Integer.toString(AUDIO_CHANNEL.SUBWOOFER.ordinal()));
        else
            serializer.attribute(null, "sub_woofer", Integer.toString(AUDIO_CHANNEL.FRONT.ordinal()));
        serializer.endTag(null, "SubWooferInput");

        serializer.startTag(null, "amp_turn_on");
        if (radioAmpOnSense.isChecked())
            serializer.attribute(null, "state", "on_sense");
        else
            serializer.attribute(null, "state", "always_on");
        serializer.attribute(null, "level",Integer.toString(ampTurnOnDelay));
        serializer.endTag(null, "amp_turn_on");
        serializer.endTag(null, "input_sources");
    }

}

