package com.ckt_works.AX_DSP;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;

// Created by Abeck on 12/2/2016. //
@SuppressWarnings("unchecked")

public class FragmentOutputs_X extends CScreenNavigation implements Button.OnClickListener, com.ckt_works.AX_DSP.NoDefaultSpinner.OnItemSelectedListener,
                                                                   CompoundButton.OnCheckedChangeListener, View.OnTouchListener,
                                                                   IGroupInterface{

    class InputChannel {
        DspInput position;
        int groupIndex;
        boolean inverted;
        boolean muted;
        boolean settingLocationSpinner;
        boolean updatingScreen;
    }

     // Declare our Objects
   // private DspService dspService;  // Interface to the Applications DspService
    private final DspCommand input_source_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INPUT_SOURCE);
    private final DspCommand input_level_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INPUT_LEVEL);
    private final DspCommand amp_turn_on_delay_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_AMP_TURN_ON_DELAY);
    private final DspCommand amp_turn_on_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_AMP_TURN_ON);
    private final DspCommand mute_sw_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_MUTE_SW);
    private final DspCommand invert_sw_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_INVERT_SW);
    private final DspCommand set_speaker_location_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_SPEAKER_LOCATION);
    private final DspCommand set_group_command = new DspCommand(DSP_COMMANDS.DSP_COMMAND_SET_GROUP);

    // Declare our variables
    private boolean controlsAreVisible = false;
    private boolean inputControlsAreEnabled = true;
    private boolean updatingControls = false;       // If true data is not send to the DSP board
    private int ampTurnOnDelay = 0;
    private int lastAmpTurnOnDelay = 0;
    private int numberSupportedChannels = 10;

    // Declare our controls
   // private RadioButton radioAmpOnAlways;
  //  private RadioButton radioAmpOnSense;
   // private TextView textAmpTurnOnDelay;
   // private HorizontalSlider sliderAmpTurnOnDelay;

    private InputChannel inputChannels[];
    private DspGroup groups[];

    private LinearLayout[] LayoutOutputs;
    private int layoutIds[] = {R.id.LayoutOutputs_1, R.id.LayoutOutputs_2, R.id.LayoutOutputs_3, R.id.LayoutOutputs_4, R.id.LayoutOutputs_5, R.id.LayoutOutputs_6, R.id.LayoutOutputs_7, R.id.LayoutOutputs_8, R.id.LayoutOutputs_9, R.id.LayoutOutputs_10};

    private com.ckt_works.AX_DSP.NoDefaultSpinner spinnerLocations[];
    private int locationSpinnnerIds[] = {R.id.Spinner_Location_1, R.id.Spinner_Location_2, R.id.Spinner_Location_3, R.id.Spinner_Location_4, R.id.Spinner_Location_5,
                                         R.id.Spinner_Location_6, R.id.Spinner_Location_7, R.id.Spinner_Location_8, R.id.Spinner_Location_9, R.id.Spinner_Location_10};
    private ArrayAdapter<String> adapterLocations[];

    private com.ckt_works.AX_DSP.NoDefaultSpinner spinnerGroups[];
    private int groupSpinnnerIds[] = {R.id.Spinner_Group_1, R.id.Spinner_Group_2, R.id.Spinner_Group_3, R.id.Spinner_Group_4, R.id.Spinner_Group_5,
                                      R.id.Spinner_Group_6, R.id.Spinner_Group_7, R.id.Spinner_Group_8, R.id.Spinner_Group_9, R.id.Spinner_Group_10};
    private ArrayAdapter<String> adapterGroups[];

    private ImageView iconGroups[];
    private int iconGroupIds[] = {R.id.Icon_Group_1, R.id.Icon_Group_2, R.id.Icon_Group_3, R.id.Icon_Group_4, R.id.Icon_Group_5,
                                   R.id.Icon_Group_6, R.id.Icon_Group_7, R.id.Icon_Group_8, R.id.Icon_Group_9, R.id.Icon_Group_10};
    private int pngMasterIds[] = {R.drawable.icon_master_gray, R.drawable.icon_master_red, R.drawable.icon_master_orange,
                                  R.drawable.icon_master_yellow, R.drawable.icon_master_green, R.drawable.icon_master_blue,
                                  R.drawable.icon_master_violet};
    private int pngSlaveIds[] = {R.drawable.icon_slave_gray, R.drawable.icon_slave_red, R.drawable.icon_slave_orange,
            R.drawable.icon_slave_yellow, R.drawable.icon_slave_green, R.drawable.icon_slave_blue,
            R.drawable.icon_slave_violet};



    private CheckBox checkboxesMute[];
    private int checkboxMuteIds[] = {R.id.Check_Mute_1, R.id.Check_Mute_2, R.id.Check_Mute_3, R.id.Check_Mute_4, R.id.Check_Mute_5,
                                     R.id.Check_Mute_6, R.id.Check_Mute_7, R.id.Check_Mute_8, R.id.Check_Mute_9, R.id.Check_Mute_10};
    private CheckBox checkboxesInvert[];
    private int checkboxInvertIds[] = {R.id.Check_Invert_1, R.id.Check_Invert_2, R.id.Check_Invert_3, R.id.Check_Invert_4, R.id.Check_Invert_5,
                                       R.id.Check_Invert_6, R.id.Check_Invert_7, R.id.Check_Invert_8, R.id.Check_Invert_9, R.id.Check_Invert_10};
    private MainActivity mainActivity;

    // Obligatory Constructor, newInstance & onCreate functions
    public static FragmentOutputs_X newInstance() {
        return new FragmentOutputs_X();
    }

    public FragmentOutputs_X(){
        inputChannels = new InputChannel[locationSpinnnerIds.length];
        for (int index = 0; index < locationSpinnnerIds.length; index++) {
            inputChannels[index] = new InputChannel();
            inputChannels[index].position = new DspInput(DspInput.SPEAKER_POSITIONS.NOT_USED);
            inputChannels[index].groupIndex = 0;
            inputChannels[index].muted = false;
            inputChannels[index].inverted = false;
            inputChannels[index].settingLocationSpinner = false;
            inputChannels[index].updatingScreen = false;
        }

        groups = new DspGroup[DspGroup.maxNumberGroups];
        for (int index =0; index < DspGroup.maxNumberGroups; index++)
        {
            groups[index] =  new DspGroup(index);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainActivity =  ((MainActivity) getActivity());

        View view = inflater.inflate(R.layout.layout_outputs_x, container, false);
        if (view != null)
        {
            Context my_context = container.getContext();
            adapterLocations =  (ArrayAdapter<String>[])new ArrayAdapter[locationSpinnnerIds.length];
            spinnerLocations = new com.ckt_works.AX_DSP.NoDefaultSpinner[locationSpinnnerIds.length];
            ArrayAdapter<String>[] adapterGroups =  new ArrayAdapter[locationSpinnnerIds.length];
            spinnerGroups = new com.ckt_works.AX_DSP.NoDefaultSpinner[locationSpinnnerIds.length];
            iconGroups = new ImageView[locationSpinnnerIds.length];
            checkboxesMute = new CheckBox[locationSpinnnerIds.length];
            checkboxesInvert  = new CheckBox[locationSpinnnerIds.length];
            LayoutOutputs = new LinearLayout[layoutIds.length];

            for (int channel = 0; channel < locationSpinnnerIds.length; channel++) {
                LayoutOutputs[channel] = (LinearLayout) view.findViewById(layoutIds[channel]);

                adapterLocations[channel] = new ArrayAdapter<String>(my_context, R.layout.layout_spinner_selected_item, DspInput.strNames);
                adapterLocations[channel].setDropDownViewResource(R.layout.layout_spinner_dropdown);
                spinnerLocations[channel] = (com.ckt_works.AX_DSP.NoDefaultSpinner) view.findViewById(locationSpinnnerIds[channel]);
                spinnerLocations[channel].setSelection( inputChannels[channel].position.GetPosition());
                spinnerLocations[channel].setEnabled(false);
                spinnerLocations[channel].setAdapter(adapterLocations[channel]);
                spinnerLocations[channel].setOnItemSelectedListener(this);

                adapterGroups[channel] = new ArrayAdapter<String>(my_context, R.layout.layout_spinner_selected_item, DspInput.strGroups);
                adapterGroups[channel].setDropDownViewResource(R.layout.layout_spinner_dropdown);
                spinnerGroups[channel] = (com.ckt_works.AX_DSP.NoDefaultSpinner) view.findViewById(groupSpinnnerIds[channel]);
                spinnerGroups[channel].setSelection(inputChannels[channel].groupIndex);
                spinnerGroups[channel].setEnabled(false);
                spinnerGroups[channel].setAdapter(adapterGroups[channel]);
                spinnerGroups[channel].setOnItemSelectedListener(this);
                iconGroups[channel] = (ImageView) view.findViewById(iconGroupIds[channel]);

                checkboxesMute[channel] = (CheckBox) view.findViewById(checkboxMuteIds[channel]);
                checkboxesMute[channel].setEnabled(false);
                checkboxesMute[channel].setChecked(false);
                checkboxesMute[channel].setOnCheckedChangeListener(this);

                checkboxesInvert[channel] = (CheckBox) view.findViewById(checkboxInvertIds[channel]);
                checkboxesInvert[channel].setEnabled(false);
                checkboxesInvert[channel].setChecked(false);
                checkboxesInvert[channel].setOnCheckedChangeListener(this);
            }

/*            radioAmpOnAlways = (RadioButton) view.findViewById(R.id.radioAmpOnAlways);
            radioAmpOnAlways.setEnabled(false);
            radioAmpOnAlways.setOnClickListener(this);

            radioAmpOnSense = (RadioButton) view.findViewById(R.id.radioAmpOnSense);
            radioAmpOnSense.setEnabled(false);
            radioAmpOnSense.setOnClickListener(this);

            textAmpTurnOnDelay = (TextView) view.findViewById(R.id.textAmpTurnOnDelay);
            textAmpTurnOnDelay.setText("0 Sec.");

            sliderAmpTurnOnDelay = (HorizontalSlider) view.findViewById(R.id.sliderAmpTurnOnDelay);
            sliderAmpTurnOnDelay.setRange(0, 10);
            sliderAmpTurnOnDelay.setValue(0);       // This will force update of the frequency display
            sliderAmpTurnOnDelay.setImages(R.drawable.slider_bar_horz, R.drawable.slider_bar_horz_disabled);
            sliderAmpTurnOnDelay.setDelegateOnTouchListener(this);
            sliderAmpTurnOnDelay.setEnabled(false);
//            updatingControls = true;
*/
        }
        return(view);
    }

    // Returns True if any channel is assigned to a location, else returns False
    public boolean AnyChannelAssigned() {
        boolean anyChannelAssigned = false;
        for (int channel = 0; channel < inputChannels.length; channel++) {
            int location = inputChannels[channel].position.GetPosition();
            if (location != 0) {
                anyChannelAssigned = true;
                break;
            }
        }
        return(anyChannelAssigned);
    }


    int GetPngId(int channel) {
        int id = -1;

        if (channel >=0 && channel < locationSpinnnerIds.length) {
            int groupIndex = inputChannels[channel].groupIndex -1;
            if (groupIndex >= 0 && groupIndex < pngMasterIds.length) {
                if (groups[groupIndex+1].ChannelIsMaster(channel))
                    id = pngMasterIds[groupIndex];
                else
                    id = pngSlaveIds[groupIndex];
            }
        }
        return(id);
    }

    boolean isSlave(int channel) {
        DspGroup channelGroup = groups[inputChannels[channel].groupIndex];  // Get group passed group is a member of
        boolean isSlave = channelGroup.ChannelIsSlave(channel);
        //Log.i("<<<< IsSlave", "Group: " + Integer.toString(inputChannels[channel].groupIndex) + "  Channel: " + Integer.toString(channel) + " Slave:" + Boolean.toString(isSlave));
        return (isSlave);
    }


    boolean isSlaveOfGroup(int groupNumber, int channel) {
        boolean isSlave = false;

        if (channel >= 0 && channel < locationSpinnnerIds.length && groupNumber < groups.length) {
            DspGroup channelGroup = groups[groupNumber];
            isSlave = channelGroup.ChannelIsSlave(channel);
        }
        return(isSlave);
    }

    boolean isMaster(int channel) {
        DspGroup channelGroup = groups[inputChannels[channel].groupIndex];  // Get group passed group is a member of
        return (channelGroup.ChannelIsMaster(channel));
    }



    @Override
    // This function handles the Touch Event for all controls on this screen
    public boolean onTouch (View control, MotionEvent event) {
/*
        if (control == sliderAmpTurnOnDelay) {
            int progressValue = (int)sliderAmpTurnOnDelay.getSliderValue();
            textAmpTurnOnDelay.setText(Integer.toString(progressValue) + " Sec.");
            DspService dspService = mainActivity.GetDspService();
            if (dspService != null) {
                ampTurnOnDelay = progressValue * 10;
                if (lastAmpTurnOnDelay != ampTurnOnDelay) {
                    lastAmpTurnOnDelay = ampTurnOnDelay;
                    amp_turn_on_delay_command.SetData(ampTurnOnDelay);
                    dspService.SendDspCommand(amp_turn_on_delay_command);
                    Log.i("Input OnTouch", Integer.toString(progressValue));
                }
            }
        }
*/
        return(true);
    }

    @Override
    // Fired when the user clicks on any dropdown
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String header = "";
        if (parent != null && view != null)
            header = ">>>>";

        String ParentState = (parent == null ? "NULL" : "GOOD");
        String ViewState = (view == null ? "NULL" : "GOOD");

Log.i(header + "FragmentOutputs","onItemSelected" + " Parent: " + ParentState + " View: " + ViewState);
        if (parent != null && view != null) {
            int spinnerId = parent.getId();
            if (view.isEnabled()) {
                for (int channel = 0; channel < locationSpinnnerIds.length; channel++) {
                    if (spinnerId == locationSpinnnerIds[channel]) {
                        inputChannels[channel].position.SetSpeakerPosition(position);
Log.i("onItemSelected", "Set Location [" + Integer.toString(channel) + "] = " + Integer.toString(position) + "  Setting location: " + Boolean.toString(inputChannels[channel].settingLocationSpinner ));

                        if (!inputChannels[channel].position.LocationIsUsed()) {
                            checkboxesMute[channel].setEnabled(false);
                            checkboxesInvert[channel].setEnabled(false);
                            spinnerGroups[channel].setEnabled(false);
                            spinnerGroups[channel].setSelection(0);
                        } else {
                            checkboxesMute[channel].setEnabled(true);
                            checkboxesInvert[channel].setEnabled(true);
                            spinnerGroups[channel].setEnabled(true);
                        }

                        if (inputChannels[channel].settingLocationSpinner == false) {
                            int location_id = inputChannels[channel].position.GetSpeakerLocationId();
Log.i("Updating Spkr Location", Integer.toString(channel) + " = " + Integer.toString(location_id));

                            DspService dspService = mainActivity.GetDspService();
                            if (dspService != null) {
                                int group_channel = inputChannels[channel].groupIndex;
                                byte data[] = {(byte) channel, (byte) location_id, (byte) group_channel, (byte) position};
                                set_speaker_location_command.SetData(data);
                                //set_speaker_location_command.SetData((byte) channel, (byte) location_id, (byte) group_channel );
                                dspService.SendDspCommand(set_speaker_location_command);
Log.i("Sending Spkr Location", Integer.toString(channel) + " = " + Integer.toString(location_id));
                            }
                        }
                        inputChannels[channel].settingLocationSpinner = false;
                        Log.i("onItemSelected", "Set Location - cleared settingLocationSpinner - Channel: " + Integer.toString(channel) + " = " + inputChannels[channel].settingLocationSpinner);
                        break;     // Exit For loop;
                    }

                    else if (spinnerId == groupSpinnnerIds[channel]) {
Log.i("onItemSelected", "Set Group");
                        if ( inputChannels[channel].updatingScreen == false) {
                            int current_group = inputChannels[channel].groupIndex;
                            RemoveChannelFromGroup(groups[current_group], channel);
                            boolean is_master = AddChannelToGroup(groups[position], channel);
                            if (is_master == false) {
                                int groupId = inputChannels[channel].groupIndex;
                                int masterChannel = groups[groupId].GetMasterChannel();
                                if (masterChannel >= 0 && masterChannel < 10)
                                    mainActivity.mEqualizer_X_Fragment.CopyEqFilter(masterChannel, channel);
                            }
                            UpdateMasterSlaveIcon(channel);
                            Log.i("onItemSelected", "Set Group [" + Integer.toString(channel) + "] = " + Integer.toString(position));
                            DspService dspService = mainActivity.GetDspService();
                            if (dspService != null) {
                                byte groupId = (byte) inputChannels[channel].groupIndex;
                                if (groups[position].ChannelIsMaster(channel))        // If the master channel ...
                                    groupId |= 0x80;                                // set high order bit
                                Log.i(" --- Group ID:", Integer.toString(channel) + " = " + Integer.toString(groupId));
                                set_group_command.SetData((byte) channel, groupId);
                                dspService.SendDspCommand(set_group_command);
                            }
                        }
                        inputChannels[channel].updatingScreen = false;
                        break;     // Exit For loop;
                    }
                }
            }
        }
    }

    void RemoveChannelFromGroup (DspGroup group,int channel){
        group.RemoveChannel(channel, this);                 // Remove the channel from the existing Group
    }


    boolean AddChannelToGroup(DspGroup group, int channel) {
        boolean channel_is_master = false;
        if (group.groupID >= 0 && group.groupID < groups.length) {
            inputChannels[channel].groupIndex = group.groupID;          // Add it to the new group
            group.AddChannel(channel);
            channel_is_master = group.ChannelIsMaster(channel);
        }
        return(channel_is_master);
    }


    void UpdateMasterSlaveIcon(int channel) {
        int pngId = GetPngId(channel);
        if (pngId >= 0) {
            iconGroups[channel].setBackgroundResource(pngId);
            iconGroups[channel].setVisibility(View.VISIBLE);
        }
        else
            iconGroups[channel].setVisibility(View.INVISIBLE);
    }

    // Removes the passed channel from it's current group and adds it to the passed group
    // Updates the slider
    public void SetGroup(int group, int channel) {
        Log.i("SetGroup", "Channel: " + Integer.toString(channel) + " Current Group: " + inputChannels[channel].groupIndex);
        Log.i(" ----- ", "New Group: " + Integer.toString(group));
        spinnerGroups[channel].setSelection(group);
    }

    public int GetGroupIndex(int channel) {
        return(inputChannels[channel].groupIndex);
    }

    public DspGroup GetChannelGroup(int channel) {
        int groupIndex = inputChannels[channel].groupIndex;
        return(GetGroup(groupIndex));
    }

    public DspGroup GetGroup(int groupIndex) {
        if (groupIndex > 0 && groupIndex < locationSpinnnerIds.length)
            return(groups[groupIndex]);
        else
            return(groups[0]);
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Callback when nothing is selected from the list
    }

    @Override
    public void onHiddenChanged (boolean hidden){
        if (!hidden ){
            numberSupportedChannels = mainActivity.GetNumberSupportedChannels();
            for (int channel = 0; channel < 10; channel++) {
                if (channel >= numberSupportedChannels)
                    LayoutOutputs[channel].setVisibility(View.GONE);
                else {
                    LayoutOutputs[channel].setVisibility(View.VISIBLE);
                    inputChannels[channel].settingLocationSpinner = false;
                    int location = inputChannels[channel].position.GetPosition();
                    int current_location = spinnerLocations[channel].getSelectedItemPosition();
                    if (location != current_location) {
                        inputChannels[channel].settingLocationSpinner = true;
                        spinnerLocations[channel].setSelection(location, false);
                    }

                    if (inputChannels[channel].groupIndex != 0)
                        inputChannels[channel].updatingScreen = true;
                    spinnerGroups[channel].setSelection(inputChannels[channel].groupIndex);

                    checkboxesMute[channel].setChecked(inputChannels[channel].muted);

                    //checkboxesInvert[channel].setOnCheckedChangeListener(null);
                    checkboxesInvert[channel].setChecked(inputChannels[channel].inverted);
                    UpdateMasterSlaveIcon(channel);
                }
            }

// TODO - THIS SHOULD BE BASED ON THE STATE OF THE BLUETOOTH CONNECTION
//            EnableControls(true);

        }
    }

    @Override
    public void onCheckedChanged(CompoundButton button, boolean isChecked) {
        if (mainActivity.GetIsReading() == false) {
            int button_id = button.getId();
            DspService dspService = mainActivity.GetDspService();
            if (dspService != null) {
                for (int index = 0; index < checkboxesInvert.length; index++) {
                    CheckBox checkbox = (CheckBox) button;
                    if (button_id == checkboxInvertIds[index]) {
                        EnableControls(false);
                        inputChannels[index].inverted = checkbox.isChecked();
                        invert_sw_command.SetData((byte) index, (byte) (inputChannels[index].inverted ? 1 : 0));
                        dspService.SendDspCommand(invert_sw_command);
                        break;
                    } else if (button_id == checkboxMuteIds[index]) {
                        EnableControls(false);
                        inputChannels[index].muted = checkbox.isChecked();
                        mute_sw_command.SetData((byte) index, (byte) (inputChannels[index].muted ? 1 : 0));
                        dspService.SendDspCommand(mute_sw_command);
                        break;
                    }
                }
            }
        }
    }


    @Override
    // Handle user clicking radio buttons
    public void onClick(View button) {
/*
        int button_id = button.getId();
        DspService dspService = mainActivity.GetDspService();

        if  (button_id == R.id.radioAmpOnAlways) {
            amp_turn_on_command.SetData((byte)0);
            dspService.SendDspCommand(amp_turn_on_command);
        }

        else if (button_id == R.id.radioAmpOnSense){
            amp_turn_on_command.SetData((byte) 1);
            dspService.SendDspCommand(amp_turn_on_command);
        }

        EnableControls(false);
*/
    }


    public void EnableControls(Boolean state)
    {
        if (inputChannels[0] != null) {
            controlsAreVisible = state;
//            radioAmpOnAlways.setEnabled(state);
//            radioAmpOnSense.setEnabled(state);
//            sliderAmpTurnOnDelay.setEnabled(state);

            for (int index = numberSupportedChannels -1; index >= 0; index--) {
                if (!inputChannels[index].position.LocationIsUsed())  {
                    checkboxesMute[index].setEnabled(false);
                    checkboxesInvert[index].setEnabled(false);
//                    spinnerGroups[index].setEnabled(false);
                }
                else {
                    checkboxesMute[index].setEnabled(state);
                    checkboxesInvert[index].setEnabled(state);
                    spinnerGroups[index].setEnabled(state);
                }
                spinnerLocations[index].setEnabled(state);
            }

//            if (inputControlsAreEnabled)
//                EnableInputControls(state);
        }
    }

/*    public void EnableInputControls(Boolean state)
    {
    }
*/

    // Send Amplifier data
/*    public int SendAmpData() {
        int send_time = 0;
        DspService dspService = mainActivity.GetDspService();

        if (mainActivity.Connected()) {
            if (radioAmpOnAlways.isChecked())
                amp_turn_on_command.SetData((byte)0);
            else
                amp_turn_on_command.SetData((byte)1);
            dspService.SendDspCommand(amp_turn_on_command);
            dspService.WaitForAck();

            amp_turn_on_delay_command.SetData(ampTurnOnDelay);
            dspService.SendDspCommand(amp_turn_on_delay_command);
            send_time = dspService.WaitForAck();
        }
        return(send_time);
    }

    // Sets the Amp Turn On command data and the radio buttons to the indicated state
    void SetAmpTurnOnState(int amp_turn_on_state) {
        boolean state = (amp_turn_on_state != 0);
        radioAmpOnAlways.setChecked(!state);
        radioAmpOnSense.setChecked(state);
    }

    void SetAmpTurnOnState(int amp_turn_on_state, int amp_turn_on_delay) {
        this.SetAmpTurnOnState(amp_turn_on_state);
        sliderAmpTurnOnDelay.setValue(amp_turn_on_delay);
        textAmpTurnOnDelay.setText(Integer.toString(amp_turn_on_delay) + " Sec.");
    }
*/
    void SetData(int channel, int locationId, int groupIndex, boolean muted, boolean inverted) {
        if (channel >= 0 && channel < inputChannels.length) {
            int newLocationId = (locationId >=0 && locationId < DspInput.SPEAKER_POSITIONS.MAX_VALUE.value) ? locationId : 0;
            inputChannels[channel].position.position = DspInput.SPEAKER_POSITIONS.values()[newLocationId];
            inputChannels[channel].muted = muted;
            inputChannels[channel].inverted = inverted;
            int group = (((int) groupIndex) & (int)0x7F);
            if (group >= 0 && group < 10) {
                inputChannels[channel].groupIndex = group;
                AddChannelToGroup(groups[group], channel);
//                if ((groupIndex & 0x80) > 0) {
//                    groups[group].ForceMaster(channel);
//                    inputChannels[channel].master = true;
//                }
            }
        }
    }

    String GetLocationName(int channel_index) {
        String name = "Undefined";
        if (channel_index < inputChannels.length) {
            name = inputChannels[channel_index].position.GetLocationName();
        }
        return(name);
    }


    String GetLocationAbbreviation(int channel_index) {
        String name = "Undefined";
        if (channel_index < inputChannels.length) {
//            int nameIndex = inputChannels[channel_index].locationIndex;
//            name = DspInput.GetPositionAbbreviation(nameIndex);
            name = inputChannels[channel_index].position.GetPositionAbbreviation();
        }
        return(name);
    }


    Boolean IsUsed(int channel_index) {
        return(inputChannels[channel_index].position.LocationIsUsed());
//        return( DspInput.LocationIsUsed(channel_index));
    }



    Boolean IsMuted(int channel_index) {
        boolean muted = false;
        if (channel_index < inputChannels.length)
            muted = inputChannels[channel_index].muted;

        return(muted);
    }
    // Parses and XML file and updates the Clipping Sensitivity data
    public void ParseXmlData(XmlPullParser myParser) throws XmlPullParserException, IOException {

        Boolean done = false;

        int event = myParser.getEventType();
        while (!done) {
            String name = myParser.getName();

            if (event == XmlPullParser.END_TAG)
            {
/* TODO MOVE TO LEVELS
                if (name.equals("amp_turn_on"))
                {
                    String str_amp_turn_on_state = myParser.getAttributeValue(null, "state");
                    byte amp_turn_on_state = 0;
                    if (str_amp_turn_on_state != null)
                        if (str_amp_turn_on_state.contains("sense"))
                            amp_turn_on_state = 1;
                    SetAmpTurnOnState(amp_turn_on_state);
                   done = true;
                }

                else
*/                  if (name.equals("input_channel")) {
                    int channel = Integer.valueOf(myParser.getAttributeValue(null, "channel"));
                    int locationId = Integer.valueOf(myParser.getAttributeValue(null, "location"));
                    int groupIndex = Integer.valueOf(myParser.getAttributeValue(null, "group"));
                    boolean inverted = Boolean.valueOf(myParser.getAttributeValue(null, "inverted"));
                    boolean muted = Boolean.valueOf(myParser.getAttributeValue(null, "muted"));
                    boolean master = Boolean.valueOf(myParser.getAttributeValue(null, "master"));

                    Log.i(">> Inputs", "Channel " + Integer.toString(channel));
                    Log.i(">>>> Location ID", Integer.toString(locationId));
                    Log.i(">>>> Group Index", Integer.toString(groupIndex));
                    Log.i(">>>> Inverted", Boolean.toString(inverted));
                    Log.i(">>>> Muted", Boolean.toString(muted));
                    Log.i(">>>> Master", Boolean.toString(master));

                    if (channel >= 0 && channel < inputChannels.length) {
                        inputChannels[channel].position.position = DspInput.SPEAKER_POSITIONS.values()[locationId];
                        inputChannels[channel].groupIndex = groupIndex;
                        inputChannels[channel].inverted = inverted;
                        inputChannels[channel].muted = muted;
                        if (master)
                            groups[groupIndex].ForceMaster(channel);
                        else
                            groups[groupIndex].ForceSlave(channel);

                    }
                }
                done = true;
            }
            event = myParser.next();
        }
    }

    // Write our data to the XML file using the Serializer passed to  us
    public void WriteXml(XmlSerializer serializer) throws IOException
    {
        for (int channel = 0; channel < inputChannels.length; channel++) {
            serializer.startTag(null, "input_channel");
            serializer.attribute(null, "channel", Integer.toString(channel));
//int int_value = inputChannels[channel].position.locationId.GetValue();
//String str_value = Integer.toString(int_value);
            serializer.attribute(null, "location", Integer.toString(inputChannels[channel].position.position.GetValue()));
            serializer.attribute(null, "group", Integer.toString(inputChannels[channel].groupIndex));
            serializer.attribute(null, "inverted", Boolean.toString(inputChannels[channel].inverted));
            serializer.attribute(null, "muted", Boolean.toString(inputChannels[channel].muted));
            serializer.attribute(null, "master", Boolean.toString(groups[inputChannels[channel].groupIndex].ChannelIsMaster(channel)));
            serializer.endTag(null, "input_channel");
        }

/*
        serializer.startTag(null, "input_sources");
        serializer.startTag(null, "amp_turn_on");
        if (radioAmpOnSense.isChecked())
            serializer.attribute(null, "state", "on_sense");
        else
            serializer.attribute(null, "state", "always_on");
        serializer.endTag(null, "amp_turn_on");
        serializer.endTag(null, "input_sources");
*/
    }

    public int SendInputData(int channel) {
        int send_time = 0;
        MainActivity main_Activity = (MainActivity) getActivity();
        if (main_Activity.Connected()) {
            DspService dspService = mainActivity.GetDspService();
            byte[] input_data = GetInputData(channel);
            main_Activity.dspService.SetWaitingForAck(true);
            dspService.SendDspInput(input_data);
            send_time = main_Activity.dspService.WaitForAck();
        }
        return (send_time);
    }


// TODO - the group, mute and invert data should be integrated into DspINput and this function moved there
        public byte[] GetInputData(int channel){
        int groupIndex = inputChannels[channel].groupIndex;
            byte[] data = new byte[6];
            data[0] = (byte)channel;
            data[1] = (byte)inputChannels[channel].position.GetSpeakerLocationId();
            data[2] = (byte)groupIndex;
            if (groups[groupIndex].ChannelIsMaster(channel))            // Set high order bit of group index if master
                data[2] |= 0x80;
            data[3] = (inputChannels[channel].muted == true) ? (byte)1 : (byte)0;
            data[4] = (inputChannels[channel].inverted == true) ? (byte)1 : (byte)0;
            data[5] = (byte)inputChannels[channel].position.position.value;
            return(data);
        }

    }


