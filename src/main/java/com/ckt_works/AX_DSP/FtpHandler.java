package com.ckt_works.AX_DSP;

import android.os.AsyncTask;

import android.util.Log;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;

public class FtpHandler {

    public enum REMOTE_DOWNLOAD_STATUS {REMOTE_DOWNLOAD_IDLE, REMOTE_DOWNLOAD_WAITING, REMOTE_DOWNLOAD_SUCCESS, REMOTE_DOWNLOAD_FAILED};

    private REMOTE_DOWNLOAD_STATUS remote_download_status;
    private String LOG_TAG = "FtpCLient";
    private String ftp_server = "ftp.axxessupdater.com"; //"ftp.andybeck.com";
    private int ftp_port_number = 21;
    private String ftp_user_name = "AXDSPXL@axxessupdater.com"; //AxDsp";
    private String ftp_password = "&QVR;Y[7nE6b"; //"Jessie123*";
    private String save_to_file_name = "vehicle_types.xml";
    private String ftp_file_name;
    private File vehicle_file;

    private InetAddress ip_address;
    private String file_status;
    private FTPFile[] files;

    private DownloadFileTask download_file_task;
    private UpdateFileStatusTask update_file_info_task;
    private MainActivity main_activity;

    // Constructors
    FtpHandler(MainActivity mainActivity) {
        main_activity = mainActivity;
        remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_IDLE;
    }

    FtpHandler(MainActivity mainActivity, String server, int port_number, String user_name, String password) {
        main_activity =  mainActivity;
        ftp_server = server;
        ftp_port_number = port_number;
        ftp_user_name = user_name;
        ftp_password = password;
        remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_IDLE;
    }

    // Download the requested file from the server and save it to local storage
    public void DownloadFileInBackground(String file_name, String save_to_file_name){
        ftp_file_name = file_name;
        vehicle_file = new File(main_activity.getFilesDir(), save_to_file_name);
        download_file_task = new DownloadFileTask();
        remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_WAITING;
        download_file_task.execute();
    }


    public REMOTE_DOWNLOAD_STATUS GetRemoteDownloadStatus() {
        return(remote_download_status);
    }

    public void UpdateRemoteFileStatus(String file_name)
    {
        update_file_info_task = new UpdateFileStatusTask(file_name);
        update_file_info_task.execute();
    }

    public String GetStatusFileName(int index) {
        String filename = "";
        if (files.length >= (index -1))
            filename = files[index].getName();

        return (filename);
    }


    public String GetStatusFileName() {
        return(GetStatusFileName(0));
    }

    public long GetStatusFileTimeMilliseconds(int index) {
        long file_time = 0;
        if (files.length >= (index -1))
            file_time = files[index].getTimestamp().getTimeInMillis();

        return (file_time);
    }
    public long GetStatusFileTimeMilliseconds() {
        return(GetStatusFileTimeMilliseconds(0));
    }

    private boolean FtpDownloadFile(String server, int portNumber, String user, String password, String filename, File localFile) {
        FTPClient ftp_client = null;
        boolean success = false;

        try {
            ftp_client = new FTPClient();
            ftp_client.connect(server);
            Log.d(LOG_TAG, "Connected. Reply: " + ftp_client.getReplyString());

            int reply = ftp_client.getReplyCode();

            if(!FTPReply.isPositiveCompletion(reply)) {
                ftp_client.disconnect();
                Log.e("FtpClient","FTP server refused connection.");
                System.exit(1);
            }

            ftp_client.login(user, password);
            Log.d(LOG_TAG, "Logged in");
            ftp_client.setFileType(FTP.BINARY_FILE_TYPE);
            Log.d(LOG_TAG, "Downloading");
            ftp_client.enterLocalPassiveMode();

            OutputStream outputStream = null;
            try {
                outputStream = new BufferedOutputStream(new FileOutputStream(localFile));
                success = ftp_client.retrieveFile(filename, outputStream);
            }
            finally {
                if (outputStream != null) {
                    outputStream.close();

                if (ftp_client != null) {
                    ftp_client.logout();
                    ftp_client.disconnect();
                    }
                }
            }
  //          return success;
        }
        catch (Exception ex) {
            Log.e("FtpClient", "Error: " + ex.getMessage());
        }
        return(success);
    }


    // Download the the Vehicle Types file from the server via FTP
    private class DownloadFileTask extends AsyncTask<Void, Void, Void> {
        boolean download_successful = false;

        protected Void doInBackground(Void... arg0) {
            download_successful = FtpDownloadFile(ftp_server, ftp_port_number, ftp_user_name, ftp_password, save_to_file_name, vehicle_file);
            onPostExecute();
            return(null);
        }


        // Called by the "onPostExecute" call in "doInBackground"
        void onPostExecute() {
            if (download_successful == true) {
                remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_SUCCESS;
            }
            else {
                remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_FAILED;
                Log.w("tag", "Can't establish Internet Connection");
            }
        }
    }

    // Update the list of files on the server
    private class UpdateFileStatusTask extends AsyncTask<Void, Void, Void> {
        private boolean update_successful = false;
        FTPClient ftp_client = null;

        UpdateFileStatusTask(String file_name) {
            ftp_file_name = file_name;
        }

        protected Void doInBackground(Void... arg0) {
            try {
                ftp_client = new FTPClient();
                ftp_client.connect(ftp_server);
                Log.d(LOG_TAG, "Connected. Reply: " + ftp_client.getReplyString());
                ftp_client.login(ftp_user_name, ftp_password);
                Log.d(LOG_TAG, "Logged in");
                ftp_client.enterLocalPassiveMode();
                file_status = ftp_client.getStatus(ftp_file_name);
                files = ftp_client.listFiles();
                update_successful = true;
                if (ftp_client != null) {
                    ftp_client.logout();
                    ftp_client.disconnect();
                }
            }
            catch (Exception ex) {
                Log.e("FtpClient", "Error: " + ex.getMessage());
            }
            onPostExecute();
            return(null);
        }

        // Called by the "onPostExecute" call in "doInBackground"
        void onPostExecute() {
            if (update_successful == true) {
                remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_SUCCESS;
            }
            else {
                remote_download_status = REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_FAILED;
                        Log.w("tag", "Can't establish Internet Connection");
            }
        }
    }
}

