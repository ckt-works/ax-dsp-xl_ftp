
package com.ckt_works.AX_DSP;
// Created by Abeck on 12/27/2016. //


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

//enum HORZ_SLIDER_TYPE {HORZ_SLIDER_FULL, HORZ_SLIDER_LEFT, HORZ_SLIDER_RIGHT}


public class HorizontalSlider extends CwSlider implements OnTouchListener {
    private Boolean is_enabled = false;
    private Boolean set_slider_value = true;

    // Views that contain the visual elements
    private ImageView mThumbImageView;
    private ImageView mSliderBarImageView;


    // ID's of the background images
    private int enabledImage = R.drawable.slider_bar_horz_hash;
    private int disabledImage = R.drawable.slider_bar_horz_hash_disabled;

    // Images used for thumb and the slider bar
    private Bitmap mSliderBarDisabledBitmap;
    private Bitmap mThumbBitmap;
    private Bitmap mSliderBarBitmap;

    private Bitmap rawThumbBitmap;
    private Bitmap rawBarEnabledBitmap;
    private Bitmap rawBarDisabledBitmap;

    // Default values
    private int slider_min_value = 0;
    private int slider_max_value = 1000;
    private int slider_range = slider_max_value - slider_min_value;
    private final int slider_sensitivity = 10;

    // Used internally during touches event
    private int slider_height;
    private int slider_width;
    private float value_to_set = 0;
    private int slider_left_position;
    private int slider_right_position;
    private int slider_top_position;
    private int slider_bottom_position;

    // Used to adjust displayed position on thumb images
    private int thumb_offset = 0;
    private int m_thumb_size;
    private int thumb_x_position;       // The touch position relative to the left/right of the slider view

    // Used during draw to control bitmap copying
    Rect enable_bitmap_src;
    Rect enable_bitmap_dest;
    Rect disable_bitmap_src;
    Rect disable_bitmap_dest;
    HORZ_SLIDER_TYPE slider_type = HORZ_SLIDER_TYPE.HORZ_SLIDER_FULL;


    // Holds the object that is listening to this slider.
    private OnTouchListener mDelegateOnTouchListener;

    /// Default constructors. Tell Android that we're doing custom drawing and that we want to listen to touch events.
    public HorizontalSlider(Context context) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
    }

    public HorizontalSlider(Context context, int min_value, int max_value) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
        slider_min_value = min_value;
        slider_max_value = max_value;
    }

    public HorizontalSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        rawThumbBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_thumb_horz);

        //rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_horz);
        //rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_horz_disabled);
 //       rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_horz_hash);
 //       rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_horz_hash_disabled);
        rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), enabledImage);
        rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), disabledImage);
        setOnTouchListener(this);
    }

    public HorizontalSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setWillNotDraw(false);
        this.setOnTouchListener(this);
    }

     public void setNotUsed(boolean state){}

    public  void setImages(int enabled_image, int disabled_image)
    {
        enabledImage = enabled_image;
        disabledImage = disabled_image;
        rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), enabledImage);
        rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), disabledImage);
    }

    public void setDecimalMultiplier(int multiplier){}

    public void setDelegateOnTouchListener(OnTouchListener onTouchListener) {
        mDelegateOnTouchListener = onTouchListener;
    }

   public int setValue(float value){
       value_to_set = value; //slider_min_value + ((percent * slider_range)/ slider_max_value);
       set_slider_value = true;
       invalidate();
       return ((int)value);
   }

/*
     // This sets the value that the slider thumb should be set to as a percent
    public void setProgress(float value) {
        value_to_set = value; //slider_min_value + ((percent * slider_range)/ slider_max_value);
        set_slider_value = true;
        invalidate();
    }
*/
    public void SetType (HORZ_SLIDER_TYPE type)
    {
        slider_type = type;
        SetSliderRects();
    }

    public int GetMinValue(){ return(slider_min_value);}

    //  This sets the range of the slider values. i.e. 0 to 1, or -10 to 10.
    public void setRange(int min, int max) {
        slider_min_value = min;
        slider_max_value = max;
        slider_range = max - min;
    }




    // Returns the current value of the slider
    public float getSliderValue() {
        float relative_position = (float) thumb_x_position / (float)slider_width;
        float value = relative_position * (float) slider_range;
        value += (float) slider_min_value;
        if (value > (float) slider_max_value)
            value = (float) slider_max_value;
        else if (value < (float) slider_min_value)
            value = (float) slider_min_value;
        return(value);
    }


    @Override
    public void setEnabled(boolean state) {
        super.setEnabled(state);
        is_enabled = state;
    }

    @Override
    // Loads the  child view resources if not already loaded
    public void onFinishInflate() {
        if (mThumbImageView == null) {
            mThumbImageView = (ImageView) this.getChildAt(1);
            this.removeView(mThumbImageView);
        }

        if (mSliderBarImageView == null) {
            mSliderBarImageView = (ImageView) this.getChildAt(0);
            this.removeView(mSliderBarImageView);
        }
        super.onFinishInflate();
    }

    @Override
    // Creates our scaled bitmaps and layout dependent variables
    public void onLayout (boolean changed, int left, int top, int right, int bottom){
        m_thumb_size = mThumbImageView.getLayoutParams().width; // MeasuredWidth();
        int thumb_height = mThumbImageView.getLayoutParams().height; // .getMeasuredHeight();
        mThumbBitmap = Bitmap.createScaledBitmap(rawThumbBitmap, m_thumb_size, thumb_height, false);
        thumb_offset = mThumbBitmap.getHeight() / 2;

        slider_left_position = thumb_offset;
        slider_right_position = right -= m_thumb_size;

        slider_width = right - left;
        slider_height = (m_thumb_size * 2 / 3);

        slider_top_position = thumb_offset - (slider_height / 2);
        slider_bottom_position -= m_thumb_size;

        mSliderBarBitmap = Bitmap.createScaledBitmap(rawBarEnabledBitmap, slider_width, slider_height, false);
        mSliderBarDisabledBitmap = Bitmap.createScaledBitmap(rawBarDisabledBitmap, slider_width, slider_height, false);

        SetSliderRects();
        super.onLayout(changed, left, top, right, bottom);
    }

    private void SetSliderRects()
    {
        if (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT)
        {
// TODO ANDY - replace mSliderBarBitmap.getHeight() with slider_height???
            enable_bitmap_src = new Rect(0, 0, slider_width, slider_height);
            enable_bitmap_dest = new Rect(slider_left_position, slider_top_position, slider_width, slider_top_position + slider_height);
            disable_bitmap_src =  new Rect(0, 0, slider_width, slider_height);
            disable_bitmap_dest =  new Rect(slider_left_position, slider_top_position, slider_width, slider_top_position + slider_height);
        }

        else if (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT)
        {
// TODO ANDY - replace mSliderBarBitmap.getHeight() with slider_height???
            enable_bitmap_src =  new Rect(0, 0, slider_width, slider_height);
            enable_bitmap_dest =  new Rect(slider_left_position, slider_top_position, slider_width, slider_top_position + slider_height);
            disable_bitmap_src = new Rect(0, 0, slider_width, slider_height);
            disable_bitmap_dest = new Rect(slider_left_position, slider_top_position, slider_width, slider_top_position + slider_height);
        }
    }


    // Process touch events
    public boolean onTouch(View view, MotionEvent event) {
        if (is_enabled) {
            int action = event.getAction();
            if  (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE)
            {
                int new_position = (int) event.getX();
                new_position -= thumb_offset;
                invalidate();
                thumb_x_position = new_position;
                if (mDelegateOnTouchListener != null)
                    mDelegateOnTouchListener.onTouch(view, event);
                return true;
            }
        }
        return false;
    }

    @Override
    // Draws the slider
    protected void onDraw (Canvas canvas)
    {
        if (set_slider_value)                      // Adjust thumb position (this handles the case where setValue() was called)
        {
            float new_value = value_to_set - (float)slider_min_value;
            float position_percent = new_value / (float)slider_range;
            thumb_x_position = (int)(slider_width * position_percent);
            set_slider_value = false;
        }

                    // Keep the slider thumb in bounds
        {
            if (thumb_x_position < 0)
                thumb_x_position = 0;
            else if (thumb_x_position > slider_width)
                thumb_x_position = slider_width;
        }

        if (mSliderBarBitmap != null) {       // Draw the slider bar

            int image_split = thumb_x_position+ thumb_offset;
            if (is_enabled) {
                switch (slider_type) {
                    case HORZ_SLIDER_FULL:
                        canvas.drawBitmap(mSliderBarBitmap, slider_left_position, slider_top_position, null);
                        break;

                    case HORZ_SLIDER_LEFT:
                        enable_bitmap_src.right = image_split;
                        enable_bitmap_dest.right = image_split;
                        canvas.drawBitmap(mSliderBarBitmap, enable_bitmap_src, enable_bitmap_dest, null);

                        disable_bitmap_src.left = image_split;
                        disable_bitmap_dest.left = image_split;
                        canvas.drawBitmap(mSliderBarDisabledBitmap, disable_bitmap_src, disable_bitmap_dest, null);
                        break;

                    case HORZ_SLIDER_RIGHT:
                        enable_bitmap_src.left = image_split;
                        enable_bitmap_dest.left = image_split;
                        canvas.drawBitmap(mSliderBarBitmap, enable_bitmap_src, enable_bitmap_dest, null);

                        disable_bitmap_src.right = image_split;
                        disable_bitmap_dest.right = image_split;
                        canvas.drawBitmap(mSliderBarDisabledBitmap, disable_bitmap_src, disable_bitmap_dest, null);
                        break;
                }
            }
            else    // NOT enabled
                canvas.drawBitmap(mSliderBarDisabledBitmap, slider_left_position, slider_top_position, null);

        }

        if (mThumbBitmap != null)             // Draw the slider thumb
            canvas.drawBitmap(mThumbBitmap, thumb_x_position, 0, null);
    }
}