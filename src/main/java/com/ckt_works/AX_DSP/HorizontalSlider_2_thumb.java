
package com.ckt_works.AX_DSP;
// Created by Abeck on 12/27/2016. //


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

enum HORZ_SLIDER_TYPE {HORZ_SLIDER_FULL, HORZ_SLIDER_LEFT, HORZ_SLIDER_RIGHT, HORZ_SLIDER_BETWEEN}
enum SLIDER_VALUE {SLIDER_LOWER, SLIDER_UPPER}


public class HorizontalSlider_2_thumb extends CwSlider implements OnTouchListener {
    private final int MIN_BANDWIDTH = 10;
    private Boolean is_enabled = false;
    private Boolean set_slider_value = true;

    // Views that contain the visual elements
    private ImageView mThumbImageView;
    //private ImageView mThumbImageView_2;
    private ImageView mSliderBarImageView;



    // ID's of the background images
    private int enabledImage = R.drawable.slider_bar_horz_thin_border;
    private int disabledImage = R.drawable.slider_bar_horz_disabled_thin_border;

    // Images used for thumb and the slider bar

    private SLIDER_VALUE selected_slider = SLIDER_VALUE.SLIDER_UPPER;


    private Bitmap rawThumbBitmapUpper;
    private Bitmap mThumbBitmapUpper;

    private Bitmap rawThumbBitmapLower;
    private Bitmap mThumbBitmapLower;

    private Bitmap rawBarEnabledBitmap;
    private Bitmap rawBarDisabledBitmap;
    private Bitmap mSliderBarDisabledBitmap;
    private Bitmap mSliderBarBitmap;

    private int barTopPosition;

    // Default values
    private int slider_min_value = 0;
    private int slider_max_value = 1000;
    private int slider_range = slider_max_value - slider_min_value;
    private final int slider_sensitivity = 10;

    // Used internally during touches event
    private int window_height;
    private int slider_bar_height;
    private int slider_width;
    private int value_to_set = 0;
    private int slider_left_position;
    private int slider_right_position;
    private int slider_top_position;
    private int slider_bottom_position;

    // Used to adjust displayed position on thumb images
    private int yPosition = 0;
    private int thumb_offset = 0;
    private int mThumbWidth;
    private int m_thumb_height;

    private float upperThumbValue;
    private float lowerThumbValue;
    private int upperThumbPosition;       
    private int lowerThumbPosition;

    // Used during draw to control bitmap copying
    Rect enable_bitmap_src;
    Rect enable_bitmap_dest;
    Rect disable_bitmap_src;
    Rect disable_bitmap_dest;
    HORZ_SLIDER_TYPE slider_type = HORZ_SLIDER_TYPE.HORZ_SLIDER_BETWEEN;


    // Holds the object that is listening to this slider.
    private OnTouchListener mDelegateOnTouchListener;

    /// Default constructors. Tell Android that we're doing custom drawing and that we want to listen to touch events.
    public HorizontalSlider_2_thumb(Context context) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
    }

    public HorizontalSlider_2_thumb(Context context, int min_value, int max_value) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
        slider_min_value = min_value;
        slider_max_value = max_value;
    }

    public HorizontalSlider_2_thumb(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        rawThumbBitmapUpper = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_thumb_upper);
        rawThumbBitmapLower = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_thumb_lower);

        rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), enabledImage);
        rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), disabledImage);
        setOnTouchListener(this);
    }

    public HorizontalSlider_2_thumb(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setWillNotDraw(false);
        this.setOnTouchListener(this);
    }

    public void setNotUsed(boolean state){}


    public  void setImages(int enabled_image, int disabled_image)
    {
        enabledImage = enabled_image;
        disabledImage = disabled_image;
        rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), enabledImage);
        rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), disabledImage);
    }

    public void setDecimalMultiplier(int multiplier){}

    public void setDelegateOnTouchListener(OnTouchListener onTouchListener) {
        mDelegateOnTouchListener = onTouchListener;
    }

/*
    public int setValue(SLIDER_VALUE slider, float value){
        float new_value = value - (float)slider_min_value;
        float position_percent = new_value / (float)slider_range;
        value_to_set = (int)(slider_width * position_percent);

//        if (value_to_set < 0)
//            value_to_set = 0;
//        else if (value_to_set > slider_width)
//            value_to_set = slider_width;

        if (slider == SLIDER_VALUE.SLIDER_LOWER)
            lowerThumbPosition = value_to_set;
        else
            upperThumbPosition = value_to_set;
        
//        set_slider_value = true;
        invalidate();
        return(value_to_set);   
    }
*/

   public int setValue(float value) {
        float new_value = value - (float)slider_min_value;
        float position_percent = new_value / (float)slider_range;
        value_to_set = (int)(slider_width * position_percent);

       if (value_to_set < 0)
           value_to_set = 0;
       else if (value_to_set > slider_width)
           value_to_set = slider_width;

        set_slider_value = true;
        return(value_to_set);
   }

    public void setValue(SLIDER_VALUE selected_slider, float value) {
        if (selected_slider == SLIDER_VALUE.SLIDER_UPPER)
            upperThumbValue = value;
        else
            lowerThumbValue = value;
        UpdateThumbPositions();
/*
        float new_value = value - (float) slider_min_value;
        float position_percent = new_value / (float) slider_range;
        int new_position = (int) (slider_width * position_percent);

//    new_position -= thumb_offset;
        if ((new_position >= 0) && (new_position <= slider_width)) {
            if (selected_slider == SLIDER_VALUE.SLIDER_UPPER) {
                if (new_position <= slider_width) {
                    upperThumbPosition = new_position;
                    if (lowerThumbPosition > upperThumbPosition + MIN_BANDWIDTH)
                        lowerThumbPosition = upperThumbPosition - MIN_BANDWIDTH;
                }
            } else {
                if (new_position >= 0) {
                    lowerThumbPosition = new_position;
                    if (upperThumbPosition < lowerThumbPosition + MIN_BANDWIDTH)
                        upperThumbPosition = lowerThumbPosition + MIN_BANDWIDTH;
                }
            }
        }
*/
        invalidate();

    }
/*
     // This sets the value that the slider thumb should be set to as a percent
    public void setProgress(float value) {
        value_to_set = value; //slider_min_value + ((percent * slider_range)/ slider_max_value);
        set_slider_value = true;
        invalidate();
    }
*/
    public void SetType (HORZ_SLIDER_TYPE type)
    {
        slider_type = type;
//        SetSliderRects();
        invalidate();
    }

    public int GetMinValue(){ return(slider_min_value);}

    //  This sets the range of the slider values. i.e. 0 to 1, or -10 to 10.
    public void setRange(int min, int max) {
        slider_min_value = min;
        slider_max_value = max;
        slider_range = max - min;
    }

    public HORZ_SLIDER_TYPE GetSliderType() { return (slider_type);}

    private float ComputeSliderValue(float relative_position) {
        float value = relative_position * (float) slider_range;
        value += (float) slider_min_value;
        if (value > (float) slider_max_value)
            value = (float) slider_max_value;
        else if (value < (float) slider_min_value)
            value = (float) slider_min_value;
        return(value);
    }

    // Returns the current value of the slider
    public float getSliderValue() {
        float relative_position = 0;
        if (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT)
            relative_position = (float) lowerThumbPosition / (float)slider_width;
        else
            relative_position = (float) upperThumbPosition / (float)slider_width;
        return(ComputeSliderValue(relative_position));
    }


    // Returns the current value of the slider
    public float getSliderValue(SLIDER_VALUE slider) {
        float relative_position = 0;
        if (slider == SLIDER_VALUE.SLIDER_LOWER)
            relative_position = (float) lowerThumbPosition / (float)slider_width;
        else
            relative_position = (float) upperThumbPosition / (float)slider_width;
        return(ComputeSliderValue(relative_position));
    }


    @Override
    public void setEnabled(boolean state) {
        super.setEnabled(state);
        is_enabled = state;
    }

    @Override
    // Loads the  child view resources if not already loaded
    // Must load them in reverse order to use the correct indices - they are deleted after loading so the indices change
    public void onFinishInflate() {
        ImageView mThumbImageView_2 = null;

        if (mThumbImageView_2 == null) {
            mThumbImageView_2 = (ImageView) this.getChildAt(2);
            this.removeView(mThumbImageView_2);
            mThumbWidth = mThumbImageView_2.getLayoutParams().width; // MeasuredWidth();
            m_thumb_height = mThumbImageView_2.getLayoutParams().height; // .getMeasuredHeight();
        }

        if (mThumbImageView == null) {
            mThumbImageView = (ImageView) this.getChildAt(1);
            this.removeView(mThumbImageView);
        }

        if (mSliderBarImageView == null) {
            mSliderBarImageView = (ImageView) this.getChildAt(0);
            this.removeView(mSliderBarImageView);
        }

        super.onFinishInflate();
    }

    @Override
    // Creates our scaled bitmaps and layout dependent variables
    public void onLayout (boolean changed, int left, int top, int right, int bottom){
        mThumbBitmapUpper = Bitmap.createScaledBitmap(rawThumbBitmapUpper, mThumbWidth, m_thumb_height, false);
        mThumbBitmapLower = Bitmap.createScaledBitmap(rawThumbBitmapLower, mThumbWidth, m_thumb_height, false);

        barTopPosition = (m_thumb_height * 70)/ 100; // 80) / 100;

        int thumb_height = mThumbBitmapUpper.getHeight();
        thumb_offset = mThumbBitmapUpper.getWidth() / 2;
        window_height = (thumb_height * 25 ) /10;

        slider_left_position = thumb_offset;
        slider_right_position = right -= mThumbWidth;

        slider_width = right - left;
        slider_bar_height = (thumb_height * 2 / 3);

        slider_top_position = thumb_height - (slider_bar_height / 2);
        slider_bottom_position += thumb_height;

        mSliderBarBitmap = Bitmap.createScaledBitmap(rawBarEnabledBitmap, slider_width, slider_bar_height, false);
        mSliderBarDisabledBitmap = Bitmap.createScaledBitmap(rawBarDisabledBitmap, slider_width, slider_bar_height, false);

        UpdateThumbPositions();

        /*float new_value = upperThumbValue - (float) slider_min_value;
        float position_percent = new_value / (float) slider_range;
        int new_position = (int) (slider_width * position_percent);
        if ((new_position >= 0) && (new_position <= slider_width))
            upperThumbPosition = new_position;

        new_value = lowerThumbValue - (float) slider_min_value;
        position_percent = new_value / (float) slider_range;
        new_position = (int) (slider_width * position_percent);
        if ((new_position >= 0) && (new_position <= slider_width))
            lowerThumbPosition = new_position;
*/

//        SetSliderRects();
        super.onLayout(changed, left, top, right, bottom);
    }

    private void UpdateThumbPositions() {
        float new_value = upperThumbValue - (float) slider_min_value;
        float position_percent = new_value / (float) slider_range;
        int new_position = (int) (slider_width * position_percent);
        if ((new_position >= 0) && (new_position <= slider_width))
            upperThumbPosition = new_position;

        new_value = lowerThumbValue - (float) slider_min_value;
        position_percent = new_value / (float) slider_range;
        new_position = (int) (slider_width * position_percent);
        if ((new_position >= 0) && (new_position <= slider_width))
            lowerThumbPosition = new_position;
    }

    private void SetSliderRects()
    {
        if (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT) {
            disable_bitmap_src =  new Rect(0, 0, slider_width, slider_bar_height);
            disable_bitmap_dest =  new Rect(0, barTopPosition, slider_width, barTopPosition + slider_bar_height);
            enable_bitmap_src = new Rect(0, 0, slider_width, slider_bar_height);
            enable_bitmap_dest = new Rect(slider_left_position, barTopPosition, slider_width, barTopPosition + slider_bar_height);

            //           enable_bitmap_src =  new Rect(0, 0, slider_width, slider_bar_height);
 //           enable_bitmap_dest =  new Rect(0, barTopPosition-1, slider_width, barTopPosition + slider_bar_height+1);
 //           disable_bitmap_src = new Rect(0, 0, slider_width, slider_bar_height);
 //           disable_bitmap_dest = new Rect(slider_left_position, barTopPosition, slider_width, barTopPosition + slider_bar_height);

/*            enable_bitmap_src = new Rect(0, 0, slider_width, slider_bar_height);
            enable_bitmap_dest = new Rect(slider_left_position, barTopPosition, slider_width, barTopPosition + slider_bar_height);
            disable_bitmap_src =  new Rect(0, 0, slider_width, slider_bar_height);
            disable_bitmap_dest =  new Rect(slider_left_position, barTopPosition, slider_width, barTopPosition + slider_bar_height);
*/        }

        else if (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT) {
            enable_bitmap_src =  new Rect(0, 0, slider_width, slider_bar_height);
            enable_bitmap_dest =  new Rect(0, barTopPosition, slider_width, barTopPosition + slider_bar_height);
            disable_bitmap_src = new Rect(0, 0, slider_width, slider_bar_height);
            disable_bitmap_dest = new Rect(slider_left_position, barTopPosition, slider_width, barTopPosition + slider_bar_height);
        }
        else {
            enable_bitmap_src =  new Rect(0, 0, slider_width, slider_bar_height);
            enable_bitmap_dest =  new Rect(0, barTopPosition, slider_width, barTopPosition + slider_bar_height);

        }
    }

     // Process touch events
    public boolean onTouch(View view, MotionEvent event) {
  //      if (is_enabled)
        {
            int action = event.getAction();

            if (action == MotionEvent.ACTION_UP) {
                //event.setAction(255);
                if (mDelegateOnTouchListener != null)
                    mDelegateOnTouchListener.onTouch(view, event);
                return true;
            }

            else if  (action == MotionEvent.ACTION_DOWN) {
                yPosition = (int) event.getY();
                selected_slider = (yPosition < m_thumb_height) ? SLIDER_VALUE.SLIDER_UPPER : SLIDER_VALUE.SLIDER_LOWER;
            }

            if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE)
            {
                int new_position = (int) event.getX();
                new_position -= thumb_offset;
                if ((new_position >= 0) && (new_position <= slider_width)) {

                    if (selected_slider == SLIDER_VALUE.SLIDER_UPPER) {
                        if (new_position <= slider_width) {
                            upperThumbPosition = new_position;
                            if (lowerThumbPosition > upperThumbPosition + MIN_BANDWIDTH)
                                lowerThumbPosition = upperThumbPosition - MIN_BANDWIDTH;
                        }
                     } else {
                        if (new_position >= 0) {
                            lowerThumbPosition = new_position;
                            if (upperThumbPosition < lowerThumbPosition + MIN_BANDWIDTH)
                                upperThumbPosition = lowerThumbPosition + MIN_BANDWIDTH;
                        }
                     }
                    invalidate();
                    if (mDelegateOnTouchListener != null)
                        mDelegateOnTouchListener.onTouch(view, event);
                }
                return true;
            }
        }
        return false;
    }

    @Override
    // Draws the slider
    protected void onDraw (Canvas canvas)
    {
        SetSliderRects();

        if (mSliderBarBitmap != null) {       // Draw the slider bar

 //           int image_split = upperThumbPosition+ thumb_offset;
//            if (is_enabled)
            {
                switch (slider_type) {
                    case HORZ_SLIDER_FULL:
                        canvas.drawBitmap(mSliderBarBitmap, slider_left_position, barTopPosition, null);
                        break;

                    case HORZ_SLIDER_BETWEEN:
 //                       disable_bitmap_src.left = 0;
 //                       disable_bitmap_dest.left = 0;
 //                       canvas.drawBitmap(mSliderBarDisabledBitmap, disable_bitmap_src, disable_bitmap_dest, null);
                        canvas.drawBitmap(mSliderBarDisabledBitmap, slider_left_position, barTopPosition, null);

                        int barOffset = mThumbWidth /2 ;
                        enable_bitmap_src.left = 0; // lowerThumbPosition + barOffset;
                        enable_bitmap_src.right = enable_bitmap_src.width(); // upperThumbPosition + barOffset;
                        enable_bitmap_dest.left = lowerThumbPosition + barOffset;
                        enable_bitmap_dest.right = upperThumbPosition + barOffset;
                        canvas.drawBitmap(mSliderBarBitmap, enable_bitmap_src, enable_bitmap_dest, null);

//                        if (upperThumbPosition < lowerThumbPosition + MIN_BANDWIDTH)
//                            upperThumbPosition = lowerThumbPosition + MIN_BANDWIDTH;
                        canvas.drawBitmap(mThumbBitmapUpper, upperThumbPosition, 0, null);

//                        if (lowerThumbPosition > upperThumbPosition + MIN_BANDWIDTH)
//                            lowerThumbPosition = upperThumbPosition - MIN_BANDWIDTH;
                        canvas.drawBitmap(mThumbBitmapLower, lowerThumbPosition, m_thumb_height, null);

                        break;

                    case HORZ_SLIDER_LEFT:
                        enable_bitmap_src.right = lowerThumbPosition+ thumb_offset;
                        enable_bitmap_dest.right =  lowerThumbPosition+ thumb_offset;
                        canvas.drawBitmap(mSliderBarBitmap, enable_bitmap_src, enable_bitmap_dest, null);

                        disable_bitmap_src.left = lowerThumbPosition+ thumb_offset;
                        disable_bitmap_dest.left = lowerThumbPosition+ thumb_offset;
                        canvas.drawBitmap(mSliderBarDisabledBitmap, disable_bitmap_src, disable_bitmap_dest, null);

                        canvas.drawBitmap(mThumbBitmapLower, lowerThumbPosition, m_thumb_height, null);
                        break;

                    case HORZ_SLIDER_RIGHT:
                        enable_bitmap_src.left =  upperThumbPosition+ thumb_offset;
                        enable_bitmap_dest.left =  upperThumbPosition+ thumb_offset;
                        canvas.drawBitmap(mSliderBarBitmap, enable_bitmap_src, enable_bitmap_dest, null);

                        disable_bitmap_src.right =  upperThumbPosition+ thumb_offset;
                        disable_bitmap_dest.right =  upperThumbPosition+ thumb_offset;
                        canvas.drawBitmap(mSliderBarDisabledBitmap, disable_bitmap_src, disable_bitmap_dest, null);

                        canvas.drawBitmap(mThumbBitmapUpper, upperThumbPosition, 0, null);
                        break;
                }
            }
//            else    // NOT enabled
//                canvas.drawBitmap(mSliderBarDisabledBitmap, slider_left_position, slider_top_position, null);

        }

        if (mThumbBitmapUpper != null && (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_RIGHT || slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_BETWEEN))             // Draw the slider thumbs
            canvas.drawBitmap(mThumbBitmapUpper, upperThumbPosition, 0, null);

        if (mThumbBitmapLower != null&& (slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_LEFT || slider_type == HORZ_SLIDER_TYPE.HORZ_SLIDER_BETWEEN))             // Draw the slider thumbs
            canvas.drawBitmap(mThumbBitmapLower, lowerThumbPosition, m_thumb_height, null);

    }
}