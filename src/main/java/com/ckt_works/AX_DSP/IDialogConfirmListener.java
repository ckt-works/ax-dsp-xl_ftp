package com.ckt_works.AX_DSP;
// Created by Abeck on 3/28/2017. //

public interface IDialogConfirmListener {

    void onDialogConfirmClick(boolean answer);

}

