package com.ckt_works.AX_DSP;

interface IDialogPasswordListener {
    void onPasswordDialogClick(boolean answer, String password);
}
