package com.ckt_works.AX_DSP;
// Created by Abeck on 2/16/2017. //

public interface IDialogVehicleListener {
    void onApplyClick(String Manufacturer, String Model, boolean has_oe_amp, int vehicle_id, int model_id);
}

