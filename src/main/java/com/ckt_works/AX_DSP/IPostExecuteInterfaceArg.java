package com.ckt_works.AX_DSP;
// Created by Abeck on 4/17/2017.

public interface IPostExecuteInterfaceArg {
    void onPostExecuteArg(String arg);
}
