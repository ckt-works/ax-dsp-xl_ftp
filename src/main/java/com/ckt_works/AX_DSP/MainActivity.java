package com.ckt_works.AX_DSP;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.ckt_works.AX_DSP.DSP_COMMANDS.DSP_COMMAND_GET_PASSWORD;

// NOTE: these must be in the same order as the enumerations in the 257386 firmware
enum GAIN_CHANNELS {GAIN_FRONT, GAIN_REAR, GAIN_SUBWOOFER}
enum CROSSOVER_TYPES {CROSSOVER_LOW_PASS(0),  CROSSOVER_BAND_PASS(1), CROSSOVER_HIGH_PASS(2);
    public int value;
    CROSSOVER_TYPES(int value) {
        this.value= value;
    }

}

enum AUDIO_CHANNEL {FRONT(0), REAR(1), SUBWOOFER(2), CHANNEL_4(3), CHANNEL_5(4), ALL(5);
    int value;
    AUDIO_CHANNEL(int value) {
        this.value= value;
    }
}

enum CONNECTION_TYPE {CONNECT_NORMAL, CONNECT_PASSWORD_VALIDATE}

enum DEVICE_SUPPORT {SUPPORT_AX_DSP(1), SUPPORT_AX_DSP_X(2), SUPPORT_AX_DSP_LC(4);
    private int value;
    DEVICE_SUPPORT(int value) {
        this.value= value;
    }
    int getValue() {return(value);}
}

enum AX_DSP_TYPE {AX_DSP_420, AX_DSP_440}


public class MainActivity extends FragmentActivity implements FragmentBluetoothConnection.IBluetoothActionListener,
                          View.OnClickListener{

    // TODO ANDY FOR TESTING
    //File test_file;
    //FtpHandler ftp_handler;
    //volatile boolean remote_file_updated;


    static final int HAS_NO_OE_AMP = 0x01;

    public static final String KEY_DEVICE_NAME = "KEY_DEVICE_NAME";
    public static final String KEY_DEVICE_ADDRESS = "KEY_DEVICE_ADDRESS";
    public static final String KEY_DEVICE_RSSI = "KEY_DEVICE_RSSI";
    public static final String[] KEYS = {"KEY_DEVICE_NAME", "KEY_MAC_ADDRESS", "KEY_DEVICE_RSSI"};

    public static final String KEY_FILE_NAME = "KEY_FILE_NAME";
    public static final String[] KEY_FILE_NAMES = {"KEY_FILE_NAME"};
    public static final int[] IDS = {android.R.id.text1};

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int PERMISSION_REQUEST_WRITE_EXT_STORAGE = 2;

    private static final String TAG = "AX-DSP";

    private DialogProgress progress_dialog;
    private DialogProgressCountUpDown progress_dialog_countUpDown;

    private boolean bluetooth_enabled = false;          // TRUE if the Bluetooth has been enabled - position & BLE
    private boolean file_save_enabled = false;
    private final int REQUEST_LOCATION = 199;
    private Context my_context;

    private boolean dirty_flag = false;                 // Set when data has changed, but not "locked down" into 386
    private boolean not_applied = false;                // Set when data has been recalled from a file, but not applied
    private boolean reading_data = false;               // Set when we are reading data from the DSP - used to prevent acting like a control was clicked

    private boolean scanning_for_devices = false;
    private boolean mShuttingDown = false;
    private Messenger mMessenger;
    private Messenger mScannerService = null;

    private Timer rssiTimer;

    public VehicleTypeFile vehicleTypeFile = null;
    public DspService dspService = null;
    private CONNECTION_TYPE connectionType = CONNECTION_TYPE.CONNECT_NORMAL;

    private BleScannerService.State mState;// = BleScannerService.State.UNKNOWN;

    private AX_DSP_TYPE ax_dsp_type = AX_DSP_TYPE.AX_DSP_420;   // Type of Ax-DSP we're connected to - 420 or 440
    private int numberSupportedChannels = 10;                   // Number of channels for the connected device
    private int numberSupportedEqBands = 31;

    // Declare all of our Fragments
    private final FragmentInstruction mSetupInstructionFragment = FragmentInstruction.newInstance();
    private final FragmentBluetoothConnection mBluetoothConnectionFragment = FragmentBluetoothConnection.newInstance();
    protected final FragmentConfig mConfigurationFragment = FragmentConfig.newInstance();
//    protected final FragmentCrossover mCrossoverFragment = FragmentCrossover.newInstance();
    protected final FragmentCrossover_X mCrossover_X_Fragment = FragmentCrossover_X.newInstance();
    protected final FragmentEqSettings_X mEqualizer_X_Fragment = FragmentEqSettings_X.newInstance();
    protected final FragmentEqSettings_LC mEqualizer_LC_Fragment = FragmentEqSettings_LC.newInstance();
    protected final FragmentDelayAdjustInput mDelayAdjustFragment = FragmentDelayAdjustInput.newInstance();
    protected final FragmentDelayAdjustInput_X mDelayAdjust_X_Fragment = FragmentDelayAdjustInput_X.newInstance();
    protected final FragmentLevels mLevelsFragment = FragmentLevels.newInstance();
    protected final FragmentInputs mInputsFragment = FragmentInputs.newInstance();
    //protected
    public final FragmentOutputs_X mOutputs_X_Fragment = FragmentOutputs_X.newInstance();

    // Declare our navigation buttons
    private Button nav_button_instruct;
    private Button nav_button_scan;
    private Button nav_button_config;
    private Button nav_button_inputs;
    private Button nav_button_crossover;
    private Button nav_button_levels;
    private Button nav_button_eq_adjust;
    private Button nav_button_delay_adjust;

    private TextView textStatusConnection;
    private TextView textAlertMessage;
    private View mainContentView;


    private boolean connected = false;
    private String connected_to_name;
    private ArrayList<CScreenNavigation> screen_fragments = new ArrayList<>();

    private GoogleApiClient googleApiClient;

    // TODO ANDY REMOVE THIS
    //public String packageName = "com.ckt_works.AX_DSP_X";
    private Handler scanHandler;
    Handler requestHandler;

    public void SetDeviceName(String name){
        connected_to_name = name;
    }
    public String GetDeviceName(){
        return(connected_to_name);
    }

    public void SetAxDspType(AX_DSP_TYPE type) {
        ax_dsp_type = type;
        numberSupportedChannels = (ax_dsp_type == AX_DSP_TYPE.AX_DSP_440) ? 5 : 10;
        numberSupportedEqBands = (ax_dsp_type == AX_DSP_TYPE.AX_DSP_440) ? 15 : 31;
    }
    public AX_DSP_TYPE GetAxDspType() {return(ax_dsp_type);}
    public int GetNumberSupportedChannels() {return(numberSupportedChannels);}
    public int GetNumberEqBands() {return(numberSupportedEqBands);}

    private final ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mState = BleScannerService.State.UNKNOWN;
            mScannerService = new Messenger(service);
            try {
                Message msg = Message.obtain(null, BleScannerService.MSG_REGISTER);
                    if (msg != null) {
                        msg.replyTo = mMessenger;
                        mScannerService.send(msg);
                    } else {
                    mScannerService = null;
                }
            } catch (Exception e) {
                Log.w(TAG, "Error connecting to BleScannerService", e);
                mScannerService = null;
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mScannerService = null;
        }
    };

    public MainActivity() {
        super();
        mMessenger = new Messenger(new IncomingHandler(this));
    }


    @TargetApi(android.os.Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

// TODO andy Remove - for testing only
// BuildEqData();

        boolean saved = (savedInstanceState == null);
        setContentView(R.layout.layout_main);

        mainContentView = findViewById(R.id.main_content);
        nav_button_instruct = findViewById(R.id.navButtonInstruct);
        nav_button_instruct.setOnClickListener(this);
        nav_button_scan = findViewById(R.id.navButtonConnection);
        nav_button_scan.setOnClickListener(this);
        nav_button_config = findViewById(R.id.navButtonConfig);
        nav_button_config.setOnClickListener(this);
        nav_button_inputs = findViewById(R.id.navButtonInputs);
        nav_button_inputs.setOnClickListener(this);
        nav_button_crossover = findViewById(R.id.navButtonCrossover);
        nav_button_crossover.setOnClickListener(this);
        nav_button_levels = findViewById(R.id.navButtonLevels);
        nav_button_levels.setOnClickListener(this);
        nav_button_eq_adjust = findViewById(R.id.navButtonEqAdjust);
        nav_button_eq_adjust.setOnClickListener(this);
        nav_button_delay_adjust = findViewById(R.id.navButtonDelayAdjust);
        nav_button_delay_adjust.setOnClickListener(this);
        textStatusConnection = findViewById(R.id.textConnectionStatus);
        textStatusConnection.setOnClickListener(this);
        textAlertMessage = findViewById(R.id.textAlerts);
        textAlertMessage.setVisibility(View.INVISIBLE);
        SetDirtyFlag(false);
        SetNotAppliedFlag(false);

        Intent mScannerServiceIntent = new Intent(this, BleScannerService.class);
        Intent mDspServiceIntent = new Intent(this, DspService.class);
//        Intent enableLocationIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);

        AddFragments(true); //(savedInstanceState == null);

        scanHandler = new Handler();
        mShuttingDown = false;

        bindService(mScannerServiceIntent, mConnection, BIND_AUTO_CREATE);
        bindService(mDspServiceIntent, mDspConnection, BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(BleStatusChangeReceiver, makeGattUpdateIntentFilter());

        InitializeGuiHandler();

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            bluetooth_enabled = true;
            file_save_enabled = true;
        }
        else           // Android M Permission check 
        {
            if (this.checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                bluetooth_enabled = true;

            else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ThemeAlertboxDialog);
                builder.setMessage("You must grant Location Access so this App\ncan scan for AX-DSP's.\n\nClick the 'ALLOW' button on the next Pop-up.");
                builder.setPositiveButton(android.R.string.ok, null);
                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    //@Override 
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                    }
                });
                AlertDialog dialog = builder.show();
                TextView messageText = dialog.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);
            }

            if (this.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                file_save_enabled = true;
            else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ThemeAlertboxDialog);
                builder.setMessage("You must grant Memory Write Access so this App\ncan store Settings Data on your device.\n\nClick the 'ALLOW' button on the next Pop-up.");
                builder.setPositiveButton(android.R.string.ok, null);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    //@Override 
                    public void onDismiss(DialogInterface dialog) {
                        requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_WRITE_EXT_STORAGE);

                    }
                });
                AlertDialog dialog = builder.show();
                TextView messageText = dialog.findViewById(android.R.id.message);
                messageText.setGravity(Gravity.CENTER);
            }
        }
        EnableNavButtons();

        // TODO - ANDY DO WE NEED THIS HERE????
       // SetAxDspType(AX_DSP_TYPE.AX_DSP_440);

        StartRssiReader();
//        vehicleTypeFile = new VehicleTypeFile(this, false);

    }

    void InitializeGuiHandler() {
        HandlerThread handlerThread = new HandlerThread("HandlerThread");
        handlerThread.start();
        requestHandler = new Handler(handlerThread.getLooper());
    }




    @Override
    // Called when the user responds to a permission request dialog
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse position permission granted");
                    bluetooth_enabled = true;
                } else {
                    bluetooth_enabled = false;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ThemeAlertboxDialog);
                    builder.setMessage("Since position access has not been granted, this App will not be able to scan for AX-DSP's.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.show();
                    TextView messageText = dialog.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER);                }
            }
            break;

            case PERMISSION_REQUEST_WRITE_EXT_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "External write to storage permission granted");
                    file_save_enabled = true;
                }
                else {
                    file_save_enabled = false;
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.ThemeAlertboxDialog);
                    builder.setMessage("Since Memory Write Access has not been granted, "
                    + "this App will not be able to store Settings Data on your device.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.show();
                    TextView messageText = dialog.findViewById(android.R.id.message);
                    messageText.setGravity(Gravity.CENTER);
                }

            break;
        }
    }

    public void SetConnected(boolean state) {
        connected = state;
    }
    public boolean Connected(){return(connected);}

    // Connection Type Set & Get functions
    public void SetConnectionType(CONNECTION_TYPE type){connectionType = type;}
    public CONNECTION_TYPE GetConnectionType(){return (connectionType);}


    class UpdateRssiTask extends TimerTask {
        public void run() {
            if (connected && dspService != null)
                dspService.ReadConnectedRssi();
        }
    }

    void StartRssiReader() {
        rssiTimer = new Timer();
        TimerTask updateRssi = new UpdateRssiTask();
        rssiTimer.scheduleAtFixedRate(updateRssi, 0, 1500);
    }


    @Override
    // Switch to the option fragment (screen) associated with the clicked navigation button
    public void onClick(View button){
        int button_id = button.getId();

        if (button_id == textStatusConnection.getId())
            textStatusConnection.bringToFront();

        else if (button_id == nav_button_instruct.getId())
            switchScreen(mSetupInstructionFragment);

        else if (button_id == nav_button_scan.getId()) {
            switchScreen(mBluetoothConnectionFragment);
        }
        else if (button_id == nav_button_config.getId())
            switchScreen(mConfigurationFragment);

        else if (button_id == nav_button_inputs.getId())
            switchScreen(mOutputs_X_Fragment);

        else if (button_id == nav_button_crossover.getId())
            switchScreen(mCrossover_X_Fragment);

        else if (button_id == nav_button_levels.getId())
            switchScreen(mLevelsFragment);

        else if (button_id == nav_button_eq_adjust.getId()) {
            if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
                switchScreen(mEqualizer_LC_Fragment);
            else
                switchScreen(mEqualizer_X_Fragment);
        }
        else if (button_id == nav_button_delay_adjust.getId())
            switchScreen(mDelayAdjust_X_Fragment);
    }

    public void DisplayConfigurationScreen() {
        switchScreen(mConfigurationFragment);
    }

    @Override
    public void onSaveInstanceState (Bundle outState){
        super.onSaveInstanceState(outState);
        mShuttingDown = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
       // switchScreen(mBluetoothConnectionFragment);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    // Initialize all of the fragment dspService members
    private void SetFragmentDspServices()
    {
        dspService.SetMainActivity(this);
    }

    void CrossoverFragmentWriteXML(XmlSerializer serializer) {
        try {
                mCrossover_X_Fragment.WriteXml(serializer);
         }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    void CrossoverFragmentParseXML(XmlPullParser parser) {
        try {
                mCrossover_X_Fragment.ParseXmlData(parser);
        }
        catch (XmlPullParserException | IOException ex) {
            ex.printStackTrace();
        }
    }

    void EqualizerFragmentWriteXML(XmlSerializer serializer) {
        try {
            if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
                mEqualizer_LC_Fragment.WriteXml(serializer);
            else
                mEqualizer_X_Fragment.WriteXml(serializer);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    void EqualizerFragmentParseXML(XmlPullParser parser) {
        try {
            if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_440)
                mEqualizer_LC_Fragment.ParseXmlData(parser);
            else
                mEqualizer_X_Fragment.ParseXmlData(parser);
        }
        catch (XmlPullParserException | IOException ex) {
            ex.printStackTrace();
        }
    }

    void InputFragmentWriteXML(XmlSerializer serializer) {
        try {
            mOutputs_X_Fragment.WriteXml(serializer);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    void InputFragmentParseXML(XmlPullParser parser) {
        try {
            mOutputs_X_Fragment.ParseXmlData(parser);
        }
        catch (XmlPullParserException | IOException ex) {
            ex.printStackTrace();
        }
    }

    void DelayFragmentWriteXML(XmlSerializer serializer) {
        try {
            mDelayAdjust_X_Fragment.WriteXml(serializer);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    void DelayFragmentParseXML(XmlPullParser parser) {
        try {
            mDelayAdjust_X_Fragment.ParseXmlData(parser);
         }
        catch (XmlPullParserException | IOException ex) {
            ex.printStackTrace();
        }
    }


    //service connected/disconnected
private final ServiceConnection mDspConnection = new ServiceConnection() {
    public void onServiceConnected(ComponentName className, IBinder rawBinder) {
        dspService = ((DspService.LocalBinder) rawBinder).getService();
        Log.d(TAG, "onServiceConnected mService= " + dspService);
        if (!dspService.initialize(my_context)) {
            Log.e(TAG, "Unable to initialize Bluetooth");
        }
        SetFragmentDspServices();
    }

    public void onServiceDisconnected(ComponentName classname) {
        ////     mService.disconnect(mDevice);
        dspService = null;
        SetFragmentDspServices();
    }
};

    public void ProgressBarDisplay(String title)
    {
        ProgressBarDismiss();
        progress_dialog = new DialogProgress();                 // Display a Progress Bar while we read from the 386
        Bundle progress_title = new Bundle();
        progress_title.putString("TITLE", title);
        progress_dialog.setArguments(progress_title);
        progress_dialog.setCancelable(false);
//        progress_dialog.setTargetFragment(this, 0);
        FragmentTransaction fragmentManager =  getSupportFragmentManager().beginTransaction();
        progress_dialog.show(fragmentManager, "Progress");
        Log.i("++++ProgressBarDisplay", title);
    }


    public void ProgressBarDismiss(){
        if (progress_dialog != null) {
            progress_dialog.dismiss();
            progress_dialog = null;
//            Log.i("++++ProgressBarDismiss", "  ");
        }

        if (progress_dialog_countUpDown != null) {
            progress_dialog_countUpDown.dismiss();
            progress_dialog_countUpDown = null;
        }
    }


    public void DisplayProgressCountdownDialog(String title, int timeToDisplay) {
        progress_dialog_countUpDown = new DialogProgressCountUpDown();                 // Display a Progress Bar while we wait for 386 to lock down data

        Bundle progress_title = new Bundle();
        progress_title.putString("TITLE", title);
        progress_title.putString("TIME", Integer.toString(timeToDisplay));
        progress_dialog_countUpDown.setArguments(progress_title);
        progress_dialog_countUpDown.setCancelable(false);
        FragmentTransaction fragmentManager =  getSupportFragmentManager().beginTransaction();
        progress_dialog_countUpDown.show(fragmentManager, "DisplayProgress");
    }

    public void DisplayProgressCountupDialog(String title, int timeToDisplay) {
        progress_dialog_countUpDown = new DialogProgressCountUpDown();

        Bundle progress_data = new Bundle();
        progress_data.putString("TITLE", title);
        progress_data.putString("TIME", Integer.toString(timeToDisplay));
        progress_data.putString("DIRECTION", "UP");
        progress_dialog_countUpDown.setArguments(progress_data);

        progress_dialog_countUpDown.setCancelable(false);
        FragmentTransaction fragmentManager =  getSupportFragmentManager().beginTransaction();
        progress_dialog_countUpDown.show(fragmentManager, "DisplayProgress");
    }

    public void SetEqChannelsBands()
    {
        if (ax_dsp_type == AX_DSP_TYPE.AX_DSP_440)
            mEqualizer_LC_Fragment.SetChannelsBands(5, 15);
        else
            mEqualizer_X_Fragment.SetChannelsBands(10, 31);
    }

    public void GetDspStatus() {
        mConfigurationFragment.GetDspStatus();
    }

    public void UpdateConnectionStatus(String statusString, boolean display_rssi)
    {
        if (display_rssi) {
            String rssiString = " (" + dspService.GetConnectedRssi() + ")";
            statusString += rssiString;
        }
        textStatusConnection.setText(statusString);
    }

    public void CommErrorReceived(String source) {
        dspService.disconnect();
        EnableControls(false);
        String message = "Disconnected from AX-DSP!\n" + source;
        DialogAlert comm_error_alert = DialogAlert.newInstance("Communications", message);
        FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
        comm_error_alert.setTargetFragment(mBluetoothConnectionFragment,0);
        comm_error_alert.show(fragmentManager, "alert");
        switchScreen(mBluetoothConnectionFragment);
    }

    // Receives inter-process messages from other components
   private final BroadcastReceiver BleStatusChangeReceiver = new BroadcastReceiver()
   {

       public void onReceive(Context context, Intent intent) {
           String action = intent.getAction();
           if (action != null) {
               switch (action) {
                   case DspService.ACTION_RSSI_UPDATE:
                       // String rssi;
                       // rssi = intent.getExtras().getString("EXTRA_DATA");
                       UpdateConnectionStatus("Connected to " + connected_to_name, true);
                       //Log.i( "ACTION_RSSI_UPDATE", rssi);
                       break;

                   case DspService.ACTION_GATT_CONNECTED:
                       Log.i("BroadcastReceiver", "DspService Connected");
                       Log.i("Main.onReceive", "ACTION_GATT_CONNECTED");
                       break;

                   case DspService.ACTION_GATT_SERVICES_DISCOVERED:
                       Log.i("BroadcastReceiver", ">>>>>>>>> Services Discovered");
// TODO  EnableDspServiceNotification in DspService
                       dspService.EnableDspServiceNotification();
                        dspService.ReadConnectedRssi();
                       break;

                   case DspService.ACTION_CONNECTION_COMPLETE:
                       Log.i("BroadcastReceiver", "<<<<<<<<< AX-DSP Connection Complete");
                       if (connectionType == CONNECTION_TYPE.CONNECT_PASSWORD_VALIDATE) {
Log.i("DSP_CMD_GET_PASSWORD", "Start");
                           // Get Password from AX_DSP-X
                           DspCommand get_command = new DspCommand(DSP_COMMAND_GET_PASSWORD);
                           dspService.GetDspData(get_command);
                           dspService.WaitForData();
//try{Thread.sleep(2000);
try{
    Thread.sleep(20);
}
catch (Exception ex){
    Log.d("BleStatusChangeReceiver", ex.getMessage());
}
Log.i("DSP_CMD_GET_PASSWORD", "End");
// Do this after password has been received in FragmentBluetoothConnection
//                       mBluetoothConnectionFragment.ValidatePassword();
                       } else if (connectionType == CONNECTION_TYPE.CONNECT_NORMAL) {
                           Log.i("NO PASSWORD", "--");
                           UpdateConnectionStatus("Connected to " + connected_to_name, true);
                           connected = true;
                           switchScreen(mConfigurationFragment);            // go to the configuration screen
                           mConfigurationFragment.ConnectionComplete();     // and finish the connection process
                       }
                       break;

                   case DspService.ACTION_DSP_DATA_READ_COMPLETE:
                       Log.i("BroadcastReceiver", "ACTION_DSP_DATA_READ_COMPLETE");
                       ProgressBarDismiss();
                       EnableControls(true);
                       break;

                   case DspService.ACTION_GATT_DISCONNECTED: {
                        Log.i("BroadcastReceiver", "DspService Disconnected");
                        if (!scanning_for_devices)
                            UpdateConnectionStatus("Not Connected", false);

                        ProgressBarDismiss();
                        connected = false;
                        if (!mShuttingDown)
                            switchScreen(mBluetoothConnectionFragment);

                        if (dspService != null)
                            dspService.close();

                       if (getWindow().getDecorView().isShown())     // If we are "active" - not shutting down
                           CommErrorReceived("AX-DSP DISCONNECTED");
                        }
                        break;

                   case DspService.ACTION_ACK_RECEIVED:
                       Log.i("BroadcastReceiver", "ACK Received");
                       EnableControls(true);
 // TODO ANDY DO WE NEED THIS???                      ProgressBarDismiss();
                       break;

                   case DspService.ACTION_NAK_RECEIVED: {
                       Log.i("BroadcastReceiver", "NAK Received");
                       ShowSnackbar(mainContentView, "NAK received from AX-DSP", Snackbar.LENGTH_SHORT);
                       EnableControls(true);
                   }
                   break;

                   case DspService.ACTION_NAK_DSP_NOT_ON: {
                       Log.w("BroadcastReceiver", "NAK DSP NOT ON Received");
/*                   ShowSnackbar(mainContentView, "DSP is not ON", Snackbar.LENGTH_LONG);
                   EnableControls(true);
*/
                       switchScreen(mBluetoothConnectionFragment);
                       dspService.disconnect();
                       SetDirtyFlag(false);
                       SetNotAppliedFlag(false);
                       mConfigurationFragment.onPostExecute();
                       String message = "DSP is not initialized.... reconnect to AX-DSP! ";
                       DialogAlert comm_error_alert = DialogAlert.newInstance("DSP is not on", message);
                       FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                       comm_error_alert.show(fragmentManager, "alert");
                   }
                   break;

                   case DspService.ACTION_NAK_INVALID_VEHICLE:
                       Log.i("BroadcastReceiver", "Invalid Vehice Type");
                       break;

                   case DspService.ACTION_ACK_TIME_OUT: {
                       String data = intent.getExtras().getString("EXTRA_DATA");
                       if (data == null)
                           data = "?";
                       Log.i("BroadcastReceiver", "Timed Out waiting for ACK - " + data);
                       ShowSnackbar(mainContentView, "Time Out from AX-DSP", Snackbar.LENGTH_LONG);
                       CommErrorReceived("ACK TIME OUT");
                   }
                   break;

                   case DspService.ACTION_DATA_TIME_OUT: {
                       Log.i("BroadcastReceiver", "Timed Out waiting for Data");
                       ShowSnackbar(mainContentView, "Data Time Out from AX-DSP", Snackbar.LENGTH_SHORT);
                       CommErrorReceived("ACTION_DATA_TIME_OUT");
                   }
                   break;

                   case DspService.ACTION_CHARACTERISTIC_ERROR: {
                       Log.i("BroadcastReceiver", "Characteristic error");
                       try {
                           String error_type;
                           error_type = intent.getExtras().getString("EXTRA_DATA");
                           if (error_type != null) {
                               String message = "Type: " + error_type;
                               DialogAlert comm_error_alert = DialogAlert.newInstance("Data Error", message);
                               FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                               comm_error_alert.show(fragmentManager, "alert");
                           }
                       }
                       catch(Exception ex) {
                            Log.e("DspService", "ACTION_CHARACTERISTIC_ERROR");
                       }
                       EnableControls(true);
                   }
                   break;

                   case DspService.ACTION_COMM_ERROR: {
                       Log.i("BroadcastReceiver", "Comm Error");
                       byte[] data_bytes = intent.getExtras().getByteArray("EXTRA_DATA");
                       String message = "Type: " + (data_bytes != null ? data_bytes[0] : 0);
                       DialogAlert comm_error_alert = DialogAlert.newInstance("Communications Error", message);
                       FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                       comm_error_alert.show(fragmentManager, "alert");
                       EnableControls(false);
                   }
                   break;

                   case DspService.ACTION_DEVICE_NOT_AX_DSP: {
                       Log.i("BroadcastReceiver", "Device does not support AX-DSP");
                       switchScreen(mBluetoothConnectionFragment);
                       DialogAlert comm_error_alert = DialogAlert.newInstance("Communications Error", "Invalid Service");
                       FragmentTransaction fragmentManager = getSupportFragmentManager().beginTransaction();
                       comm_error_alert.show(fragmentManager, "alert");
                       connected = false;
                       switchScreen(mBluetoothConnectionFragment);
                       EnableControls(false);
                   }
                   break;

                   case DspService.ACTION_DATA_AVAILABLE:
// Log.i("Main BroadcastReceiver", "ACTION_DATA_AVAILABLE");
                       break;

                   case DspService.ACTION_DSP_DATA_AVAILABLE:
                       byte[] data = intent.getExtras().getByteArray(DspService.BLE_BYTES);
                       if (data != null) {
                           int data_type = (int) data[0];                               // First byte is the data type

                           if (data_type == DSP_DATA.DATA_PASSWORD.ordinal()) {          // Device Password
                               String password = "";                                   // Make a string out of the bytes passed
                               for (int index = 1; index < data.length; index++) {
                                   if (data[index] == 0)
                                       break;
                                   else
                                       password += (char) data[index];
                               }
                               mBluetoothConnectionFragment.SetPassword(password);
                           }

                           if (data_type == DSP_DATA.DATA_DIRTY_FLAG.ordinal()) {
                               SetDirtyFlag((data[1] == 1));
                           }

                           else if (data_type == DSP_DATA.DATA_DSP_ON_STATUS.ordinal()) {
                               byte status = data[1];
                               mConfigurationFragment.UpdateDspIsOnState(status);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_VEHICLE_TYPE.ordinal()) {
                               int vehicle_id = ((int) (data[2] << 8) & 0xFF) | data[1] & 0xFF;    // 2nd & 3rd are vehicle ID
                               int model_id = ((int) (data[4] << 8)) | data[3] & 0xFF;              // 4th & 5th are model ID
                               mConfigurationFragment.SetVehicleID(vehicle_id, model_id);
                               Log.i("DSP_DATA_AVAILABLE", "vehicle_id = " + vehicle_id + " model_id model_id" + model_id);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_AMP_TURN_ON.ordinal()) {
                               int amp_turn_on_delay = 0;
                               byte amp_turn_on_state = data[1];
                               if (data.length > 2) {
                                   amp_turn_on_delay = (((int) data[3]) & 0xff) * 256;      // Funky code to convert signed bytes to unsigned int
                                   amp_turn_on_delay += ((int) data[2]) & 0xff;
                                   amp_turn_on_delay /= 10;
                                   if (amp_turn_on_delay < 0 || amp_turn_on_delay > 10)
                                       amp_turn_on_delay = 0;
                               }
                               mInputsFragment.SetAmpTurnOnState(amp_turn_on_state); //, amp_turn_on_delay);

                           }

                           else if ((data_type == DSP_DATA.DSP_DATA_INPUT_CONFIG.ordinal())) {
                                int channel = data[1];
                               int location_id = data[2];
                               int groupIndex = data[3];
                               boolean muted = false;
                               boolean inverted = false;

                               if (data.length > 4) {
                                   muted = (data[4] == 1) ? true : false;
                                   inverted = (data[5] == 1) ? true : false;
                               }

                               mOutputs_X_Fragment.SetData(channel, location_id, groupIndex, muted, inverted);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_CLIPPING_LEVEL.ordinal()) {
                               int clipping_level = data[1];
                               int slider_level = data[2];
                               if (slider_level < 1 || slider_level > 10)
                                   slider_level = 1;
                               mLevelsFragment.SetClippingLevel(clipping_level, slider_level);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_CHIME_VOLUME.ordinal()) {
                               int chime_volume = (int) data[1];
                               mLevelsFragment.SetChimeVolume(chime_volume);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_CROSSOVER_DATA.ordinal()) {
//Log.i("XOVER DATA RECEIVED",  Integer.toString(data.length));
                               int low_frequency, high_frequency;
                               int channel = data[1];
                               int type_index = data[2];
                               if (type_index < 0 || type_index > 2)
                                   type_index = 0;
                               CROSSOVER_TYPES filter_type = CROSSOVER_TYPES.values()[type_index];
                               low_frequency = (((int) data[3]) & 0xff) * 256;      // Funky code to convert signed bytes to unsigned int
                               low_frequency += ((int) data[4]) & 0xff;
                               high_frequency = (((int) data[5]) & 0xff) * 256;
                               high_frequency += ((int) data[6]) & 0xff;
                               int number_filters = 2;
                               if (data.length > 7)
                                   number_filters = data[7];
                               mCrossover_X_Fragment.SetCrossoverFilterData(channel, filter_type, low_frequency, high_frequency, number_filters);
                           }

                           else if (data_type == DSP_DATA.DSP_DATA_EQ_GAIN.ordinal()) {
                                if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_440) {
                                       int[] gains = new int[5];
                                       for (int channel = 0; channel < 5; channel++) {
                                           gains[channel] = (int) data[channel + 1]; // << 24;
//Log.i("EQ Gain " + Integer.toString(channel), Integer.toString(gains[channel]));
                                       }
                                       mEqualizer_LC_Fragment.SetGains(gains);
                                       mEqualizer_LC_Fragment.UpdateControls();
                                   }

                               else {
                                   int[] gains = new int[10];
                                   for (int channel = 0; channel < 10; channel++) {
                                       gains[channel] = (int) data[channel + 1]; // << 24;
Log.i("EQ Gain " + Integer.toString(channel), Integer.toString(gains[channel]));
                                   }
                                   mEqualizer_X_Fragment.SetGains(gains);
                                   mEqualizer_X_Fragment.UpdateControls();
                               }
                           }

                            else if (data_type == DSP_DATA.DSP_DATA_EQ_BOOST.ordinal()) {
                               byte channel = data[1];
                               byte band = data[2];
                               byte number_bands = data[3];
                               int number_channels = GetNumberSupportedChannels();

                                if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_420) {
                                   for (int index = 0; index < number_bands; index++) {
 Log.i("EQ Boost " + Byte.toString(band), Byte.toString(data[index + 4]));
                                       mEqualizer_X_Fragment.SetEqBoostLevel(channel, band++, data[index + 4]);
                                   }

                                   if (channel == number_channels-1 && band == 31)              // Last Filter Data Received...
                                       mEqualizer_X_Fragment.UpdateControls();  // update the EQ controls
                               }
                               else  {
                                   for (int index = 0; index < number_bands; index++) {
 Log.i("EQ Boost " + Byte.toString(band), Byte.toString(data[index + 4]));
                                       mEqualizer_LC_Fragment.SetEqBoostLevel(channel, band++, data[index + 4]);
                                   }
                                   if (channel == number_channels-1 && band == number_bands)              // Last Filter Data Received...
                                       mEqualizer_LC_Fragment.UpdateControls();  // update the EQ controls
                               }
// TODO FIX THIS
                           }

                            else if (data_type == DSP_DATA.DSP_DATA_DELAY.ordinal()) {
                               int number_channels = GetNumberSupportedChannels();
                               int delayValues[] = new int[number_channels];
                               for (int channel = 0; channel < number_channels; channel++) {
                                   int delay = ((int) data[channel + 1]) & 0xff;                  // Data starts at byte 1
                                   if (delay > 99)
                                       delay = 0;
                                   delayValues[channel] = delay;
//Log.i("Speaker Delay " + Integer.toString(channel), Integer.toString(delay));
                               }
                               mDelayAdjust_X_Fragment.SetDelayData(delayValues);
                            }
                       }
//                     Log.i("Main BroadcastReceiver", "ACTION_DSP_DATA_AVAILABLE");
                       break;
               }
           }
       }
   };


    private void EnableNavButtons() {

   //    nav_button_instruct.setEnabled(state);
   //    nav_button_scan.setEnabled(state);
   //    nav_button_config.setEnabled(state);
       nav_button_inputs.setEnabled(connected);
       nav_button_crossover.setEnabled(connected);
       nav_button_levels.setEnabled(connected);
       nav_button_eq_adjust.setEnabled(connected);
       nav_button_delay_adjust.setEnabled(connected);

    }

    private void EnableControls(Boolean state){
        EnableNavButtons();
        if (!bluetooth_enabled)
            state = false;
        mConfigurationFragment.EnableControls(state);
//        mCrossoverFragment.EnableControls(state);
        mCrossover_X_Fragment.EnableControls(state);
        //mEqualizerFragment.EnableControls(state);
        mEqualizer_X_Fragment.EnableControls(state);
        mEqualizer_LC_Fragment.EnableControls(state);
        mDelayAdjustFragment.EnableControls(state);
        mDelayAdjust_X_Fragment.EnableControls(state);
        mLevelsFragment.EnableControls(state);
        mInputsFragment.EnableControls(state);
        mOutputs_X_Fragment.EnableControls(state);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        IntentFilter intentFilter = new IntentFilter();

        intentFilter.addAction(DspService.ACTION_RSSI_UPDATE);
        intentFilter.addAction(DspService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(DspService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(DspService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(DspService.ACTION_DATA_AVAILABLE);
        intentFilter.addAction(DspService.ACTION_DSP_DATA_AVAILABLE);
        intentFilter.addAction(DspService.ACTION_ACK_RECEIVED);
        intentFilter.addAction(DspService.ACTION_NAK_RECEIVED);
        intentFilter.addAction(DspService.ACTION_NAK_INVALID_VEHICLE);
        intentFilter.addAction(DspService.ACTION_NAK_DSP_NOT_ON);
        intentFilter.addAction(DspService.ACTION_ACK_TIME_OUT);
        intentFilter.addAction(DspService.ACTION_DATA_TIME_OUT);
        intentFilter.addAction(DspService.ACTION_DSP_DATA_READ_COMPLETE);
        intentFilter.addAction(DspService.ACTION_COMM_ERROR);
        intentFilter.addAction(DspService.ACTION_DEVICE_NOT_AX_DSP);
        intentFilter.addAction(DspService.ACTION_CHARACTERISTIC_ERROR);
        intentFilter.addAction(DspService.ACTION_CONNECTION_COMPLETE);
        return intentFilter;
    }

    private void BuildScreenNavigation() {
/*        CScreenNavigation screen;
        CScreenNavigation previous = null;
        CScreenNavigation next;
        for (int i = 0; i < screen_fragments.size(); i++) {
            screen = screen_fragments.get(i);
            if (i < screen_fragments.size() - 1)
                next = screen_fragments.get(i + 1);
            else
                next = screen_fragments.get(0);

            screen.SetNavigation(previous, next);
            previous = screen;
        }
*/
    }

    public void DisplayAlert(String title, String message) {
        DialogAlert comm_error_alert = DialogAlert.newInstance(title, message);
        FragmentTransaction fragmentManager =  getSupportFragmentManager().beginTransaction();
        comm_error_alert.show(fragmentManager, "alert");
    }

    // Add a single fragment to the application
    private void AddFragment(FragmentTransaction fragmentManager, CScreenNavigation screen, Integer navigation_button, Boolean add_to_activity) {
        screen.SetNavigationButton(navigation_button);
        screen_fragments.add(screen);
        if (add_to_activity)
            fragmentManager.add(R.id.main_content, screen, "Fragment");
        fragmentManager.hide(screen);
    }

    // Add all of our screen fragments and set up our screen navigation
    private void AddFragments(@SuppressWarnings("SameParameterValue") Boolean add_to_activity) {
//        add_to_activity = true;
        FragmentTransaction fragmentManager =  getSupportFragmentManager().beginTransaction();
        AddFragment(fragmentManager, mSetupInstructionFragment, R.id.navButtonInstruct, add_to_activity);
        AddFragment(fragmentManager, mBluetoothConnectionFragment, R.id.navButtonConnection, add_to_activity);
        AddFragment(fragmentManager, mConfigurationFragment, R.id.navButtonConfig, add_to_activity);
        // AddFragment(fragmentManager, mOeEqualizationFragment,, add_to_activity);
        AddFragment(fragmentManager, mDelayAdjustFragment, R.id.navButtonDelayAdjust, add_to_activity);
        AddFragment(fragmentManager, mDelayAdjust_X_Fragment, R.id.navButtonDelayAdjust, add_to_activity);
        AddFragment(fragmentManager, mLevelsFragment, R.id.navButtonLevels, add_to_activity);

        AddFragment(fragmentManager, mCrossover_X_Fragment, R.id.navButtonCrossover, add_to_activity);
        AddFragment(fragmentManager, mOutputs_X_Fragment, R.id.navButtonInputs, add_to_activity);

// TODO ANDY CHECK
 //       if (GetAxDspType() == AX_DSP_TYPE.AX_DSP_440) {
            AddFragment(fragmentManager, mEqualizer_LC_Fragment, R.id.navButtonEqAdjust, add_to_activity);
//        }
//        else{
             AddFragment(fragmentManager, mEqualizer_X_Fragment, R.id.navButtonEqAdjust, add_to_activity);
//        }

        fragmentManager.show(mSetupInstructionFragment);
        fragmentManager.commit();

        Button selected_button = findViewById(R.id.navButtonInstruct);     // Highlight the first button (Instructions)
        selected_button.setBackgroundResource (R.drawable.nav_button_selected);
        BuildScreenNavigation();
    }



    @Override
    protected void onStop() {
         Log.i("--->","onStop");

        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i("--->","onStart");
        vehicleTypeFile = new VehicleTypeFile(this, true);
// TODO ANDY WHICH SHOULD WE DO
//        vehicleTypeFile = new VehicleTypeFile(this, false);
    }

    @Override
    protected void onDestroy() {
        mShuttingDown = true;
        if (dspService != null)
            dspService.disconnect();
        unbindService(mConnection);
        unbindService(mDspConnection);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
// ANDY COMMENT OUT MENU
//        getMenuInflater().inflate(R.menu.main, menu);
//        mPreviousItem = menu.findItem(R.id.action_prev);
//        mNextItem = menu.findItem(R.id.action_next);

        //mBluetoothConnectionFragmentFragment = FragmentBluetoothConnection.newInstance(null);
        return true;
    }



    private CScreenNavigation getVisibleFragment() {
        CScreenNavigation screen = null;
        for (int i = 0; i < screen_fragments.size(); i++) {
            screen = screen_fragments.get(i);
            if (screen.isVisible())
                break;
        }
        return (screen);
    }


    // Switch our fragment to the new screen
    private void switchScreen(CScreenNavigation new_fragment) {
        CScreenNavigation oldFragment = getVisibleFragment();        // Find the fragment that is being displayed

        if (!new_fragment.equals(oldFragment)) {                    // If we're changing fragments...
                                                                    // Set "old" navigation button to Not Selected
            Button nav_button = (Button)findViewById(oldFragment.GetNavigationButton());
            if (nav_button != null)
                nav_button.setBackgroundResource (R.drawable.nav_button_not_selected);
                                                                    // Set "new" navigation button to Selected
            nav_button = (Button)findViewById(new_fragment.GetNavigationButton());
            if (nav_button != null)
                nav_button.setBackgroundResource (R.drawable.nav_button_selected);

                getSupportFragmentManager()                         // replace the current fragment with the new on
                .beginTransaction()
                .hide(oldFragment)
                .show(new_fragment)
                .commit();

            if (new_fragment == mBluetoothConnectionFragment) {            // Make sure Location services are running - required to do BLE Scanning
                LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    EnableLocation();
                }
            }
        }

    }

/*    public boolean WaitingForAck(){
        return(dspService.WaitingForAck());
    }
*/
    public DspService GetDspService() { return(dspService);}



    public void SendDspEQ(DspEqFilter filter)
    {
        dspService.SendDspEqFilter(filter);
    }

    public void SendDspEQ(DspEqFilter filter, int groupIndex)
    {
        int groupMask = 0;
        DspGroup dspGroup = mOutputs_X_Fragment.GetGroup(groupIndex);
        dspService.SendDspEqFilter(filter, dspGroup.GetSlaveMask());
    }

    public void SendDspGain(DspGain gain)
    {
        dspService.SendDspGain(gain);
    }


    private void EnableLocation() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                        @Override
                        public void onConnected(Bundle bundle) {
                        }

                        @Override
                        public void onConnectionSuspended(int i) {
                            googleApiClient.connect();
                        }
                    })
                    .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                        @Override
                        public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                            Log.d("Location error","Location error " + connectionResult.getErrorCode());
                        }
                    }).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

            builder.setAlwaysShow(true);


            SettingsClient client = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

            task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                      ShowSnackbar (mainContentView, "addOnSuccessListener", "addOnSuccessListener".length());
//                    Toast.makeText(MapActivity.this, "addOnSuccessListener", Toast.LENGTH_SHORT).show();
                    // All location settings are satisfied. The client can initialize
                    // location requests here.
                    // ...
                }
            });

            task.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    ShowSnackbar (mainContentView, "addOnFailureListener", "addOnFailureListener".length());
                    //Toast.makeText(MapActivity.this, "addOnFailureListener", Toast.LENGTH_SHORT).show();
                    if (e instanceof ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(), and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);
                        } catch (Exception sendEx) {
                            // Ignore the error.
                        }
                    }
                }
            });

/*
            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult result) {
                    Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(MainActivity.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                    }
                }
            });
*/
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_LOCATION:
                switch (resultCode) {
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
// ANDY ADD MESSAGE HERE                        finish();
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }

    }


    public void Disconnect(){
        dspService.disconnect();
        UpdateConnectionStatus("Not Connected", false);
        connected = false;
        EnableControls(false);
    }

    public  void stopScan()
    {
        scanning_for_devices = false;
        Message scan_msg = Message.obtain(null, BleScannerService.MSG_STOP_SCAN);
        if (scan_msg != null) {
            try {
                mScannerService.send(scan_msg);

            } catch (RemoteException e) {
                Log.w(TAG, "Lost connection to service", e);
                unbindService(mConnection);
            }
        }
    }

    public  void startScan() {
        scanning_for_devices = true;
// TODO ADD THIS BACK?????        dspService.disconnect();

        mBluetoothConnectionFragment.ClearDeviceList();
		Message scan_msg = Message.obtain(null, BleScannerService.MSG_START_SCAN);
		if (scan_msg != null) {
			try {
				mScannerService.send(scan_msg);
                UpdateConnectionStatus("Scanning for AX-DSP's...", false);

			} catch (RemoteException e) {
				Log.w(TAG, "Lost connection to service", e);
				unbindService(mConnection);
			}
		}
	}

	public void CancelScanCallback()
	{
		scanHandler.removeCallbacks(null);
/*
        Message connected_msg = Message.obtain(null, BleScannerService.MSG_DEVICE_CONNECT);
        if (connected_msg != null) {
            try {
                mScannerService.send(connected_msg);
            } catch (RemoteException e) {
                Log.w(TAG, "Lost connection to service", e);
                unbindService(mConnection);
            }
        }
*/
    }


	@Override
	public void onConnect(String macAddress) {
        dspService.connect(macAddress);
	}

    @Override
    public void onPasswordGet(String macAddress) {
        dspService.connect(macAddress);
    }


    private static class IncomingHandler extends Handler {
		private WeakReference<MainActivity> mActivity;

		public IncomingHandler(MainActivity activity) {
			mActivity = new WeakReference<>(activity);
		}

		@Override
		public void handleMessage(Message msg) {
			MainActivity activity = mActivity.get();
			if (activity != null) {
				switch (msg.what) {
					case BleScannerService.MSG_STATE_CHANGED:
						activity.stateChanged(BleScannerService.State.values()[msg.arg1]);
						break;

					case BleScannerService.MSG_DEVICE_FOUND:
						Bundle data = msg.getData();
						if (data != null && data.containsKey(MainActivity.KEY_DEVICE_NAME))
                        {
                            String name = data.getString(MainActivity.KEY_DEVICE_NAME);
                            String address = data.getString(KEY_DEVICE_ADDRESS);
                            String rssi = data.getString(KEY_DEVICE_RSSI);
                            activity.mBluetoothConnectionFragment.AddDevice(name, address, rssi);
						}
						break;
				}
			}
			super.handleMessage(msg);
		}
	}

	private void stateChanged(BleScannerService.State newState) {
		boolean disconnected = mState == BleScannerService.State.CONNECTED;
		mState = newState;
		switch (mState) {
			case SCANNING:
                scanning_for_devices = true;
				break;

            case DONE_SCANNING:
                if (scanning_for_devices) {
                    UpdateConnectionStatus("Done Scanning", false);
                    mBluetoothConnectionFragment.UpdateScanState(true);
                }
                break;

			case BLUETOOTH_OFF:
				Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                int ENABLE_BT = 1;
//                startActivityForResult(enableBtIntent, ENABLE_BT);
                startActivity(enableBtIntent);
				break;

			case IDLE:
				if (disconnected) {
                    scanning_for_devices = false;
					switchScreen(mBluetoothConnectionFragment);
				}
					break;

			case CONNECTED:                                             // If we just connected..
                scanning_for_devices = false;
				scanHandler.removeCallbacks(null);
				break;

            case DISCONNECTING:
                scanning_for_devices = false;
                switchScreen(mBluetoothConnectionFragment);                // Go to Connection screen on Device disconnect
                break;

		}
	}

    public boolean BluetoothIsEnabled(){ return(bluetooth_enabled);}
    public boolean FileSaveIsEnabled(){ return(file_save_enabled);}


    public String GetVehicleManufacturerString(){return(mConfigurationFragment.GetVehicleManufacturerString());}
    public String GetVehicleModelString(){return(mConfigurationFragment.GetVehicleModelString());}
    public boolean GetVehicleHasOE_Amp(){return(mConfigurationFragment.GetVehicleHasOE_Amp());}
    public int GetVehicleID() {return(mConfigurationFragment.GetVehicleID());}
    public int GetModelID() {return(mConfigurationFragment.GetModelID());}

    public void SetNotAppliedFlag(boolean state)
    {
        not_applied = state;
        UpdateAlertMessage();
    }

    public boolean GetNotAppliedFlag(){return(not_applied);}


    public void SetDirtyFlag(boolean state)
    {
//Log.i("SetDirtyFlag", Boolean.toString(state));
        dirty_flag = state;
        UpdateAlertMessage();
    }

    public boolean GetDirtyFlag(){return(dirty_flag);}

    public void SetIsReading(boolean state){ reading_data = state;}
    public boolean GetIsReading(){ return(reading_data);}
    private void UpdateAlertMessage()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (not_applied) {
                    textAlertMessage.setText("WARNING: Data has been Recalled but not Applied");
                    textAlertMessage.setVisibility(View.VISIBLE);
                }

                else if (dirty_flag) {
                    textAlertMessage.setText("WARNING: Settings have changed - Make sure to 'Lock Down' before Exiting App");
                    textAlertMessage.setVisibility(View.VISIBLE);
                }

                else
                    textAlertMessage.setVisibility(View.INVISIBLE);
            }
        });
    }

    public InputStream GetVehicleTypeStream() {
         InputStream input_stream = vehicleTypeFile.GetInputStream();
        return(input_stream);
    }

    public Activity getActivity() {
        return(this);
    }

    public void ShowSnackbar (View showView, String message, int length){
        Snackbar snackbar = Snackbar.make(showView, message, length);
        snackbar.show();
    }

    public boolean AnyChannelAssigned() {
        return(mOutputs_X_Fragment.AnyChannelAssigned());
    }

    public void BuildEqData() {
        int band = 11;
        DspEqFilter filter = new DspEqFilter(1, 1, 25);
        filter.SetFilterDataFormat(32);
        int freq = 16000;
        filter.SetFrequency(freq);
        Log.i(":", "BYTE boost_factors_" + freq + "Hz[][30]=");
        Log.i(":", "{");

        int boost;
        for (boost = -20; boost <= 20; boost++){
            filter.SetBoost((float)boost);
//if (boost == 10)
//    Log.i("wait", "for me");
            byte[] data = filter.GetFilterData();
            byte[] filter_data = new byte[30];
            int data_index = 0;
            int filter_index = 0;
//if (boost == 10)
//    Log.i("wait", "for me");
                while(filter_index < 30 ) {
                filter_data[filter_index++] = data[data_index+3];
                filter_data[filter_index++] = data[data_index+2];
                filter_data[filter_index++] = 0;
                filter_data[filter_index++] = data[data_index+1];
                filter_data[filter_index++] = data[data_index];
                filter_data[filter_index++] = 0;
                data_index+=4;
            }

            String filter_string = "";
            filter_index = 0;
            while(filter_index < filter_data.length) {
                filter_string += filter.byteToHexString(filter_data[filter_index++]) + ",";
                filter_string += filter.byteToHexString(filter_data[filter_index++]) + ",";
                filter_string += filter.byteToHexString(filter_data[filter_index++]) + ", ";
            }
             Log.i(":" , "  {" + filter_string + "},");
        }
        Log.i(":", "}");
    }

}