
package com.ckt_works.AX_DSP;

    import android.content.Context;
    import android.util.AttributeSet;
    import android.view.View;
    import android.widget.AdapterView;

public class NoDefaultSpinner extends androidx.appcompat.widget.AppCompatSpinner {
    private OnItemSelectedListener listener;
    private AdapterView<?> lastParent;
    private View lastView;
    private long lastId;
    private boolean listenEnabled = true;

    public NoDefaultSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        initInternalListener();
    }

    public NoDefaultSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initInternalListener();
        listenEnabled = true;
    }

    public void SetListenState(boolean enabled) {
        listenEnabled = enabled;
    }

    public boolean isListenEnabled() {
        return listenEnabled;
    }

    private void initInternalListener() {
        super.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (listenEnabled) {
                    lastParent = parent;
                    lastView = view;
                    lastId = id;
                    if (listener != null) {
                        listener.onItemSelected(parent, view, position, id);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //lastParent = parent; // do we need it?
                if (listener != null) {
                    listener.onNothingSelected(parent);
                }
            }
        });
    }

    @Override
    public void setSelection(int position) {
        if (position == getSelectedItemPosition() && listener != null) {
            listener.onItemSelected(lastParent, lastView, position, lastId);
        } else {
            super.setSelection(position);
        }
    }

    @Override
    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }
}