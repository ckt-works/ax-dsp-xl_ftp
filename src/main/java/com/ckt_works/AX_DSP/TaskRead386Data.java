package com.ckt_works.AX_DSP;
// Created by Abeck on 2/23/2017. //

import android.content.Intent;
import android.os.AsyncTask;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;


// Read all of the device's Generic Access Characteristics
public class TaskRead386Data extends AsyncTask<Void, Integer, Void> //<Void, Integer, String>
{
    final private int SERVICE_DELAY_TIME = 1000;
    public  DspService dsp_service = null;
    public android.content.Context my_context = null;

    private AUDIO_CHANNEL read_eq_channels = AUDIO_CHANNEL.ALL;
    private boolean read_eq_data_only = false;

    // Get all of the data from the 386 in a background task - so we don't stall the UI thread
    protected Void doInBackground(Void...arg0) {
        Thread.currentThread().setName("TaskRead386Data");
        if (dsp_service != null)
        {
            try {

            DspCommand get_command = new DspCommand();

            Thread.sleep(SERVICE_DELAY_TIME);                             // <==== For some reason we need to wait a bit before reading characteristics
            Log.i("DoInBackGround","Get Device info");

            if (!read_eq_data_only) {
                // Get all of the DSP data
                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_MANUF_NAME_UUID);
                dsp_service.WaitForData();

                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.BLUE_NRG_HARDWARE_VERSION_UUID);
                dsp_service.WaitForData();

                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_NAME_UUID);
                dsp_service.WaitForData();

                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.BLUE_NRG_FIRMWARE_VERSION_UUID);
                dsp_service.WaitForData();

                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_SOFTWARE_VERSION_UUID);
                dsp_service.WaitForData();

                dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_SERIAL_NUMBER_UUID);
                dsp_service.WaitForData();

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_DIRTY_FLAG);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET DIRTY FLAG");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_VEHICLE_TYPE);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET VEHICLE TYPE");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CHIME_VOLUME);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET CHIME VOLUME");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CROSSOVER_DATA);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET CROSSOVER");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CLIPPING_LEVEL);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET CLIPPING LEVEL");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_AMP_TURN_ON);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET AMP TURN ON");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_INPUT_CONFIG);
                byte channel = (byte) AUDIO_CHANNEL.FRONT.ordinal();
                get_command.SetData(channel);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET FRONT INPUT LEVEL");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_INPUT_CONFIG);
                channel = (byte) AUDIO_CHANNEL.REAR.ordinal();
                get_command.SetData(channel);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET REAR INPUT LEVEL");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_INPUT_CONFIG);
                channel = (byte) AUDIO_CHANNEL.SUBWOOFER.ordinal();
                get_command.SetData(channel);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET SUB WOOFER INPUT LEVEL");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_SPEAKER_DELAY);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET SPEAKER DELAY");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_GAIN);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET EQ GAIN");
            }

            // Get Front Channel EQ settings
            if (read_eq_channels == AUDIO_CHANNEL.FRONT || read_eq_channels == AUDIO_CHANNEL.ALL) {
                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                byte get_data[] = new byte[3];
                get_data[0] = 0;                // Front Channel
                get_data[1] = 0;                // start with band 0
                get_data[2] = 16;               // get 16 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET FRONT EQ BOOST 0-15");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                get_data[0] = 0;                // Front Channel
                get_data[1] = 16;               // start with band 16
                get_data[2] = 15;               // get 15 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET FRONT EQ BOOST 16-31");
            }

            // Get Rear Channel EQ settings
            if (read_eq_channels == AUDIO_CHANNEL.REAR || read_eq_channels == AUDIO_CHANNEL.ALL) {
                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                byte get_data[] = new byte[3];
                get_data[0] = 1;                // Rear Channel
                get_data[1] = 0;                // start with band 0
                get_data[2] = 16;               // get 16 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET REAR EQ BOOST 0-15");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                get_data[0] = 1;                // Rear Channel
                get_data[1] = 16;               // start with band 16
                get_data[2] = 15;               // get 15 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET REAR EQ BOOST 16-31");
            }

            // Get Sub Woofer Channel EQ settings
            if (read_eq_channels == AUDIO_CHANNEL.SUBWOOFER || read_eq_channels == AUDIO_CHANNEL.ALL) {
                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                byte get_data[] = new byte[3];
                get_data[0] = 2;                // Sub Woofer Channel
                get_data[1] = 0;                // start with band 0
                get_data[2] = 16;               // get 16 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET SUB EQ BOOST 0-15");

                get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                get_data[0] = 2;                // Sub Woofer Channel
                get_data[1] = 16;               // start with band 16
                get_data[2] = 15;               // get 15 bands
                get_command.SetData(get_data, get_data.length);
                dsp_service.GetDspData(get_command);
                dsp_service.WaitForData();
                Log.i("DoInBackGround", "GET SUB EQ BOOST 16-31");
            }

        }catch (InterruptedException ex)
            {
                Log.i("DoInBackGround","Error");
                ex.printStackTrace();
            }
        }
        onPostExecute("Done Getting Data");
        return(null);
    }


    // Called by the "onPostExecute" call in "doInBackground"
    void onPostExecute(String result) {
        Log.i("---> onPostExecute", result);
        final Intent intent = new Intent(com.ckt_works.AX_DSP.DspService.ACTION_DSP_DATA_READ_COMPLETE);
        LocalBroadcastManager.getInstance(my_context).sendBroadcast(intent);
   }
}