package com.ckt_works.AX_DSP;
// Created by Abeck on 2/23/2017. //

import android.content.Intent;
import android.os.AsyncTask;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;


// Read all of the device's Generic Access Characteristics
//public class TaskRead386DataX extends AsyncTask<Void, Integer, Void> //<Void, Integer, String>
public class TaskRead386DataX extends AsyncTask<String, Integer, String>
{
    MainActivity main_activity;

    //private IPostExecuteInterface post_activity = null;
    private final int SERVICE_DELAY_TIME = 1000;
    public DspService dsp_service = null;
    public android.content.Context my_context = null;

    private boolean read_eq_data_only = false;
    private int send_time = 0;
    private int number_channels = 10;
    private int numberEqBands = 31;

    TaskRead386DataX(MainActivity Main) {
        Log.i("TaskRead386DataX", "Constructor" );
        main_activity = Main;
        number_channels = main_activity.GetNumberSupportedChannels();
        numberEqBands = main_activity.GetNumberEqBands();
        main_activity.SetIsReading(true);
        Main.SetIsReading(true);
        Main.ProgressBarDisplay("Downloading...");
    }

    TaskRead386DataX(MainActivity Main, IPostExecuteInterface post) {
        this(Main);
    //    post_activity =  post;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
      //  main_activity.ProgressBarDisplay("Downloading...");
    }



    // Get all of the data from the 386 in a background task - so we don't stall the UI thread
//    protected Void doInBackground(Void...arg0) {
    @Override
    protected String doInBackground(String... params) {
        Thread.currentThread().setName("TaskRead386DataX");
        Log.i("TaskRead386DataX", "doInBackground" );
        if (dsp_service != null)
        {
            try {
                DspCommand get_command = new DspCommand();

                Thread.sleep(SERVICE_DELAY_TIME);                             // <==== For some reason we need to wait a bit before reading characteristics
                Log.i("DoInBackGround","Get Device info");

                if (!read_eq_data_only) {
                    // Get all of the DSP data
//Log.i("DoInBackGround", "DSP386_MANUF_NAME_UUID");
                    dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_MANUF_NAME_UUID);
                    send_time = dsp_service.WaitForData();

                    if (send_time >= 0) {
//Log.i("DoInBackGround", "BLUE_NRG_HARDWARE_VERSION_UUID");
                        dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.BLUE_NRG_HARDWARE_VERSION_UUID);
                        send_time = dsp_service.WaitForData();
                    }

//Log.i("DoInBackGround", "DSP386_NAME_UUID");
                    if (send_time >= 0) {
                        dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_NAME_UUID);
                        send_time = dsp_service.WaitForData();
                    }

//Log.i("DoInBackGround", "BLUE_NRG_FIRMWARE_VERSION_UUID");
                    if (send_time >= 0) {
                        dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.BLUE_NRG_FIRMWARE_VERSION_UUID);
                        send_time = dsp_service.WaitForData();
                    }

//Log.i("DoInBackGround", "DSP386_SOFTWARE_VERSION_UUID");
                    if (send_time >= 0) {
                        dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_SOFTWARE_VERSION_UUID);
                        send_time = dsp_service.WaitForData();
                    }

//Log.i("DoInBackGround", "DSP386_SERIAL_NUMBER_UUID");
                    if (send_time >= 0) {
                        dsp_service.GetGenericAccessCharacteristic(com.ckt_works.AX_DSP.DspService.DSP386_SERIAL_NUMBER_UUID);
                        send_time = dsp_service.WaitForData();
                    }

//Log.i("DoInBackGround", "GET DIRTY FLAG");
                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_DIRTY_FLAG);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                    }

                    if (send_time >= 0) {
                        Log.i("DoInBackGround", "GET VEHICLE TYPE");
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_VEHICLE_TYPE);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                    }

                    if (send_time >= 0) {
//Log.i("DoInBackGround", "GET CHIME VOLUME");
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CHIME_VOLUME);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                    }
                    for (int channel =0; channel < number_channels; channel++) {
                        if (send_time >= 0) {
// Log.i("DoInBackGround", "GET CROSSOVER " + Integer.toString(channel));
                            get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CROSSOVER_DATA);
                            byte[] get_data = new byte[1];
                            get_data[0] = (byte) channel;
                            get_command.SetData(get_data, get_data.length);
                            dsp_service.GetDspData(get_command);
                            send_time = dsp_service.WaitForData();
                        }
                    }

                    for (int channel =0; channel < number_channels; channel++) {
                        if (send_time >= 0) {
                            //Log.i("DoInBackGround", "GET INPUT CONFIG " + Integer.toString(channel));
                            get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_INPUT_CONFIG);
                            get_command.SetData(channel);
                            dsp_service.GetDspData(get_command);
                            send_time = dsp_service.WaitForData();
                        }
                    }

                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_CLIPPING_LEVEL);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                        //Log.i("DoInBackGround", "GET CLIPPING LEVEL");
                    }

                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_AMP_TURN_ON);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
//Log.i("DoInBackGround", "GET AMP TURN ON");
                    }

                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_SPEAKER_DELAY);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
//Log.i("DoInBackGround", "GET SPEAKER DELAY");
                    }

                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_GAIN);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                        Log.i("DoInBackGround", "GET EQ GAIN");
                    }
                }

                // Get  EQ settings
                for (int channel =0; channel < number_channels; channel++) {
                    byte[] get_data = new byte[3];
                    if (send_time >= 0) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                        get_data[0] = (byte) channel;                // Channel
                        get_data[1] = 0;                            // start with band 0
                        get_data[2] = (numberEqBands > 16) ? 16 : (byte)numberEqBands;           // Get first group of EQ bands
                        get_command.SetData(get_data, get_data.length);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                        Log.i("DoInBackGround", "GET EQ Channel " + Integer.toString(channel) + " Bands 0-15");
                    }

                    if (send_time >= 0 && numberEqBands > 16) {
                        get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_EQ_BOOST);
                        get_data[0] = (byte) channel;                    //  Channel
                        get_data[1] = 16;                               // start with band 16
                        get_data[2] = 15;                               // get 15 bands
                        get_command.SetData(get_data, get_data.length);
                        dsp_service.GetDspData(get_command);
                        send_time = dsp_service.WaitForData();
                        Log.i("DoInBackGround", "GET EQ Channel " + Integer.toString(channel) + " Bands 16-31");
                    }
                }
            }
            catch (InterruptedException ex){
                Log.i("DoInBackGround","Error");
                ex.printStackTrace();
            }
        }
        //onPostExecute("Done Getting Data");
        return(null);
    }


    // Called by the "onPostExecute" call in "doInBackground"
//    void onPostExecute(String result) {
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (send_time < 0)
            Log.i("TaskRead386DataX", "Timeout Error");
        else
            Log.i("TaskRead386DataX", "Success");
        main_activity.SetIsReading(false);
        main_activity.ProgressBarDismiss();
        final Intent intent = new Intent(com.ckt_works.AX_DSP.DspService.ACTION_DSP_DATA_READ_COMPLETE);
        LocalBroadcastManager.getInstance(my_context).sendBroadcast(intent);
       // if (post_activity != null)
       //     post_activity.onPostExecute();
    }
}