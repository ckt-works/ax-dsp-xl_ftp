package com.ckt_works.AX_DSP;
// Created by Abeck on 4/26/2017. //

import android.os.AsyncTask;
import android.util.Log;

public class TaskReadDiagnosticData extends AsyncTask<Void, Integer, Void> {

    private DspService dspService;
    private IPostExecuteInterface post_activity;

    TaskReadDiagnosticData(IPostExecuteInterface Post, DspService service) {
        post_activity = Post;
        dspService = service;
    }

    // Send all of our data to the 386 in a background task - so we don't stall the UI thread
    protected Void doInBackground(Void... arg0) {
        Thread.currentThread().setName("TaskReadDiagnosticData");
        Log.d("TaskReadDiagnosticData", "Sending Data");
        if (dspService != null) {
            DspCommand get_command = new DspCommand();
            get_command.SetCommand(DSP_COMMANDS.DSP_COMMAND_GET_DIAGNOSTICS);
            dspService.GetDspData(get_command);
            dspService.WaitForData();

            if (!this.isCancelled())
                onPostExecute();            // Don't do "nPostExecute" if task was cancelled
        }
        return(null);
    }

    // Called by the "onPostExecute" call in "doInBackground"
    void onPostExecute() {
        Log.i("TaskReadDiagnosticData", "onPostExecute");
        if (post_activity != null && !this.isCancelled()) {
            post_activity.onPostExecute();
        }
    }

}


