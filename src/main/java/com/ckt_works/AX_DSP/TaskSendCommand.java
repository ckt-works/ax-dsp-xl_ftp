package com.ckt_works.AX_DSP;
// Created by Abeck on 5/10/2017. //

import android.os.AsyncTask;
import android.util.Log;

public class TaskSendCommand extends AsyncTask<Void, Integer, Void> {

    DspService dsp_service;
    private IPostExecuteInterface post_activity = null;
    private IPostExecuteInterfaceArg post_activity_arg = null;
    String arg = null;
    private DspCommand command_to_send;
    private int max_ack_time = -1;

    TaskSendCommand(DspService service, IPostExecuteInterface Post, DspCommand command) {
        dsp_service = service;
        post_activity = Post;
        command_to_send = command;
    }

    TaskSendCommand(DspService service, IPostExecuteInterface Post, DspCommand command, int ack_time) {
        this(service, Post, command);
        max_ack_time = ack_time;
    }

    TaskSendCommand(DspService service, IPostExecuteInterfaceArg Post, DspCommand command, String arg) {
        dsp_service = service;
        post_activity_arg = Post;
        command_to_send = command;
    }

    TaskSendCommand(DspService service, IPostExecuteInterfaceArg Post, DspCommand command, String arg, int ack_time) {
        this(service, Post, command, arg);
        max_ack_time = ack_time;
    }


    // Send a command to the 386 in a background task - so we don't stall the UI thread
    protected Void doInBackground(Void... arg0) {
        Thread.currentThread().setName("SendCommandTask");
        Log.i("SendCommandTask", "Sending Command");
        if (max_ack_time > 0)
        dsp_service.SendDspCommand(command_to_send, max_ack_time);
        else
            dsp_service.SendDspCommand(command_to_send);
        onPostExecute();
        return (null);
    }

    // Called by the "onPostExecute" call in "doInBackground"
    void onPostExecute() {
        Log.i("SendCommandTask", "" + "Done Sending Command");
        if (post_activity != null)
            post_activity.onPostExecute();

        if (post_activity_arg != null)
            post_activity_arg.onPostExecuteArg(arg);

    }
}
