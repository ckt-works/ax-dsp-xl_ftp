package com.ckt_works.AX_DSP;
// Created by Abeck on 4/17/2017. //

import android.os.AsyncTask;
import android.util.Log;

import static android.os.SystemClock.uptimeMillis;

public class TaskSendData extends AsyncTask<Void, Integer, Void> {

    private MainActivity main_activity;
    private IPostExecuteInterface post_activity;
    private boolean send_eq_data_only = false;
    private AUDIO_CHANNEL send_eq_channels = AUDIO_CHANNEL.ALL;
    private int eq_channel;
    private boolean eq_channel_based = false;
    private int send_time = 0;
    int this_send_time = 0;
    private long start_time;
    private int number_channels = 10;
    private int numberEqBands = 31;

    TaskSendData(MainActivity Main)
    {
        main_activity = Main;
        number_channels = main_activity.GetNumberSupportedChannels();
        numberEqBands = main_activity.GetNumberEqBands();
    }

    public void SetMainActivity(MainActivity Main, IPostExecuteInterface Post)
    {
        main_activity = Main;
        post_activity = Post;
    }

    public void SetPostCallBeck(IPostExecuteInterface Post)
    {
        post_activity = Post;
    }

    public void SendEqOnly(boolean state)
    {
        send_eq_data_only = state;
    }

    public void SendEqChannel(AUDIO_CHANNEL channels)
    {
        send_eq_channels = channels;
        this.eq_channel_based = false;
    }

    public void SendEqChannel(int channel)
    {
        this.eq_channel = channel;
        this.eq_channel_based = true;
    }


    // Send all of our data to the DSP in a background task - so we don't stall the UI thread
    protected Void doInBackground(Void... arg0) {
        Thread.currentThread().setName("SendDataTask");
        // Log.d("SendDataTask", "Sending Data");
        send_time = 0;
        try {
//            AX_DSP_TYPE ax_dsp_type = main_activity.GetAxDspType();
            start_time = uptimeMillis();

            // Pause CAN message reception while sending this data
            this_send_time = main_activity.mConfigurationFragment.SetCANPause((byte)0x7F);
            if (this_send_time < 0)
                Log.i("TaskSendData", "Set CAN Pause = TRUE - TIMEOUT");

            if (!send_eq_data_only) {
                if (this_send_time >= 0) {
                    send_time += this_send_time;
                    this_send_time = main_activity.mConfigurationFragment.SendVehicleType();
                    if (this_send_time < 0)
                        Log.i("TaskSendData", "Send Vehicle Type - TIMEOUT");
                }

                if (this_send_time >= 0) {
                    send_time += this_send_time;
                    this_send_time = main_activity.mLevelsFragment.SendData();
                    if (this_send_time < 0)
                        Log.i("TaskSendData", "Send Levels - TIMEOUT");

                }

                if (this_send_time >= 0) {
                    send_time += this_send_time;
                    this_send_time = main_activity.mDelayAdjust_X_Fragment.SendData();
                    if (this_send_time < 0)
                        Log.i("TaskSendData", "Send Delays - TIMEOUT");
                }

 /*               if (this_send_time >= 0) {
                    send_time += this_send_time;
                    this_send_time = main_activity.mOutputs_X_Fragment.SendAmpData();
                    if (this_send_time < 0)
                        Log.i("TaskSendData", "Send Amp Data - TIMEOUT");
                }
*/
                for (int channel = 0; channel < number_channels; channel++) {
                    if (this_send_time >= 0) {
                        send_time += this_send_time;
                        this_send_time = main_activity.mCrossover_X_Fragment.SendData(channel);
                        if (this_send_time < 0)
                            Log.i("TaskSendData", "Send Crossover CH:" + channel + " - TIMEOUT");
                    } else
                        break;
                }


                // Pause CAN message reception while sending this data
                this_send_time = main_activity.mConfigurationFragment.SetCANPause((byte)0x7F);
                if (this_send_time < 0)
                    Log.i("TaskSendData", "Set CAN Pause = TRUE - TIMEOUT");

                    for (int channel = 0; channel < number_channels; channel++) {
                        if (this_send_time >= 0) {
                            send_time += this_send_time;
                            this_send_time = main_activity.mOutputs_X_Fragment.SendInputData(channel);
                            if (this_send_time < 0)
                                Log.i("TaskSendData", "Send Outputs CH:" + channel + " - TIMEOUT");
                        } else
                            break;
                    }
                }

                if (this_send_time >= 0) {
                    if (eq_channel_based) {                        // If only sending one channel
                        send_time += this_send_time;
                        this_send_time = main_activity.mEqualizer_X_Fragment.SendData(this.eq_channel);
                        if (this_send_time < 0)
                            Log.i("TaskSendData", "Send EQ CH:" + this.eq_channel + " - TIMEOUT");
                    }
                    else {                                       // otherwise send all channels
                        // Pause CAN message reception while sending this data
                        this_send_time = main_activity.mConfigurationFragment.SetCANPause((byte)0x7F);
                        if (this_send_time < 0)
                            Log.i("TaskSendData", "Set CAN Pause = TRUE - TIMEOUT");
                        //Log.i("TaskSendData", "Send EQ LOOP :" + loop);
                        AX_DSP_TYPE ax_dsp_type = main_activity.GetAxDspType();
                        for (int channel = 0; channel < number_channels; channel++) {
                            if (this_send_time >= 0) {
                                send_time += this_send_time;
                                Thread.sleep(100);
                                if (ax_dsp_type == AX_DSP_TYPE.AX_DSP_440)
                                    this_send_time = main_activity.mEqualizer_LC_Fragment.SendData(channel);
                                else
                                    this_send_time = main_activity.mEqualizer_X_Fragment.SendData(channel);
                                if (this_send_time < 0)
                                    Log.i("TaskSendData", "Send EQ CH:" + channel + " - TIMEOUT");
                            } else
                                break;
                        }
                    }
                }

            this_send_time = main_activity.mConfigurationFragment.SetCANPause((byte)0);
            if (this_send_time < 0)
                Log.i("TaskSendData", "Set CAN Pause = FALSE - TIMEOUT");
        }
        catch (Exception ex) {
            Log.e("TaskSendData", "Error: " + ex.getMessage());
        }
        onPostExecute();
        return (null);
    }
    
    // Called by the "onPostExecute" call in "doInBackground"
    void onPostExecute() {
        if (this_send_time < 0) {
            DspEqFilter test_filter = new DspEqFilter();
            int seq_id = test_filter.GetSequenceNumber();
            main_activity.CommErrorReceived("EQ ID: " + seq_id);
        }
         Log.i(">>>>>>> SendDataTask", "" + "Done Sending Data. Send Time = " + Integer.toString(send_time) + "ms; Total Time = " + Long.toString(uptimeMillis() - start_time) + "ms");
        if (post_activity != null)
            post_activity.onPostExecute();

    }
    
}
