package com.ckt_works.AX_DSP;

// Created by Andy Beck on 10/5/2017.

// This class manages the "Vehicle Type File" which maintains data on the supported vehicle types.
// It ensures that the file is current and up-to-date.  The file (vehicle_types.xml) is a local file
// that can be read only by the application.
// When this class is created it checks for the presence of the file.  If the file is not
// present it is created and the contents of the "vehicle_types.xml" asset file is copied into it.
// A connection to the the FTP server is then made and the file is downloaded from the FTP server
// and copied into the local vehicle_types.xml file.

import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class VehicleTypeFile {

    private String DownloadFileName = "download_vehicle_types.xml";     // Where we store the downloaded file - will save it to local stroage if download is successful
    private String VehicleFileName = "vehicle_types.xml";               // Name of vehicle type file used for local storage
    private SharedPreferences VehicleVersionFile = null;               // File that stores versioning info for the Vehicle Type file
    private String VehicleVersion;
    private String VehicleFileUpdateDateTime = "";


    FtpHandler ftp_handler;
    volatile boolean remote_file_updated;
    private File xmlFile = null;                            // Local file that contains vehicle type info

    // flags that control access to the AWS S3 server
    private boolean waitingForAWS = false;
    private boolean newAwsFile = false;

    MainActivity mainActivity;                          // Used for callbacks to functions in the MainActivity
    private IPostExecuteInterface doneCallbackFunction = null;
    public Integer loop_count = 0;

    // Constructors
    VehicleTypeFile(final MainActivity activity, final boolean updateFromServer, IPostExecuteInterface done_callback) {
        doneCallbackFunction = done_callback;
        mainActivity = activity;
        xmlFile = new File(mainActivity.getFilesDir(), VehicleFileName);

        final Thread thread = new Thread(new Runnable() {                                 // Run a thread that updates the Available Vehicle Files
            @Override
            public void run() {
                try {
                    xmlFile = new File(mainActivity.getFilesDir(), VehicleFileName);        // Create a File object to access the local vehicle types file
// TODO FOR TESTING ONLY - DELETE FILE TO EMULATE NEW INSTALL
//xmlFile.delete();
                    if (!xmlFile.exists()) {                                                // If none exists, make a copy of the one in the Assets file
                        CopyAssetFile();                                                    // Set the file date/time as "Now"
                        SetVehicleFileBuildDateTime();
                    }


                    ftp_handler = new FtpHandler(mainActivity);
                    FtpHandler.REMOTE_DOWNLOAD_STATUS download_status;
                    ftp_handler.DownloadFileInBackground(VehicleFileName, DownloadFileName);
                    do {
                        download_status = ftp_handler.GetRemoteDownloadStatus();
                        Thread.sleep(1);
                    }
                    while (download_status == FtpHandler.REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_WAITING);

                    if (download_status == FtpHandler.REMOTE_DOWNLOAD_STATUS.REMOTE_DOWNLOAD_SUCCESS) {
                        CopyFile(DownloadFileName, VehicleFileName);
                        SetVehicleFileDateTime();
                    } else
                        mainActivity.DisplayAlert("No Internet Connection", "Connect to the Internet to enable download of the latest Vehicle List");

                    mainActivity.ProgressBarDismiss();

                } catch (Exception ex) {
                    Log.e("ftp_handler error ", ex.getMessage());
                }
            }
        });
        thread.start();
    }


    VehicleTypeFile(MainActivity activity, boolean updateFromServer) {
        this(activity, updateFromServer, null);
    }

    public void DoneUpdating() {
        if (doneCallbackFunction != null) {
            doneCallbackFunction.onPostExecute();
        }
        Log.i("----->", "Done Updating Vehicle Type File");
    }

    // Copies the File from the assets folder to "local storage"
    void CopyAssetFile() {

        try {
            AssetManager am = mainActivity.getAssets();

            InputStream assetStream = am.open(VehicleFileName);

            xmlFile = new File(mainActivity.getFilesDir(), VehicleFileName);
            xmlFile.createNewFile();
            OutputStream localFile = new FileOutputStream(xmlFile);
            byte[] data = new byte[assetStream.available()];
            assetStream.read(data);
            localFile.write(data);
            assetStream.close();
            localFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Copies a file
    void CopyFile(String source_file_name, String dest_file_name) {
        OutputStream dest_stream = null;
        InputStream source_stream = null;
        try {
            File source_file = new File(mainActivity.getFilesDir(), source_file_name);
            source_stream = new FileInputStream(source_file);

            File dest_file = new File(mainActivity.getFilesDir(), dest_file_name);
            dest_file.createNewFile();
            dest_stream = new FileOutputStream(dest_file);
            byte[] data = new byte[source_stream.available()];
            source_stream.read(data);
            dest_stream.write(data);
            source_stream.close();
            dest_stream.close();
        } catch (IOException e) {
            e.printStackTrace();
            try {
                if (source_stream != null)
                    source_stream.close();
                if (dest_stream != null)
                    dest_stream.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    // Return an Input stream referencing the local Vehicle Type file
    InputStream GetInputStream() {
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(xmlFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return (inputStream);
    }

    public String GetVehicleFileDateTime() {
        VehicleVersionFile = mainActivity.getPreferences(MODE_PRIVATE);
        VehicleFileUpdateDateTime = VehicleVersionFile.getString("UpdateTime", "Unknown");
        return (VehicleFileUpdateDateTime);
    }

    private void SetVehicleFileDateTime() {
        Date buildDate = new Date(BuildConfig.TIMESTAMP);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy, HH:mm");

        VehicleFileUpdateDateTime = df.format(buildDate);
        VehicleVersionFile = mainActivity.getPreferences(MODE_PRIVATE);
        VehicleVersion = "Local File";
        SharedPreferences.Editor editor = VehicleVersionFile.edit();
        editor.putString("VehicleTypeVersion", VehicleVersion);
        editor.putString("UpdateTime", VehicleFileUpdateDateTime + " (Downloaded)");
        editor.apply();
    }

    private void  SetVehicleFileBuildDateTime() {
     try
        {
            long timestamp = BuildConfig.TIMESTAMP;
            Date buildDate = new Date(timestamp);
            Log.i("MyProgram", "This .apk was built on " + buildDate.toString());

            VehicleVersionFile = mainActivity.getPreferences(MODE_PRIVATE);
            VehicleVersion = "Local File";
            SharedPreferences.Editor editor = VehicleVersionFile.edit();
            editor.putString("VehicleTypeVersion", VehicleVersion);
            editor.putString("UpdateTime",  buildDate.toString() + " (Default)");
            editor.apply();
        }
        catch(Exception e){
         Log.e("VehicleTypeFile", "SetVehicleFileBuildDateTime: " + e.getMessage());
        }

    }

}
