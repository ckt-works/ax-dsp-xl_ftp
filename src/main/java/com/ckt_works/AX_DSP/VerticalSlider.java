
package com.ckt_works.AX_DSP;
// Created by Abeck on 12/27/2016. //


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

public class VerticalSlider extends CwSlider implements OnTouchListener {

    private int decimal_multiplier = 10;
    private int slider_y_position;       // The touch position relative to the top/bottom of the slider view

    private Boolean is_not_used = false;
    private Boolean is_enabled = false;
    private Boolean set_slider_value = true;


    // Views that contain the visual elements
    private ImageView mThumbImageView;
    private ImageView mSliderBarImageView;

    // Images used for thumb and the slider bar
    private Bitmap mSliderBarDisabledBitmap;
    private Bitmap mSliderBarNotUsedBitmap;
    private Bitmap mThumbBitmap;
    private Bitmap mSliderBarBitmap;

    private Bitmap rawThumbBitmap;
    private Bitmap rawBarEnabledBitmap;
    private Bitmap rawBarDisabledBitmap;
    private Bitmap rawBarNotUsedBitmap;

    // Default values
    private int slider_min_value = -10;
    private int slider_max_value = 10;
    private int slider_range = slider_max_value - slider_min_value;

    // Used internally during touches event
    //private int view_height;
    private int slider_height;
    private float value_to_set = 0;
    private int slider_left_position;
    private int slider_top_position;
    private int slider_bottom_position;
    private int thumb_left_position;

    // Used to adjust displayed position on thumb images
    private int thumb_offset = 0;

    // Holds the object that is listening to this slider.
    private OnTouchListener mDelegateOnTouchListener;



    /// Default constructors. Tell Android that we're doing custom drawing and that we want to listen to touch events.
    public VerticalSlider(Context context) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
    }

    public VerticalSlider(Context context, int min_value, int max_value) {
        super(context);
        setWillNotDraw(false);
        setOnTouchListener(this);
        slider_min_value = min_value;
        slider_max_value = max_value;
    }
    public VerticalSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
        setWillNotDraw(false);
        rawThumbBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_thumb);
        rawBarEnabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar);
        rawBarDisabledBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_disabled);
        rawBarNotUsedBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.slider_bar_hidden);
        setOnTouchListener(this);
    }

    public VerticalSlider(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setWillNotDraw(false);
        this.setOnTouchListener(this);
    }

    @Override
    // Saves the top & bottom locations of the slider
    public void onLayout (boolean changed, int left, int top, int right, int bottom){
        slider_top_position = 0; //top;
        slider_bottom_position = bottom;

        int m_thumb_size = mThumbImageView.getLayoutParams().width;
        int thumb_height = mThumbImageView.getLayoutParams().height; // .getMeasuredHeight();
        mThumbBitmap = Bitmap.createScaledBitmap(rawThumbBitmap, m_thumb_size, thumb_height, false);
        thumb_offset = mThumbBitmap.getHeight() / 2;

        slider_top_position += thumb_offset;
        slider_bottom_position -= m_thumb_size;

        slider_height = bottom - top;
        slider_height -= thumb_height; // (3 * thumb_height) / 2;
        slider_bottom_position = slider_height;

        int slider_width = (m_thumb_size * 2 / 3);
//        slider_left_position = ((right - left)- slider_width)/2; // (orig_slider_width / 2) - (slider_width * 5 / 6);
        slider_left_position = (right - left) /2;
        slider_left_position -= (slider_width /2);
        mSliderBarBitmap = Bitmap.createScaledBitmap(rawBarEnabledBitmap, slider_width, slider_height, false);
        mSliderBarDisabledBitmap = Bitmap.createScaledBitmap(rawBarDisabledBitmap, slider_width, slider_height, false);
        mSliderBarNotUsedBitmap = Bitmap.createScaledBitmap(rawBarNotUsedBitmap, slider_width, slider_height, false);

        thumb_left_position = (right - left) /2;
        thumb_left_position -= m_thumb_size/2;
        //int slider_sensitivity = slider_height / (slider_range * decimal_multiplier);

        super.onLayout(changed, left, top, right, bottom);
    }



    // This must be called by the object that wants to listen to the touch events
    public void setDelegateOnTouchListener(OnTouchListener onTouchListener) {
        mDelegateOnTouchListener = onTouchListener;
    }

    public void setDecimalMultiplier(int multiplier){ decimal_multiplier = multiplier;}

    public int GetMinValue(){ return(slider_min_value);}
    
    //  This sets the range of the slider values. i.e. 0 to 1, or -10 to 10.
    public void setRange(int min, int max) {
        slider_min_value = min;
        slider_max_value = max;
        slider_range = max - min;
    }

    // This sets the value that the slider thumb should be set to  - between slider_min_value and slider_max_value
    public int setValue(float value) {
        value_to_set = value;
        set_slider_value = true;
        invalidate();
        return((int)value);
    }


    // Returns the current value of the slider
    public float getSliderValue() {
        float relative_position = ((float) slider_bottom_position - (float) slider_y_position) / (float)slider_height;
        float value = relative_position * (float) slider_range;
        value += (float) slider_min_value;

        int int_value = Math.round(value * decimal_multiplier);
        value =  ((float)int_value) / decimal_multiplier;

        if (value > (float) slider_max_value)
            value = (float) slider_max_value;
        else if (value < (float) slider_min_value)
            value = (float) slider_min_value;
        return(value);
    }

    @Override
    public void setEnabled(boolean state) {
        super.setEnabled(state);
        is_enabled = state;
    }

    public void setNotUsed(boolean state) {
        is_enabled = state;
        is_not_used = state;
    }


    @Override
    // Loads the  child view resources if not already loaded
    public void onFinishInflate() {
        if (mThumbImageView == null) {
            mThumbImageView = (ImageView) this.getChildAt(1);
            this.removeView(mThumbImageView);
        }

        if (mSliderBarImageView == null) {
            mSliderBarImageView = (ImageView) this.getChildAt(0);
            this.removeView(mSliderBarImageView);
        }
        super.onFinishInflate();
    }

    // Process touch events
    public boolean onTouch(View view, MotionEvent event) {
        boolean processed = false;
//Log.i("oSlider nTouch", "action = " + Integer.toString(event.getAction()));

        if (is_enabled) {
            int action = event.getAction();

            if (action == MotionEvent.ACTION_DOWN)
                processed = updateTouchPosition(view, event);

            else if (action == MotionEvent.ACTION_MOVE )
                processed = updateTouchPosition(view, event);

            else if (action == MotionEvent.ACTION_UP )
                processed = updateTouchPosition(view, event);
        }
        return(processed);
    }




    private boolean updateTouchPosition(View view,  MotionEvent event){
        boolean processed = false;
        int new_position = (int) event.getY();
        new_position -= thumb_offset;
//                if (new_position <= slider_bottom_position && new_position <= slider_top_position) {
//                          if (Math.abs(new_position - slider_y_position) >= slider_sensitivity) {
        invalidate();
        slider_y_position = new_position;
        if (mDelegateOnTouchListener != null)
            mDelegateOnTouchListener.onTouch(view, event);
        processed = true;
//    }
        return(processed);
    }


    @Override
    // Draws the slider
    protected void onDraw (Canvas canvas)
    {
        if (set_slider_value)                      // Adjust thumb position (this handles the case where setValue() was called)
        {
            float new_value = value_to_set - (float)slider_min_value;
            float position_percent = new_value / (float)slider_range;
            slider_y_position = slider_height - (int)(slider_height * position_percent);
            set_slider_value = false;
        }

        else            // Keep the slider thumb in bounds
        {
            if (slider_y_position < 0)
                slider_y_position = 0;
            else if (slider_y_position > slider_bottom_position)
                slider_y_position = slider_bottom_position;
        }

        if (mSliderBarBitmap != null) {       // Draw the slider bar
            if (is_not_used)
                canvas.drawBitmap(mSliderBarNotUsedBitmap, slider_left_position, slider_top_position, null);
            else if (is_enabled)
                canvas.drawBitmap(mSliderBarBitmap, slider_left_position, slider_top_position, null);
            else
                canvas.drawBitmap(mSliderBarDisabledBitmap, slider_left_position, slider_top_position, null);
        }

        if (mThumbBitmap != null)             // Draw the slider thumb
            canvas.drawBitmap(mThumbBitmap, thumb_left_position, slider_y_position, null);
    }
}